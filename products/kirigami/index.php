<?php
    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Kirigami",
        'description' => "Kirigami lets you build convergent, responsive, elegant and open graphical user interfaces based on Qt Quick technology.",
        'cssFile' => ['https://cdn.kde.org/aether-devel/products.css?2', 'https://cdn.kde.org/aether-devel/products/kirigami.css?2'],
    ]);

    require('../../aether/header.php');
    $site_root = "../../";
?>

<main class="container">

<h1>Kirigami</h1>

<article class="splashLogo">
	<h1>Introducing Kirigami UI</h1>
	<p>Build apps that adapt beautifully to mobile, desktop, and everything in between.</p>
</article>


<article class="section-links">
	<h1>Get Started</h1>
	<div class="section-links-content columns">
		<div>
			<h2>Developer Documentation</h2>
			<p>
				Implement an application in Kirigami with Qt and QML.
			</p>
			<a href="https://api.kde.org/frameworks/kirigami/html/index.html" target="_blank">Learn more</a>
		</div>
		<div>
			<h2>Design Guidelines</h2>
			<p>
				Use Kirigami standards to their fullest for a flexible and consistent user experience.
			</p>
			<a href="https://hig.kde.org/" target="_blank">Learn more</a>
		</div>
	</div>
</article>

<article class="section-convergent columns">
	<div class="image"></div>
	<div>
		<h1>Convergent.</h1>
		<p>
			The line between desktop and mobile is blurring, and users expect the same quality
			experience on every device. Applications using Kirigami adapt brilliantly to
			mobile, desktop, and everything in between.
		</p>
	</div>
</article>

<article class="section-responsive">
	<div class="image"></div>
	<div>
		<h1>Responsive.</h1>
		<p>
			Kirigami apps adapt to more than just the devices we use them on, always
			offering the optimal layout depending on how they are used by your audience.
		</p>
	</div>
</article>

<article class="section-elegant columns">
	<div class="image"></div>
	<div>
		<h1>Elegant.</h1>
		<p>
			Kirigami is smooth and animated, uses consistent components,
			and a clearly defined workflow. People using Kirigami will appreciate the
			smart workflow and no-gimmick design.
		</p>
	</div>
</article>

<article class="section-open columns">
	<div class="image"></div>
	<div>
		<h1>Open.</h1>
		<p>
			Kirigami is built entirely on open technologies by the KDE community,
			developers will never need to pay fees for it, nor will they be locked into
			a specific platform. Be confident knowing that Kirigami gives control back
			to developers,
		</p>
		<p>Kirigami is one of the 70 <a href="/products/frameworks">KDE Frameworks</a>, a collection of high quality add on libraries for Qt apps.</p>
	</div>
</article>

</main>
<?php
  require('../../aether/footer.php');
