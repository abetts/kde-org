<?php
	require('aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Donate",
		'section' => 'donate'
		//'cssFile' => 'content/community/portal.css'
	]);

	require('aether/header.php');

?>
	<main class="container" id="community-lists">
		<img src="content/donate/jointhegame.png" style="width: 100%; max-width: 800px; display: block; margin: 20px auto;" />
		<h1 style="text-align: center;">Join the Game</h1>
		<p style="max-width: 800px; margin-left: auto; margin-right: auto; text-align: center;">
			Help create beautiful software that gives you full freedom and protects your privacy. 
		</p>
		<p style="max-width: 800px; margin-left: auto; margin-right: auto; text-align: center;">
			<a href="https://jointhegame.kde.org/" class="learn-more">Become a supporting member</a><br/>
		</p>
		
		<section class="row">
			<article class="col-md">
				<h3>One-Time Donations</h3>
				<p>
					If you do not wish to join the game, you can also make a one-time contribution.
				</p>
				<a href="https://www.kde.org/community/donations/index.php" class="learn-more">Learn More</a>
			</article>
			<article class="col-md">
				<h3>Supporting Member Programme</h3>
				<p>
					Are you part of a business or a corporation interested in 
					sponsoring KDE development? In addition to making great 
					software possible, supporting members also have their logos
					added to the KDE website and many printed promotional 
					materials.
				</p>
				<a href="https://ev.kde.org/getinvolved/supporting-members.php" class="learn-more">Learn More</a>
			</article>
		</section>
	</main>
<?php


	require('aether/footer.php');

?>
