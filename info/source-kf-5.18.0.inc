
<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha1 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/attica-5.18.0.tar.xz">attica-5.18.0</a></td>
   <td align="right">59kB</td>
   <td><tt>80e4344be0fe2722238bfce362474949e1380575</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/baloo-5.18.0.tar.xz">baloo-5.18.0</a></td>
   <td align="right">176kB</td>
   <td><tt>a2f28d5c507bf609e687102673d027088d6575ce</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/bluez-qt-5.18.0.tar.xz">bluez-qt-5.18.0</a></td>
   <td align="right">71kB</td>
   <td><tt>450cef73d3c771617005fdb15723debcab5f9765</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/breeze-icons-5.18.0.tar.xz">breeze-icons-5.18.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>89efd0ae68895326747b8e2b2577bd2b4cc46e13</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/extra-cmake-modules-5.18.0.tar.xz">extra-cmake-modules-5.18.0</a></td>
   <td align="right">277kB</td>
   <td><tt>ce741f28486f616564aec7d56cedf88eccab9a25</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/frameworkintegration-5.18.0.tar.xz">frameworkintegration-5.18.0</a></td>
   <td align="right">1.7MB</td>
   <td><tt>2ea81f5a82d6fc756d01a31ab507c6d6684004b0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kactivities-5.18.0.tar.xz">kactivities-5.18.0</a></td>
   <td align="right">177kB</td>
   <td><tt>130b9bc135162f37b99ecb0afc20132f2c785936</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kapidox-5.18.0.tar.xz">kapidox-5.18.0</a></td>
   <td align="right">129kB</td>
   <td><tt>b4ff24da6f90f8ce12f80d5a6c742c80a558d6e7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/karchive-5.18.0.tar.xz">karchive-5.18.0</a></td>
   <td align="right">106kB</td>
   <td><tt>44607f5c7662842eaf70926675149a1e139c8f67</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kauth-5.18.0.tar.xz">kauth-5.18.0</a></td>
   <td align="right">80kB</td>
   <td><tt>74aa6a471faebf3c80c1f411422ad92e152e8925</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kbookmarks-5.18.0.tar.xz">kbookmarks-5.18.0</a></td>
   <td align="right">112kB</td>
   <td><tt>eb725f9f4e6f42bb1965587ec2763abdc12b6380</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kcmutils-5.18.0.tar.xz">kcmutils-5.18.0</a></td>
   <td align="right">229kB</td>
   <td><tt>8f255815246d6184c5632300dfa1913a8d0b37b1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kcodecs-5.18.0.tar.xz">kcodecs-5.18.0</a></td>
   <td align="right">211kB</td>
   <td><tt>8d5f09a9fa5977658a31a13ae9cef4d81f741da3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kcompletion-5.18.0.tar.xz">kcompletion-5.18.0</a></td>
   <td align="right">113kB</td>
   <td><tt>6e4e095711c073056d1bd101d24e7ba1c4c214cc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kconfig-5.18.0.tar.xz">kconfig-5.18.0</a></td>
   <td align="right">219kB</td>
   <td><tt>d23e28f8b8651970cb12c6485639165340006811</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kconfigwidgets-5.18.0.tar.xz">kconfigwidgets-5.18.0</a></td>
   <td align="right">350kB</td>
   <td><tt>3aa99f49d570d6cba6bbf86b4c3a2ca9f88a3d29</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kcoreaddons-5.18.0.tar.xz">kcoreaddons-5.18.0</a></td>
   <td align="right">304kB</td>
   <td><tt>7c41dbcc217d0e93cb276c84b23a55ad803e80a3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kcrash-5.18.0.tar.xz">kcrash-5.18.0</a></td>
   <td align="right">19kB</td>
   <td><tt>1332e6853e8142b36631290a9f5b18683b4407df</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kdbusaddons-5.18.0.tar.xz">kdbusaddons-5.18.0</a></td>
   <td align="right">33kB</td>
   <td><tt>7df0c6a23366e39a9ee9c4b96243d286aca639ae</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kdeclarative-5.18.0.tar.xz">kdeclarative-5.18.0</a></td>
   <td align="right">163kB</td>
   <td><tt>3dc6325396dd3966d73988773b59c269be9db6ca</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kded-5.18.0.tar.xz">kded-5.18.0</a></td>
   <td align="right">35kB</td>
   <td><tt>0a5ce340c32b0f5759f644864e7346759393802b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kdesignerplugin-5.18.0.tar.xz">kdesignerplugin-5.18.0</a></td>
   <td align="right">85kB</td>
   <td><tt>907c01a90807f83a0972c73787e03555c33c4be5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kdesu-5.18.0.tar.xz">kdesu-5.18.0</a></td>
   <td align="right">43kB</td>
   <td><tt>4d1e400e2cbb45d8e6ac84102503e76c85268f4a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kdewebkit-5.18.0.tar.xz">kdewebkit-5.18.0</a></td>
   <td align="right">28kB</td>
   <td><tt>c0abfb2e0fdfdee0105b7f79aa0cb9e23e3399bc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kdnssd-5.18.0.tar.xz">kdnssd-5.18.0</a></td>
   <td align="right">55kB</td>
   <td><tt>9359446f782bfdfa98f0453f341b988fbd72923d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kdoctools-5.18.0.tar.xz">kdoctools-5.18.0</a></td>
   <td align="right">398kB</td>
   <td><tt>9432c73607bf06260d68f0b00acc0196168943ad</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kemoticons-5.18.0.tar.xz">kemoticons-5.18.0</a></td>
   <td align="right">92kB</td>
   <td><tt>68afe050e3325b0e723c41325b3d62d064b4ea1f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kfilemetadata-5.18.0.tar.xz">kfilemetadata-5.18.0</a></td>
   <td align="right">121kB</td>
   <td><tt>03a6cd9d1701eb88ab7098860f1c38229c2ce03e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kglobalaccel-5.18.0.tar.xz">kglobalaccel-5.18.0</a></td>
   <td align="right">79kB</td>
   <td><tt>3bd4a64515ee3f10ece4378163f4d79e819a8860</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kguiaddons-5.18.0.tar.xz">kguiaddons-5.18.0</a></td>
   <td align="right">38kB</td>
   <td><tt>771d2c2e9ce074f89ee75e5685edaec10c00e709</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/ki18n-5.18.0.tar.xz">ki18n-5.18.0</a></td>
   <td align="right">586kB</td>
   <td><tt>c2b3b02f3776f579151a2423acd67b844440ee8f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kiconthemes-5.18.0.tar.xz">kiconthemes-5.18.0</a></td>
   <td align="right">191kB</td>
   <td><tt>015547e8939aad42bcff53daee344011cc3772f4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kidletime-5.18.0.tar.xz">kidletime-5.18.0</a></td>
   <td align="right">25kB</td>
   <td><tt>e9938c1bca0761621cda1cdd2052b43f7127baf4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kimageformats-5.18.0.tar.xz">kimageformats-5.18.0</a></td>
   <td align="right">83kB</td>
   <td><tt>b36ff047b533db584facaa1163198d9bd1cbec6a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kinit-5.18.0.tar.xz">kinit-5.18.0</a></td>
   <td align="right">114kB</td>
   <td><tt>ac954783c1906eef3225fbb5dd24eff25de90fd1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kio-5.18.0.tar.xz">kio-5.18.0</a></td>
   <td align="right">2.7MB</td>
   <td><tt>8dd4b9a1de0b32daac7953e4549b52d768f80fae</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kitemmodels-5.18.0.tar.xz">kitemmodels-5.18.0</a></td>
   <td align="right">374kB</td>
   <td><tt>c1b78605bf285401a5e5552e4eb69a961717ed90</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kitemviews-5.18.0.tar.xz">kitemviews-5.18.0</a></td>
   <td align="right">72kB</td>
   <td><tt>c10a50c6607e548f9d5727040971cbe8e1074851</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kjobwidgets-5.18.0.tar.xz">kjobwidgets-5.18.0</a></td>
   <td align="right">86kB</td>
   <td><tt>be976e65048054fdb97d9f609e5e660e0c7e6d5b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/knewstuff-5.18.0.tar.xz">knewstuff-5.18.0</a></td>
   <td align="right">838kB</td>
   <td><tt>b846d42f9c65cfa28ebe2ee36fb1195ffc13e022</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/knotifications-5.18.0.tar.xz">knotifications-5.18.0</a></td>
   <td align="right">91kB</td>
   <td><tt>615d90a627e98aa9a9fa93bb33303d825bed2ce0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/knotifyconfig-5.18.0.tar.xz">knotifyconfig-5.18.0</a></td>
   <td align="right">81kB</td>
   <td><tt>d02f1a408559138d0b53cd4e141cea4ea8d7fa1e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kpackage-5.18.0.tar.xz">kpackage-5.18.0</a></td>
   <td align="right">104kB</td>
   <td><tt>5d5984898ac215e032c842cabcd26a42406c4c10</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kparts-5.18.0.tar.xz">kparts-5.18.0</a></td>
   <td align="right">150kB</td>
   <td><tt>172baa0e2c59de5cacac3d77caa260e8c34e76fe</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kpeople-5.18.0.tar.xz">kpeople-5.18.0</a></td>
   <td align="right">56kB</td>
   <td><tt>0b6813a0dc628ae4cce91f4b4d6fd9cb1e338458</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kplotting-5.18.0.tar.xz">kplotting-5.18.0</a></td>
   <td align="right">28kB</td>
   <td><tt>bf20f765a1382a12872fec48277e8c96839d74e9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kpty-5.18.0.tar.xz">kpty-5.18.0</a></td>
   <td align="right">55kB</td>
   <td><tt>5c695d1c66c1287da834e825d784b237edfa7016</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kservice-5.18.0.tar.xz">kservice-5.18.0</a></td>
   <td align="right">262kB</td>
   <td><tt>a80efe117321a0a718a6ac5cd35d9126fffa8f52</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/ktexteditor-5.18.0.tar.xz">ktexteditor-5.18.0</a></td>
   <td align="right">2.7MB</td>
   <td><tt>1141243733c036bcff5003cf78158d59149a5e5e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/ktextwidgets-5.18.0.tar.xz">ktextwidgets-5.18.0</a></td>
   <td align="right">299kB</td>
   <td><tt>c408d2a1515a889a8902c4c7636b629b565eb634</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kunitconversion-5.18.0.tar.xz">kunitconversion-5.18.0</a></td>
   <td align="right">612kB</td>
   <td><tt>344d874fb3c406380ff07fface8476d067683174</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kwallet-5.18.0.tar.xz">kwallet-5.18.0</a></td>
   <td align="right">279kB</td>
   <td><tt>d45b5c4d8e365a52620d2da9c4c6a8374f92e297</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kwidgetsaddons-5.18.0.tar.xz">kwidgetsaddons-5.18.0</a></td>
   <td align="right">2.0MB</td>
   <td><tt>b6120dd7ca1ffe52d4fecaf396ac64ad3dfe5e94</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kwindowsystem-5.18.0.tar.xz">kwindowsystem-5.18.0</a></td>
   <td align="right">159kB</td>
   <td><tt>6aa1d1e590dc500a7511a95a8d0d6bac93eb19d2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kxmlgui-5.18.0.tar.xz">kxmlgui-5.18.0</a></td>
   <td align="right">844kB</td>
   <td><tt>5609b50abad6f9ad618a8de3eb6f1307530265b9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/kxmlrpcclient-5.18.0.tar.xz">kxmlrpcclient-5.18.0</a></td>
   <td align="right">27kB</td>
   <td><tt>2fc483856b1f55e8ad4864d6a43b54b3846b5220</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/modemmanager-qt-5.18.0.tar.xz">modemmanager-qt-5.18.0</a></td>
   <td align="right">96kB</td>
   <td><tt>1b9d7b2fcfe86ffa70e234bc6be93700d1045673</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/networkmanager-qt-5.18.0.tar.xz">networkmanager-qt-5.18.0</a></td>
   <td align="right">151kB</td>
   <td><tt>731435e36ede7dd4436d06bf1f825a38fa94dc84</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/oxygen-icons5-5.18.0.tar.xz">oxygen-icons5-5.18.0</a></td>
   <td align="right">220MB</td>
   <td><tt>ba66e54d955cb18bb18fe4d8bdcc7c106bfcd24a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/plasma-framework-5.18.0.tar.xz">plasma-framework-5.18.0</a></td>
   <td align="right">3.9MB</td>
   <td><tt>9eb6be83b9209f17e1479189dc1ca04999632c6e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/solid-5.18.0.tar.xz">solid-5.18.0</a></td>
   <td align="right">261kB</td>
   <td><tt>c0a63501525d499f1fdd64e736c6701c688f0324</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/sonnet-5.18.0.tar.xz">sonnet-5.18.0</a></td>
   <td align="right">275kB</td>
   <td><tt>afba8621921985d78d0f0b34a37986c167e5e70d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/frameworks/5.18/threadweaver-5.18.0.tar.xz">threadweaver-5.18.0</a></td>
   <td align="right">1.3MB</td>
   <td><tt>bb5d9b6935831bede0591ea6c3eca83d8806d698</tt></td>
</tr>

</table>
