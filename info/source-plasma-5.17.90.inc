<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha256 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/bluedevil-5.17.90.tar.xz">bluedevil-5.17.90</a></td>
   <td align="right">162kB</td>
   <td><tt>ab23e3f58e44fed3e43380539f2a2185701bbb4a708d87016448cdfb202aa5f0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/breeze-5.17.90.tar.xz">breeze-5.17.90</a></td>
   <td align="right">15MB</td>
   <td><tt>8d71915045648b375ec36a288107cdf5eebe6ebb89beca83f25f2fe5603421f4</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/breeze-grub-5.17.90.tar.xz">breeze-grub-5.17.90</a></td>
   <td align="right">2.9MB</td>
   <td><tt>62a7338669eb1156d054a9a116946c626aa4c6a2973b77ed96eeabd95ff0c1cc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/breeze-gtk-5.17.90.tar.xz">breeze-gtk-5.17.90</a></td>
   <td align="right">43kB</td>
   <td><tt>1aa8e66e0ebbcc72e3b4e70885d48959d1d1eba8b02cf8bcfcd989211e6118a9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/breeze-plymouth-5.17.90.tar.xz">breeze-plymouth-5.17.90</a></td>
   <td align="right">103kB</td>
   <td><tt>f69b4a496ef8796bcb41327a51590d0db542c77dde365177b31c6ef16c6f00f3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/discover-5.17.90.tar.xz">discover-5.17.90</a></td>
   <td align="right">9.9MB</td>
   <td><tt>8d04564312ab0856f134db4ef56df9107bdf64b2e2659cbb3386c576787357fa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/drkonqi-5.17.90.tar.xz">drkonqi-5.17.90</a></td>
   <td align="right">728kB</td>
   <td><tt>ef2f7793e3ab1d13bda9df31ea96cff56e2643cdcfd56a3287e64a1ca5b6b66f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kactivitymanagerd-5.17.90.tar.xz">kactivitymanagerd-5.17.90</a></td>
   <td align="right">85kB</td>
   <td><tt>a49aa159d1df85d2f0f2af6f49b457442f56358c3308a655ef39bb6c5d3ab0af</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kde-cli-tools-5.17.90.tar.xz">kde-cli-tools-5.17.90</a></td>
   <td align="right">577kB</td>
   <td><tt>bf43e314e51fd46cac822cf154515bffc5029e44b7b447c9bcd2a9a6135e947c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kdecoration-5.17.90.tar.xz">kdecoration-5.17.90</a></td>
   <td align="right">42kB</td>
   <td><tt>f657eb40a69c25e8b2a8ed8330595c5e1a7eb4c8bd6b9bb89389a58f8e312e7d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kde-gtk-config-5.17.90.tar.xz">kde-gtk-config-5.17.90</a></td>
   <td align="right">22kB</td>
   <td><tt>2061f8b6851fb1d950f737e4ba68d898990a7140f92576a2d13578f39e6294b2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kdeplasma-addons-5.17.90.tar.xz">kdeplasma-addons-5.17.90</a></td>
   <td align="right">599kB</td>
   <td><tt>da91ee9f87a5c488c30eb7e1289b98c072cca0c0553bb5eddee0007779842e51</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kgamma5-5.17.90.tar.xz">kgamma5-5.17.90</a></td>
   <td align="right">78kB</td>
   <td><tt>d1015c38a793aed52f38acf8c3f29502361f26a4bb2e10267bcd1c5e4b1105c8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/khotkeys-5.17.90.tar.xz">khotkeys-5.17.90</a></td>
   <td align="right">1.7MB</td>
   <td><tt>fbb2a634388839ba0831fcb73dd009673ea4d67f64af4ff0fde5119c6568cafa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kinfocenter-5.17.90.tar.xz">kinfocenter-5.17.90</a></td>
   <td align="right">1.2MB</td>
   <td><tt>756b07f13a740d44eac2e7dd41ea7ef26b657630dad9a5efc104736ab9924b35</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kmenuedit-5.17.90.tar.xz">kmenuedit-5.17.90</a></td>
   <td align="right">794kB</td>
   <td><tt>cfbb0f874f2b91fba82d7518347089868039e7b49d0d2f592e547c4f2cdd8735</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kscreen-5.17.90.tar.xz">kscreen-5.17.90</a></td>
   <td align="right">109kB</td>
   <td><tt>1c591aef19a4a5a793679abcc5ff6e100a7563ef26b3c2dde5a08fc0a1647da3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kscreenlocker-5.17.90.tar.xz">kscreenlocker-5.17.90</a></td>
   <td align="right">121kB</td>
   <td><tt>fb51a3bd6f98674fdd0ea0939fea76e227748d83ee9bbde08c38ffa1ac11410f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/ksshaskpass-5.17.90.tar.xz">ksshaskpass-5.17.90</a></td>
   <td align="right">21kB</td>
   <td><tt>04ff14ea1f80caf0723bc00037152a43b17539ebc999a3ddce461e456f485afa</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/ksysguard-5.17.90.tar.xz">ksysguard-5.17.90</a></td>
   <td align="right">505kB</td>
   <td><tt>2e98a7253122f4d6815dd5bff1ed58fc3a54d1682d7cdf99a8c77dc78a49f276</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kwallet-pam-5.17.90.tar.xz">kwallet-pam-5.17.90</a></td>
   <td align="right">19kB</td>
   <td><tt>bfe70a8559b904189d16e8d5b5b0df2992aa0197f63ee06421897223e0ebe345</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kwayland-integration-5.17.90.tar.xz">kwayland-integration-5.17.90</a></td>
   <td align="right">20kB</td>
   <td><tt>1e9880b05b56b5c006cc51a937ad85bd3042d596d715b258590358a53af113bd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kwin-5.17.90.tar.xz">kwin-5.17.90</a></td>
   <td align="right">5.9MB</td>
   <td><tt>de1d7b1b9b59818ea88839ce96e3f43b84a07b321a1557e385137de2cbcb1f80</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/kwrited-5.17.90.tar.xz">kwrited-5.17.90</a></td>
   <td align="right">19kB</td>
   <td><tt>fab06772ad69942f177ab7d2881d7b775146a64ccb85f9cbfe12f94e48943c76</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/libkscreen-5.17.90.1.tar.xz">libkscreen-5.17.90.1</a></td>
   <td align="right">83kB</td>
   <td><tt>f4f935e511a377966b593a51a3cdddc7624bb9358ebf9792e9b76b9dffefe9fb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/libksysguard-5.17.90.tar.xz">libksysguard-5.17.90</a></td>
   <td align="right">594kB</td>
   <td><tt>1126896f5c47453a56828e8842fce00ddd4d63e09658aef69a2bede904383787</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/milou-5.17.90.tar.xz">milou-5.17.90</a></td>
   <td align="right">62kB</td>
   <td><tt>5f1a60bc4858526954e21f1d84979f78eec25f18725d597681b71d20a456bb69</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/oxygen-5.17.90.tar.xz">oxygen-5.17.90</a></td>
   <td align="right">4.2MB</td>
   <td><tt>6b2795f2fe9a3284962cb2b1e94921499962ae304d4436c73c1cf99715736b19</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-browser-integration-5.17.90.tar.xz">plasma-browser-integration-5.17.90</a></td>
   <td align="right">151kB</td>
   <td><tt>52c71f9b95eb76c62da35654a6dd5a199fc4fd18f8c44206f6c4fdaf9a7bdbeb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-desktop-5.17.90.tar.xz">plasma-desktop-5.17.90</a></td>
   <td align="right">8.9MB</td>
   <td><tt>26d073941a76ce36a1bbe9c7a330e438021c6a508645c84e6328f6835245aae9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-integration-5.17.90.tar.xz">plasma-integration-5.17.90</a></td>
   <td align="right">56kB</td>
   <td><tt>3ec209d139aa3d985a0b99db214c0dd57209da298be617d7793aee233b291ead</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-nano-5.17.90.tar.xz">plasma-nano-5.17.90</a></td>
   <td align="right">29kB</td>
   <td><tt>fc7fc1829c23a87bbd51368d493e16dbf83f27dd8bf62d22e242f7e9fdd178e9</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-nm-5.17.90.4.tar.xz">plasma-nm-5.17.90.4</a></td>
   <td align="right">805kB</td>
   <td><tt>b9a507d05806ec1cc83b2c6c5c6076901d5e3aaf4849f4a0691858cdfb282d74</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-pa-5.17.90.tar.xz">plasma-pa-5.17.90</a></td>
   <td align="right">102kB</td>
   <td><tt>9ee5239a0ac9e388d90f6337100e75c6340b5e02096510cd4995579c204c4bea</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-phone-components-5.17.90.tar.xz">plasma-phone-components-5.17.90</a></td>
   <td align="right">1.6MB</td>
   <td><tt>7acb1833cfe0403de48365a2de7d1bacd2cbf28e3c42e7cda40adf425e0a594e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-sdk-5.17.90.tar.xz">plasma-sdk-5.17.90</a></td>
   <td align="right">256kB</td>
   <td><tt>9ee7386eedb6a1637b246fbdaec205d226c84630f7b23fee64323978aabfb440</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-tests-5.17.90.tar.xz">plasma-tests-5.17.90</a></td>
   <td align="right">1kB</td>
   <td><tt>cc59388e1f2f81d746da99556362f545e76162af187b03f1088d9a1485b1f993</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-thunderbolt-5.17.90.tar.xz">plasma-thunderbolt-5.17.90</a></td>
   <td align="right">49kB</td>
   <td><tt>12a3fc9b7ff7cf5aae384f59620fdb3c0b325af288af3bff48b001a3aa0582e5</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-vault-5.17.90.tar.xz">plasma-vault-5.17.90</a></td>
   <td align="right">132kB</td>
   <td><tt>f2b632a1c6b43ddd5ce51db7345da70af42cde57e63b995f7e39f39d81be058e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-workspace-5.17.90.tar.xz">plasma-workspace-5.17.90</a></td>
   <td align="right">4.7MB</td>
   <td><tt>871d222fa650ab177953550c6a0b9b8ab4739c646fcccb680292fa76e1ae3d22</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plasma-workspace-wallpapers-5.17.90.tar.xz">plasma-workspace-wallpapers-5.17.90</a></td>
   <td align="right">32MB</td>
   <td><tt>b56fa1ba8b04ac29ee6c05f232fc066ad5a96d64bd689b02132553c28f18de3c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/plymouth-kcm-5.17.90.tar.xz">plymouth-kcm-5.17.90</a></td>
   <td align="right">40kB</td>
   <td><tt>1c222a4a63912abf20678da2e003d26a0e4922fa958e12c297713a7f4a4e5f64</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/polkit-kde-agent-1-5.17.90.tar.xz">polkit-kde-agent-1-5.17.90</a></td>
   <td align="right">43kB</td>
   <td><tt>7ffecfb65fedfe60d79f99e3ac83d41393239a85bceddc0b954cde4f8a702184</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/powerdevil-5.17.90.tar.xz">powerdevil-5.17.90</a></td>
   <td align="right">583kB</td>
   <td><tt>f17e353cebb6c36ed4b47d5a595562f3a0d5e04bc6ea5963c553dae836790086</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/sddm-kcm-5.17.90.tar.xz">sddm-kcm-5.17.90</a></td>
   <td align="right">60kB</td>
   <td><tt>2fcffad1689c4dfa28ee9fedbedec39a010a21659fc5ddae9b3c6a7db38be0bd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/systemsettings-5.17.90.tar.xz">systemsettings-5.17.90</a></td>
   <td align="right">171kB</td>
   <td><tt>0635f0df71850a04bbb737c4b0a07380c900a78ea33cf9e8eeb01d67566af97d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/user-manager-5.17.90.tar.xz">user-manager-5.17.90</a></td>
   <td align="right">2.0MB</td>
   <td><tt>f842b06bac980a356e866f4d190f148df97d44de21940715d8bc04c8c38a66dc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/unstable/plasma/5.17.90/xdg-desktop-portal-kde-5.17.90.tar.xz">xdg-desktop-portal-kde-5.17.90</a></td>
   <td align="right">62kB</td>
   <td><tt>b53495cf06d04ca30851fec5df21a5c89342b57b75536571ab1fb6ea2d67c44b</tt></td>
</tr>

</table>
