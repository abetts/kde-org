<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">Sha256 Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/bluedevil-5.8.7.tar.xz">bluedevil-5.8.7</a></td>
   <td align="right">141kB</td>
   <td><tt>bf349f24f2194a0c77441237acebdf2658f2f11400673e43428f7da9d5149b83</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/breeze-5.8.7.tar.xz">breeze-5.8.7</a></td>
   <td align="right">31MB</td>
   <td><tt>ace62e902d8ec53aff761b26482c666663b0807274e8de6f6c8d7f39dcf21c56</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/breeze-grub-5.8.7.tar.xz">breeze-grub-5.8.7</a></td>
   <td align="right">3.0MB</td>
   <td><tt>4ad15028e94cde827fc4c23ad9499471095e2a06a225f50cf057031b681ff0d7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/breeze-gtk-5.8.7.tar.xz">breeze-gtk-5.8.7</a></td>
   <td align="right">206kB</td>
   <td><tt>7a032ba552cf2786c8cbaa8ae5aefcc60d81e1669cf53569db0abb2384e3793e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/breeze-plymouth-5.8.7.tar.xz">breeze-plymouth-5.8.7</a></td>
   <td align="right">92kB</td>
   <td><tt>7c68cd5bebfa24fc32222725a192e737a96473d969dda3f4402a3fe5f54c9d5d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/discover-5.8.7.tar.xz">discover-5.8.7</a></td>
   <td align="right">8.7MB</td>
   <td><tt>c984694de3a7788f984a66c5b9b358a4c07ab99280bb83f4ca9bc248fd68b46b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kactivitymanagerd-5.8.7.tar.xz">kactivitymanagerd-5.8.7</a></td>
   <td align="right">81kB</td>
   <td><tt>6560629efaeb23853f859df058db88f5c5f6fb99c8818029587ac5c1ea76706a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kde-cli-tools-5.8.7.tar.xz">kde-cli-tools-5.8.7</a></td>
   <td align="right">478kB</td>
   <td><tt>cb2c253e8a9baa466067d05938c669c05ae8bac7f8ba349fe2bb8edbb7cdebe7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kdecoration-5.8.7.tar.xz">kdecoration-5.8.7</a></td>
   <td align="right">33kB</td>
   <td><tt>3736940fb1d07048757e7fdff3d68f3770d328a2e60d2e24875bb27f8166fae7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kde-gtk-config-5.8.7.tar.xz">kde-gtk-config-5.8.7</a></td>
   <td align="right">146kB</td>
   <td><tt>f92c3ad4f3bfa9b206ac60a600ab1d04e0434c2eb7041d4718c3917d7010cd46</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kdeplasma-addons-5.8.7.tar.xz">kdeplasma-addons-5.8.7</a></td>
   <td align="right">1.9MB</td>
   <td><tt>2da02ce581197fda737631ed3be7734c58d6f80162b6011257f68fd3cb0f5c10</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kgamma5-5.8.7.tar.xz">kgamma5-5.8.7</a></td>
   <td align="right">59kB</td>
   <td><tt>c61c24706eb004f419c2785d66c09d367de7d2a287e2012c7b73f7646c7903eb</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/khotkeys-5.8.7.tar.xz">khotkeys-5.8.7</a></td>
   <td align="right">592kB</td>
   <td><tt>28b13c3246f5af19dcc4b1a4443ca0143fc4451a9b251a3958c459c530b02913</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kinfocenter-5.8.7.tar.xz">kinfocenter-5.8.7</a></td>
   <td align="right">1.2MB</td>
   <td><tt>8e6c43201fb4581317638608623a32e290e22824d39b62a056b956989a46fd8f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kmenuedit-5.8.7.tar.xz">kmenuedit-5.8.7</a></td>
   <td align="right">638kB</td>
   <td><tt>1d5265a7aad05050a58b4f2ba9088500b3fca8d767f43b27bb5dcc5c4621f055</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kscreen-5.8.7.tar.xz">kscreen-5.8.7</a></td>
   <td align="right">111kB</td>
   <td><tt>0ffc05b99edbedc01bec775ab7a93b90c5b2c202dfbc5b8381339c5ab593ad6e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kscreenlocker-5.8.7.tar.xz">kscreenlocker-5.8.7</a></td>
   <td align="right">106kB</td>
   <td><tt>bc7c61a24a5fb7ba84ac24508f5c82a36506b18deb331d8098873a7ce428d003</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/ksshaskpass-5.8.7.tar.xz">ksshaskpass-5.8.7</a></td>
   <td align="right">19kB</td>
   <td><tt>1d85fc95734bf7cf5a689e4b528deca7850192a47f9e2d8cb54cf9be44374f7e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/ksysguard-5.8.7.tar.xz">ksysguard-5.8.7</a></td>
   <td align="right">481kB</td>
   <td><tt>ab69a8f3c8d21422b17e689dc0cf1b2137723dafded343ad3ea8db1802014080</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kwallet-pam-5.8.7.tar.xz">kwallet-pam-5.8.7</a></td>
   <td align="right">17kB</td>
   <td><tt>51f74dd2ca2757aa5ef04bef17b825d2aef6d28728c957c87d445c3aa4b7835a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kwayland-integration-5.8.7.tar.xz">kwayland-integration-5.8.7</a></td>
   <td align="right">17kB</td>
   <td><tt>6c5a82e4b85e7051834b0d2b862aa6f42964259cba8356c4ec9d769d252b8cba</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kwin-5.8.7.tar.xz">kwin-5.8.7</a></td>
   <td align="right">3.8MB</td>
   <td><tt>62266b62b68757aabd5f94d2f599f5d774fd225f4767a3916ab5f150881c879b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/kwrited-5.8.7.tar.xz">kwrited-5.8.7</a></td>
   <td align="right">19kB</td>
   <td><tt>4959662e5c035ea2323e3a5087000c672fbcdb567e8626f708fc7e987921d717</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/libkscreen-5.8.7.tar.xz">libkscreen-5.8.7</a></td>
   <td align="right">91kB</td>
   <td><tt>9c8e2e47b0b15e105b320d6f581b9e210509100c315007a4746fe14637a3c273</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/libksysguard-5.8.7.tar.xz">libksysguard-5.8.7</a></td>
   <td align="right">562kB</td>
   <td><tt>e353865b4fa45b870a65c360f071b9b138cb81acd02ce6752c094d8de7761ba3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/milou-5.8.7.tar.xz">milou-5.8.7</a></td>
   <td align="right">53kB</td>
   <td><tt>130c30e4737fa5634dc8c8bb96ac1821d5b96b72a77052ab7cb956719ea2826f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/oxygen-5.8.7.tar.xz">oxygen-5.8.7</a></td>
   <td align="right">4.2MB</td>
   <td><tt>6b6bcf69bc50182671e55680ba2e260d569a6203131108728ca58d584716453b</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/plasma-desktop-5.8.7.1.tar.xz">plasma-desktop-5.8.7.1</a></td>
   <td align="right">6.1MB</td>
   <td><tt>a27f6098773e06efbd624b85dc2952e421b4a2f7b2076eb940c24acb6c500991</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/plasma-integration-5.8.7.tar.xz">plasma-integration-5.8.7</a></td>
   <td align="right">49kB</td>
   <td><tt>1584cb3977abc3b8ec9acad2fffd5b701dc370b308b5bf86b7547afab76707e7</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/plasma-nm-5.8.7.tar.xz">plasma-nm-5.8.7</a></td>
   <td align="right">638kB</td>
   <td><tt>bcf3756a0b5c9955257efc5a293120248dc31e05cbe40ac4fef00914a7bc5dd1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/plasma-pa-5.8.7.tar.xz">plasma-pa-5.8.7</a></td>
   <td align="right">68kB</td>
   <td><tt>86cb57424c82e1d88928138eb4f24417e76abe4b028e641ee268020f257a9d05</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/plasma-sdk-5.8.7.tar.xz">plasma-sdk-5.8.7</a></td>
   <td align="right">706kB</td>
   <td><tt>44bb4b03e55688a4698466cba4a649404500f329f9ba7f754e5eeee630a7841d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/plasma-tests-5.8.7.tar.xz">plasma-tests-5.8.7</a></td>
   <td align="right">1kB</td>
   <td><tt>6fd47653785d1e09a218bdd57222ba995805561788845e9a9c1ce353494b7225</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/plasma-workspace-5.8.7.tar.xz">plasma-workspace-5.8.7</a></td>
   <td align="right">6.6MB</td>
   <td><tt>e6b0f6855405a01f8cbc8a75f7c79943cd9515a23e66f66905d04b8bc4cfab5f</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/plasma-workspace-wallpapers-5.8.7.tar.xz">plasma-workspace-wallpapers-5.8.7</a></td>
   <td align="right">43MB</td>
   <td><tt>59cb92dca9c5a96132fd0d55044ce0a50ee26371602611bc9a900629bd7fc716</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/polkit-kde-agent-1-5.8.7.tar.xz">polkit-kde-agent-1-5.8.7</a></td>
   <td align="right">40kB</td>
   <td><tt>d04596a34d36084ffca6a4acf789370c0aa63f5ff93f83fc869e8821449b4bd1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/powerdevil-5.8.7.tar.xz">powerdevil-5.8.7</a></td>
   <td align="right">350kB</td>
   <td><tt>3e51ed8bf7e160565ca47ce4065ea359ca23ac0047b602868e686973a3fcaacf</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/sddm-kcm-5.8.7.tar.xz">sddm-kcm-5.8.7</a></td>
   <td align="right">48kB</td>
   <td><tt>5ab79a919239dc300ab497aa1e585258b0249008ad83db9c8faa2d8ea51d6379</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/systemsettings-5.8.7.tar.xz">systemsettings-5.8.7</a></td>
   <td align="right">154kB</td>
   <td><tt>5e08c5f9ab7dc5a5797973de47277a740c6cb4937c05942e5501e5c3cfde61b3</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/plasma/5.8.7/user-manager-5.8.7.tar.xz">user-manager-5.8.7</a></td>
   <td align="right">531kB</td>
   <td><tt>3d39abfd6c34304145db46f4d9fbf9162106c8da175556b342ae35867bf89adc</tt></td>
</tr>

</table>
