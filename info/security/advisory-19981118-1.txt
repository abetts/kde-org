Dear KDE users, administrators and developers,

 
KDE Screensavers are usually running SUID root. Security issues have 
been posted to Bugtraq on Nov 16 1998, under the subject "KDE 1.0's
klock can be used to gain root priveledges". The KDE team has now
published  a fix for the KDE1.0 branch and the current branch.
 
With this change, screensavers and klock are not running SUID anymore. 
This will solve every potential exploit, like misuse of buffer overruns 
to gain root rights or executing a wrong executable under SUID rights. 
 
The following text explains the technique used to solve the problem. 
An advisory for distributors, users and administrators follows the 
technical description. 
 
 
Technique
---------
An authentification program, kcheckpass, has been introduced. This 
is a separate, helper program, that runs SUID and is called each 
time a password has to be checked. The password is passed via 
STDIN to the program and the result of the authentification 
process is returned in the return code of the program. 
This program is small and supposed to be free from security hazzles. 
 
 
 
 
Advisory 
-------- 
Administrators should remove any SUID bit from KDE executables. 
 
After updating to the fixed KDE1.0 tree or to the current KDE, 
administrators should 
1) check the access rights of the installed executables. The 
   screensavers must not be installed SUID anymore. If in doubt, 
   remove the SUID bits manually by a suitable command, like 
   "chmod -s *.kss klock" under Linux. 
2) check the access rights of the kcheckpass program. This program 
   should only be installed SUID root under certain authentification 
   systems, like the shadow password suite. 
3) Distributions using the shadow password system can be made more 
   secure by creating a "shadow" group and setting the access rights 
   of /etc/shadow and kcheckpass like in the following example: 
 
 -rw-r-----   1 root     shadow        746 Sep  2 21:35 /etc/shadow 
 -rwxr-sr-x   1 root     shadow       4720 Nov 17 22:32 /opt/kde/bin/kcheckpass 
 
   Distributors are strongly encouraged to follow this scheme. This 
   way, the kcheckpass is running under the effective user id of the user 
   itself and the effective group "shadow". With this, kcheckpass has only 
   one additional right against regular users: The right to read /etc/shadow. 
   Attackers won't be able to make wider use of "kcheckpass". 



Availability of the fix
-----------------------
The patches are already integrated in the KDE1.0 and the KDE1.1 branches.
You can use cvs/cvsup to get current sources. You can also get the patch
from KDE's ftp Server ftp://ftp.kde.org and its mirrors, which you can apply
against a clean KDE1.0 kdebase package.
It has been uploaded under the name kdebase1.0-klock-patch and should
show up soon on a suitable place on KDE's ftp Server. The precise location
will be announced later, for example http://www.kde.org/news_dyn.html will
provide this information as soon as it available.
 
Christian Esken <a href="mailto:esken@kde.org">esken@kde.org</a>
