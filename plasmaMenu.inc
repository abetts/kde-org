<?php
	global $plasmaMenu;

	$plasmaMenu->addMenu("Community", "/community/", "green.icon.png", "#acff08");
		$plasmaMenu->addMenuEntry("About KDE", "/community/whatiskde/");
			$plasmaMenu->addSubMenuEntry("Software Compilation", "/community/whatiskde/softwarecompilation.php");
			$plasmaMenu->addSubMenuEntry("Project Management", "/community/whatiskde/management.php");
			$plasmaMenu->addSubMenuEntry("Development Model", "/community/whatiskde/devmodel.php");
			$plasmaMenu->addSubMenuEntry("Internationalization", "/community/whatiskde/i18n.php");
			$plasmaMenu->addSubMenuEntry("KDE e.V.", "https://ev.kde.org");
			$plasmaMenu->addSubMenuEntry("KDE Free Qt Foundation", "/community/whatiskde/kdefreeqtfoundation.php");
			$plasmaMenu->addSubMenuEntry("History", "/community/history/");
			$plasmaMenu->addSubMenuEntry("Awards", "/community/awards/");
			$plasmaMenu->addSubMenuEntry("Press Contact", "/contact/");
		$plasmaMenu->addMenuEntry("Announcements", "/announcements/");
		$plasmaMenu->addMenuEntry("Events", "http://www.kde.org/events/");
		$plasmaMenu->addMenuEntry("Get Involved", "http://community.kde.org/Get_Involved/");
		$plasmaMenu->addMenuEntry("Donate", "/community/donations/index.php");
		$plasmaMenu->addMenuEntry("Be a Supporting Member", "https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=5");	
		$plasmaMenu->addMenuEntry("Manifesto", "http://manifesto.kde.org/");
		$plasmaMenu->addMenuEntry("Code Of Conduct", "/code-of-conduct/");
		$plasmaMenu->addMenuEntry("Press Page", "/presspage/");
		$plasmaMenu->addMenuEntry("Thanks", "/thanks.php");
		$plasmaMenu->addMenuEntry("Store", "/stuff/metastore.php");
	
	$plasmaMenu->addMenu("Workspaces", "/workspaces/", "orange.icon.png", "#ffae00");
		$plasmaMenu->addMenuEntry("Plasma Desktop", "/workspaces/plasmadesktop/");
		
	$plasmaMenu->addMenu("Applications", "/applications/", "red.icon.png", "#ff96af");
		$plasmaMenu->addMenuEntry("Development", "/applications/development/");
		$plasmaMenu->addMenuEntry("Education", "/applications/education/");
		$plasmaMenu->addMenuEntry("Games", "/applications/games/");
		$plasmaMenu->addMenuEntry("Graphics", "/applications/graphics/");
		$plasmaMenu->addMenuEntry("Internet", "/applications/internet/");
		$plasmaMenu->addMenuEntry("Multimedia", "/applications/multimedia/");
		$plasmaMenu->addMenuEntry("Office", "/applications/office/");
		$plasmaMenu->addMenuEntry("System", "/applications/system/");
		$plasmaMenu->addMenuEntry("Utilities", "/applications/utilities/");
	
	$plasmaMenu->addMenu("Developer Platform", "/developerplatform/", "gray.icon.png", "#aaa");
	$plasmaMenu->addShortName("Dev. Platform");
		$plasmaMenu->addMenuEntry("Techbase Wiki", "http://techbase.kde.org/");
		$plasmaMenu->addMenuEntry("API Docs", "http://api.kde.org/");
		$plasmaMenu->addMenuEntry("Tutorials", "http://techbase.kde.org/Development/Tutorials/");
	
	$plasmaMenu->addMenu("Support", "/support/", "purple.icon.png", "#e285ff");
		$plasmaMenu->addMenuEntry("International Sites", "/support/international.php");
		$plasmaMenu->addMenuEntry("Documentation", "http://docs.kde.org/");
		$plasmaMenu->addMenuEntry("Userbase Wiki", "http://userbase.kde.org/");
		$plasmaMenu->addMenuEntry("Sys Admin Wiki", "http://techbase.kde.org/SysAdmin");
		$plasmaMenu->addMenuEntry("Forums", "http://forum.kde.org/");
		$plasmaMenu->addMenuEntry("Report a Bug", "https://bugs.kde.org/");
		$plasmaMenu->addMenuEntry("Mailing Lists", "/support/mailinglists/");
		$plasmaMenu->addMenuEntry("Security Advisories", "/info/security/");
?>
