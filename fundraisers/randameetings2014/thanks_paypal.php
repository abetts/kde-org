<?php
    require('../../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Donation received"
    ]);

    require('../../aether/header.php');
    $site_root = "../../";
?>

<main class="container">

<h1>Donation received</h1>

<h2>Thank you!</h2>

<p>
Thank you very much for your donation to the Randa Meetings 2014 fundraiser!
<br>
<br>
You can see your donation on the <a href="index.php">the Randa Meetings 2014 fundraiser page</a>.
</p>

</main>
<?php
  require('../../aether/footer.php');
