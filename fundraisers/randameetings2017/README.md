### Randa Meetings 2017

This is the website boilerplate for the Fundraising Campaign of Randa
Meetings 2017.

**Please edit/create on var/www/config.php the database credentials on deploy.
You can use var/www/config.php.dist like base**

To run using docker:
```bash
$ docker build . randa
$ docker run --rm -it -p 80:80 --net=host randa
```
Make sure that you have a mysql running with the following table:

```sql
create table randameetings2017donations(
	id INT NOT NULL AUTO_INCREMENT,
    created_at DATETIME,
    donorName VARCHAR(255),
    amount FLOAT(10,2),
    transaction_id VARCHAR(255) UNIQUE,
    PRIMARY KEY(id)
);
```
And setup the credentials on the config file.
The Dockerfile will copy all the files to the php/apache image and run it.

Access on your localhost on: 127.0.0.1:80

Any questions you can ping me on telegram: lays147 or send an email to:
laysrodriguessilva@gmail.com
