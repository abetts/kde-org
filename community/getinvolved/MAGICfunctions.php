<?php

/** Love() begs users for feedback to your page and shows them that you are actively working on it.
*  $name and $email, for the maintainer must be set to use this function
*
* @param $end_date - love is given until this date (ex. "2010-09-10") 
*
* @todo Copy emails to kde-www if time() > end_date + 10 days ???
*/
function MAGIC_love( $end_date )
{
    global $name, $mail;
    $end_date = strtotime($end_date);

    if (!$name)
            die ('Fatal Error: You must set $name before you give a page love.');
    if (!$mail)
            die ('Fatal Error: You must set $mail before you give a page love.');
    if ($end_date - time() > 3600*24*45)
            die ('Fatal Error: KDE LOVE cannot last more than a month.');

    if ($_POST['love_feedback_message'] )
    {
#        $_SERVER["REQUEST_URI"] = str_replace('', '', $_SERVER["REQUEST_URI"])
        $subject = 'KDE LOVE feedback: http://'. $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
        $message = "Hello $name\n\n".
        'You have received feedback from a user about about page at KDE. To stop getting these messages, remove '.
        'the MAGIC_love() call on that page.'.
        "\n\nURL: http://". $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"].
        "\nFROM: ".$_POST['love_feedback_from'].
        "\nEMAIL: ".$_POST['love_feedback_mail'].
        "\nMESSAGE: ".$_POST['love_feedback_message'];

        mail($mail, $subject, $message);
        echo '<div style="border: 2px solid #d99; background: #fcc">';
        echo '<table><tr><td><span style="color:red; font-size:300%">&hearts;</span>';
        echo '<td><h4>Thanks for the feedback, we really appreciate it!</h4></table></div>';
        return;
    }

    if (time() > $end_date )
    {
        # Love expired, htfu maintainer!
        $subject = 'KDE LOVE expired: http://'. $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
        $message = "Hello $name\n\n".
        'You have requested user feedback using the annoying LOVE feature. '.
        'Please implement any changes from the feedback you received and remove the MAGIC_love() call on the page.'.
        "\n\nURL: http://". $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"].
        "\nLOVE EXPIRED: ".date(DATE_RFC822, $end_date).

        "\n\n\nCheers,\nThe KDE LOVE System";
        mail($mail, $subject, $message);

        echo "expire: ". date(DATE_RFC822, $end_date);
        echo "<br>now it is: ". date(DATE_RFC822);
    }
    else
    {
        $display = $_GET['love_feedback'] ? 'block' : 'none';
?>
        <div style="border: 2px solid #d99; background: #fcc">
        <form method=POST>
        <table><tr><td><span style="color:red; font-size:300%">&hearts;</span>
        <td><h4>This page is receiving special love from <?php echo $name ?>.</h4>
        <p>We are actively working on this page to make it better! 
        <strong>Please give feedback</strong> so we can consider it while editing this page,
        especially information you find elsewhere that may be useful here.</p>
        <p><a onclick="document.getElementById('love_feedback').style.display='block';return false;" href="?love_feedback=1">
          Give Feedback</a></p>
        <p>
          <table id="love_feedback" style="display:<?php echo $display ?>" >
          <tr><td>From:<td><input name="love_feedback_from" size=40>
          <tr><td>Email:<td><input name="love_feedback_mail" size=40>
          <tr><td>Message:<td><textarea name="love_feedback_message" cols=80 rows=8></textarea>
          <tr><td><td><input type=submit value="Send feedback">
          </table>
        </p>
        </table></form></div>
<?php
    }
}


?>
