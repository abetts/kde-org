<?php

/**
 * @copyright 2019 Carl Schwan <carl@carlschwan.eu>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE
 * License as published by the Free Software Foundation; either 
 * version 3 of the License, or any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU AFFERO GENERAL PUBLIC LICENSE for more details.
 *  
 * You should have received a copy of the GNU Lesser General Public 
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

die("You shall not pass");

require_once('www_config.php');

/**
 * Create sample dabase for testing purpose
 */
class Fixture
{
    private $db;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
        $this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );//Error Handling
        $this->generateGlobalDonationTable();
        $this->generateDonationTable('randameetings2014donations');
        $this->generateDonationTable('randameetings2016donations');
        $this->generateDonationTable2('randameetings2017donations');
        $this->generateNews();
    }

    private function generateGlobalDonationTable()
    {
        $this->db->exec("CREATE TABLE IF NOT EXISTS donations (
            id INT AUTO_INCREMENT PRIMARY KEY,
            date DATETIME,
            amount FLOAT(10, 2),
            message VARCHAR(255),
            transactionid VARCHAR(255) UNIQUE,
            donate_url VARCHAR(255) );");

        $stmt = $this->db->prepare("INSERT into donations VALUES( NULL, :date, :payment_amount, :memo, :txn_id, :donate_url);");


        for ($i = 0; $i < 10; $i++) {
            $stmt->execute([
                'date' => (new DateTime())->format('Y-m-d H:i:s'),
                'payment_amount' => $i * 10.5,
                'memo' => "Thanks $i",
                'txn_id' => time() + $i,
                'donate_url' => "test.org/test_donation"
            ]);
        }
        $query = $this->db->query("SELECT * from donations");
    
        while ($row = $query->fetch()) {
            echo $row['amount'];
        }
    }
    
    private function generateDonationTable($name)
    {
        $stmt = $this->db->exec("CREATE TABLE IF NOT EXISTS $name (
            id INT AUTO_INCREMENT PRIMARY KEY,
            date DATETIME,
            amount FLOAT(10, 2),
            message VARCHAR(255),
            transactionid VARCHAR(255) UNIQUE );");

        $stmt = $this->db->prepare("INSERT into $name VALUES( NULL, :date, :payment_amount, :memo, :txn_id);");

        for ($i = 0; $i < 10; $i++) {
            $stmt->execute([
                'date' => (new DateTime())->format('Y-m-d H:i:s'),
                'payment_amount' => $i * 10.5,
                'memo' => "Thanks $i",
                'txn_id' => time() + $i,
            ]);
        }
        $query = $this->db->query("SELECT * from $name");
    
        while ($row = $query->fetch()) {
            echo $row['amount'] . '<br />';
        }
    }
    
    private function generateDonationTable2($name)
    {
        $stmt = $this->db->exec("CREATE TABLE IF NOT EXISTS $name (
            id INT AUTO_INCREMENT PRIMARY KEY,
            CREATED_AT DATETIME,
            amount FLOAT(10, 2),
            message VARCHAR(255),
            donor_name VARCHAR(255),
            transactionid VARCHAR(255) UNIQUE );");

        $stmt = $this->db->prepare("INSERT into $name VALUES( NULL, :date, :payment_amount, :memo, :name, :txn_id);");

        for ($i = 0; $i < 10; $i++) {
            $stmt->execute([
                'date' => (new DateTime())->format('Y-m-d H:i:s'),
                'payment_amount' => $i * 10.5,
                'memo' => "Thanks $i",
                'name' => "FooBar",
                'txn_id' => time() + $i,
            ]);
        }
        $query = $this->db->query("SELECT * from $name");
    
        while ($row = $query->fetch()) {
            echo $row['amount'] . '<br />';
        }
    }
    
    private function generateNews()
    {
        $stmt = $this->db->exec("CREATE TABLE IF NOT EXISTS news (
            id INT AUTO_INCREMENT PRIMARY KEY,
            title VARCHAR(255),
            url VARCHAR(255),
            timestamp DATETIME ); ");

        $stmt = $this->db->prepare("INSERT into news VALUES( NULL, :title, :url, :timestamp);");

        for ($i = 0; $i < 10; $i++) {
            $stmt->execute([
                'title' => 'hello' . $i,
                'url' => 'https://kde.org/hello' + $i,
                'timestamp' => (new DateTime())->format('Y-m-d H:i:s'),
            ]);
        }
        $query = $this->db->query("SELECT * from news");
    
        while ($row = $query->fetch()) {
            echo $row['title'] . '<br />';
        }
    }
}

$fixture = new Fixture($dbConnection);
echo "Done";
