<?php

//error_reporting(E_ALL);

// Some helper functions for the visual guide

function screenshot($thumbnail, $filename, $align=false, $description=false, $height=false) {
    if (!$align) {
        $align="center";
    }

    $out = '<div  align="'. $align .'" style="width: auto; margin-top: 20px; margin-botton: 20px;">';
    if (!empty($filename)) {
            //$out .= '<a href="screenshots/'. str_replace(".png", ".jpg", $filename) .'" title="Click to enlarge">';
            $out .= '<a href="screenshots/'. $filename .'" title="Click to enlarge">';
    }
    if ($height) {
        $height=" height=\"$height\" ";
    }
    //$out .= '<img src="screenshots/' . str_replace(".png", ".jpg", $thumbnail) .'" align="'. $align .'" ' . $height . ' />';
    $out .= '<img src="screenshots/' . $thumbnail .'" align="'. $align .'" ' . $height . ' />';
    if (!empty($filename)) {
            $out .= '</a>';
    }
    if ($description) {
        $out .= '<br /><em>' . $description . '</em>';
    }
    $out .= '</div>';
    print $out;
};


?>