<?php
  $page_title = "Anuncio de lanzamiento de KDE 4.3 Final";
  $site_root = "../";
  include "header.inc";
?>

Also available in:
<?php
  $release = '4.3';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
  La comunidad de KDE ofrece nuevas innovaciones con el lanzamientos de KDE 4.3
</h3>

<p align="justify">
  <strong>
    KDE 4.3 (nombre clave: <i>"Caizen"</i>) ofrece nuevas innovaciones a los usuarios de escritorios libres y a los desarrolladores de software
  </strong>
</p>

<p align="justify">
El 4 de agosto del 2009, la <a href=http://www.kde.org/>comunidad de KDE</a> anuncia la disponibilidad inmediata de la esperada <i>"Caizen"</i> (también conocida como KDE 4.3), con muchas mejoras de la experiencia de usuario y de su plataforma de desarrollo. Esta versión continúa puliendo las características únicas de anteriores versiones, a la vez que trae nuevas innovaciones. La versión 4.2 estaba enfocada a usuarios finales, mientras que KDE 4.3 ofrece un producto estable y completo dirigido al hogar y a pequeñas oficinas.</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="images/kde430-desktop.png"><img src="images/kde430-desktop_thumb.jpg" align="center" width="540" height="337"  /></a><br />
<em>El escritorio KDE 4.3</em></div>

<p align=justify>
La comunidad de KDE <strong>ha solucionado más de 10.000 bugs</strong> y <strong>ha implementado cerca de 2.000 solicitudes de características</strong> en los últimos 6 meses. Los cerca de 700 colaboradores han verificado unos 63.000 cambios. Para tener una visión general de los cambios en el espacio de trabajo, grupos de aplicaciones, y plataforma de desarrollo, siga leyendo.
</p>


<h3>
  El escritorio mejora el rendimiento y la usabilidad
</h3>
<br />
<p align=justify>
    El espacio de trabajo de KDE provee una experiencia de escritorio completa y potente, que ofrece una excelente integración con Linux y otros UNIX. Entre los principales componentes del espacio de trabajo de KDE se incluye:
<ul>
  <li>
    <strong>KWin</strong>, un potente gestor de ventanas que provee modernos efectos gráficos 3D.
  </li>
  <li>
    <strong>El Plasma Desktop Shell</strong>, un sistema de escritorio y paneles de vanguardia, que ofrece mejoras en la productividad y en la integración online mediante miniaplicaciones personalizables.
  </li>
  <li>
    <strong>Dolphin</strong>, un administrador de archivos fácil de usar, transparente a redes, y adaptable a contenidos.
  </li>
  <li>
    <strong>KRunner</strong>, un sistema de lanzamiento y búsqueda, para ejecutar órdenes y encontrar información útil.
  </li>
  <li>
    Fácil acceso a los controles del sistema y del escritorio mediante las <strong>Preferencias del sistema</strong>.
  </li>
</ul>
A continuación puede leer una breve lista de las mejoras en el espacio de trabajo del escritorio KDE:
<ul>
  <li>
    El <a href="http://plasma.kde.org">Plasma Desktop Shell</a> introduce un <strong>nuevo tema por defecto</strong>, Air. Air parece mucho más ligero y se ajusta mejor al tema por defecto para las aplicaciones. Plasma también ha <strong>mejorado mucho en rendimiento</strong>. Se ha reducido el uso de memoria y las animaciones son más suaves. Ahora <strong>las actividades pueden estar vinculadas a los escritorios virtuales</strong>, permitiendo a los usuarios tener diferentes miniaplicaciones en cada uno de sus escritorios. Además, Plasma ha mejorado en <strong>las notificaciones y gestión de trabajos</strong>. Las tareas en funcionamiento son agrupadas en una barra simple para prevenir que se muestren demasiados diálogos a la vez. Las animaciones se utilizan para representar que las tareas están todavía en funcionamiento, mediante diálogos suaves y deslizantes dentro de la bandeja del sistema y animando el icono de notificación. Entre los pequeños cambios en Plasma se encuentran los <strong>atajos de teclado totalmente configurables</strong> y una navegación con el teclado más minuciosa, la posibilidad de crear una miniaplicación Plasma al arrastrar o copia contenido en el escritorio, y muchas <strong>nuevas y mejoradas miniaplicaciones Plasma</strong>. El elemento gráfico "Vista de carpeta" ahora permite al usuario <strong>navegar dentro de una carpeta simplemente pasando el puntero sobre ella</strong>, y el <strong>nuevo componente Translatoid</strong> traduce palabras y frases correctamente en su escritorio usando Google Translate. Además, KRunner hace más sencillo <strong>descubrir el funcionamiento de sus complementos</strong> mediante un botón de ayuda que muestra la sintaxis de los mismos en el área de los resultados. <strong>Las acciones también tienen una pequeña configuración</strong> para permitir, por ejemplo, lanzar aplicaciones desde la cuenta de otro usuario.<br />
    <br />
    </li>
</ul>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/desktop.png"><img src="screenshots/desktop_thumb.jpg" align="center" width="504" height="315"  /></a><br />
<em>Integración web en KDE 4.3</em></div>
<ul>
  <li>
    El administrador de archivos Dolphin <strong>muestra las vistas previas de los archivos de una carpeta</strong> y de los vídeos para ayudar a identificar elementos. La <strong>papelera puede ser configurada</strong> desde las preferencias de Dolphin, y limitar el tamaño de la misma para prevenir que el disco se llene con archivos borrados. El menú que se muestra al pulsar con el botón derecho sobre un elemento es ahora configurable en las preferencias y el diálogo de configuración ha sido rediseñado para hacerlo <strong>más fácil de usar</strong>. La nueva <strong>dirección network:/</strong> muestra otros ordenadores y servicios en la red (actualmente está limitado por lo que se anuncia en los protocolos DNS-SD/zeroconf, pero serán añadidos otros en el futuro).<br />
    <br />
  </li>
  <li>
    Un mayor refinamiento en las herramientas del espacio de trabajo le hace más fácil trabajar con su ordenador. Las <strong>preferencias del sistema (más rápidas)</strong> proporcionan una vista en árbol opcional para la configuración y muchas mejoras en los diálogos de la misma. Hay <strong>nuevos efectos</strong> como "Sheet" y "Slide Back" y un <strong>mejor rendimiento en KWin</strong> que hace la gestión de ventanas más suave, mientras que la integración con los temas Plasma es ahora más consistente. <strong>Klipper</strong>, una herramienta que guarda el historial de las cosas copiadas al portapapeles, ahora puede <strong>actuar inteligentemente según el contenido</strong>, determinando automáticamente una lista de aplicaciones que pueden manejar el objeto copiado al portapapeles y permitiendo al usuario iniciar una de inmediato.<br />
    <br />
  </li>
</ul>
</p>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<embed src="http://blip.tv/play/hIsigZW3agI" type="application/x-shockwave-flash" width="480" height="390" allowscriptaccess="always" allowfullscreen="true"></embed>
<br />
Un screencast mostrando algunas de las mejoras arriba mencionadas <a href="http://blip.tv/file/get/Jospoortvliet-KDE43DesktopWorkspaceDemo820.ogv">(versión Ogg Theora)</a></div>
<h3>
  Avances en las aplicaciones
</h3>
<p align=justify>
La comunidad de KDE proporciona un gran número de aplicaciones sofisticadas que sacan provecho del potente KDE Application Framework. En la distribución de KDE se incluye una selección de ellas, divididas por categorías en varios grupos, que incluyen:
</p>
<ul>
  <li>
    Aplicaciones de red de KDE
  </li>
  <li>
    KDE Multimedia
  </li>
  <li>
    Herramientas gráficas de KDE
  </li>
  <li>
    KDE PIM Suite (para la gestión de la información personal y comunicaciones)
  </li>
  <li>
    Aplicaciones educativas de KDE
  </li>
  <li>
    Juegos de KDE
  </li>
  <li>
    Utilidades de KDE
  </li>
  <li>
    Plataforma de desarrollo de software de KDE
  </li>
</ul>
<p align=justify>
Juntas forma un completo conjunto de aplicaciones esenciales para el escritorio, que funcionan en los sistemas operativos más modernos. A continuación puede ver una selección de mejoras de algunas de estos grupos de aplicaciones:
</p>
<ul>
  <li>
    Las <strong>utilidades de KDE</strong> han sufrido muchas mejoras. Entre otras cosas, <strong>KGpg</strong>, la herramienta de privacidad usada para cifrar y firmar archivos y correos electrónicos ahora <strong>integra Solid</strong> para detectar la disponibilidad de una conexión de red y ha mejorado el <strong>diálogo de importación de claves</strong>. <strong>Ark</strong>, el gestor de compresión y descompresión, <strong>ahora soporta LZMA/XZ</strong>, también ha mejorado el soporte de zip, rar, y 7zip, y trabaja mejor "arrastrando y pegando". KDELirc, una interfaz gráfica para <strong>el sistema de control remoto por infrarrojos de Linux</strong> (LIRC), ha sido portado a KDE 4 y se incluye de nuevo. <strong>Okteta</strong>, el editor hexadecimal de KDE tiene ahora una <strong>herramienta de verificación</strong> (checksum), un navegador de archivos, y un gestor de favoritos en la barra lateral. <strong>Lokalize</strong>, la herramienta de traducción de KDE, introduce soporte para scripts, nuevos formato de ficheros y la <strong>traducción de documentos ODF</strong>.<br />
    <br />
  </li>
  <li>
    Los <strong>juegos de KDE</strong> ahora usan un <strong>estilo egipcio</strong> similar en muchos de ellos. KGoldrunner incluye, aparte de una mejora en el pausado, retomado, grabado y reproducido de los juegos, un nuevo juego: <strong>"La Maldición de la Momia"</strong>; KMahjongg trae 70 nuevos niveles creados por usuarios; y se ha añadido un <strong>nuevo juego</strong>, KTron. Algunos juegos poseen <strong>nuevas características</strong> como la acción de Vaporización en Killbots y una mejora en la IA de Bovo. Gracias al trabajo de la carga/guardado de archivos, y en el mostrado de imágenes escalables, muchos juegos <strong>arrancan y funcionan más rápido</strong>.<br />
    <br />
  </li>
  <li>
    El <strong>Gestor de Información Personal de KDE (PIM)</strong> ha sido mejorado en rendimiento como en estabilidad, entre otras áreas. El cliente de mensajería instantánea <strong>Kopete</strong> mejora su lista de contactos y KOrganizer se puede sincronizar con <strong>Google Calendar</strong>. Kmail soporta la inserción de imágenes entre líneas en los correos y en el <strong>notificador Alarm</strong> se ha ganado la funcionalidad de exportación, así como una mejor configuración de arrastrar y soltar.<br />
    <br />
  </li>
</ul>
<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/games.png"><img src="screenshots/games_thumb.jpg" align="center" width="504" height="315"  /></a><br />
<em>Algunos temas egipcios</em></div>
<ul>
  <li>
    En el caso de que falle alguna de las aplicaciones de KDE, la <strong>nueva herramienta de notificación de fallos</strong> hará más fácil para el usuario contribuir a la estabilidad de KDE. Dicha herramienta califica con hasta tres estrellas la calidad de los datos del fallo obtenidos. También ofrece sugerencias sobre cómo mejorar la calidad de dichos datos y del propio notificador, mientras guía al usuario a través del proceso de informe. Durante el ciclo de las betas de esta versión, la nueva herramienta de notificación de fallos ya ha demostrado el aumento de la calidad de los informes de fallos.<br />
  </li>
</ul>

<h3>
  Plataforma de aceleración del desarrollo
</h3>
<p align=justify>
La comunidad de KDE trae muchas novedades para los desarrolladores de aplicaciones con el Framework de Desarrollo de Aplicaciones de KDE. Construido sobre los pilares de la biblioteca Qt de Nokia, es un framework integrada y coherente, en respuesta a lo que los desarrolladores necesitan.
</p>
<p align=justify>
El Framework de Desarrollo de Aplicaciones de KDE ayuda a los desarrolladores a crear aplicaciones robustas de manera eficiente, racionalizando las tareas complejas y tediosas normalmente asociadas con el desarrollo de aplicaciones. Su uso por parte de las aplicaciones de KDE ofrece un escaparate de su flexibilidad y utilidad.
</p>
<p align=justify>
Liberado bajo la licencia LGPL (permitido el desarrollo de código abierto y propietario) y multiplataforma (Linux, UNIX, Mac y MS Windows), contiene entre otras cosas un poderoso modelo de componentes (<strong>KParts</strong>), una red de acceso transparente a los datos (<strong>KIO</strong>) y una gestión flexible de la configuración, docenas de miniaplicaciones útiles que van desde cuadros de diálogo de archivo hasta un marco semántico de búsqueda (<strong>Nepomuk</strong>), gestores de hardware (<strong>Solid</strong>) y acceso multimedia (<strong>Phonon</strong>). A continuación puede consultar la lista de mejoras en el Framework de Desarrollo de Aplicaciones de KDE.
</p>
<ul>
  <li>
    El Framework de Desarrollo de Aplicaciones de KDE presenta el principio de la integración con el <a href="http://www.socialdesktop.org/">escritorio social</a>, trayendo a toda la comunidad del software libre al escritorio. Ofreciendo una <strong>plataforma de colaboración abierta, compartición y comunicación</strong>, la iniciativa del escritorio social tiene por objetivo permitir a la gente compartir sus conocimientos sin ceder el control a una organización externa. La plataforma actualmente nos ofrece una miniaplicación para Plasma.
    <br />
  </li>
  <li>
    El <strong>nuevo protocolo de la bandeja del sistema</strong> desarrollado en colaboración con la <a href="http://www.freedesktop.org/wiki/">iniciativa Free Desktop</a> rompe con las especificaciones de la vieja bandeja del sistema. Esta usaba pequeñas ventanas empotradas que no permitían un gran control, limitando la flexibilidad de los usuarios y desarrollo de aplicaciones. Mientras que la nueva bandeja del sistema soporta ambos standard, tanto viejo como nuevo, a los desarrolladores se les anima a mejorar sus aplicaciones con las nuevas especificaciones. Para más información <a href="http://www.notmart.org/index.php/Software/Systray_finally_in_action">consulte este blog</a> o en <a href="http://techbase.kde.org/Projects/Plasma/NewSystemTray">TechBase</a>.<br />
    <br />
  </li>
  <li>
    The Plasma Desktop Shell introduce la <strong>geolocalización</strong> usando libgps y HostIP, que permiten a los plasmoides responder fácilmente a la localización del usuario. Otro <strong>nuevo motor de datos</strong> proporciona acceso a los <strong>recursos de Akonadi</strong> (incluyendo correo electrónico y calendario), metadatos de <a href="http://nepomuk.semanticdesktop.org/">Nepomuk</a> y estado del tecado, además de mejoras mejoras en los motores de datos ya existente. Puede descubrir cómo usar y encontrar motores de datos <a href="http://techbase.kde.org/Development/Tutorials/Plasma/DataEngines">en TechBase</a>.<br />
    <br />
  </li>
  <li>
    El Framework de Desarrollo de Aplicaciones de KDE introduce un <strong>envoltorio de PolicyKit</strong> para que a los desarrolladores les sea más fácil realizar acciones de forma segura, coherente y fácil. Proporciona un gestor de autorización y un agente de autenticación, y una sencilla biblioteca para los desarrolladores. <a href="http://techbase.kde.org/Development/Tutorials/PolicyKit/Introduction">En Techbase</a> puede leer un tutorial.<br />
    <br />
  </li>
  <li>
    <strong>Akonadi</strong>, la solución de almacenamiento PIM de Free Desktop se ha considerado <strong>listo para el uso general</strong>. Además de la disponibilidad de motores de datos para Plasma, se anime a los desarrolladores a echar un vistazo a la <a href="http://techbase.kde.org/Projects/PIM/Akonadi">página de TechBase</a> si su aplicación necesita acceder a registros de chat, correo, blogs, contactos o cualquier otro tipo de datos personales. Como tecnología multi-escritorio, Akonadi puede facilitar el acceso a cualquier tipo de datos y está diseñado para manejar grandes volúmenes de datos, permitiendo así una amplia variedad de usos.<br />
    <br />
  </li>
</ul>
</p>

<div  align="center" style="width: auto; margin-top: 20px; margin-botton: 20px;">
<a href="screenshots/social.png"><img src="screenshots/social_thumb.jpg" align="center" width="504" height="315"  /></a><br />
<em>El escritorio social y otros servicios online en acción</em></div>
<h4>
Más cambios
</h4>
<p align=justify>
Como se mencionó, lo anterior es solo una selección de los cambios y mejoras en el escritorio KDE, las aplicaciones de KDE y el Framework de Desarrollo de Aplicaciones de KDE. Una lista más amplia, aunque aún incompleta, se puede encontrar en el <a href="http://techbase.kde.org/Schedules/KDE4/4.3_Feature_Plan">plan de características de KDE 4.3</a> en <a href="http://techbase.kde.org">TechBase</a>. Puede encontrar más información sobre las aplicaciones desarrolladas por la comunidad de KDE en la <a href="https://www.kde.org/family/">página web de la familia KDE</a> y en el <a href="http://kde-apps.org">sitio web de KDE-apps</a>. Los desarrolladores de Marble, del equipo de KDE Edu, han liberado Marble 0.8 con KDE 4.3 y elaborado un registro de cambios gráfico en <a href="http://edu.kde.org/marble/current.php">su página web</a>.
</p>
<h4>
    Corra la voz y vea lo que ocurre
</h4>
<p align="justify">
La comunidad de KDE anima a todo el mundo a <strong>correr la voz</strong> en la web social. Envíe artículos a sitios web, use canales como delicious, digg, reddit, twitter o identi.ca. Subia capturas de pantalla a servicios como Facebook, FlickR, ipernity y Picasa y publíquelas en los grupos adecuados. Cree demostraciones en vídeo y envíelas a YouTube, Blip.tv, Vimeo y otros sitios. No olvide etiquetar el material que envíe con la etiqueta <strong>kde</strong> para que sea más fácil para todo el mundo encontrar el material, y para el equipo de KDE compilar recortes del anuncio de KDE 4.3. <strong>¡Ayúdenos a correr la voz, sea parte del proyecto!</strong></p>

<p align="justify">
Puedes seguir en directo lo que ocurre en torno a la liberación de KDE 4.3 en la web social a través del nuevo <a href="http://buzz.kde.org"><strong>livefeed de la Comunidad de KDE</strong></a>. Este sitio agrega en tiempo real lo que sucede en identi.ca, twitter, youtube, flickr, picasaweb, blogs y en muchas otras redes sociales. Puede encontrar el livefeed en <strong><a href="http://buzz.kde.org">buzz.kde.org</a></strong>.
</p>

<center>
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/linux_unix/KDE_KDE_4_3_0_Caizen_Release_Announcement"><img src="buttons/digg.gif" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/linux/comments/97gdu/kde_430_caizen_released/"><img src="buttons/reddit.gif"><img src="buttons/reddit.gif" /></a>
    </td>
    <td>
        <a href="http://www.twitter.com"><img src="buttons/twitter.gif" /></a>
    </td>
    <td>
        <a href="http://www.identi.ca"><img src="buttons/identica.gif" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde43/"><img src="buttons/flickr.gif" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde43"><img src="buttons/youtube.gif" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com"><img src="buttons/facebook.gif" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde43"><img src="buttons/delicious.gif" /></a>
    </td>
</tr>
</table>
<style="font-size: 5pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></style>
</center>
<h4>
  Instalación de KDE 4.3.0
</h4>
<p align="justify">
KDE, incluyendo todas sus bibliotecas y aplicaciones, está disponible bajo licencias de software libre. El software de KDE se puede ejecutar sobre varias configuraciones de hardware, sistemas operativos y cualquier gestor de ventanas o entorno de escritorio. Además de Linux y otros sistemas operativos basados en UNIX, en el sitio web de <a href="http://windows.kde.org">KDE for Windows</a> dispone de versiones para Microsoft Windows de la mayoría de aplicaciones, y versiones para Mac OS X en el sitio de <a href="http://mac.kde.org/">KDE on Mac</a>. También puede encontrar compilaciones experimentales de las aplicaciones de KDE para plataformas móviles como MS Windows Mobile y Symbian en la web, pero no están soportadas.
<br />
KDE se puede obtener en código fuente y varios formatos binarios en <a href="http://download.kde.org/stable/4.3.0/">http://download.kde.org</a> y también se puede obtener en <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a> o con cualquiera de las <a href="http://www.kde.org/download/distributions.php">principales distribuciones de GNU/Linux y sistemas UNIX</a> actuales.
</p>
<p align="justify">
  <em>Empaquetadores</em>.
  Algunos proveedores de sistemas operativos Linux/UNIX han tenido la amabilidad de proporcionar paquetes binarios de KDE 4.2.0 para algunas versiones de su distribución, y en otros casos lo han hecho voluntarios de la comunidad.<br />
Algunos de estos paquetes binarios se pueden descargar gratuitamente desde <a href="http://download.kde.org/binarydownload.html?url=/stable/4.3.0/">http://download.kde.org</a>. Durante las próximas semanas, puede que haya disponibles más paquetes binarios, así como actualizaciones para los paquetes que ya hay disponibles ahora.
</p>
<p align="justify">
La mayoría de los problemas de rendimiento con el controlador gráfico binario de <em>NVidia</em> han sido <a href="http://techbase.kde.org/User:Lemma/KDE4-NVIDIA">solucionados</a> en las últimas versiones beta del controlador disponible de NVidia. Sin embargo, debido a los cambios recientes en la <a href="http://x.org">pila gráfica</a> de Linux, puede que cierto software o configuraciones de hardware tengan problemas relativos a la velocidad. Por favor, contacte con el proveedor de su distribución o con los desarrolladores de controladores si encuentra algún problema.
</p>
<p align="justify">
  <a name="package_locations"><em>Ubicaciones de los paquetes</em></a>.
  Visite la <a href="/info/4.3.0.php">página de información sobre KDE 4.3.0</a> para obtener una lista actualizada de los paquetes binarios disponibles de los que ha sido informado el Proyecto KDE.
</p>

<h4>
  Compilación de KDE 4.3.0
</h4>
<p align="justify">
  <a name="source_code"></a>
  El código fuente completo de KDE 4.3.0 se puede <a href="http://download.kde.org/stable/4.3.0/src/">descargar libremente</a>. Las instrucciones para compilar e instalar KDE 4.3.0 están disponibles en la <a href="/info/4.3.0.php#binary">página de información sobre KDE 4.3.0</a>.
</p>
<p>
<br /><strong>*</strong> Los sistemas operativos y marcas mencionadas en esta página son propiedad de sus respectivos dueños.<br />
</p>
<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contactos de prensa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
