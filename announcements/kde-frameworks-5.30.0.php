<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.30.0");
  $site_root = "../";
  $release = '5.30.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
January 14, 2017. KDE today announces the release
of KDE Frameworks 5.30.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("add Haguichi icons from Stephen Brandt (thanks)");?></li>
<li><?php i18n("finalize calligra icon support");?></li>
<li><?php i18n("add cups icon thanks colin (bug 373126)");?></li>
<li><?php i18n("update kalarm icon (bug 362631)");?></li>
<li><?php i18n("add krfb app icon (bug 373362)");?></li>
<li><?php i18n("add r mimetype support (bug 371811)");?></li>
<li><?php i18n("and more");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("appstreamtest: handle non-installed programs");?></li>
<li><?php i18n("Enable colored warnings in ninja's output");?></li>
<li><?php i18n("Fix missing :: in API docs to trigger code styling");?></li>
<li><?php i18n("Ignore host libs/includes/cmakeconfig files in Android toolchain");?></li>
<li><?php i18n("Document usage of gnustl_shared with Android toolchain");?></li>
<li><?php i18n("Never use -Wl,--no-undefined on Mac (APPLE)");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Improve KPackage KNSHandler");?></li>
<li><?php i18n("Search for the more precise required version of AppstreamQt");?></li>
<li><?php i18n("Add KNewStuff support for KPackage");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Make compile with -fno-operator-names");?></li>
<li><?php i18n("Do not fetch more linked resources when one of them goes away");?></li>
<li><?php i18n("Added model role to retrieve the list of linked activities for a resource");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("add Android to the list of available platforms");?></li>
<li><?php i18n("include ObjC++ source files");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Generate an instance with KSharedConfig::Ptr for singleton and arg");?></li>
<li><?php i18n("kconfig_compiler: Use nullptr in generated code");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Hide the \"Show Menu Bar\" action if all the menubars are native");?></li>
<li><?php i18n("KConfigDialogManager: drop kdelibs3 classes");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Return stringlist/boolean types in KPluginMetaData::value");?></li>
<li><?php i18n("DesktopFileParser: Honor ServiceTypes field");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Add python bindings for KDBusAddons");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Introduce org.kde.kconfig QML import with KAuthorized");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("kdoctools_install: match the full path for the program (bug 374435)");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("[runtime] Introduce a KGLOBALACCEL_TEST_MODE env variable");?></li>
</ul>

<h3><?php i18n("KDE GUI Addons");?></h3>

<ul>
<li><?php i18n("Add Python bindings for KGuiAddons");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Set plugin data so that the Embeddable Image Viewer works");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Inform QIconLoader also when the desktop icon theme is changed (bug 365363)");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Take X-KDE-RunOnDiscreteGpu property into account when starting app using klauncher");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("KIO::iconNameForUrl will now return special icons for xdg locations like the user's Pictures folder");?></li>
<li><?php i18n("ForwardingSlaveBase: fix passing of Overwrite flag to kio_desktop (bug 360487)");?></li>
<li><?php i18n("Improve and export KPasswdServerClient class, the client API for kpasswdserver");?></li>
<li><?php i18n("[KPropertiesDialog] Kill \"Place in system tray\" option");?></li>
<li><?php i18n("[KPropertiesDialog] Don't change \"Name\" of \"Link\" .desktop files if file name is read-only");?></li>
<li><?php i18n("[KFileWidget] Use urlFromString instead of QUrl(QString) in setSelection (bug 369216)");?></li>
<li><?php i18n("Add option to run an app on a discrete graphics card to KPropertiesDialog");?></li>
<li><?php i18n("terminate DropJob properly after dropping to an executable");?></li>
<li><?php i18n("Fix logging category usage on Windows");?></li>
<li><?php i18n("DropJob: emit started copy job after creation");?></li>
<li><?php i18n("Add support for calling suspend() on a CopyJob before it gets started");?></li>
<li><?php i18n("Add support for suspending jobs immediately, at least for SimpleJob and FileCopyJob");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("Update proxies for recently realised class of bugs (layoutChanged handling)");?></li>
<li><?php i18n("Make it possible for KConcatenateRowsProxyModel to work in QML");?></li>
<li><?php i18n("KExtraColumnsProxyModel: Persist model indexes after emitting layoutChange, not before");?></li>
<li><?php i18n("Fix assert (in beginRemoveRows) when deselecting an empty child of a selected child in korganizer");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("Don't focus progress windows (bug 333934)");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("[GHNS Button] Hide when KIOSK restriction applies");?></li>
<li><?php i18n("Fix set up of the ::Engine when created with an absolute config file path");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Add a manual test for Unity launchers");?></li>
<li><?php i18n("[KNotificationRestrictions] Let user can specify restriction reason string");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("[PackageLoader] Don't access invalid KPluginMetadata (bug 374541)");?></li>
<li><?php i18n("fix description for option -t in man page");?></li>
<li><?php i18n("Improve error message");?></li>
<li><?php i18n("Fix the help message for --type");?></li>
<li><?php i18n("Improve installation process of KPackage bundles");?></li>
<li><?php i18n("Install a kpackage-generic.desktop file");?></li>
<li><?php i18n("Exclude qmlc files from installation");?></li>
<li><?php i18n("Don't list separately plasmoids from metadata.desktop and .json");?></li>
<li><?php i18n("Additional fix for packages with different types but same ids");?></li>
<li><?php i18n("Fix cmake failure when two packages with different type have same id");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Call the new checkAmbiguousShortcuts() from MainWindow::createShellGUI");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("KSycoca: don't follow symlink to directories, it creates a risk of recursion");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Fix: Forward dragging text results in wrong selection (bug 374163)");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Specify minimum required GpgME++ version 1.7.0");?></li>
<li><?php i18n("Revert \"If Gpgmepp is not found, try to use KF5Gpgmepp\"");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Add python bindings");?></li>
<li><?php i18n("Add KToolTipWidget, a tooltip that contains another widget");?></li>
<li><?php i18n("Fix KDateComboBox checks for valid entered dates");?></li>
<li><?php i18n("KMessageWidget: use darker red color when type is Error (bug 357210)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Warn on startup about ambiguous shortcuts (with an exception for Shift+Delete)");?></li>
<li><?php i18n("MSWin and Mac have similar autosave windowsize behaviour");?></li>
<li><?php i18n("Display application version in about dialog header (bug 372367)");?></li>
</ul>

<h3><?php i18n("Oxygen Icons");?></h3>

<ul>
<li><?php i18n("sync with breeze icons");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Fix the renderType properties for various components");?></li>
<li><?php i18n("[ToolTipDialog] Use KWindowSystem::isPlatformX11() which is cached");?></li>
<li><?php i18n("[Icon Item] Fix updating implicit size when icon sizes change");?></li>
<li><?php i18n("[Dialog] Use setPosition / setSize instead of setting everything individually");?></li>
<li><?php i18n("[Units] Make iconSizes property constant");?></li>
<li><?php i18n("There is now a global \"Units\" instance reducing memory consumption and creation time of SVG items");?></li>
<li><?php i18n("[Icon Item] Support non-square icons (bug 355592)");?></li>
<li><?php i18n("search/replace old hardcoded types from plasmapkg2 (bug 374463)");?></li>
<li><?php i18n("Fix X-Plasma-Drop* types (bug 374418)");?></li>
<li><?php i18n("[Plasma ScrollViewStyle] Show scroll bar background only on hover");?></li>
<li><?php i18n("Fix #374127 misplacement of popups from dock wins");?></li>
<li><?php i18n("Deprecate Plasma::Package API in PluginLoader");?></li>
<li><?php i18n("Recheck which representation we should using in setPreferredRepresentation");?></li>
<li><?php i18n("[declarativeimports] Use QtRendering on phone devices");?></li>
<li><?php i18n("consider an empty panel always \"applets loaded\" (bug 373836)");?></li>
<li><?php i18n("Emit toolTipMainTextChanged if it changes in response to a title change");?></li>
<li><?php i18n("[TextField] Allow disabling reveal password button through KIOSK restriction");?></li>
<li><?php i18n("[AppletQuickItem] Support launch error message");?></li>
<li><?php i18n("Fix logic for arrow handling in RTL locales (bug 373749)");?></li>
<li><?php i18n("TextFieldStyle: Fix implicitHeight value so the text cursor is centered");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("cmake: look for hunspell-1.6 as well");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Fix highlighting of Makefile.inc by adding priority to makefile.xml");?></li>
<li><?php i18n("Highlight SCXML files as XML");?></li>
<li><?php i18n("makefile.xml: many improvements, too long to list here");?></li>
<li><?php i18n("python syntax: added f-literals and improved string handling");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.30");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.5");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
