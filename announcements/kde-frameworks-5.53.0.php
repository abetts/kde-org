<?php
    include_once ("functions.inc");
    $translation_file = "www";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.53.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.53.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
December 09, 2018. KDE today announces the release
of KDE Frameworks 5.53.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Fix searches for rating 10 (5 stars) (bug 357960)");?></li>
<li><?php i18n("Avoid writing unchanged data to terms dbs");?></li>
<li><?php i18n("Do not add Type::Document/Presentation/Spreadsheet twice for MS Office docs");?></li>
<li><?php i18n("Actually initialize kcrash properly");?></li>
<li><?php i18n("Make sure there is only one MTime per document in the MTimeDB");?></li>
<li><?php i18n("[Extractor] Use QDataStream serialization in place of cooked one");?></li>
<li><?php i18n("[Extractor] Replace homegrown IO handler with QDataStream, catch HUP");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add icons for application-vnd.appimage/x-iso9660-appimage");?></li>
<li><?php i18n("Add 22px dialog-warning icon (bug 397983)");?></li>
<li><?php i18n("Fix angle and margin of 32px dialog-ok-apply (bug 393608)");?></li>
<li><?php i18n("Change primary monochrome icon colors to match new HIG colors");?></li>
<li><?php i18n("Change archive-* action icons to represent archives (bug 399253)");?></li>
<li><?php i18n("Add help-browser symlink to 16px and 22px directories (bug 400852)");?></li>
<li><?php i18n("Add new generic sorting icons; rename existing sorting icons");?></li>
<li><?php i18n("Add root version of drive-harddisk (bug 399307)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("New module: FindLibExiv2.cmake");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Add option BUILD_KPACKAGE_INSTALL_HANDLERS to skip building install handlers");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Add busy indicator during research and make the research asynchronous (bug 379281)");?></li>
<li><?php i18n("Normalize all input paths with the os.path.normpath function (bug 392428)");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Perfect alignment between QML and QWidget KCM titles");?></li>
<li><?php i18n("Add context to kcmodule connection to lambdas (bug 397894)");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Make it possible to use KAboutData/License/Person from QML");?></li>
<li><?php i18n("Fix crash if XDG_CACHE_HOME directory is too small or out of space (bug 339829)");?></li>
</ul>

<h3><?php i18n("KDE DNS-SD");?></h3>

<ul>
<li><?php i18n("now installs kdnssd_version.h to check the version of the lib");?></li>
<li><?php i18n("do not leak resolver in remoteservice");?></li>
<li><?php i18n("prevent avahi signal racing");?></li>
<li><?php i18n("fix for macOS");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Revive 'Description' property for DublinCore metadata");?></li>
<li><?php i18n("add a description property to KFileMetaData");?></li>
<li><?php i18n("[KFileMetaData] Add extractor for DSC conforming (Encapsulated) Postscript");?></li>
<li><?php i18n("[ExtractorCollection] Avoid dependency of kcoreaddons for CI tests");?></li>
<li><?php i18n("Add support for speex files to taglibextractor");?></li>
<li><?php i18n("add two more internet sources regarding tagging information");?></li>
<li><?php i18n("simplify handling of id3 tags");?></li>
<li><?php i18n("[XmlExtractor] Use QXmlStreamReader for better performance");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Fix assert when cleaning up symlinks in PreviewJob");?></li>
<li><?php i18n("Add the possibility to have a keyboard shortcut to create a file");?></li>
<li><?php i18n("[KUrlNavigator] Re-activate on mouse middle button click (bug 386453)");?></li>
<li><?php i18n("Remove dead search providers");?></li>
<li><?php i18n("Port more search providers to HTTPS");?></li>
<li><?php i18n("Export again KFilePlaceEditDialog (bug 376619)");?></li>
<li><?php i18n("Restore sendfile support");?></li>
<li><?php i18n("[ioslaves/trash] Handle broken symlinks in deleted subdirectories (bug 400990)");?></li>
<li><?php i18n("[RenameDialog] Fix layout when using the NoRename flag");?></li>
<li><?php i18n("Add missing @since for KFilePlacesModel::TagsType");?></li>
<li><?php i18n("[KDirOperator] Use the new <code>view-sort</code> icon for the sort order chooser");?></li>
<li><?php i18n("Use HTTPS for all search providers that support it");?></li>
<li><?php i18n("Disable unmount option for / or /home (bug 399659)");?></li>
<li><?php i18n("[KFilePlaceEditDialog] Fix include guard");?></li>
<li><?php i18n("[Places panel] Use new <code>folder-root</code> icon for Root item");?></li>
<li><?php i18n("[KSambaShare] Make \"net usershare info\" parser testable");?></li>
<li><?php i18n("Give the file dialogs a \"Sort by\" menu button on the toolbar");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("DelegateRecycler: Don't create a new propertiesTracker for every delegate");?></li>
<li><?php i18n("Move the about page from Discover to Kirigami");?></li>
<li><?php i18n("Hide context drawer when there is a global toolbar");?></li>
<li><?php i18n("ensure all items are laid out (bug 400671)");?></li>
<li><?php i18n("change index on pressed, not on clicked (bug 400518)");?></li>
<li><?php i18n("new text sizes for Headings");?></li>
<li><?php i18n("sidebar drawers don't move global headers/footers");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Add programmaticaly useful error signalling");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Rename NotifyByFlatpak to NotifyByPortal");?></li>
<li><?php i18n("Notification portal: support pixmaps in notifications");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Don't generate appstream data for files that lack a description (bug 400431)");?></li>
<li><?php i18n("Capture package metadata before install start");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("When re-using runners when reloading, reload their configuration (bug 399621)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Allow negative syntax definition priorities");?></li>
<li><?php i18n("Expose \"Toggle Comment\" feature through tools menu and default shortcut (bug 387654)");?></li>
<li><?php i18n("Fix hidden languages in the mode menu");?></li>
<li><?php i18n("SpellCheckBar: Use DictionaryComboBox instead of plain QComboBox");?></li>
<li><?php i18n("KTextEditor::ViewPrivate: Avoid warning \"Text requested for invalid range\"");?></li>
<li><?php i18n("Android: No need to define log2 anymore");?></li>
<li><?php i18n("disconnect contextmenu from all aboutToXXContextMenu receivers (bug 401069)");?></li>
<li><?php i18n("Introduce AbstractAnnotationItemDelegate for more control by consumer");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Updated with petroleum industry units (bug 388074)");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Autogenerate logging file + fix categories file");?></li>
<li><?php i18n("Add VirtualDesktops to PlasmaWindowModel");?></li>
<li><?php i18n("Update PlasmaWindowModel test to reflect VirtualDesktop changes");?></li>
<li><?php i18n("Cleanup windowInterface in tests before windowManagement is destroyed");?></li>
<li><?php i18n("Delete the correct item in removeDesktop");?></li>
<li><?php i18n("Cleanup Virtual Desktop Manager list entry in PlasmaVirtualDesktop destructor");?></li>
<li><?php i18n("Correct version of newly added PlasmaVirtualDesktop interface");?></li>
<li><?php i18n("[server] Text input content hint and purpose per protocol version");?></li>
<li><?php i18n("[server] Put text-input (de-)activate, en-/disable callbacks in child classes");?></li>
<li><?php i18n("[server] Put set surrounding text callback with uint in v0 class");?></li>
<li><?php i18n("[server] Put some text-input v0 exclusive callbacks in v0 class");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Add level api from Kirigami.Heading");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Update the \"About KDE\" text");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Fixed a bug(error?) in ipv4 &amp; ipv6 settings");?></li>
<li><?php i18n("Add ovs-bridge and ovs-interface setting");?></li>
<li><?php i18n("Update Ip-tunnel settings");?></li>
<li><?php i18n("Add proxy and user setting");?></li>
<li><?php i18n("Add IpTunnel setting");?></li>
<li><?php i18n("We can now build tun setting test all the time");?></li>
<li><?php i18n("Add missing IPv6 options");?></li>
<li><?php i18n("Listen for added DBus interfaces instead of registered services (bug 400359)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("feature parity of Menu with the Desktop style");?></li>
<li><?php i18n("Qt 5.9 is now the minimum required version");?></li>
<li><?php i18n("Add back (accidentally?) deleted line in CMakeLists.txt");?></li>
<li><?php i18n("100% consistency with kirigami heading sizing");?></li>
<li><?php i18n("more homogeneous look with Kirigami headings");?></li>
<li><?php i18n("install the processed version of private imports");?></li>
<li><?php i18n("Mobile text selection controls");?></li>
<li><?php i18n("Update breeze-light and breeze-dark colorschemes");?></li>
<li><?php i18n("Fixed a number of memory leaks (thanks to ASAN)");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("phabricator plugin: use Arcanist's diff.rev. order (bug 401565)");?></li>
<li><?php i18n("Provide a title for JobDialog");?></li>
<li><?php i18n("Allow the JobDialog to get a nicer initial size (bug 400873)");?></li>
<li><?php i18n("Make it possible for the menudemo to share different urls");?></li>
<li><?php i18n("Use QQC2 for the JobDialog (bug 400997)");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("consistent sizing of item compared to QWidgets");?></li>
<li><?php i18n("fix Menu sizing");?></li>
<li><?php i18n("make sure flickables are pixelAligned");?></li>
<li><?php i18n("Support for QGuiApplication-based apps (bug 396287)");?></li>
<li><?php i18n("touchscreen text controls");?></li>
<li><?php i18n("Size according to specified icon width and height");?></li>
<li><?php i18n("Honor flat property of buttons");?></li>
<li><?php i18n("Fix issue where there's only one element on the menu (bug 400517)");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Fix root disk icon change so that it doesn't erroneously change other icons");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("DictionaryComboBoxTest: Add stretch to avoid expanding Dump button");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("BrightScript: Allow sub to be unnamed");?></li>
<li><?php i18n("Add highlighting file for Wayland Debug Traces");?></li>
<li><?php i18n("Add syntax highlighting for TypeScript &amp; TypeScript React");?></li>
<li><?php i18n("Rust &amp; Yacc/Bison: improve comments");?></li>
<li><?php i18n("Prolog &amp; Lua: fix shebang");?></li>
<li><?php i18n("Fix language load after including keywords from this language in another file");?></li>
<li><?php i18n("Add BrightScript syntax");?></li>
<li><?php i18n("debchangelog: add Disco Dingo");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.53");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.9");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
