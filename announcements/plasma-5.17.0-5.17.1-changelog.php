<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.17.1 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.17.1";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Revert change to make the notification persistent. <a href='https://commits.kde.org/discover/d9db05ddef96346e8528bd7e687b3512016998b2'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24766'>D24766</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[Weather] Make update time spinbox editable. <a href='https://commits.kde.org/kdeplasma-addons/ec2392dbf70aa577b36fd4c15a752aaf60a88634'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24815'>D24815</a></li>
<li>Fix NOAA picture of the day provider. <a href='https://commits.kde.org/kdeplasma-addons/b2d041fb4a276d006b0062ab5088765d171b7c91'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408580'>#408580</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24727'>D24727</a></li>
</ul>


<h3><a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>[KCM] Try harder to display all text in the visualizations. <a href='https://commits.kde.org/kscreen/3c406779cca9717717bd432e459697f5ca47d242'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24834'>D24834</a></li>
<li>[KCM] Revert change to reduce size of screens in visualization. <a href='https://commits.kde.org/kscreen/318c16f3c604461f76d01d0f0691e3f5390fcbe5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413142'>#413142</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24833'>D24833</a></li>
<li>Fix laptop screen being off when opening the lid. <a href='https://commits.kde.org/kscreen/e47dc21a7b7824889171f01bdc4b0209c8b1fed6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371447'>#371447</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24719'>D24719</a></li>
<li>Add missing check for outputModel. <a href='https://commits.kde.org/kscreen/987f8210337de0307221a6cdacde3f222fef7b6e'>Commit.</a> </li>
<li>Gracefully replace outputModel. <a href='https://commits.kde.org/kscreen/2ab4c2f136c5d7c00cd08e99f43670b55a2a6a58'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24735'>D24735</a></li>
<li>[KCM] Add output selector ComboBox. <a href='https://commits.kde.org/kscreen/7811411c6425dd52bcd732a5910d177fee4a89d2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413014'>#413014</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24687'>D24687</a></li>
<li>[kcm] Don't transform button to show rotated icon. <a href='https://commits.kde.org/kscreen/ae555055b2ea8e41ebd8056c2b69451b893915da'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412092'>#412092</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24488'>D24488</a></li>
<li>[KCM] Show the same name in the replication model that's shown in the title. <a href='https://commits.kde.org/kscreen/ba20c56a724f739bd6ac04fff3ee5ad03946be3a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24704'>D24704</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Fix non-atomic output init. <a href='https://commits.kde.org/kwin/0d67a0b48a500b615ff4af7456b1461a38090e48'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412684'>#412684</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24829'>D24829</a></li>
<li>[aurorae] Fix visibility of Context Help button. <a href='https://commits.kde.org/kwin/982fefd38974667d818b0b91698db7defe62483a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413145'>#413145</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24851'>D24851</a></li>
<li>[effects/startupfeedback] Scale application icon size with cursor size. <a href='https://commits.kde.org/kwin/2497f901d7125d4847ab2be66440677ab443d1c0'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24714'>D24714</a></li>
<li>Glx: Don't use sRGB configs on llvmpipe with depth 16. <a href='https://commits.kde.org/kwin/4982dfd5f5ec408a19de48a1ada98f91497db48d'>Commit.</a> See bug <a href='https://bugs.kde.org/408594'>#408594</a>. Phabricator Code review <a href='https://phabricator.kde.org/D22203'>D22203</a></li>
<li>[aurorae] Fix crash on KCM teardown with Qt5.14. <a href='https://commits.kde.org/kwin/446e23af5a5fc1ff432b2435fd24da9bff537331'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24594'>D24594</a></li>
<li>[tabbox] Correctly set global short on kwin tabbox. <a href='https://commits.kde.org/kwin/5d4be83de0c4866ac5e1135da15fefe003597fa6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407000'>#407000</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24647'>D24647</a></li>
</ul>


<h3><a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Fix build with older glibc: link there to librt for clock_gettime. <a href='https://commits.kde.org/libksysguard/3cd40fede73e58cfdad4ac494491d425f38c5e41'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24749'>D24749</a></li>
</ul>


<h3><a name='plasma-browser-integration' href='https://commits.kde.org/plasma-browser-integration'>plasma-browser-integration</a> </h3>
<ul id='ulplasma-browser-integration' style='display: block'>
<li>Bail out early when setPluginLoaded would result in no change. <a href='https://commits.kde.org/plasma-browser-integration/70fe227edb7e56004521495f255a26c3a92d2754'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24693'>D24693</a></li>
<li>Show version information in about screen. <a href='https://commits.kde.org/plasma-browser-integration/4b75dd4e2be0909b8825f280e9bea6c153e9379b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24692'>D24692</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[Mouse KCM] Fix acceleration profile on X11. <a href='https://commits.kde.org/plasma-desktop/829501dd777966091ddcf94e5c5247aaa78ac832'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398713'>#398713</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24711'>D24711</a></li>
<li>[Desktop Toolbox] Add missing i18n domain. <a href='https://commits.kde.org/plasma-desktop/c79902431d9abd4ebb7c34c8350a18955367fe2f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413227'>#413227</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24854'>D24854</a></li>
<li>[KCMs] Make spinboxes editable. <a href='https://commits.kde.org/plasma-desktop/4f90a137e7a3a0e7b0c53bd1811934da49f59bd5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24814'>D24814</a></li>
<li>[applets/taskmanager] Revert "forceStripes" checkbox string change. <a href='https://commits.kde.org/plasma-desktop/6b4295b313af62b745cadbe62d8a91947baed386'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412801'>#412801</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24534'>D24534</a></li>
<li>[Style KCM] Don't explicitly reload KWin's configuration. <a href='https://commits.kde.org/plasma-desktop/58fe6dabbb0f76c60e5370ebc85b9bc890298798'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24260'>D24260</a></li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Wired connection: default to Full duplex when duplex is not set. <a href='https://commits.kde.org/plasma-nm/14f7ab5e3c686f85de57c8845bedab54ca7cb71a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413211'>#413211</a></li>
</ul>


<h3><a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a> </h3>
<ul id='ulplasma-pa' style='display: block'>
<li>[Applet] Make spinboxes editable. <a href='https://commits.kde.org/plasma-pa/e1a2940de81412c1f81831e7e1db3a4c5b3c5e51'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24816'>D24816</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[Windows Runner] Gather window again when running result. <a href='https://commits.kde.org/plasma-workspace/549374ba5f1e2b611018c6cfc4a48104a5a8e72c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/412386'>#412386</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24849'>D24849</a></li>
<li>[startplasma] Pass actual variable to kapplymousetheme. <a href='https://commits.kde.org/plasma-workspace/b59af69b652123aad540d33218653c9e87f31ec3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24823'>D24823</a></li>
<li>Fix slideshow crashing in invalidate(). <a href='https://commits.kde.org/plasma-workspace/a1cf305ffb21b8ae8bbaf4d6ce03bbaa94cff405'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413018'>#413018</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24723'>D24723</a></li>
<li>Fix reading environment variables with newline. <a href='https://commits.kde.org/plasma-workspace/595ab88bc2ee0ed32da577ec8ae22efee90865b5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413130'>#413130</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24750'>D24750</a></li>
<li>[Media Controller] Multiple artists support. <a href='https://commits.kde.org/plasma-workspace/1be4bb880fdeea93381eb45846a7d487e58beb93'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/405762'>#405762</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24740'>D24740</a></li>
<li>[gmenu-dbusmenu-proxy] Don't create ~/.gtkrc-2.0. <a href='https://commits.kde.org/plasma-workspace/ff84a4e5662ee7058073af34ead9645d63562bb1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24664'>D24664</a></li>
<li>Fixed erroneously entering do not disturb mode when there are overlapping *disabled* screens. <a href='https://commits.kde.org/plasma-workspace/013e7ae0df5da47445acc13fd842d06b83e6b139'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24679'>D24679</a></li>
<li>[Notifications] Don't bind model inside headerItem. <a href='https://commits.kde.org/plasma-workspace/e0fbab47ed66f96fc0996d6b1f4b1e1b578fd0a7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24654'>D24654</a></li>
<li>[XembedSNIProxy] Do not crash on null pointer. <a href='https://commits.kde.org/plasma-workspace/741441765601c00cb84ecb7fa7b38e69d185f51a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/409652'>#409652</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24514'>D24514</a></li>
<li>[XembedSNIProxy] Scale only big icons. <a href='https://commits.kde.org/plasma-workspace/13efbfca67f8270458d103e128ba76525f663329'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/366047'>#366047</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24531'>D24531</a></li>
<li>[XembedSNIProxy] Check size in each update(). <a href='https://commits.kde.org/plasma-workspace/acf91005b60c82f8381fb119f327f4d443c5b98d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358240'>#358240</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24529'>D24529</a></li>
</ul>


<h3><a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Set preview to correct device size. <a href='https://commits.kde.org/sddm-kcm/9e0f846727488cf4aff7fb45a5463a92bb1f5cd2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407689'>#407689</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24591'>D24591</a></li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Filter invalid services in most used model. <a href='https://commits.kde.org/systemsettings/ec90e4066c16eae9239508bd623203bf0b5e3998'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413178'>#413178</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24818'>D24818</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
