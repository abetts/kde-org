<?php
  include_once ("functions.inc");
  $version = htmlentities($_GET["version"]);
  require('../aether/config.php');

  $pageConfig = array_merge($pageConfig, [
      'title' => "Release service ".$version." Full Log Page",
      'cssFile' => '/css/announce.css'
  ]);

  require('../aether/header.php');
  $site_root = "../";
?>

<main class="releaseAnnouncment container">

<h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Release service %1", $version)?></h1>

<p>
<?php print "This is the automated full changelog for the release service ".$version."</a> from the Git repositories." ?>
</p>
<p>
Click on [Show] to show the commits for a given repository
</p>

<?php
include "fulllog_releases-$version.inc"
?>

</main>
<?php
  require('../aether/footer.php');
