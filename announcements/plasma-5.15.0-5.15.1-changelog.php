<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.15.1 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.15.1";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>ApplicationPage: Fix crash when the sources model gets refreshed. <a href='https://commits.kde.org/discover/f5e10f7676fa1d4663fa8343d0ad6305ad83c2de'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404402'>#404402</a></li>
<li>Fwupd: warn if threads could not be killed. <a href='https://commits.kde.org/discover/1a470696aa474e956f7070d856aa2342ae69ac19'>Commit.</a> </li>
<li>UpdatesPage: Fix icon sizing on small screens. <a href='https://commits.kde.org/discover/502b963bcc96e5e9abb257e2cc90d3fea2212996'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404070'>#404070</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18964'>D18964</a></li>
<li>Set parent on newly created fwupd resource. <a href='https://commits.kde.org/discover/baac08a40851699585e80b0a226c4fd683579a7b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402328'>#402328</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18946'>D18946</a></li>
<li>Don't show tooltip when search field has text and set a timeout period. <a href='https://commits.kde.org/discover/c62901d8aed637eca592c10f08fd322e89560edc'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18766'>D18766</a></li>
<li>Add a timeout for KNS backends. <a href='https://commits.kde.org/discover/003fc9fc980003ec0a4d13a020a40c5cf903c411'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399981'>#399981</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18724'>D18724</a></li>
<li>Don't enable the install button in the app page until we know the state. <a href='https://commits.kde.org/discover/6647d5d0084a0d19a1dc84695071bc0db982691a'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[weather] Center align the warning heading. <a href='https://commits.kde.org/kdeplasma-addons/fba8a13fee6b3480f0191ea9bd57e0cb3f66d73a'>Commit.</a> </li>
<li>[weather] Fix weather Notices tab not showing. <a href='https://commits.kde.org/kdeplasma-addons/46204a5e2a84b184d41d80ff469a5797a0dbf16e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18936'>D18936</a></li>
<li>[comic dataengine] Do not cache identifier only requests. <a href='https://commits.kde.org/kdeplasma-addons/37d5fc1933c7fa56545023f9b593fbc9744673a8'>Commit.</a> </li>
<li>[comic] Update comic strip on current identifier only. <a href='https://commits.kde.org/kdeplasma-addons/1e12a9df8a7143a7c226999ad4b7b3badb0389a2'>Commit.</a> </li>
<li>[comic] Fix max cache limit. <a href='https://commits.kde.org/kdeplasma-addons/e909e66b30086451b187ec3b70e7c1a5c374ee23'>Commit.</a> </li>
<li>[comic] Request current identifier on data change. <a href='https://commits.kde.org/kdeplasma-addons/fd19c36ec4e48e05780b3e94b55f91936cd6bcb5'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16873'>D16873</a></li>
<li>[comicupdater] Fix last day check. <a href='https://commits.kde.org/kdeplasma-addons/28493251921b30f89e140b50d6c7cd9eca6540cd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16856'>D16856</a></li>
</ul>


<h3><a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a> </h3>
<ul id='ulkhotkeys' style='display: block'>
<li>Harmonize usage of HAVE_X11, using #if and #cmakedefine01. <a href='https://commits.kde.org/khotkeys/ae5743738245437332b1bb670b9a3f8c429211e2'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>[effects/desktopgrid] Don't copy windows on X11. <a href='https://commits.kde.org/kwin/55d3f2933f3735958dfa0f99e6d28e02cd083576'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404442'>#404442</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19116'>D19116</a></li>
<li>Enable -DQT_NO_URL_CAST_FROM_STRING and fix compilation. <a href='https://commits.kde.org/kwin/48971e084853a5e13ba7193aa6d8ce986477748a'>Commit.</a> </li>
<li>Make the new Window Decoration KCM translatable. <a href='https://commits.kde.org/kwin/0cf7cca2ab6a26db35251706124b6a903a32d2b6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18931'>D18931</a></li>
<li>Copy layer repaints to Deleted. <a href='https://commits.kde.org/kwin/381d0df7f9610d2d314efb37f967b6d08daf4b0f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18786'>D18786</a></li>
</ul>


<h3><a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Exclude kscreen backend launcher from session management. <a href='https://commits.kde.org/libkscreen/ee3f33ba4925edfe87dac50e27b731fd93b5f4e3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19025'>D19025</a></li>
</ul>


<h3><a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>Add some returns to make gcc happy. <a href='https://commits.kde.org/libksysguard/9919edc49032aa24c96be616121b1e7eeebf6939'>Commit.</a> </li>
<li>Add missing break in ProcessesLocal::Private::getNiceness. <a href='https://commits.kde.org/libksysguard/a855c1a266b02ae5c8926556ecd3aadafdc29e14'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[Kickoff] Return Kickoff to Favorites page after running a search. <a href='https://commits.kde.org/plasma-desktop/cc33c78ff5f773b9a0549fdb98dbb3c2aa6cd79b'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18848'>D18848</a></li>
<li>Use Dialog's visibility directly rather than relying on the mainItem's. <a href='https://commits.kde.org/plasma-desktop/67d8ac416ff6933b082b84ec6f30c2c4e90a0500'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18844'>D18844</a></li>
<li>[Containment Actions Configuration] Fix changing plugin. <a href='https://commits.kde.org/plasma-desktop/42fb1de1b3fae6b121ef9c1651728a258f4e17ce'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18840'>D18840</a></li>
<li>Sort setting grid views alphabetically. <a href='https://commits.kde.org/plasma-desktop/6835000906d3dd48486d80857b420a96b03e1e61'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400404'>#400404</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18815'>D18815</a></li>
<li>[Activities KCM] Properly vertically center the buttons. <a href='https://commits.kde.org/plasma-desktop/a8da90e219454a95888fb7f474aff30c134ea40e'>Commit.</a> </li>
<li>Revert "[Activities KCM] vertically center the buttons". <a href='https://commits.kde.org/plasma-desktop/019467e53bb95a679b6c5775e91bced2b2ba8a53'>Commit.</a> </li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Autotests: remove KDE_FORK_SLAVES, to see if CI works better. <a href='https://commits.kde.org/plasma-integration/112e21bd45da50770539584dbcfeed071678ae8d'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[ksmserver] Restore legacy sessions. <a href='https://commits.kde.org/plasma-workspace/b4ddae94793644e51c503a2f5733b09c5586a5a8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404318'>#404318</a>. Phabricator Code review <a href='https://phabricator.kde.org/D19028'>D19028</a></li>
<li>Exclude kuiserver from session management. <a href='https://commits.kde.org/plasma-workspace/9d7d80134ea61a303e7820f811ad1422994e5383'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19026'>D19026</a></li>
<li>[Digital Clock] Fix 24h tri-state button broken in port to QQC2. <a href='https://commits.kde.org/plasma-workspace/006c4f5f9ee8dfb3d95604a706d01b968c1e1c8a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404292'>#404292</a></li>
<li>Harmonize usage of HAVE_X11, using #if and #cmakedefine01 everywhere. <a href='https://commits.kde.org/plasma-workspace/c534fdf1ba34d0ea8a08b9b5266384a3243271e0'>Commit.</a> </li>
<li>[weather dataengine] bbc,envcan,noaa: fix day/night calculation for observe. <a href='https://commits.kde.org/plasma-workspace/03e13b10d877733528d75f20a3f1d706088f7b9b'>Commit.</a> </li>
<li>[weather dataengine] envcan: fix forecast icons to match "ice pellets". <a href='https://commits.kde.org/plasma-workspace/28d0af6791a91eeba7c76721926d18d88bf0b99f'>Commit.</a> </li>
<li>Fix System Tray popup interactivity after echanging item visiblity. <a href='https://commits.kde.org/plasma-workspace/6fcf9a5e03ba573fd0bfe30125f4c739b196a989'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393630'>#393630</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18805'>D18805</a></li>
</ul>


<h3><a name='powerdevil' href='https://commits.kde.org/powerdevil'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Exclude powerdevil from session management. <a href='https://commits.kde.org/powerdevil/b8a44a4cc87aa68a18231944bae9f08d5c9ac693'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19024'>D19024</a></li>
</ul>


</main>
<?php
	require('../aether/footer.php');
