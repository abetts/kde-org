<?php
  $page_title = "Anunci d'alliberament del KDE 4.1";
  $site_root = "../";
  include "header.inc";
?>

També disponible en:
<?php
  $release = '4.1';
  include "../announce-i18n-bar.inc";
?>

<!-- // Boilerplate -->

<h3 align="center">
La comunitat del KDE anuncia l'alliberament del 4.1.0
</h3>

<p align="justify">
  <strong>
    El KDE allibera un escriptori i aplicacions millorades, i el dedica a la memòria de Uwe Thiem
  </strong>
</p>

<p align="justify">
29 de Juliol de 2008.
La <a href="http://www.kde.org/">Comunitat KDE</a> ha alliberat avui la versió 4.1.0 del KDE. Aquest alliberament és el segon de la sèrie KDE4 en què s'alliberen noves característiques, i que incorpora noves aplicacions i noves capacitats desenvolupades damunt dels pilars de KDE4. El KDE 4.1 és la primera versió del KDE4 que incorpora el KDE-PIM, el paquet de Tractament d'Informació Personal, amb el client de correu electrònic KMail, el planificador KOrganizer, el lector de d'alimentacions RSS Akregator, el lector de grups de notícies KNode i molts altres components integrats dins l'embolcall del Kontact. Més encara, la nova tecnologia Plasma per a l'escriptori, introduïda en el KDE 4.0, ha madurat fins al punt que pot reemplaçar la tecnologia existent al KDE3 per a la majoria d'usuaris. Igual que en la nostra anterior versió, enguany s'ha dedicat molt de temps a millorar els bastiments i les llibreries de base sobre les quals està construït el KDE.
<br />
Dirk M&uuml;ller, un dels directors d'alliberaments, ens dóna algunes xifres: <em>"S'han fet 20803 'commits' entre el KDE 4.0 i el KDE 4.1, juntament amb 15432 lliuraments de traduccions. Així mateix s'han fet gairebé 35000 'commits' a les branques de treball, alguns dels quals s'han incorporat també al KDE 4.1, en conseqüència no s'han comptabilitzat."</em>
MM&uuml;ller també ens comenta que l'equip d'administradors de sistemes del KDE ha creat 166 nous comptes per a desenvolupadors al servidor SVN de KDE.

<?php
    screenshot("desktop_thumb.png", "desktop.png", "center",
"L'escriptori KDE 4.1", "405");
?>

</p>
<p>
<strong>Les millores clau al KDE 4.1 són:</strong>
<ul>
    <li>El paquet KDE-PIM s'ha reintroduït</li>
    <li>La tecnologia Plasma madura</li>
    <li>Moltes altres noves o millorades aplicacions i bastiments</li>
</ul>

</p>


<h3>
  <a name="changes">In memoriam: Uwe Thiem</a>
</h3>
<p align="justify">
La comunitat del KDE dedica aquesta versió a Uwe Thiem, un contribuïdor al KDE durant molt de temps i que ha traspassat recentment a causa d'una sobtada insuficiència renal. La mort d'Uwe ha estat totalment inesperada i un xoc per als seus companys. L'Uwe ha estat contribuint al KDE literalment fins els darrers dies de la seva vida, i no només programant. L'Uwe també ha jugat un paper important tot educant sobre programari lliure els usuaris de l'Àfrica. Amb la sobtada mort de l'Uwe, el KDE perd una part incalculable de la seva comunitat, i també un amic. Compartim els nostres pensaments amb la seva família i tots aquells a qui ell deixa enrere.
</p>


<h3>
  <a name="changes">Passat, present i futur</a>
</h3>
<p align="justify">
Encara que el KDE 4.1 pretén ser la primera versió apta per usuaris precoços, algunes de les característiques de les que esteu acostumats a gaudir en el KDE 3.5 encara no s'han implementat. L'equip del KDE està treballant-hi i s'esforça per tal que estiguin disponibles en les properes versions. Encara que no es garanteix que s'implementarà cada una de les característiques del KDE 3.5, el KDE 4.1 ja ofereix un entorn de treball potent i farcit de capacitats.<br />
Pareu esment en que algunes opcions de la interfície amb l'usuari s'han mogut a llocs dins del context de les dades que manipulen, per la qual cosa assegureu-vos d'haver-ho mirat bé abans d'informar sobre qualsevol cosa desapareguda en combat.<p />
El KDE 4.1 és un grandíssim pas endavant en la sèrie del KDE4 i esperem que marqui la pauta del desenvolupament futur. El KDE 4.2 s'espera per al gener de 2009.
</p>


<?php
    screenshot("kdepim-screenie_thumb.png", "kdepim-screenie.png", "center",
"KDE PIM ha tornat", "305");
?>

<h3>
  <a name="changes">Millores</a>
</h3>
<p align="justify">
Alhora que en el KDE 4.1 s'han estabilitzat els nous bastiments, també s'han emfasitzat les parts visibles a l'usuari final. Seguiu llegint per a conèixer una llista de millores que aporta el KDE 4.1. Podeu trobar una informació més completa a la pàgina
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Release_Goals">Objectius de la versió KDE 4.1</a>
 i en el document més detallat
<a href="http://techbase.kde.org/Schedules/KDE4/4.1_Feature_Plan">Pla de característiques de la versió 4.1</a>.
</p>

<h4>
  Per als usuaris
</h4>
<p align="justify">

<ul>
    <li>
        El <strong>KDE-PIM</strong> retorna amb la 4.1, aportant les aplicacions necessàries per a la vostra comunicació i informació personal. El KMail com a client de correu, el KOrganizer com a planificador de tasques, l'Akregator com a lector d'alimentacions RSS i d'altres ara tornen a estar disponibles, amb l'aparença del KDE 4.
    </li>
    <li>
        Entra en escena el <strong>Dragon Player</strong>, un reproductor de vídeo d'ús senzill.
    </li>
    <li>
        L'<strong>Okteta</strong> és el nou editor hexadecimal ben integrat i molt capaç.
    </li>
    <li>
        L'emulador de física <strong>Step</strong> fa que l'aprenentatge de la física sigui fàcil i divertit.
    </li>
    <li>
        El <strong>KSystemLog</strong>, us ajuda a seguir què està passant al vostre sistema.
    </li>
    <li>
        Els <strong>nous jocs</strong> com el KDiamond (un clon del bejeweled), Kollision, KBreakOut
        i Kubrick fan irresistibles les vostres pauses en el treball.
    </li>
    <li>
        El <strong>Lokalize</strong>, ajuda els traductors a fer el KDE4 disponible en la vostra llengua (si és que aquesta no es troba entre els 50 idiomes que ja suporta el KDE4).
    </li>
    <li>
        S'ha ressuscitat el <strong>KSCD</strong>, el vostre reproductor de CD per a l'escriptori.
    </li>
</ul>

A les <a href="http://www.kde.org/users/faq.php">PMF de l'usuari final de KDE4</a> s'han recollit les respostes a preguntes freqüents, per la qual cosa també paga la pena llegir-les si voleu aprendre més sobre el KDE4.
</p>

<p align="justify">
<?php
    screenshot("dolphin-screenie_thumb.png", "dolphin-screenie.png", "center",
"El nou mecanisme de selecció del Dolphin", "483");
?>

<ul>
    <li>
        El <strong>Dolphin</strong>, el nou gestor de fitxers del KDE, té una nova vista d'arbre a la vista principal, així com també és nou el suport de pestanyes. Un nou i innovador mètode de selecció d'un sol clic permet una experiència d'usuari més consistent, i les accions contextuals de copia-a i mou-a fan aquestes accions més fàcilment accessibles. Evidentment el Konqueror també està disponible com a alternativa al Dolphin, i també es beneficia de la majoria de característiques esmentades. [<a href="#screenshots-dolphin">Captures de pantalla del Dolphin</a>]
    </li>
    <li>
        el <strong>Konqueror</strong>, el navegador de KDE, ara suporta la reobertura de finestres i pestanyes ja tancades, i també es desplaça més suaument a través de les pàgines web.
    </li>
    <li>
        El <strong>Gwenview</strong>, el visualitzador d'imatges de KDE, incorpora un nou mode de visualització a pantalla completa, una barra de miniatures per accedir fàcilment a d'altres fotos, un sistema de desfer intel·ligent, i el suport per a puntuar les imatges. [<a href="#screenshots-gwenview">Captures de pantalla del Gwenview</a>]
    </li>
    <li>
        El <strong>KRDC</strong>, el client d'escriptori remot de KDE, ara detecta escriptoris remots dins la xarxa local usant el protocol ZeroConf.
    </li>

    <li>
        El <strong>Marble</strong>, el globus terraqüi per a l'escriptori de KDE, ara s'integra amb l'<a
        href="http://www.openstreetmap.org/">OpenStreetMap</a>, amb la qual cosa podeu trobar el vostre itinerari arreu usant els mapes lliures. [<a href="#screenshots-marble">Captures de pantalla del Marble</a>]
    </li>
    <li>
        El <strong>KSysGuard</strong>, ara suporta el monitoratge de la sortida d'un procés o l'execució d'aplicacions, per la qual cosa ja no cal reiniciar les vostres aplicacions des d'un terminal quan voleu saber què esta passant.
    </li>
    <li>
        Les capacitats de gestió de finestres amb composició de <strong>KWin</strong> s'han estabilitzat més i s'han ampliat. S'han afegit nous efectes com ara el commutador de finestres Coverswitch i el famós "finestres gelatinoses". [<a href="#screenshots-kwin">Captures de pantalla del KWin</a>]
    </li>
    <li>
        El panell de configuració del <strong>Plasma</strong> s'ha ampliat. El nou panell ofereix una retroalimentació visual directa que simplifica la personalització. També podeu afegir panells i ubicar-los a diferentes vores de la pantalla. La nova miniaplicació "vista de carpeta" us permet emmagatzemar fitxers al vostre escriptori (de fet ofereix una vista d'un directori del vostre sistema). Podeu posar tantes vistes de carpeta com vulgueu a l'escriptori, cosa que permet un accés fàcil i flexible als fitxers amb què treballeu.
        [<a href="#screenshots-plasma">Captures de pantalla del Plasma</a>]
    </li>
</ul>
</p>


<h4>
  Per a desenvolupadors
</h4>
<p align="justify">

<ul>
    <li>
        El bastiment d'emmagatzematge de l'<strong>Akonadi</strong> ofereix una forma eficient d'emmagatzemar i recuperar dades de contacte i correu electrònic entre aplicacions. L'<a href="http://en.wikipedia.org/wiki/Akonadi">Akonadi</a> suporta les cerques a través de les dades i notifica dels canvis les aplicacions que l'utilitzen.
    </li>
    <li>
        Les aplicacions de KDE es poden escriure usant Python i Ruby. Aquests <strong>vincles de llenguatge</strong> es
        <a href="http://techbase.kde.org/Development/Languages">consideren</a> estables, madurs i indicats per als desenvolupadors d'aplicacions.
    </li>
    <li>
        La llibreria <strong>Libksane</strong> permet un accés senzill a les aplicacions d'escaneig d'imatges, com ara la nova aplicació d'escanejat Skanlite.
    </li>
    <li>
        Un sistema d'<strong>emoticons</strong> compartides que és utilitzat pel KMail i el Kopete.
    </li>
    <li>
        Els nous dorsals multimèdia de <strong>Phonon</strong> per a GStreamer, QuickTime i DirectShow9,
        milloren el suport multimèdia de KDE sobre Windows i Mac OS.
    </li>

</ul>
</p>

<h3>
  Noves plataformes
</h3>
<p align="justify">
<ul>
    <li>
        El suport d'<a href="http://techbase.kde.org/Projects/KDE_on_Solaris"><strong>OpenSolaris</strong></a> en el KDE s'està enfortint. EL KDE bàsicament funciona sobre OSOL, tot i que encara queden alguns errors que impedeixen avançar.
    </li>
    <li>
        Els desenvolupadors de <strong>Windows</strong> poden <a href="http://windows.kde.org">descarregar</a>
        preestrenes de les aplicacions KDE per a la seva plataforma. Les llibreries ja són relativament estables, encara que no totes les capacitats de kdelibs estan disponibles encara sobre windows. Algunes aplicacions ja corren força bé en windows, però d'altres potser no.
    </li>
    <li>
        El <strong>Mac OSX</strong> és una altra plataforma on el KDE s'està introduint.
        <a href="http://mac.kde.org">KDE en el Mac</a> no està a punt encara per a un ús en producció. Mentre que el suport multimèdia a través de Phonon ja està disponible, la integració del maquinari i les cerques encara no estan acabades.
    </li>
</ul>
</p>


<a name="screenshots-dolphin"></a>
<h3>
  Captures de pantalla
</h3>
<p align="justify">

<a name="screenshots-dolphin"></a>
<h4>Dolphin</h4>
<?php

    screenshot("dolphin-treeview_thumb.png", "dolphin-treeview.png", "center",
               "La nova vista en arbre del Dolphin us dona un accés més ràpid als directoris.
               Noteu que està deshabilitada en la configuració predeterminada.", "448");

    screenshot("dolphin-tagging_thumb.png", "dolphin-tagging.png", "center",
               "Nepomuk proporciona etiquetat i valoració al KDE -- per tant, també al Dolphin.", "337");

    screenshot("dolphin-icons_thumb.png", "dolphin-icons.png", "center",
               "Previsualització a les icones i barres d'informació proporcionan ajut visual.", "390");

    screenshot("dolphin-filterbar_thumb.png", "dolphin-filterbar.png", "center",
               "Trobeu els vostres fitxers més fàcilment amb la barra de filtrat.", "372");
?>

<a name="screenshots-gwenview"></a>
<h4>Gwenview</h4>
<?php

    screenshot("gwenview-browse_thumb.png", "gwenview-browse.png", "center",
               "Podeu navegar directoris amb imatges amb el Gwenview.
                Les accions flotants posen les tasques més usuals als vostres dits.", "440");

    screenshot("gwenview-open_thumb.png", "gwenview-open.png", "center",
               "Obrir fitxers del vostre disc o de la xarxa és igual de fàcil gràcies
               a la infrastructura del KDE.", "439");

    screenshot("gwenview-thumbnailbar_thumb.png", "gwenview-thumbnailbar.png", "center",
               "La nova barra de miniatures us permet canviar entre imatges fàcilment.
                També està disponible en el mode de pantall completa.", "684");

    screenshot("gwenview-sidebar_thumb.png", "gwenview-sidebar.png", "center",
               "La barra lateral del Gwenview dóna accés a informació adicional
               i opcions de manipulació de les imatges.", "462");

?>

<a name="screenshots-marble"></a>
<h4>Marble</h4>

<?php

    screenshot("marble-globe_thumb.png", "marble-globe.png", "center",
               "El globus d'escriptori Marble.", "427");

    screenshot("marble-osm_thumb.png", "marble-osm.png", "center",
               "La integració de Marble amb OpenStreetMap també dóna
                informació en quant el transport públic.", "425");


?>
<a name="screenshots-kwin"></a>
<h4>KWin</h4>

<?php

    screenshot("kwin-desktopgrid_thumb.png", "kwin-desktopgrid.png", "center",
               "La graella d'escriptori de KWin visualitza el concepte d'escriptori virtual i
                fa més fàcil recordar on és la finestra que busqueu.", "405");

    screenshot("kwin-coverswitch_thumb.png", "kwin-coverswitch.png", "center",
               "El canviador de caratules fa el canvi d'aplicacions amb Alt+Tab més atractiu.
               Podeu escollir-lo a la configuració dels efectes d'escriptori del KWin.", "405");

    screenshot("kwin-wobbly_thumb.png", "kwin-wobbly.png", "center",
               "KWin també pot fer tremolar les finestres (desactivat inicialment).", "405");


?>

<a name="screenshots-plasma"></a>
<h4>Plasma</h4>
<?php

    screenshot("plasma-folderview_thumb.png", "plasma-folderview.png", "center",
               "El nou applet de visió de carpetes permet mostrar el contigut de directoris.
               Deixeu anar un directori al vostre escriptri per crear un nou visor de carpetes.
               L'aplet de visioó de carpetes no tan sols permet mostrar rues locals sinó que també pot mostrar rutes de xarxa.", "405");

    screenshot("panel-controller_thumb.png", "panel-controller.png", "center",
               "El nou controlador plafons permet redimensionar i canviar de posició els plafons.
               També podeu canviar la posició dels applets al plafó arrossegant-los a la nova posició.", "116");

    screenshot("krunner-screenie_thumb.png", "krunner-screenie.png", "center",
               "Amb KRunner, podeu iniciar aplications, enviar un correu als
               vostres amics i portar a terme altres petites tasques.", "238");

    screenshot("plasma-kickoff_thumb.png", "plasma-kickoff.png", "center",
               "Kickoff, el llançador d'aplicacions de Plasma, ha tingut un rentat de cara.", "405");

    screenshot("switch-menu_thumb.png", "switch-menu_thumb.png", "center",
               "Podeu escollir entre el llançador d'aplicacions Kickoff i el menú clàssic", "344");
?>

</p>

<h4>
  Problemes coneguts
</h4>
<p align="justify">
<ul>
    <li>Els usuaris de targetes <strong>NVidia</strong> amb el controlador binari subministrat per NVidia poden patir problemes de rendiment a l'hora de redimensionar o commutar de finestra. Hem informat els enginyers de NVidia d'aquests problemes. Tanmateix, NVidia encara no ha alliberat un controlador esmenat. Podeu trobar informació sobre com millorar el rendiment gràfic a la <a href="http://techbase.kde.org/User:Lemma/GPU-Performance">Techbase</a>,
    encara que en última instància depenem del fet que NVidia solucioni els problemes del seu controlador.</li>
</ul>

</p>



<!-- ==================================================================================== -->
<h4>
  Aconseguiu-lo, executeu-lo, proveu-lo
</h4>
<p align="justify">
  Alguns voluntaris de la comunitat i proveïdors de SO Linux/UNIX han proporcionat amablement paquets binaris del KDE 4.1.0 per algunes distribucions de Linux, Mac OS X i Windows. Comproveu el gestor de paquets del vostre sistema operatiu.
</p>
<h4>
  Compiliant el KDE 4.1.0
</h4>
<p align="justify">
  <a name="source_code"></a><em>Codi font</em>.
  El codi font complet del KDE 4.1.0 es pot <a
  href="http://www.kde.org/info/4.1.0.php">descarregar lliurement</a>.
A la pàgina d'<a href="/info/4.1.0.php">informació del KDE 4.1.0</a>, or bé a la <a href="http://techbase.kde.org/Getting_Started/Build/KDE4">TechBase</a>, hi ha disponibles instruccions sobre com compilar i instal·lar el KDE 4.1.0.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contactes de premsa</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
