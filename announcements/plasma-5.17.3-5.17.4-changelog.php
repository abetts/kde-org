<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.17.4 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.17.4";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a> </h3>
<ul id='ulbreeze-gtk' style='display: block'>
<li>[GTK3] Fix invalid colour name in treeview. <a href='https://commits.kde.org/breeze-gtk/8c16c196149140675ecf7b042e4d24077e4fd752'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414391'>#414391</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25476'>D25476</a></li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Fwupd: don't whine when we have unsupported hardware. <a href='https://commits.kde.org/discover/f0652fe1be1ea016a7b5734c8cab6765a1619784'>Commit.</a> </li>
<li>--verbosity. <a href='https://commits.kde.org/discover/7feddbe819a0f0db06ecbffa7f88b1a9b8466080'>Commit.</a> </li>
<li>Sources: for some reason, QML didn't find the toolTip role anymore on my Neon system. <a href='https://commits.kde.org/discover/0d2411a30ebee3fc090fc4bf17ae8022e9181b66'>Commit.</a> </li>
<li>Pk: only trigger offline updates after a successful download. <a href='https://commits.kde.org/discover/fb3a2a15d2964e6d7d920524f8ac599544c709cd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413028'>#413028</a></li>
<li>Account for padding in the height of the screenshot popup. <a href='https://commits.kde.org/discover/4d8d007f6eef847267d917c0f6d889b51a1aebaf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413629'>#413629</a></li>
<li>Account for weirdly horizontal screenshots. <a href='https://commits.kde.org/discover/05085877bc3d70b13ebc2d4bfa104da54e48a655'>Commit.</a> See bug <a href='https://bugs.kde.org/413629'>#413629</a></li>
<li>Progressview: don't highlight the current index of the view. <a href='https://commits.kde.org/discover/0aa7dcbbeab4555e33785ef69eb378c4fb843203'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414455'>#414455</a></li>
<li>Don't include newlines in searches. <a href='https://commits.kde.org/discover/5181714fb39e597e81030c737947bd1c30281817'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414530'>#414530</a></li>
<li>Pk: improve behaviour when dealing with local files. <a href='https://commits.kde.org/discover/b653ed82664277d165cfefdd9e17e10817fc418e'>Commit.</a> </li>
<li>Pk: cache appstream component names. <a href='https://commits.kde.org/discover/3b16167facdde17c77896e3e9dd41f9fe9b82b1e'>Commit.</a> </li>
<li>Don't overwrite mainItem. <a href='https://commits.kde.org/discover/7c483074d0742340baf4414904188d80fd4d4c85'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414145'>#414145</a></li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>Dr Konqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>Disable systeminfo test on systems without uname. <a href='https://commits.kde.org/drkonqi/4f161fa0b57f61a965e75e98115d40338caad1d6'>Commit.</a> </li>
<li>Fix msvc compatibility. <a href='https://commits.kde.org/drkonqi/fb50d436d17858ea89711b5481ff82f53a80636e'>Commit.</a> </li>
</ul>


<h3><a name='kde-cli-tools' href='https://commits.kde.org/kde-cli-tools'>kde-cli-tools</a> </h3>
<ul id='ulkde-cli-tools' style='display: block'>
<li>Port to non-deprecated KWindowSystem API. <a href='https://commits.kde.org/kde-cli-tools/1f3ba9b7d93432393d26d7d502e81c3cb1019041'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>[applets/Weather] Activate Apply button when clicking on a table item. <a href='https://commits.kde.org/kdeplasma-addons/cf0f35aa070036a3cf79c8b58f43166963ffc240'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414442'>#414442</a></li>
<li>[applets/Weather] Improve station chooser window's default size and margins. <a href='https://commits.kde.org/kdeplasma-addons/647549a006f34374eb7dcc64d4fa6058401bdab9'>Commit.</a> </li>
<li>[applets/Weather] Fix "no weather station found" message overflowing layout. <a href='https://commits.kde.org/kdeplasma-addons/62574d3a658c07d677b2085e6c3d559160a238b8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414441'>#414441</a></li>
<li>[Weather] Make compact representation temperature text match size of clock text. <a href='https://commits.kde.org/kdeplasma-addons/edb1367acc396a30d745854eefa223a0a400ce53'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25413'>D25413</a></li>
<li>Fix POTD config ComboBox display issue in Lock Screen KCM. <a href='https://commits.kde.org/kdeplasma-addons/e8402b9a3156786f583fddf3b39a88f939250ccb'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25354'>D25354</a></li>
</ul>


<h3><a name='khotkeys' href='https://commits.kde.org/khotkeys'>KDE Hotkeys</a> </h3>
<ul id='ulkhotkeys' style='display: block'>
<li>Clean up config-X11.h. <a href='https://commits.kde.org/khotkeys/faef67ad1819e9580df52ffb8a8d1cdf726b8375'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25275'>D25275</a></li>
<li>Clean up config-X11.h. <a href='https://commits.kde.org/khotkeys/e52b1c88ee346f8ff295419b37229132d0d6a531'>Commit.</a> </li>
</ul>


<h3><a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Fix(kcm): give the rotation button tooltips a sensible timeout. <a href='https://commits.kde.org/kscreen/8c892596858bec6e910ca6d119f452d6bb43bb8e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414190'>#414190</a></li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Add DesktopEntry to notifyrc. <a href='https://commits.kde.org/ksysguard/76792fa39b8359cb05d059ed63998558ee0752af'>Commit.</a> See bug <a href='https://bugs.kde.org/407701'>#407701</a></li>
</ul>


<h3><a name='kwayland-integration' href='https://commits.kde.org/kwayland-integration'>KWayland-integration</a> </h3>
<ul id='ulkwayland-integration' style='display: block'>
<li>Kwayland-integration: fix compilation with no-deprecated build of kwindowsystem. <a href='https://commits.kde.org/kwayland-integration/3ce73d56ca5364242be3eb7cb1b5c4f5eaf47196'>Commit.</a> </li>
<li>Remove duplicated check for StaysOnTop, same value as KeepAbove. <a href='https://commits.kde.org/kwayland-integration/faccad8e627e14e21fadf3db4a75edd785d6691f'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Destroy dmabuf implementation on EGL backend going down. <a href='https://commits.kde.org/kwin/e2d5ec606a39e3122d5f2f729b8ab90e0523b286'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413637'>#413637</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25577'>D25577</a></li>
<li>[kcmkwin/kwindecoration] Better presentation of tabs. <a href='https://commits.kde.org/kwin/fd69924e515345d6cab67a80423565506c9b290d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25582'>D25582</a></li>
<li>Fix compilation with no-deprecated build of kwindowsystem. <a href='https://commits.kde.org/kwin/2f000e3df42ac7448dbf03fe7a673565d169d26e'>Commit.</a> </li>
<li>Fix the order of arguments passed to changeMaximize method. <a href='https://commits.kde.org/kwin/b97d9ad681e9e60ed1a296c01c1d4328f090c659'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/413424'>#413424</a>. Fixes bug <a href='https://bugs.kde.org/412888'>#412888</a>. Fixes bug <a href='https://bugs.kde.org/413554'>#413554</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25366'>D25366</a></li>
<li>[libkwineffects] Restore GL_DRAW_FRAMEBUFFER binding in GLTexture::clear. <a href='https://commits.kde.org/kwin/1d362d38fd3eb1186bc849b33b6e27d13bfae7ac'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25365'>D25365</a></li>
</ul>


<h3><a name='libksysguard' href='https://commits.kde.org/libksysguard'>libksysguard</a> </h3>
<ul id='ullibksysguard' style='display: block'>
<li>WebChannel is only an optional dependency on WebEngine, link to it explicitly. <a href='https://commits.kde.org/libksysguard/819ff6c161cfd5e0ca627b385b0b459292997aac'>Commit.</a> </li>
</ul>


<h3><a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>[KDecoration] Use QIcon::paint. <a href='https://commits.kde.org/oxygen/4afa55e2743353357509d6ecf582b70f3c33793c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25691'>D25691</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Port away from deprecated KWindowSystem API. <a href='https://commits.kde.org/plasma-desktop/9449ce8b2d32f53ef1dddde569056096e7cae428'>Commit.</a> </li>
<li>Port the pager applet away from QtWidgets. <a href='https://commits.kde.org/plasma-desktop/674dd5f7e090c283338d2fd5673c1410dda7924f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D24238'>D24238</a></li>
<li>[Folder view] Use selected text color for selected items in full representation. <a href='https://commits.kde.org/plasma-desktop/a9291006771d92345dcb6ea93c035d9896c81f1d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414287'>#414287</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25418'>D25418</a></li>
<li>[Notifications KCM] Fix keyboard navigation in apps list. <a href='https://commits.kde.org/plasma-desktop/659fe0a76f19b60021733c4a74e451d4318a8359'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/409024'>#409024</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25309'>D25309</a></li>
<li>[Workspace KCM] Set implicit width to improve default layout. <a href='https://commits.kde.org/plasma-desktop/72cc340b6f0b9c23f08ebe6621a406abb6d58537'>Commit.</a> </li>
</ul>


<h3><a name='plasma-nm' href='https://commits.kde.org/plasma-nm'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Connect groupbox toggles to change signal. <a href='https://commits.kde.org/plasma-nm/c8775cb198f3dd9d9eca1aee3f70703140096fe7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25368'>D25368</a></li>
</ul>


<h3><a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Fix Cuttlefish mouse click selection in icon grid. <a href='https://commits.kde.org/plasma-sdk/637c0e517b29c9961fdefeaa0b2b9f2317aa129a'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D25633'>D25633</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[XembedSNIProxy] Send all container windows to background on KWin restart. <a href='https://commits.kde.org/plasma-workspace/02bbef3b506aa4c605cee88bd39a6d88dea63f34'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357443'>#357443</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25603'>D25603</a></li>
<li>[XembedSNIProxy] If available, always use 32-bit color. <a href='https://commits.kde.org/plasma-workspace/29554d5676803f499631977d100c56d3afeec920'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356937'>#356937</a>. Phabricator Code review <a href='https://phabricator.kde.org/D24530'>D24530</a></li>
<li>Port to non-deprecated KWindowSystem::setMainWindow overload. <a href='https://commits.kde.org/plasma-workspace/726262b9b440e922a6873c661bd7cf3cd3578a64'>Commit.</a> </li>
<li>Fix broken multimedia control on lockscreen. <a href='https://commits.kde.org/plasma-workspace/e2cbf51aa5165303455733f63f3f390ada2f9da4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/414487'>#414487</a>. Phabricator Code review <a href='https://phabricator.kde.org/D25519'>D25519</a></li>
<li>NET::StaysOnTop -> NET::KeepAbove (since 5.0). <a href='https://commits.kde.org/plasma-workspace/d8b6a1626ba0a4e1314e883a1d71ee9f42e7aab5'>Commit.</a> </li>
</ul>


<h3><a name='plymouth-kcm' href='https://commits.kde.org/plymouth-kcm'>Plymouth KControl Module</a> </h3>
<ul id='ulplymouth-kcm' style='display: block'>
<li>Fix compilation with Qt 5.14, because of qmlRegisterType being deprecated. <a href='https://commits.kde.org/plymouth-kcm/849ac4d0d716e17a5ebdc52d41ed07667b511720'>Commit.</a> </li>
</ul>


<h3><a name='polkit-kde-agent-1' href='https://commits.kde.org/polkit-kde-agent-1'>polkit-kde-agent-1</a> </h3>
<ul id='ulpolkit-kde-agent-1' style='display: block'>
<li>Port away from deprecated API in KWindowSystem. <a href='https://commits.kde.org/polkit-kde-agent-1/c56dbfc84ab6d579e3c73ce67186fc8d1ea39d6b'>Commit.</a> </li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Unbreak build with Qt 5.14. <a href='https://commits.kde.org/systemsettings/32567d4f61b432ac7ed7a9e799e11041d1b1279e'>Commit.</a> </li>
</ul>


<h3><a name='xdg-desktop-portal-kde' href='https://commits.kde.org/xdg-desktop-portal-kde'>xdg-desktop-portal-kde</a> </h3>
<ul id='ulxdg-desktop-portal-kde' style='display: block'>
<li>Port away from deprecated API in KWindowSystem. <a href='https://commits.kde.org/xdg-desktop-portal-kde/ed2f018a9867ab1a83c3e2a3959f5fb693bc2bdf'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
