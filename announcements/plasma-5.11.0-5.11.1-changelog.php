<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.11.1 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.11.1";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px;
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-<?php print $release; ?>.php">Plasma <?php print $release; ?></a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>[kstyle] Use KWindowSystem to determine on which platform we are. <a href='https://commits.kde.org/breeze/b081281d9bcd32a64c3a500f755440901dd9ee00'>Commit.</a> </li>
</ul>


<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Fix under Qt 5.10. <a href='https://commits.kde.org/discover/23a12651a9ef4b3fe09d1183967df0d090afadca'>Commit.</a> </li>
<li>Fix tests. <a href='https://commits.kde.org/discover/80a71b865e0a2e13fd635fbd66401d0be5118b92'>Commit.</a> </li>
<li>Remove our isCompact concept and use Kirigami's. <a href='https://commits.kde.org/discover/b3dd82e21c09e6c177fe3e0459a29181fb96e5b7'>Commit.</a> </li>
</ul>


<h3><a name='drkonqi' href='https://commits.kde.org/drkonqi'>drkonqi</a> </h3>
<ul id='uldrkonqi' style='display: block'>
<li>Don't repeat work of KAboutData::setupCommandLine(). <a href='https://commits.kde.org/drkonqi/338c27fca1e74317a169fc5c6dc63572a23dcf46'>Commit.</a> </li>
<li>Call KLocalizedString::setApplicationDomain only after QApp creation. <a href='https://commits.kde.org/drkonqi/022ca239b66c24fe081992383d122b4704ab8e86'>Commit.</a> </li>
</ul>


<h3><a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Drop custom app metadata setting, KAboutData::setApplicationData does it. <a href='https://commits.kde.org/kinfocenter/403897378af5733d5b902721297f0acd51d634d2'>Commit.</a> </li>
<li>Call KLocalizedString::setApplicationDomain before first i18n usage. <a href='https://commits.kde.org/kinfocenter/c352e13c613eafbac5422f7aeab2f16e9556dae9'>Commit.</a> </li>
</ul>


<h3><a name='ksshaskpass' href='https://commits.kde.org/ksshaskpass'>KSSHAskPass</a> </h3>
<ul id='ulksshaskpass' style='display: block'>
<li>Use NO_POLICY_SCOPE with KDECompilerSettings. <a href='https://commits.kde.org/ksshaskpass/206bfeb0b3b1481e205beb2add4ef6ea129f07c4'>Commit.</a> </li>
<li>Don't repeat work of KAboutData::setupCommandLine(). <a href='https://commits.kde.org/ksshaskpass/e0d9f941ea12e74d0832d52f5ef9172891058b54'>Commit.</a> </li>
<li>Call KLocalizedString::setApplicationDomain only after QApp creation. <a href='https://commits.kde.org/ksshaskpass/5f55904ec4e58d9c448cbc95771deafcddfea13e'>Commit.</a> </li>
</ul>


<h3><a name='ksysguard' href='https://commits.kde.org/ksysguard'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Try to read CPU clock from cpufreq/scaling_cur_freq instead of /proc/cpuinfo. <a href='https://commits.kde.org/ksysguard/cbaaf5f4ff54e20cb8ec782737e04d540085e6af'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382561'>#382561</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8153'>D8153</a></li>
</ul>


<h3><a name='kwayland-integration' href='https://commits.kde.org/kwayland-integration'>KWayland-integration</a> </h3>
<ul id='ulkwayland-integration' style='display: block'>
<li>Unload poller's connection thread earlier in teardown. <a href='https://commits.kde.org/kwayland-integration/e27d843cc3204c4dee64166d5ea60beaf0bf5d47'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8285'>D8285</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Ensure leave event is send to decoration when a window maximizes. <a href='https://commits.kde.org/kwin/507d83fb08ad6b139a3ab11797370fdc7b364532'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385140'>#385140</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8016'>D8016</a></li>
<li>Update pointer position whenever a window gets (un)minimized. <a href='https://commits.kde.org/kwin/1b01f1b3009474cc41d5c58318c1f3791fcff0fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/378704'>#378704</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8145'>D8145</a></li>
<li>Fix DRM EGL crash regression. <a href='https://commits.kde.org/kwin/5bca05882624f751b39a0fa0996a11fd6303e98e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8251'>D8251</a></li>
<li>[autotests] Fix typo. <a href='https://commits.kde.org/kwin/d6a906da9225582f41f65aa4517e200731c256c6'>Commit.</a> </li>
<li>[platforms/drm] Use a shared pointer for gbm_surface. <a href='https://commits.kde.org/kwin/47343fb8f75909f6491c0534004df56ee1e53737'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385372'>#385372</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8152'>D8152</a></li>
<li>Ensure internal Wayland connection is properly setup before creating LockScreen integration. <a href='https://commits.kde.org/kwin/31b5b7f9f981ccaca001423c1a2183157bb53356'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385397'>#385397</a></li>
</ul>


<h3><a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Use KWindowSytem to determine which platform is used. <a href='https://commits.kde.org/oxygen/417e0d856fb0e1316d80c70cc745573cd0713106'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Kcm baloo: Fix extraction of folder basename for error message. <a href='https://commits.kde.org/plasma-desktop/5a8691900fea2b77ae194f509d17e7368235b4c1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8325'>D8325</a></li>
<li>Fixed application progress in task manager no longer working. <a href='https://commits.kde.org/plasma-desktop/a1a85f95bb487ac74ef2aa903ca09ae4ed2a125c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385730'>#385730</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8327'>D8327</a></li>
<li>Kimpanel: we don't change text color in new implementation. remove used code. <a href='https://commits.kde.org/plasma-desktop/2f65154995bb9153d5d2cc28872ec9893e3dc8dd'>Commit.</a> </li>
<li>Fix QFile::copy warning when the colors file doesn't exist. <a href='https://commits.kde.org/plasma-desktop/df6658eee5c63b622ff1b4239124cc1d9009314d'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8266'>D8266</a></li>
<li>Fix editing items in SimpleFavoritesModel. <a href='https://commits.kde.org/plasma-desktop/06f01ed291ca21057497fd0ceaa69f6e4d324bc3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385463'>#385463</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8178'>D8178</a></li>
<li>Use correct .desktop file name when setting KMail as default. <a href='https://commits.kde.org/plasma-desktop/ca72cd44ebc0f62abd1bc4fb9f07e87764f69d34'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8239'>D8239</a></li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>[LauncherTasksModel] Try resolving absolute path to applications: URL before adding launcher. <a href='https://commits.kde.org/plasma-workspace/5fee12f8bee6491cb03a4fa04509c96e04ccfb77'>Commit.</a> See bug <a href='https://bugs.kde.org/385594'>#385594</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8260'>D8260</a></li>
<li>Fixed issue that caused pinned applications in task manager to erroneously shift around. <a href='https://commits.kde.org/plasma-workspace/88dbb40ddedee4740b904e9a6f57beda80013550'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/385594'>#385594</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8258'>D8258</a></li>
<li>[StartupTasksModel] Try resolving startup application id to applications: URL. <a href='https://commits.kde.org/plasma-workspace/9a257ab60b9157f24e1cb9d6659185f912d992d2'>Commit.</a> See bug <a href='https://bugs.kde.org/385594'>#385594</a>. Phabricator Code review <a href='https://phabricator.kde.org/D8257'>D8257</a></li>
<li>Fix "postEvent: null receiver" warning due to nullptr->deleteLater(). <a href='https://commits.kde.org/plasma-workspace/dc64944553b82da2ed5c4f5f28e77131d07c9de1'>Commit.</a> </li>
<li>Remove compilation flag for non-existing source file. <a href='https://commits.kde.org/plasma-workspace/5c1e0aa887f78f2905dc39829e58854c8893bf63'>Commit.</a> </li>
<li>Fixed being unable to switch users from the Switch User screen. <a href='https://commits.kde.org/plasma-workspace/ac40f7dec47df9c48fa55d90be67ea4cbebcb09d'>Commit.</a> </li>
</ul>


<h3><a name='systemsettings' href='https://commits.kde.org/systemsettings'>System Settings</a> </h3>
<ul id='ulsystemsettings' style='display: block'>
<li>Use QQC2.Label over Kirigami.Label. <a href='https://commits.kde.org/systemsettings/59f70b3afe18a1b1c3e886ff5836a29e23b8a891'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D8143'>D8143</a></li>
<li>Get rid of binding loop. <a href='https://commits.kde.org/systemsettings/8ba469c236f742f4a758818e02ff7dc15ef4185d'>Commit.</a> </li>
</ul>


</main>
<?php
	require('../aether/footer.php');
