<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("KDE Ships Release Candidate of Plasma 5");
  $site_root = "../";
  $release = 'plasma-4.98.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<a href="plasma-5.0-rc.png"><img src="plasma-5.0-rc-wee.png" style="float: right; padding: 1ex; margin: 1ex; border: 0; background-image: none; " width="400" height="300" alt="<?php i18n("Plasma 5");?>" /></a>

<p>
<?php i18n("July 8, 2014.
KDE has today made available the candidate for the first release of Plasma 5, the next generation desktop.  This is one last chance to test for bugs and check for problems before the final release next week.
");?>
</p>

<!-- // Boilerplate again -->

<h2><?php i18n("Installing and providing feedback");?></h2>

<p><?php i18n("The easiest way to try it out is the <a
href='http://files.kde.org/snapshots/neon5-latest.iso'>Neon5 ISO</a>,
a live OS image updated with the latest builds straight from
source.");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='http://community.kde.org/Plasma/Next/UnstablePackages'>Package
downloads</a>"
, $release);?>
</li>
</ul>

<p><?php i18n("Some distributions have created, or are in the process
of creating, packages, these are listed on the wiki page linked above.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='http://download.kde.org/unstable/plasma/%1/src/'>Source
download</a>"
, $release);?>
</li>
</ul>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<p><?php i18n("You can provide feedback either via the <a
href='irc://#plasma@freenode.net'>#Plasma IRC channel</a>, <a
href='https://mail.kde.org/mailman/listinfo/plasma-devel'>Plasma-devel
mailing list</a> or report issues via <a
href='https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided'>bugzilla</a>. Plasma
Next is also <a
href='http://forum.kde.org/viewforum.php?f=287'>discussed on the KDE
Forums</a>. Your feedback is greatly appreciated. If you like what the
team is doing, please let them know!");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
