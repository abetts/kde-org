<?php
    include_once ("functions.inc");
    $translation_file = "www";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "KDE Plasma 5.17 Beta: Thunderbolt, X11 Night Color and Redesigned Settings",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = 'plasma-5.16.90'; // for i18n
    $version = "5.16.90";
?>

<script src="/js/use-ekko-lightbox.js" defer="true"></script>

<main class="releaseAnnouncment container">

    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Plasma %1", $version)?></h1>

    <?php include "./announce-i18n-bar.inc"; ?>

<!--
    <figure class="videoBlock">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/C2kR1_n_d-g?rel=0" allowfullscreen='true'></iframe>
    </figure>
-->

    <figure class="topImage">
        <a href="plasma-5.17/plasma-5.17.png" data-toggle="lightbox">
            <img src="plasma-5.17/plasma-5.17-wee.png" height="342" width="600" style="width: 100%; max-width: 600px; height: auto;" alt="Plasma 5.17 Beta" />
        </a>
        <figcaption><?php print i18n_var("KDE Plasma %1", "5.17 Beta")?></figcaption>
    </figure>

    <p><?php i18n("Thursday, 19 September 2019.")?></p>
    <p><?php i18n("Today KDE launches the beta release of Plasma 5.17.")?></p>

    <p><?php i18n("We've added a bunch of new features and improvements to KDE's lightweight yet full featured desktop environment.")?></p>
    
    <p><?php i18n("<a href='https://kde.org/plasma-desktop'>Plasma's updated web page</a> gives more background on why you should use it on your computer.")?></p>

    <figure style="float: right; text-align: right">
    <a href="plasma-5.17/guillermo.png" data-toggle="lightbox">
    <img src="plasma-5.17/guillermo-wee.png" style="padding: 10px" width="350" height="334" alt="<?php i18n("Guillermo Amaral");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Guillermo Amaral");?></figcaption>
    </figure>

    <p><?php i18n("System Settings has gained new features to help you manage your fancy Thunderbolt hardware, plus Night Color is now on X11 and a bunch of pages got redesigned to help you get your configuration done easier.  Our notifications continue to improve with a new icon and automatic do-not-disturb mode for presentations.  Our Breeze GTK theme now provides a better appearance for the Chromium/Chrome web browsers and applies your color scheme to GTK and GNOME apps. The window manager KWin has received many HiDPI and multi-screen improvements, and now supports fractional scaling on Wayland.");?></p>
    <p><?php i18n("You can test the Plasma 5.17 beta for the next three weeks until the final release in mid-October.  Give it a whirl with your favorite distribution!")?></p>

    <p><?php i18n("The Plasma 5.17 series is dedicated to our friend Guillermo Amaral. Guillermo was an enthusiastic KDE developer who rightly self described as 'an incredibly handsome multidisciplinary self-taught engineer'.  He brought cheer to anyone he met.  He lost his battle with cancer last summer but will be remembered as a friend to all he met.")?></p>

    <br clear="all" />

    <h3 id="desktop"><?php i18n("Plasma");?></h3>
    <figure style="float: right; text-align: right">
    <a href="plasma-5.17/unsplash-pic-of-day.png" data-toggle="lightbox">
    <img src="plasma-5.17/unsplash-pic-of-day-wee.png" style="border: 0px" width="350" height="280" alt="<?php i18n("<a href='https://unsplash.com/'>Unsplash</a> Picture of the Day");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("<a href='https://unsplash.com/'>Unsplash</a> Pic of the Day");?></figcaption>
    <br />
    <a href="plasma-5.17/inches-to-cm.png" data-toggle="lightbox">
    <img src="plasma-5.17/inches-to-cm-wee.png" style="border: 0px" width="350" height="159" alt="<?php i18n("<a href='https://unsplash.com/'>Unsplash</a> Pic of the Day");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("KRunner now converts fractional units");?></figcaption>
    <br />
    <a href="plasma-5.17/notification-widget.png" data-toggle="lightbox">
    <img src="plasma-5.17/notification-widget-wee.png" style="border: 0px" width="250" height="230" alt="<?php i18n("Improved Notifications widget and widget editing UX");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Improved Notifications widget and widget editing UX");?></figcaption>
    </figure>

    <ul>
<li><?php i18n("Do Not Disturb mode is automatically enabled when mirroring screens (e.g. when delivering a presentation)"); ?></li>
<li><?php i18n("The Notifications widget now uses an improved icon instead of displaying the number of unread notifications"); ?></li>
<li><?php i18n("Improved widget positioning UX, particularly for touch"); ?></li>
<li><?php i18n("Improved the Task Manager's middle-click behavior: middle-clicking on an open app's task opens a new instance, while middle-clicking on its thumbnail will close that instance"); ?></li>
<li><?php i18n("Slight RGB hinting is now the default font rendering mode"); ?></li>
<li><?php i18n("Plasma now starts even faster!"); ?></li>
<li><?php i18n("Conversion of fractional units into other units (e.g. 3/16\" == 4.76 mm) in KRunner and Kickoff"); ?></li>
<li><?php i18n("Wallpaper slideshows can now have user-chosen ordering rather than always being random"); ?></li>
<li><?php i18n("New <a href='https://unsplash.com/'>Unsplash</a> picture of the day wallpaper source with categories"); ?></li>
<li><?php i18n("Much better support for public WiFi login"); ?></li>
<li><?php i18n("Added the ability to set a maximum volume that's lower than 100%"); ?></li>
<li><?php i18n("Pasting text into a sticky note strips the formatting by default"); ?></li>
<li><?php i18n("Kickoff's recent documents section now works with GNOME/GTK apps"); ?></li>
<li><?php i18n("Fixed Kickoff tab appearance being broken with vertical panels"); ?></li>
    </ul>

    <br clear="all" />
    
    <h3 id="systemsettings"><?php i18n("System Settings: Thunderbolt, X11 Night Color and Overhauled Interfaces");?></h3>
    <figure style="float: right; text-align: right">
    <a href="plasma-5.17/night-color.png" data-toggle="lightbox">
    <img src="plasma-5.17/night-color-wee.png" style="border: 0px" width="350" height="250" alt="<?php i18n("Night Color settings are now available on X11 too");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Night Color settings are now available on X11 too");?></figcaption>
    <br />
    <a href="plasma-5.17/thunderbolt.png" data-toggle="lightbox">
    <img src="plasma-5.17/thunderbolt-wee.png" style="border: 0px" width="350" height="344" alt="<?php i18n("Thunderbolt device management");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Thunderbolt device management");?></figcaption>
    <br />
    <a href="plasma-5.17/settings-consistency.png" data-toggle="lightbox">
    <img src="plasma-5.17/settings-consistency-wee.png" style="border: 0px" width="390" height="280" alt="<?php i18n("Reorganized Appearance settings, consistent sidebars and headers");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Reorganized Appearance settings, consistent sidebars and headers");?></figcaption>
    </figure>

    <ul>
<li><?php i18n("New settings panel for managing and configuring Thunderbolt devices"); ?></li>
<li><?php i18n("The Night Color settings are now available on X11 too.  It gets a modernized and redesigned user interface, and the feature can be manually invoked in the settings or with a keyboard shortcut."); ?></li>
<li><?php i18n("Overhauled the user interface for the Displays, Energy, Activities, Boot Splash, Desktop Effects, Screen Locking, Screen Edges, Touch Screen, and Window Behavior settings pages and the SDDM advanced settings tab"); ?></li>
<li><?php i18n("Reorganized and renamed some settings pages in the Appearance section"); ?></li>
<li><?php i18n("Basic system information is now available through System Settings"); ?></li>
<li><?php i18n("Added accessibility feature to move your cursor with the keyboard when using Libinput"); ?></li>
<li><?php i18n("You can now apply a user's font, color scheme, icon theme, and other settings to the SDDM login screen to ensure visual continuity on single-user systems"); ?></li>
<li><?php i18n("New 'sleep for a few hours and then hibernate' feature"); ?></li>
<li><?php i18n("The Colors page now displays the color scheme's titlebar colors"); ?></li>
<li><?php i18n("It is now possible to assign a global keyboard shortcut to turn off the screen"); ?></li>
<li><?php i18n("Standardized appearance for list headers"); ?></li>
<li><?php i18n("The 'Automatically switch all running streams when a new output becomes available' feature now works properly"); ?></li>
    </ul>

    <br clear="all" />
    
    <h3 id="breeze"><?php i18n("Breeze Theme");?></h3>
    <figure style="float: right; text-align: right">
    <a href="plasma-5.17/window-borders.png" data-toggle="lightbox">
    <img src="plasma-5.17/window-borders-wee.png" style="border: 0px" width="350" height="250" alt="<?php i18n("Window borders are now turned off by default");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Window borders are now turned off by default");?></figcaption>
    </figure>

    <ul>
<li><?php i18n("The Breeze GTK theme now respects your chosen color scheme"); ?></li>
<li><?php i18n("Active and inactive tabs in Google Chrome and Chromium now look visually distinct"); ?></li>
<li><?php i18n("Window borders are now turned off by default"); ?></li>
<li><?php i18n("Sidebars in settings windows now have a consistent modernized appearance"); ?></li>
    </ul>

    <br clear="all" />
    
    <h3 id="ksysguard"><?php i18n("System Monitor");?></h3>
    <figure style="float: right; text-align: right">
    <a href="plasma-5.17/ksysguard.png" data-toggle="lightbox">
    <img src="plasma-5.17/ksysguard-wee.png" style="border: 0px" width="350" height="157" alt="<?php i18n("CGroups in System Monitor");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("CGroups in System Monitor");?></figcaption>
    </figure>

    <ul>
<li><?php i18n("System Monitor can now show CGroup details to look at container limits"); ?></li>
<li><?php i18n("Each process can now report its network usage statistics"); ?></li>
<li><?php i18n("It is now possible to see NVidia GPU stats"); ?></li>
    </ul>

    <br clear="all" />
    
    <h3 id="discover"><?php i18n("Discover");?></h3>
    <figure style="float: right; text-align: right">
    <a href="plasma-5.17/discover.png" data-toggle="lightbox">
    <img src="plasma-5.17/discover-wee.png" style="border: 0px" width="350" height="255" alt="<?php i18n("Discover now has icons on the sidebar");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Discover now has icons on the sidebar");?></figcaption>
    </figure>

    <ul>
<li><?php i18n("Real progress bars and spinners in various parts of the UI to better communicate progress information"); ?></li>
<li><?php i18n("Better 'No connection' error messages"); ?></li>
<li><?php i18n("Icons in the sidebar and icons for Snap apps"); ?></li>
    </ul>
    <br clear="all" />
    
    <h3 id="kwin"><?php i18n("KWin: Improved Display Management");?></h3>
    <ul>
<li><?php i18n("Fractional scaling added on Wayland"); ?></li>
<li><?php i18n("It is now once again possible to close windows in the Present Windows effect with a middle-click"); ?></li>
<li><?php i18n("Option to configure whether screen settings apply only for the current screen arrangement or to all screen arrangements"); ?></li>
<li><?php i18n("Many multi-screen and HiDPI improvements"); ?></li>
<li><?php i18n("On Wayland, it is now possible to resize GTK headerbar windows from window edges"); ?></li>
<li><?php i18n("Scrolling with a wheel mouse on Wayland now always scrolls the correct number of lines"); ?></li>
<li><?php i18n("On X11, it is now possible to use the Meta key as a modifier for the window switcher that's bound to Alt+Tab by default"); ?></li>
    </ul>

    <br clear="all" />

    <a href="plasma-5.16.5-5.16.90-changelog.php"><?php print i18n_var("Full Plasma %1 changelog", "5.16.90"); ?></a>

    <!-- // Boilerplate again -->
    <section class="row get-it">
        <article class="col-md">
            <h2><?php i18n("Live Images");?></h2>
            <p>
                <?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
            </p>
            <a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
            <a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Package Downloads");?></h2>
            <p>
                <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
            </p>
            <a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Get KDE Software on Your Linux Distro wiki page");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Source Downloads");?></h2>
            <p>
                <?php i18n("You can install Plasma 5 directly from source.");?>
            </p>
            <a href='https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source' class='learn-more'><?php i18n("Community instructions to compile it");?></a>
            <a href='../info/plasma-5.15.90.php' class='learn-more'><?php i18n("Source Info Page");?></a>
        </article>
    </section>

    <section class="give-feedback">
        <h2><?php i18n("Feedback");?></h2>

        <p class="kSocialLinks">
            <?php i18n("You can give us feedback and get updates on our social media channels:"); ?>
            <a class="shareFacebook" href="https://www.facebook.com/kde/" rel="nofollow">Post on Facebook</a>
            <a class="shareTwitter" href="https://twitter.com/kdecommunity" rel="nofollow">Share on Twitter</a>
            <a class="shareDiaspora" href="https://joindiaspora.com/people/9c3d1a454919ef06" rel="nofollow">Share on Diaspora</a>
            <a class="shareReddit" href="http://www.reddit.com/r/kde/" rel="nofollow">Share on Reddit</a>
            <a class="shareYouTube" href="https://www.youtube.com/channel/UCF3I1gf7GcbmAb0mR6vxkZQ" rel="nofollow">Share on YouTube</a>
            <a class="shareMastodon" href="https://mastodon.technology/@kde" rel="nofollow">Share on Mastodon</a>
            <a class="shareLinkedIn" href="https://www.linkedin.com/company/29561/" rel="nofollow">Share on LinkedIn</a>
            <a class="sharePeerTube" href="https://peertube.mastodon.host/accounts/kde/videos" rel="nofollow">Share on PeerTube</a>
            <a class="shareVK" href="https://vk.com/kde_ru" rel="nofollow">Share on VK</a>
        </p>
        <p>
            <?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
        </p>

        <p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>Plasma Matrix chat room</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "https://webchat.kde.org/#/room/#cutehmi:kde.org", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

        <p><?php i18n("Your feedback is greatly appreciated.");?></p>
    </section>

    <h2>
        <?php i18n("Supporting KDE");?>
    </h2>

    <p align="justify">
        <?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='%3'>Join the Game</a> initiative.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/civicrm/contribute/transact?id=5"); ?>
    </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');


