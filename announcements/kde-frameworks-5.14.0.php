<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.14.0");
  $site_root = "../";
  $release = '5.14.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
September 12, 2015. KDE today announces the release
of KDE Frameworks 5.14.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("In many frameworks");?></h3>

<ul>
<li><?php i18n("Rename private classes to avoid exporting them accidentally");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Add org.kde.baloo interface to root object for backward compatibility");?></li>
<li><?php i18n("Install a fake org.kde.baloo.file.indexer.xml to fix compilation of plasma-desktop 5.4");?></li>
<li><?php i18n("Re-organize D-Bus interfaces");?></li>
<li><?php i18n("Use json metadata in kded plugin and fix plugin name");?></li>
<li><?php i18n("Create one Database instance per process (bug 350247)");?></li>
<li><?php i18n("Prevent baloo_file_extractor being killed while committing");?></li>
<li><?php i18n("Generate xml interface file using qt5_generate_dbus_interface");?></li>
<li><?php i18n("Baloo monitor fixes");?></li>
<li><?php i18n("Move file url export to main thread");?></li>
<li><?php i18n("Make sure cascaded configs are taken into account");?></li>
<li><?php i18n("Do not install namelink for private library");?></li>
<li><?php i18n("Install translations, spotted by Hrvoje Senjan.");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Don't forward deviceChanged signal after device was removed (bug 351051)");?></li>
<li><?php i18n("Respect -DBUILD_TESTING=OFF");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Add macro to generate logging category declarations for Qt5.");?></li>
<li><?php i18n("ecm_generate_headers: Add COMMON_HEADER option and multiple header functionality");?></li>
<li><?php i18n("Add -pedantic for KF5 code (when using gcc or clang)");?></li>
<li><?php i18n("KDEFrameworkCompilerSettings: only enable strict iterators in debug mode");?></li>
<li><?php i18n("Also set the default visibility for C code to hidden.");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Also propagate window titles for folder-only file dialogs.");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Only spawn one action loader (thread) when the actions of the FileItemLinkingPlugin are not initialized (bug 351585)");?></li>
<li><?php i18n("Fixing the build problems introduced by renaming the Private classes (11030ffc0)");?></li>
<li><?php i18n("Add missing boost include path to build on OS X");?></li>
<li><?php i18n("Setting the shortcuts moved to activity settings");?></li>
<li><?php i18n("Setting the private activity mode works");?></li>
<li><?php i18n("Refactor of the settings UI");?></li>
<li><?php i18n("Basic activity methods are functional");?></li>
<li><?php i18n("UI for the activity configuration and deletion pop-ups");?></li>
<li><?php i18n("Basic UI for the activities creation/deletion/configuration section in KCM");?></li>
<li><?php i18n("Increased the chunk size for loading the results");?></li>
<li><?php i18n("Added missing include for std::set");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Windows fix: remove existing files before we replace them with os.rename.");?></li>
<li><?php i18n("Use native paths when calling python to fix Windows builds");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("Fix bad behavior / running OOM on Windows (bug 345860)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Optimize readEntryGui");?></li>
<li><?php i18n("Avoid QString::fromLatin1() in generated code");?></li>
<li><?php i18n("Minimize calls to expensive QStandardPaths::locateAll()");?></li>
<li><?php i18n("Finish the port to QCommandLineParser (it has addPositionalArgument now)");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Port solid-networkstatus kded plugin to json metadata");?></li>
<li><?php i18n("KPixmapCache: create dir if it doesn't exist");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Sync Catalan user.entities with English (en) version.");?></li>
<li><?php i18n("Add entities for sebas and plasma-pa");?></li>
</ul>

<h3><?php i18n("KEmoticons");?></h3>

<ul>
<li><?php i18n("Performance: cache a KEmoticons instance here, not a KEmoticonsTheme.");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("PlainTextExtractor: enable O_NOATIME branch on GNU libc platforms");?></li>
<li><?php i18n("PlainTextExtractor: make the Linux branch work also without O_NOATIME");?></li>
<li><?php i18n("PlainTextExtractor: fix error check on open(O_NOATIME) failure");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Only start kglobalaccel5 if needed.");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Gracefully handle no newline at end of pmap file");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("KIconLoader: fix reconfigure() forgetting about inherited themes and app dirs");?></li>
<li><?php i18n("Adhere better to the icon loading spec");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("eps: fix includes related to Qt Caterogized Logging");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Use Q_OS_WIN instead of Q_OS_WINDOWS");?></li>
<li><?php i18n("Make KDE_FORK_SLAVES work under Windows");?></li>
<li><?php i18n("Disable installation of desktop file for ProxyScout kded module");?></li>
<li><?php i18n("Provide deterministic sort order for KDirSortFilterProxyModelPrivate::compare");?></li>
<li><?php i18n("Show custom folder icons again (bug 350612)");?></li>
<li><?php i18n("Move kpasswdserver from kded to kiod");?></li>
<li><?php i18n("Fix porting bugs in kpasswdserver");?></li>
<li><?php i18n("Remove legacy code for talking very very old versions of kpasswdserver.");?></li>
<li><?php i18n("KDirListerTest: use QTRY_COMPARE on both statements, to fix race showed by CI");?></li>
<li><?php i18n("KFilePlacesModel: implement old TODO about using trashrc instead of a full-blown KDirLister.");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("New proxymodel: KConcatenateRowsProxyModel");?></li>
<li><?php i18n("KConcatenateRowsProxyModelPrivate: fix handling of layoutChanged.");?></li>
<li><?php i18n("More checking on the selection after sorting.");?></li>
<li><?php i18n("KExtraColumnsProxyModel: fix bug in sibling() which broke e.g. selections");?></li>
</ul>

<h3><?php i18n("Package Framework");?></h3>

<ul>
<li><?php i18n("kpackagetool can uninstall a package from a package file");?></li>
<li><?php i18n("kpackagetool is now smarter about finding the right servicetype");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("KSycoca: check timestamps and run kbuildsycoca if needed. No kded dependency anymore.");?></li>
<li><?php i18n("Don't close ksycoca right after opening it.");?></li>
<li><?php i18n("KPluginInfo now correctly handles FormFactor metadata");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Merge allocation of TextLineData and ref count block.");?></li>
<li><?php i18n("Change default keyboard shortcut for \"go to previous editing line\"");?></li>
<li><?php i18n("Syntax highlighting Haskell comment fixes");?></li>
<li><?php i18n("Speed up code-completion pop-up appearance");?></li>
<li><?php i18n("minimap: Attempt to improve the look and feel (bug 309553)");?></li>
<li><?php i18n("nested comments in Haskell syntax highlighting");?></li>
<li><?php i18n("Fix problem with wrong unindent for python (bug 351190)");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KPasswordDialog: let the user change the password visibility (bug 224686)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Fix KSwitchLanguageDialog not showing most languages");?></li>
</ul>

<h3><?php i18n("KXmlRpcClient");?></h3>

<ul>
<li><?php i18n("Avoid QLatin1String wherever it allocates heap memory");?></li>
</ul>

<h3><?php i18n("ModemManagerQt");?></h3>

<ul>
<li><?php i18n("Fix metatype conflict with the latest nm-qt change");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Added new properties from the latest NM snapshot/releases");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("reparent to flickable if possible");?></li>
<li><?php i18n("fix package listing");?></li>
<li><?php i18n("plasma: Fix applet actions might be nullptr (bug 351777)");?></li>
<li><?php i18n("The onClicked signal of PlasmaComponents.ModelContextMenu now works properly");?></li>
<li><?php i18n("PlasmaComponents ModelContextMenu can now create Menu sections");?></li>
<li><?php i18n("Port platformstatus kded plugin to json metadata...");?></li>
<li><?php i18n("Handle an invalid metadata in PluginLoader");?></li>
<li><?php i18n("Let the RowLayout figure out the size of the label");?></li>
<li><?php i18n("always show the edit menu when the cursor is visible");?></li>
<li><?php i18n("Fix loop on ButtonStyle");?></li>
<li><?php i18n("Don't change the flat-iness of a button on pressed");?></li>
<li><?php i18n("on touchscreen and mobile scrollbars are transient");?></li>
<li><?php i18n("adjust flick velocity&amp;deceleration to dpi");?></li>
<li><?php i18n("custom cursor delegate only if mobile");?></li>
<li><?php i18n("touch friendly text cursor");?></li>
<li><?php i18n("fix parenting and popping up policy");?></li>
<li><?php i18n("declare __editMenu");?></li>
<li><?php i18n("add missing cursot handles delegates");?></li>
<li><?php i18n("rewrite the EditMenu implementation");?></li>
<li><?php i18n("use the mobile menu only conditionally");?></li>
<li><?php i18n("reparent the menu to root");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.14");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.3");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
