<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $release = '4.13';
  $release_full = '4.13.0';
  $page_title = i18n_noop("KDE Software Compilation 4.13");
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<?php showscreenshotpng("plasma-4.13.png", ""); ?>
<!-- <img src="screenshots/plasma-4.13.png" style="float: right; border: 0; background-image: none; margin-top: -80px;" alt="<?php i18n("The KDE Plasma Workspaces 4.13");?>" /> -->
<br />
<p>
<?php i18n("April 16, 2014");?>
</p>

<font size="+1">
<?php i18n("The KDE Community proudly announces the latest major updates to KDE Applications delivering new features and fixes. Major improvements are made to KDE's Semantic Search technology, benefiting many applications. With Plasma Workspaces and the KDE Development Platform frozen and receiving only long term support, those teams are focusing on the transition to Frameworks 5. This release is translated into 53 languages; more languages are expected to be added in subsequent monthly minor bugfix releases.");?><br />
</font>

<?php
  include "../announce-i18n-bar.inc";
?>

<h2><a href="applications.php"><img src="images/applications.png" class="app-icon" alt="<?php i18n("The KDE Applications 4.13");?>"/> <?php i18n("KDE Applications 4.13 Benefit From The New Semantic Search, Introduce New Features");?></a></h2>
<p>
<?php i18n("The latest major updates to the KDE Applications are delivering new features and fixes. Kontact (the personal information manager) has been the subject of intense activity, benefiting from the improvements to KDE's Semantic Search technology and bringing new features. Document viewer Okular and advanced text editor Kate have gotten interface-related and feature improvements. In the education and game areas, we introduce the new foreign speech trainer Artikulate; Marble (the desktop globe) gets support for Sun, Moon, planets, bicycle routing and nautical miles. Palapeli (the jigsaw puzzle application) has leaped to unprecedented new dimensions and capabilities. <a href='https://www.kde.org/announcements/4.13/applications.php'>read the announcement</a>.");?>
</p>

<h2><img src="images/platform.png" class="app-icon" alt="<?php i18n("The KDE Development Platform 4.13");?>"/> <?php i18n("KDE Development Platform 4.13 Introduces Improved Semantic Search");?></h2>
<p>
<?php i18n("The KDE Development Platform libraries are frozen and receive only bugfixes and minor improvements. The upgrade in the version number for the Development Platform is only for packaging convenience. All bug fixes and minor features developed since the release of Applications and Development Platform 4.11 have been included. The only major change in this release is the introduction of an improved Semantic Search, which brings better performance and reliability to searching on the Linux Desktop.");?>
</p>
<p>
<?php i18n("Development of the next generation KDE Development Platform—called KDE Frameworks 5—is in beta stage. Read <a href='http://dot.kde.org/2013/09/25/frameworks-5'>this article</a> to find out what is coming and <a href='https://www.kde.org/announcements/'>see here</a> for the latest announcements.");?>
</p>
<h3><?php i18n("Improved Semantic Search");?></h3>
<p>
<?php i18n("The major new addition to the KDE Development Platform is the <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>next generation Semantic Search</a>. To maintain compatibility, this is included as a new component rather than a replacement for the previous Semantic Search. Applications need to be ported to the new search component; most KDE Applications have already been ported. Downstream distributions can decide whether or not to ship the deprecated Semantic Search alongside the new version.");?>
</p>
<p>
<?php i18n("The improvements to search bring significant benefits in terms of faster, more relevant results, greater stability, lower resource usage and less data storage. The upgrade requires a one-time database migration that will take a few minutes of increased processing power.");?>
</p>

<h2><?php i18n("Support KDE");?></h2>


<a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=5"><img src="images/join-the-game.png" width="231" height="120"
alt="<?php i18n("Join the Game");?>" align="left"/> </a>
<p align="justify"> <?php i18n("KDE e.V.'s new <a
href='https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=5'>Supporting Member program</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.");?></p>

<br clear="all" />
<p>&nbsp;</p>

<h2><?php i18n("Spread the Word and See What's Happening: Tag as &quot;KDE&quot;");?></h2>
<p>
<?php i18n("KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.13 releases of KDE software.");?>
</p>
<p>
<?php i18n("You can discuss this release on <a href='http://dot.kde.org/2014/04/16/kde-releases-applications-and-development-platform-413'>our news site</a>.");?>
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.13/&amp;title=KDE%20releases%20version%204.13%20of%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.13/" data-text="#KDE releases version 4.13 of Applications and Platform →" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.13/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.13%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.13/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde412"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde412"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde412"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>

<h2><?php i18n("About these release announcements");?></h2>
<p>
<?php i18n("These release announcements were prepared by the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past four months.");?>
</p>

<?php
  include("footer.inc");
?>
