<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.27.0");
  $site_root = "../";
  $release = '5.27.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
October 08, 2016. KDE today announces the release
of KDE Frameworks 5.27.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<p>New mimetypes icons.</p>
<img width=640 height=400 src="kf-5.27-mimetypes-icons.png">

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Use correct config entry in autostart condition");?></li>
<li><?php i18n("Fix sorted insert (aka flat_map like insert) (bug 367991)");?></li>
<li><?php i18n("Add missing close env, as pointed out by Loïc Yhuel (bug 353783)");?></li>
<li><?php i18n("Transaction not created =&gt; don't try to abort them");?></li>
<li><?php i18n("fix missing m_env = nullptr assignment");?></li>
<li><?php i18n("Make e.g. Baloo::Query thread safe");?></li>
<li><?php i18n("On 64-bit systems baloo allows now &gt; 5 GB index storage (bug 364475)");?></li>
<li><?php i18n("Allow ctime/mtime == 0 (bug 355238)");?></li>
<li><?php i18n("Handle corruption of index database for baloo_file, try to recreate the database or abort if that fails");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Fix crash when trying to add device to unknown adapter (bug 364416)");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("New mimetypes icons");?></li>
<li><?php i18n("Update some kstars icons (bug 364981)");?></li>
<li><?php i18n("Wrong style actions/24/format-border-set (bug 368980)");?></li>
<li><?php i18n("Add wayland app icon");?></li>
<li><?php i18n("Add xorg app icon (bug 368813)");?></li>
<li><?php i18n("Revert distribute-randomize, view-calendar + reapply the transform fix (bug 367082)");?></li>
<li><?php i18n("Change folder-documents from one file to the multiple file cause in a folder more than one file is included (bug 368224)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Make sure we don't add the appstream test twice");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Sorting activities in the cache alphabetically by name (bug 362774)");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Many changes to the overall layout of the generated API docs");?></li>
<li><?php i18n("Correct tags path, depending of if the lib is part of a group or not");?></li>
<li><?php i18n("Search: Fix href of libraries which are not part of a group");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Fix memory leak with KTar's KCompressionDevice");?></li>
<li><?php i18n("KArchive: fix memory leak when an entry with the same name already exists");?></li>
<li><?php i18n("Fix memory leak in KZip when handling empty directories");?></li>
<li><?php i18n("K7Zip: Fix memory leaks on error");?></li>
<li><?php i18n("Fix memory leak detected by ASAN when open() fails on the underlying device");?></li>
<li><?php i18n("Remove bad cast to KFilterDev, detected by ASAN");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("Add missing export macros on classes Decoder and Encoder");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Fix memory leak in SignalsTestNoSingletonDpointer, found by ASAN");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Register QPair&lt;QString,QString&gt; as metatype in KJobTrackerInterface");?></li>
<li><?php i18n("Don't convert as url an url which has a double-quote character");?></li>
<li><?php i18n("Windows compile fix");?></li>
<li><?php i18n("Fix very old bug when we remove space in url as \"foo &lt;&lt;url&gt; &lt;url&gt;&gt;\"");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("CMake option KCRASH_CORE_PATTERN_RAISE to forward to kernel");?></li>
<li><?php i18n("Change default log level from Warning to Info");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Cleanup. Do not install includes that point to non-existing includes and also remove those files");?></li>
<li><?php i18n("Use more correct and with c++11 available std::remove_pointer");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Fix 'checkXML5 prints generated html to stdout for valid docbooks' (bug 369415)");?></li>
<li><?php i18n("Fix bug not been able to run native tools in package using cross compiled kdoctools");?></li>
<li><?php i18n("Setup targets for cross compiling running kdoctools from other packages");?></li>
<li><?php i18n("Add cross compiling support for docbookl10nhelper");?></li>
<li><?php i18n("Add cross compile support for meinproc5");?></li>
<li><?php i18n("Convert checkxml5 into a qt executable for cross platform support");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Improve epub extractor, less segfaults (bug 361727)");?></li>
<li><?php i18n("Make odf indexer more error prove, check if the files are there (and are files at all) (meta.xml + content.xml)");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Fix KIO slaves using only tls1.0");?></li>
<li><?php i18n("Fix ABI break in kio");?></li>
<li><?php i18n("KFileItemActions: add addPluginActionsTo(QMenu *)");?></li>
<li><?php i18n("Show copy buttons only after checksum has been calculated");?></li>
<li><?php i18n("Add missing feedback when computing a checksum (bug 368520)");?></li>
<li><?php i18n("Fix KFileItem::overlays returning empty string values");?></li>
<li><?php i18n("Fix launching terminal .desktop files with konsole");?></li>
<li><?php i18n("Classify nfs4 mounts as probablySlow, like nfs/cifs/..");?></li>
<li><?php i18n("KNewFileMenu: show New Folder action shortcut (bug 366075)");?></li>
</ul>

<h3><?php i18n("KItemViews");?></h3>

<ul>
<li><?php i18n("In listview mode use the default implementation of moveCursor");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Add KAuthorized checks to allow disabling of ghns in kdeglobals (bug 368240)");?></li>
</ul>

<h3><?php i18n("Package Framework");?></h3>

<ul>
<li><?php i18n("Don't generate appstream files for components that are not in rdn");?></li>
<li><?php i18n("Make kpackage_install_package work with KDE_INSTALL_DIRS_NO_DEPRECATED");?></li>
<li><?php i18n("Remove unused var KPACKAGE_DATA_INSTALL_DIR");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Fix URLs with a trailing slash being always assumed to be directories");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Fix ASAN build (duplicates.cpp uses KPeople::AbstractContact which is in KF5PeopleBackend)");?></li>
</ul>

<h3><?php i18n("KPty");?></h3>

<ul>
<li><?php i18n("Use ECM path to find utempter binary, more reliable than simple cmake prefix");?></li>
<li><?php i18n("Call the utempter helper executable manually (bug 364779)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("XML files: remove hard-coded color for values");?></li>
<li><?php i18n("XML: Remove hard-coded color for values");?></li>
<li><?php i18n("XML Schema Definition: Turn 'version' into an xs:integer");?></li>
<li><?php i18n("Highlighting definition files: round version up to next integer");?></li>
<li><?php i18n("support multi char captures only in {xxx} to avoid regressions");?></li>
<li><?php i18n("Support regular expressions replaces with captures &gt; \9, e.g. \111 (bug 365124)");?></li>
<li><?php i18n("Fix rendering of characters spanning into next line, e.g. underlines are no longer cut off with some fonts/font-sizes (bug 335079)");?></li>
<li><?php i18n("Fix crash: Make sure the display cursor is valid after text folding (bug 367466)");?></li>
<li><?php i18n("KateNormalInputMode needs to rerun SearchBar enter methods");?></li>
<li><?php i18n("try to \"fixup\" rendering of underlines and stuff like that (bug 335079)");?></li>
<li><?php i18n("Show \"View Difference\" button only, if 'diff' is installed");?></li>
<li><?php i18n("Use non-modal message widget for externally modified file notifications (bug 353712)");?></li>
<li><?php i18n("fix regression: testNormal did only work because of test execution at once");?></li>
<li><?php i18n("split the indent test into separate runs");?></li>
<li><?php i18n("Support \"Unfold Toplevel Nodes\" action again (bug 335590)");?></li>
<li><?php i18n("Fix crash when showing top or bottom messages multiple times");?></li>
<li><?php i18n("fix eol setting in mode lines (bug 365705)");?></li>
<li><?php i18n("highlight .nix files as bash, guess can't hurt (bug 365006)");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Check whether kwallet is enabled in Wallet::isOpen(name) (bug 358260)");?></li>
<li><?php i18n("Add missing boost header");?></li>
<li><?php i18n("Remove duplicate search for KF5DocTools");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("[server] Don't send key release for not pressed keys and no double key press (bug 366625)");?></li>
<li><?php i18n("[server] When replacing the clipboard selection previous DataSource needs to be cancelled (bug 368391)");?></li>
<li><?php i18n("Add support for Surface enter/leave events");?></li>
<li><?php i18n("[client] Track all created Outputs and add static get method");?></li>
</ul>

<h3><?php i18n("KXmlRpcClient");?></h3>

<ul>
<li><?php i18n("Convert categories to org.kde.pim.*");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("We need to set the state during initialization");?></li>
<li><?php i18n("Replace all blocking calls for initialization with just one blocking call");?></li>
<li><?php i18n("Use standard o.f.DBus.Properties interface for PropertiesChanged signal for NM 1.4.0+ (bug 367938)");?></li>
</ul>

<h3><?php i18n("Oxygen Icons");?></h3>

<ul>
<li><?php i18n("Remove invalid directory from index.theme");?></li>
<li><?php i18n("Introduce dupe test from breeze-icons");?></li>
<li><?php i18n("Convert all duplicated icons into symlinks");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Improve timetracker output");?></li>
<li><?php i18n("[ToolButtonStyle] Fix menu arrow");?></li>
<li><?php i18n("i18n: handle strings in kdevtemplate files");?></li>
<li><?php i18n("i18n: review strings in kdevtemplate files");?></li>
<li><?php i18n("Add removeMenuItem to PlasmaComponents.ContextMenu");?></li>
<li><?php i18n("update ktorrent icon (bug 369302)");?></li>
<li><?php i18n("[WindowThumbnail] Discard pixmap on map events");?></li>
<li><?php i18n("Don't include kdeglobals when dealing with a cache config");?></li>
<li><?php i18n("Fix Plasma::knownLanguages");?></li>
<li><?php i18n("resize the view just after setting the containment");?></li>
<li><?php i18n("Avoid creating a KPluginInfo from a KPluginMetaData instance");?></li>
<li><?php i18n("running tasks must have some indicator");?></li>
<li><?php i18n("task bar lines according to RR 128802 marco give the ship it");?></li>
<li><?php i18n("[AppletQuickItem] Break from loop when we found a layout");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.27");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.5");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
