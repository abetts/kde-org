<?php
  $page_title = "KDE 3.1.0 to 3.1.1 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.1 and 3.1.1 releases.
</p>
<p>
Please see the <a href="changelog3_0_5to3_1.php">3.0.5 to 3.1 changelog</a> for further information.
</p>

<h3>arts</h3>
<ul>
<li>Several memory corruption fixes.</li>
</ul>

<h3>kdelibs</h3>
<ul>
<li>kdialog: Fix screen numbering problem for centerOnScreen() static method
<li>kprogress: Fix math problem in displaying correct percentage for large numbers</li>
<li>kio_http: Fix data processing for webdav(s) listing of directories and files</li>
<li>kate: Many small bugfixes, including:
	<ul>
		<li>Fixed code completion drop-down box position</li>
		<li>Fixed "wrap cursor disabled" bugs</li>
		<li>Fixed vertical scrollbar middle mouse behaviour</li>
		<li>Fixed remove whitespace feature</li>
    <li>Now clears the redo history when it is irrelevant</li>
    <li>Fixed crash after starting up with a non-existant directory in the file selector history</li>
	</ul>
</li>
<li>kparts: Fix extension of transparently downloaded files, this fixes ark
(used to display temp file instead of archive content)
</li>
<li>klauncher: Fixed support for "Path=" entry in .desktop files.
This entry can be used to specify a working directory.
</li>
<li>kio: Don't let ChModJob's -X emulation interfere with mandatory file locking.</li>
<li>kdeui: Fix for alternate background coloring in Konqueror list views.</li>
<li>kdeui: Fix to prevent an event loop in conjunction with Qt 3.1.2.</li>
<li>kio/bookmarks: Properly handle multiple shown bookmarks with the same URL; fixes crash on exit in
Konqueror when bookmarkbar is on and some bookmarks points to the same place</li>
<li>kstyles: Handle focus indicators on label-less checkboxes better</li>
<li>kdeprint: Don't freeze when there is no route to the selected CUPS server</li>
<li>SSL: add support for OpenSSL 0.9.7</li>
<li>SSL: ADH ciphers are now explicitly disabled in all cases</li>
<li>SSL: new CA root certificate added</li>
<li>Several Xinerama related fixes</li>
<li>QXEmbed fixes for various situations that don't handle XEMBED well</li>
<li>Java on IRIX with SGI 1.2 VM is fixed</li>
<li>khtml: Several major bugfixes, partially incorporated fixes from Safari as well.</li>
</ul>

<h3>kdeaddons</h3>
<ul>
</ul>

<h3>kdeadmin</h3>
<ul>
<li>Linux Kernel Configurator: Add details about the lightbulb icon's meaning</li>
<li>Linux Kernel Configurator: Support for more incorrect kernel configuration files</li>
</ul>

<h3>kdeartwork</h3>
<ul>
</ul>

<h3>kdebase</h3>
<ul>
<li>kcmshell: Restored backwards compatibility wrt KDE 3.0 module names</li>
<li>klipper: Escape "&amp;" everywhere.</li>
<li>konsole:
	<ul>
		<li>Removed "get prompt back"-hacks, don't assume emacs key shell bindings.</li>
		<li>Fixed usage of background images with spaces in the filename.</li>
		<li>Profile support fixes (disabled menubar etc.)</li>
		<li>Bookmarks invoked from "Terminal Sessions" kicker menu now set correct title.</li>
		<li>Fixed a problem with the "Linux" font that prevented it from being used with fontconfig.</li>
	</ul>
</li>
<li>kdesktop: Made desktop menus pop up in unused Kicker space work.</li>
<li>kicker: Fixed empty taskbar sometimes showing scroll buttons.</li>
<li>konqueror:
    <ul>
        <li>Various startup performance improvements</li>
	<li>Fix crash when sidebar tree expands hidden directory</li>
	<li>Fix crash when config button hidden from config button's menu</li>
	<li>Extensive fixes to Netscape plugins, fixing crashes and plugin support</li>
	<li>Changes to default preview settings, making the defaults safer on various platforms</li>
    </ul>
</li>
<li>Java configuration module: Make it possible to properly configure Java in all cases</li>
<li>Previews: Fixed a privacy issue where previews of HTML files could access files or hosts on the network.</li>
</ul>

<h3>kdebindings</h3>
<ul>
<li>Removing Qt bindings for private interfaces that were accidentally
    included and now caused compilation failures with Qt 3.1.2.</li>
<li>Several other build related fixes.</li>
</ul>

<h3>kdeedu</h3>
<ul>
<li>KStars:
	<ul>
		<li>Fixed bug #51708: No longer exits if starting position
		    is below horizon (only affected some systems)</li>
		<li>Fixed bug #52205: Country of Lhasa is China, not Tibet.
		<li>Fixed too-narrow coordinates field in statusbar.</li>
		<li>Fixed bug in "length of day" calculator module; it
		    now properly accounts for latitude and longitude</li>

	</ul>
</li>
</ul>

<h3>kdegames</h3>
<ul>
<li>Atlantik: Many small bugfixes, including:
	<ul>
		<li>Better handling of incoming messages</li>
		<li>Fixed token animation</li>
	</ul>
</li>
<li>kbackgammon: Common crash fix.</li>
</ul>

<h3>kdegraphics</h3>
<ul>
<li>KIconEdit: Fix the ellipse/circle tool not to leave any "holes" in the drawings</li>
<li>Kooka: Some UI crashes fixed</li>
<li>KViewShell: Default paper size is fixed</li>
<li>KGhostView: Fixed wheel-mouse scrolling</li>
</ul>

<h3>kdemultimedia</h3>
<ul>
<li>KsCD:
    <ul>
        <li>Stopped KsCD from pausing after tracks in random mode</li>
        <li>Correctly associate extra CDDB information with tracks</li>
        <li>Support non-Latin encodings properly in CDDB entries and elsewhere</li>
        <li>Proper systemtray behaviour</li>
        <li>Updated key accel code to avoid depricated calls</li>
    </ul>
</li>
<li>Movie previews have been removed due to severe unresolved stability problems</li>
</ul>

<h3>kdenetwork</h3>
<ul>
<li>Desktop Sharing server (krfb):
    <ul>
        <li>fix problems on X11 servers with 8 bit depth</li>
        <li>fix problems on X11 servers with big-endian framebuffer</li>
        <li>allow X11 servers without XShm (thin clients). Warning: requires a lot of bandwidth</li>
        <li>remove read timeouts. This should solve problems with some clients that got disconnected after a minute of inactivity (but increases the time to detect dead clients)</li>
        <li>fix problem with clients that support both RichCursor and SoftCursor encodings (like krdc from HEAD)</li>
    </ul>
</li>
<li>Desktop Sharing client (krdc):
    <ul>
        <li>fix: when an error occurred in fullscreen krdc did not restore the original resolution</li>
        <li>fix: krdc stopped to repaint the framebuffer after a disconnect while the error dialog was displayed</li>
        <li>the quality setting in medium quality mode has been increased because the original setting looked too bad with Keramik</li>
    </ul>
</li>
</ul>

<h3>kdepim</h3>
<ul>
<li>KOrganizer bug fixes:
  <ul>
    <li>Use correct default duration for events crossing a day boundary (#53477).</li>
    <li>Correctly save category colors (#54913).</li>
    <li>Don't show todos more than once in what's next view.</li>
    <li>Include todos in print output of month view (#53291).</li>
    <li>Don't restrict maximum size of search dialog (#54912).</li>
    <li>Make cancel button of template selection dialog work (#54852).</li>
    <li>Don't break sorting when changing todos by context menu (#53680).</li>
    <li>Update views on changes of todos directly in the todo list (#43162).</li>
    <li>Save state of statusbar (#55380).</li>
  </ul>
</li>
<li>knotes: Escape "&amp;" in note titles</li>
</ul>

<h3>kdesdk</h3>
<ul>
<li>Cervisia:
	<ul>
		<li>Fixed line break in protocol view</li>
		<li>Fixed timestamp for files that are not in cvs (#55053)</li>
		<li>Fixed handling of Cervisia's options like 'Update Recursively' when
		run as embedded part in konqueror (#55665)</li>
	</ul>
</li>
</ul>

<h3>kdetoys</h3>
<ul>
<li>kworldclock: Fixed that all clocks show the same time.</li>
<li>kweather: Made it work again with non-english locales. (#52147)</li>
<li>kweather: Prevent KWeather from looping and freezing Kicker when not connected
              to the internet.(#49191)</li>
</ul>

<h3>kdeutils</h3>
<ul>
<li>klaptopdaemon: Fix serious stalling problems on GNU/Linux with ACPI</li>
<li>kcalc: Now a KMainWindow instead of a KDialog to fix various UI inconsistencies</li>
<li>kdf: Support escapes in fstab</li>
</ul>

<h3>quanta</h3>
<ul>
<li>Bugfixes:
  <ul>
    <li>Allow resizing of the main window even with large user toolbars [#53230]
    <li>Insert valid DTD definitions [#53274]</li>
    <li>Honor the View Default settings from Settings-&gt;Configure Editor [#53569]</li>
    <li>Be less braindead regarding the Show DTD Toolbar setting [#53739]</li>
    <li>Be able to select also directories in tag dialogs [#54819]</li>
    <li>Do not complain about text files being binary ones on a system with broken mimetypes [#54924]</li>
    <li>Bring up the "File Changed" dialog only, when the file content has changed[#55678]</li>
    <li>Select Tag Area behaviour fixed for optional tags</li>
    <li>Insert non-translated string in CSS code parts</li>
    <li>Insert "border-top", "border-right", etc. correctly in CSS</li>
    <li>Don't quote the script line more than once in the action configuration dialog</li>
    <li>Memory leak fixed: editor parts were not deleted when a file was closed</li>
    <li>Fix insertion of "img" tags in HTML documents</li>
    <li>Upload/rescan project/add to new project tree view behaviour fixed</li>
    <li>Fix renaming of file in the Project Tree, when a file with the new name was already present in the project</li>
    <li>Rename only what has to be renamed in the project and enable project saving after a rename</li>
    <li>Use the correct encoding for newly created files</li>
    <li>Saving of Author and E-Mail project options was broken in some cases</li>
    <li>Fix the numbering of new documents</li>
    <li>Fix the execution of actions</li>
    <li>Fix crash when deleting an action</li>
    <li>Insert valid single tags from the toolbar</li>
  </ul>
</li>
<li>Enhancements:
  <ul>
    <li>Show the tag attributes (Alt-Down) menu lower than the current line</li>
    <li>"Insert in cursor position" for script actions replaces the selection if there was some text selected</li>
    <li>New DCOP interface (WindowManagerIf) added to enable the modification of the opened documents from a script</li>
    <li>DTD for Quanta tags (DTD definition) added</li>
    <li>XHTML 1.0 Strict DTD added</li>
  </ul>
</li>

</ul>

<?php include "footer.inc" ?>
