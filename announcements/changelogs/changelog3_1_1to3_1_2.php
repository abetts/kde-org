<?php
  $page_title = "KDE 3.1.1 to 3.1.2 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 3.1.1 and 3.1.2 releases.
</p>
<p>
Please see the <a href="changelog3_1to3_1_1.php">3.1 to 3.1.1 changelog</a> for further information.
</p>

<h3>arts</h3>
<ul>
</ul>

<h3>kdelibs</h3>
<ul>
<li>https authentication through proxy fixed.</li>
<li>KZip failed for some .zip archives.</li>
<li>Fixed a bug in socket code that made KDEPrint crash.</li>
<li>kspell: Support for Hebrew spell checking using <a href="http://www.ivrix.org.il/projects/spell-checker/">hspell</a> (requires hspell 0.5).</li>
</ul>

<h3>kdeaddons</h3>
<ul>
</ul>

<h3>kdeadmin</h3>
<ul>
</ul>

<h3>kdeartwork</h3>
<ul>
<li>Fixed a potential printf() format string attack in the slideshow screensaver</li>
</ul>

<h3>kdebase</h3>
<ul>
<li>kwin: Alt+Tab works while drag'n'drop (unless the application also
grabs keyboard).</li>
<li>kio_smtp: Doesn't eat 100% CPU when connection breaks and fixed a confusing error message when AUTH failed
(&quot;Unknown Command&quot; vs. &quot;Authorization failed&quot;)</li>
<li>kscreensaver: Fixed issue where kdesktop_lock would stay running indefinitely in the background if it could not grab the keyboard/mouse, preventing the screen from being locked manually.</li>
<li>kscreensaver: Screensavers are now stopped when asking for the password [#<a href="http://bugs.kde.org/show_bug.cgi?id=56803">56803</a>]</li>
<li>kio_smb: Several bugfixes for the smbro-ioslave.</li>
<li>kdesktop: fixed minicli layout problem with Qt 3.1.2</li>
<li>kdm: fixed incorrect user window width with Qt 3.1.2</li>
<li>Konqueror: Create DCOP interface for mainwindow when object begins to exist.</li>
<li>Konqueror: Fixed tab open delays when it can't reach website.</li>
<li>Konsole: Don't flicker when selecting entire lines.</li>
<li>Konsole: Crash, selection and sort fixes in schema and session editors.</li>
<li>Konsole: Fixed mouse-wheel in mouse mode.</li>
<li>Konsole: Allow programs to resize windows if enabled.</li>
<li>Konsole: Keep output steady when triple-click selecting.</li>
<li>Konsole: Added "Print" menu command.</li>
<li>kicker: Fixed kasbar only drawing last icon from a group.</li>
</ul>

<h3>kdebindings</h3>
<ul>
</ul>

<h3>kdeedu</h3>
<ul>
</ul>

<h3>kdegames</h3>
<ul>
</ul>

<h3>kdegraphics</h3>
<ul>
<li>kghostview: Better handling of half-broken Postscript and PDF files [#<a href="http://bugs.kde.org/show_bug.cgi?id=44855">44855</a>]</li>
<li>kghostview: Fix the opening of files on the command line, which was not working if the paths contained non-alphanumeric characters or were absolute paths [#<a href="http://bugs.kde.org/show_bug.cgi?id=56169">56169</a> and #<a href="http://bugs.kde.org/show_bug.cgi?id=51533">51533</a>]</li>
<li>kghostview: Work around -dMaxBitmap bug in gs version 6.5x [#<a href="http://bugs.kde.org/show_bug.cgi?id=37287">37287</a>]</li>
<li>kghostview: Reset orientation and paper size selectors after opening a new document [#<a href="http://bugs.kde.org/show_bug.cgi?id=51014">51014</a>]</li>
<li>kghostview: Security fix for #<a href="http://bugs.kde.org/show_bug.cgi?id=56808">56808</a>. The security patch which was present in version 3.1.1a caused problems for some users and has been corrected [#<a href="http://bugs.kde.org/show_bug.cgi?id=57441">57441</a>]</li>
<li>kghostview: ghostscript version 8 is now supported [#<a href="http://bugs.kde.org/show_bug.cgi?id=53343">53343</a>]</li>
</ul>

<h3>kdemultimedia</h3>
<ul>
<li>Ogg Vorbis Info List view column titles were incorrect.</li>
<li>kmix: Vertical label widget to sliders in replacement of large horizontal labels</li>
<li>kmix: Fixed alsa 0.9x to restore current volumes</li>
<li>kmix: Fixed multiple alsa 0.9x cards detection</li>
</ul>
54325
<h3><a name="kdenetwork">kdenetwork</a></h3>
<ul>
<li>KMail: Don't select multiple folders during keyboard navigation</li>
<li>KMail: Never display vCards inline</li>
<li>KMail: Make new mail notification work for people who run KMail without KDE</li>
<li>KMail: Improved URL highlighting</li>
<li>KMail: Properly determine SSL/TLS state for non-transport sending (bug <a href="http://bugs.kde.org/show_bug.cgi?id=49902">49902</a>)</li>
<li>KMail: Draw a frame around text attachments which are displayed inline</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=55377">55377</a> (Hiding attachments causes HTML messages to be renderend as HTML code)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=56049">56049</a> (wrong encoding of command line arguments)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=53665">53665</a> (Error when reply to a HTML message)</li>
<li>KMail: Use the correct charset for replies to PGP/MIME encrypted messages</li>
<li>KMail: Fix the bug which broke the signature of all PGP/MIME signed messages with attachments</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=56393">56393</a> (kmail crashes when I try change the name of an imap account)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=56570">56570</a> (kmail doesn't show non-mime Japanese message)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=56592">56592</a> (Displaying folded Content-Description in MIME tree viewer is broken)</li>
<li>KMail: Disable external editor when no external editor is specified</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=53889">53889</a> (IMAP: Kmail crashes after authorization-dialog)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=56930">56930</a> (BCC, No EMail-List expansion)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=42646">42646</a> (multipart/digest forwarding is broken - uses empty boundary)</li>
<li>KMail: Always make sure that the text body of the message ends with a linefeed. This fixes interoperatibility problems with other OpenPGP compliant mail clients.</li>
<li>KMail: Prevent the user from trying to move local folders to IMAP servers as the user might lose the folders if he tries it.</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=57660">57660</a> ('send again' does not copy the BCC address)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=56321">56321</a> (More whitespace in read/unread mails column)</li>
<li>KMail: Tell gpg explicitely not to use gpg-agent if it's apparently not available. Prevents weird "Wrong passphrase" error message.</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=57016">57016</a> (pgp signature is wrong displayed)</li>
<li>KMail: Fix bug <a href="http://bugs.kde.org/show_bug.cgi?id=57809">57809</a> (kmail segfaults when checking for new mail if fcntl locking is used)</li>
<li>Desktop Sharing server (krfb): Compile fixes for systems without IPv6</li>
<li>Desktop Sharing client (krdc): fix: client crashed sometimes while connecting on XFree 4.3</li>
<li>Desktop Sharing client (krdc): fix: resize the right screen resolution in multi-screen setups</li>
</ul>

<h3>kdepim</h3>
<ul>
<li>korganizer: Exchange plugin supports secure WebDAV.</li>
<li>korganizer: Fix timezone handling when timezone names are Unicode strings.</li>
</ul>

<h3>kdesdk</h3>
<ul>
<li>kbabel: Splash screen can be clicked and does not cover error messages anymore.</li>
</ul>

<h3>kdetoys</h3>
<ul>
</ul>

<h3>kdeutils</h3>
<ul>
<li>kedit: Save immediately when Settings/Save Settings is called.</li>
<li>khexedit: Fixed insertion of local files.</li>
</ul>

<h3>quanta</h3>
<ul>
<li>Bugfixes:
  <ul>
    <li>more accurate selection of modified files in the upload dialog [#<a href="http://bugs.kde.org/show_bug.cgi?id=55988">55988</a>]</li>
    <li>fix execution of script actions which does not have any argument [#<a href="http://bugs.kde.org/show_bug.cgi?id=56211">56211</a>]</li>
    <li>closing the files (and closing Quanta) isn't slow anymore [#<a href="http://bugs.kde.org/show_bug.cgi?id=56233">56233</a>]</li>
    <li>fix shortcut for Color dialog [#<a href="http://bugs.kde.org/show_bug.cgi?id=56235">56235</a>]</li>
    <li>store the upload options in the project file [#<a href="http://bugs.kde.org/show_bug.cgi?id=56237">56237</a>]</li>
    <li>fix the Insert/Overwrite mode handling [#<a href="http://bugs.kde.org/show_bug.cgi?id=56382">56382</a>]</li>
    <li>store and use the spell checking settings [#<a href="http://bugs.kde.org/show_bug.cgi?id=56561">56561</a>]</li>
    <li>show files with ":" in the name correctly in the Project Tree [#<a href="http://bugs.kde.org/show_bug.cgi?id=56639">56639</a>]</li>
    <li>show the directory selection dialogs in the project options [#<a href="http://bugs.kde.org/show_bug.cgi?id=56698">56698</a>]</li>
    <li>don't deny opening of empty local files [#<a href="http://bugs.kde.org/show_bug.cgi?id=57718">57718</a>]</li>
    <li>allow selection of empty directories in Rescan/Upload/New project dialogs [#<a href="http://bugs.kde.org/show_bug.cgi?id=56778">56778</a>]</li>
    <li>don't close a modified document if saving has failed [#<a href="http://bugs.kde.org/show_bug.cgi?id=58013">58013</a>]</li>
    <li>don't truncate the file after a preview [#<a href="http://bugs.kde.org/show_bug.cgi?id=58080">58080</a>]</li>
    <li>fix ocassional crash when pressing Ctrl-H while viewing the documentation</li>
    <li>fix user toolbar handling</li>
    <li>fix the height of the toolbar tab</li>
    <li>fix failure of re-opening a file from File->Open Recent</li>
    <li>do not crash when accessing the Plugins menu after a plugin was removed, but the Edit dialog was closed with Cancel</li>
    <li>don't use the preview prefix after the project is closed</li>
    <li>fix Quanta tagxml DTD</li>
    <li>fix the DTD tag files, add warning if they contain syntax errors</li>
    <li>use the Attribute Quotation setting</li>
  </ul>
</li>
<li>Enhancements:
  <ul>
    <li>bring up the message output window, when an action/plugin wants to printed some message [#<a href="http://bugs.kde.org/show_bug.cgi?id=55645">55645</a>]</li>
    <li>show files with relative path to the current document in "url" autocompletion [#<a href="http://bugs.kde.org/show_bug.cgi?id=55989">55989</a>]</li>
    <li>switch to the first editable widget when the tag editing dialog appears</li>
    <li>insert &lt;em> and &lt;strong> instead of &lt;i> and &lt;b> in HTML documents</li>
    <li>new DCOP methods added to the WindowManagerIf</li>
    <ul>
      <li><em>QString</em> projectURL()</li>
      <li><em>QStringList</em> openedURLs()</li>
    </ul>
    <li>DocBook 4.2 DTD added</li>
  </ul>
</li>
<li><strong>Kommander:</strong>
  <ul>
    <li>do not add the .kmdr extension to files ending with .kmdr</li>
    <li>remove some warning/error messages polluting the output</li>
    <li>flush the stdout buffer after writing to it</li>
  </ul>
</li>
</ul>

<?php include "footer.inc" ?>
