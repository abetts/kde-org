2006-03-25 19:22 +0000 [r522490]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/url.cpp:
	  Backport: Fix a typo that would cause a bad comparison of
	  domains. Thanks for the great debugger tree of KDevelop.

2006-03-28 23:26 +0000 [r523745]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/sessionwidget.cpp:
	  Backport: Remember to load settings when start another check in
	  the same session.

2006-03-29 11:46 +0000 [r523862]  adridg

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/searchmanager.cpp:
	  ?: needs the same type for arguments 2 and 3; compile in the face
	  of NO_CAST_ASCII. CCMAIL: kde@freebsd.org

2006-04-18 20:18 +0000 [r531252]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/treeview.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/url.cpp:
	  Backport: - Resolve entities in URIs. - Show decoded URLs.

2006-04-20 12:20 +0000 [r531837]  reed

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/utils/Makefile.am: use
	  the cflags libxslt gives us, not everyone has it in /usr ;)

2006-04-22 10:10 +0000 [r532620]  amantia

	* branches/KDE/3.5/kdewebdev/kommander/widgets/closebutton.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/components/debugger/dbgp/dbgpnetwork.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/components/debugger/gubed/quantadebuggergubed.cpp:
	  Use delete[] where needed.

2006-04-22 13:40 +0000 [r532674]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/resultview.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/treeview.h,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/parser/htmlparser.cpp:
	  Commit Christoph suggestions.

2006-04-22 13:45 +0000 [r532676]  mojo

	* branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/resultview.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/ui/treeview.cpp,
	  branches/KDE/3.5/kdewebdev/klinkstatus/src/engine/linkchecker.h:
	  Finnish previous commit.

2006-04-22 14:14 +0000 [r532680]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/project/projectupload.cpp:
	  Rework code according to Christoph's suggestion: the loop is not
	  needed.

2006-04-22 18:09 +0000 [r532764-532762]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/treeviews/templatestreeview.cpp:
	  Fixed "suspicious code": & !QDir::Hidden is wrong and not needed.

	* branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp:
	  Fixed "suspicious code": initialize dbg to 0L, not to itself.

2006-04-22 18:18 +0000 [r532770-532767]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/node.cpp: Fixed
	  "suspicious code": these part of the code doesn't make sense if
	  there is no parent for the current node.

	* branches/KDE/3.5/kdewebdev/quanta/components/tableeditor/tableeditor.cpp:
	  Fixed "suspicious code": use && instead of &.

2006-04-22 18:44 +0000 [r532779]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkacommon.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/wkafkapart.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/htmldocumentproperties.cpp:
	  Fixed "suspicious code": various NULL pointer access fixes.

2006-04-22 18:58 +0000 [r532781]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/components/debugger/dbgp/dbgpnetwork.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/project/project.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parsers/dtd/dtdparser.cpp:
	  Fixed "suspicious code": NULL pointer access fixes and fix a
	  boolean expression handling bug.

2006-04-22 19:28 +0000 [r532784]  amantia

	* branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/files_unix.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/editor/formfile.cpp,
	  branches/KDE/3.5/kdewebdev/kimagemapeditor/kimedialogs.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/libxsldbg/debugXSL.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgdebugger.cpp,
	  branches/KDE/3.5/kdewebdev/kommander/widgets/subdialog.cpp,
	  branches/KDE/3.5/kdewebdev/kxsldbg/kxsldbgpart/xsldbgconfigimpl.cpp:
	  The last round of "suspicious code" fixes.

2006-04-24 09:34 +0000 [r533259-533258]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkacommon.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/utility/tagaction.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkahtmlpart.cpp:
	  Fix some misbehavior in VPL when using the toolbars (it inserted
	  <strong strong="true"> instead of <strong>). Avoid some crashes
	  in VPL.

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp: Hopefully this
	  will avoid a crash on exit.

2006-04-24 09:47 +0000 [r533263]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parsers/tag.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkacommon.cpp: I
	  hate asserts. Use Q_ASSERT and a guard instead.

2006-04-24 09:52 +0000 [r533264]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkacommon.cpp:
	  Fix compilation.

2006-05-19 10:04 +0000 [r542425]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkacommon.cpp:
	  Avoid some more crashes in VPL. The real crash-avoider fix is in
	  KHTML.

2006-05-19 10:26 +0000 [r542437]  amantia

	* branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/VERSION,
	  branches/KDE/3.5/kdewebdev/quanta/src/quanta.h,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: 3.5.2->3.5.3

2006-05-19 12:08 +0000 [r542467]  amantia

	* branches/KDE/3.5/kdewebdev/lib/qextfileinfo.cpp: QDir::cdUp does
	  not work when the path does not exist, so use KURL. Fixes
	  #126314. BUG: 126314

2006-05-20 13:22 +0000 [r542826]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/src/quanta.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/project/projectprivate.cpp: -
	  silently ignore files from a project view that do not exist
	  anymore - show a correct error message if a file does not exist
	  BUG: 126588

2006-05-20 13:55 +0000 [r542833]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/data/dtep/html-strict/s.tag,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/xhtml-frameset/img.tag,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/html-strict/i.tag,
	  branches/KDE/3.5/kdewebdev/quanta/scripts/htmlquickstart.kmdr,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog,
	  branches/KDE/3.5/kdewebdev/quanta/data/dtep/xhtml/img.tag: Make
	  the img and script tags standard compliant. BUG: 125596

2006-05-20 14:04 +0000 [r542834]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkacommon.cpp,
	  branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Don't loose
	  important spaces when applying source indentation. BUG: 125213

2006-05-20 14:59 +0000 [r542905]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/data/icons/22x22/Makefile.am,
	  branches/KDE/3.5/kdewebdev/quanta/data/toolbars/html/forms.toolbar.tgz,
	  branches/KDE/3.5/kdewebdev/quanta/data/icons/22x22/button.png
	  (added), branches/KDE/3.5/kdewebdev/quanta/ChangeLog: Add input
	  button to the Forms toolbar. BUG: 125202

2006-05-22 18:35 +0000 [r543766]  amantia

	* branches/KDE/3.5/kdewebdev/quanta/parts/kafka/kafkacommon.cpp:
	  Comment out unused variable. BUG: 120897

