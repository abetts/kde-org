<?php
  $page_title = "KDE 2.1 to 2.1.1 Changelog";
  $site_root = "../../";
  include "header.inc";
?>

<p>
This page tries to present as much as possible of the problem
corrections that occurred in KDE between the 2.1 and 2.1.1 releases.
The primary goals of the 2.1.1 release being more complete documentation
and translations, the amount of code change is quite minimal - only the
most critical bugs have been fixed, the rest of the development is
currently being done in the branch that will lead to the 2.2 release.
</p>

<h3>General</h3>
<ul>
  <li>Fixed Chinese support (GBK/GB2312)</li>
  <li>New language: Lithuanian</li>
  <li>Many improvements to translations, documentation and icons</li>
  <li>Removed old support for problematic "kdefonts" file</li>
</ul>

<h3>Konqueror</h3>
<ul>
  <li>Security: don't store typed passwords in the completion/history</li>
  <li>KHTML: Support for layers</li>
  <li>KHTML: Middle-mouse button opens URL copied to clipboard</li>
  <li>KHTML: Fix for "Find text" not able to switch frames</li>
  <li>KHTML: Removed incompatibility between PHP and khtml's multipart post</li>
  <li>KHTML: All reported crashes fixed</li>
  <li>KHTML: Smaller fixes (parser, renderer, forms)</li>
  <li>HTTP: Better support for error pages</li>
  <li>HTTP: Fix for broken images due to wrong Accept header</li>
  <li>UserAgent configuration: Fix for disabled "Add" button</li>
  <li>Filemanager: Show number instead of ??? if uid/gid can't be resolved</li>
  <li>Filemanager: There is no "Creation Date" with ext2</li>
  <li>Filemanager: Fix for changing permissions and updating</li>
  <li>Filemanager: Readded support for KDE-1's FSType=Default in Device desktop files</li>
  <li>Tar files: Support for symlinks and hard links</li>
  <li>Sidebar: Fix for common crash on deleting subdirs</li>
  <li>Sidebar: Fix for bookmark module filling in other modules</li>
  <li>Fix for crash with missing 'extra' toolbar in rc file</li>
  <li>Improved error reporting and stability in case of errors when loading a component</li>
</ul>

<h3>Session management</h3>
<ul>
  <li>Fix for the never-disappearing splash screen during start</li>
</ul>

<h3>KMail</h3>
<ul>
  <li>Handle Asian encodings in headers correctely</li>
  <li>Fix a crash with Turkish locale</li>
</ul>

<h3>Konsole</h3>
<ul>
  <li>...</li>
</ul>

<?php include "footer.inc" ?>
