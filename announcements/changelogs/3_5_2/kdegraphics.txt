2006-01-23 23:11 +0000 [r501814]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.cpp: Don't let people
	  overwrite a pdf with itself while open as this DESTROYS the
	  document and anyway it's a useless thing to do. i18n guys sorry
	  for adding a new message, but this is grave enough. Coolo, any
	  chance we can backport this without the messagebox to 3.5.1?
	  CCMAIL: kde-i18n-doc@kde.org CCMAIL: coolo@kde.org

2006-01-27 08:24 +0000 [r502738]  thiago

	* branches/KDE/3.5/kdegraphics/kpdf/configure.in.in: Use
	  KDE_CHECK_LARGEFILE instead

2006-01-29 17:44 +0000 [r503607]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/xpdf/GfxFont.cc: So not
	  assume ctu will exist BUGS: 120985

2006-02-01 08:49 +0000 [r504468]  kebekus

	* branches/KDE/3.5/kdegraphics/kdvi/dviRenderer_prescan.cpp,
	  branches/KDE/3.5/kdegraphics/kdvi/special.cpp: fixes problems
	  with PS header inclusion, fixes bug #105477

2006-02-02 22:12 +0000 [r505042]  mueller

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/SplashXPathScanner.cc:
	  CVE-2006-0301 buffer overflow

2006-02-03 18:21 +0000 [r505352]  chrsmrtn

	* branches/KDE/3.5/kdegraphics/kfile-plugins/raw/Makefile.am,
	  branches/KDE/3.5/kdegraphics/kfile-plugins/raw/parse.c.orig
	  (removed): Remove apparently unnecessary parse.c.orig.

2006-02-03 19:21 +0000 [r505378]  jriddell

	* branches/KDE/3.5/kdegraphics/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdegraphics
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdegraphics

2006-02-06 20:25 +0000 [r506449]  aacid

	* branches/KDE/3.5/kdegraphics/kgamma/kcmkgamma/xf86configpath.cpp,
	  branches/KDE/3.5/kdegraphics/kgamma/xf86gammacfg/xf86gammacfg.cpp:
	  Patch from Michael von Ostheim to fix some kgamma problems with
	  new Xorg BUGS: 121268

2006-02-08 00:34 +0000 [r506965]  goutte

	* branches/KDE/3.5/kdegraphics/kfile-plugins/raw/x-image-raw.desktop
	  (removed),
	  branches/KDE/3.5/kdegraphics/kfile-plugins/raw/Makefile.am: The
	  image/x-raw mimetype definition has been moved to
	  kdelibs/mimetypes/image (See revision 506964) CCBUG:121536

2006-02-08 01:00 +0000 [r506971]  goutte

	* branches/KDE/3.5/kdegraphics/kfile-plugins/raw/x-image-raw.magic
	  (removed),
	  branches/KDE/3.5/kdegraphics/kfile-plugins/raw/Makefile.am: The
	  magic rules for image/x-raw are now in kdelibs/kio/magic

2006-02-09 21:21 +0000 [r507715]  whuss

	* branches/KDE/3.5/kdegraphics/kviewshell/plugins/djvu/djvurenderer.cpp:
	  Backport of commit 507712 Fix conversion from GUTF8String to
	  QString. CCBUG: 120409

2006-02-10 09:37 +0000 [r507855]  whuss

	* branches/KDE/3.5/kdegraphics/kviewshell/plugins/djvu/djvurenderer.cpp:
	  Backport of commit 507850 Fix remaining conversions from
	  GUTF8String to QString and vice versa. BUG: 120407

2006-02-17 18:10 +0000 [r510661]  kebekus

	* branches/KDE/3.5/kdegraphics/kdvi/fontMap.cpp: fixes bug #120656

2006-02-17 20:10 +0000 [r510693]  woebbe

	* branches/KDE/3.5/kdegraphics/kview/kviewviewer/kviewviewer.h,
	  branches/KDE/3.5/kdegraphics/kview/kimageviewer/viewer.h: The
	  signal imageOpened() was pure virtual in Viewer and overridden in
	  KViewViewer which led to QMetaObject::findSignal:KViewViewer:
	  Conflict with KImageViewer::Viewer::imageOpened() So I removed -
	  the override in KViewViewer - the virtual in Viewer BIC isn't an
	  issue, as the headers aren't installed.

2006-02-18 11:52 +0000 [r510876]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/part.h,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/thumbnaillist.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/minibar.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/toc.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/minibar.h,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/pageview.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/part.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/ui/toc.h: Reoder some deletes,
	  add some removeObserver and use some magic QGuardPtr so that when
	  embedded on konqueror and changing page it does crash, i still
	  think that konqueror should not delete kpart created widgets
	  behind its back, but probably it's easier to change kpdf than all
	  the other code. BUGS: 121556

2006-02-19 13:34 +0000 [r511281]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/ui/toc.cpp: Enable multiple
	  lines in TOC items BUGS: 122270

2006-02-21 23:25 +0000 [r512159]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/imlibwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickdata.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imlibwidget.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/kuickdata.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.h: before
	  zooming in, check if the new width * height is larger than
	  desktop-width * height * a-given-factor (defaults to 4) In that
	  case a confirmation messagebox will be shown before performing
	  the zoom. BUG: 58738

2006-02-22 00:05 +0000 [r512164]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/imlibwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imlibwidget.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/ChangeLog,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.h:
	  auto-scale images on rotation BUG: 48811

2006-02-22 00:34 +0000 [r512167]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/version.h: - warn
	  before loading >= 10 images from the commandline - bump version
	  for KDE 3.5.2 BUG: 73511

2006-02-22 01:15 +0000 [r512174-512173]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/kuickshow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.h: revert
	  r409203 sorry Bero, the arrow keys are already taken for
	  scrolling in larger images. However, you can use PageUp /
	  PageDown to switch back and forth between images. CCMAIL:
	  bero@kde.org BUG: 120615

	* branches/KDE/3.5/kdegraphics/kuickshow/ChangeLog: add new entries

2006-02-22 13:26 +0000 [r512417]  thiago

	* branches/KDE/3.5/kdegraphics/kmrml/kmrml/server/Makefile.am:
	  Cannot add -no-undefined, but link to the rest of the dependency
	  libraries

2006-02-22 15:06 +0000 [r512444]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/imlibwidget.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imlibwidget.h,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.cpp,
	  branches/KDE/3.5/kdegraphics/kuickshow/ChangeLog,
	  branches/KDE/3.5/kdegraphics/kuickshow/src/imagewindow.h: make
	  autohide-cursor finally work BUG: 66597

2006-02-26 20:18 +0000 [r513885]  lueck

	* branches/KDE/3.5/kdegraphics/doc/kpdf/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kview/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kuickshow/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kiconedit/index.docbook,
	  branches/KDE/3.5/kdegraphics/doc/kghostview/index.docbook:
	  updated kdegraphics docbooks for KDE 3.5.2
	  CCMAIL:kde-doc-english@kde.org BUG:119614

2006-02-28 20:42 +0000 [r514606]  whuss

	* branches/KDE/3.5/kdegraphics/kdvi/dviWidget.cpp: "Control + Left
	  Click" for source hyperlinks. "Middle Click" still works of
	  course. BUG: 122791

2006-03-04 11:07 +0000 [r515595]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/COPYING,
	  branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/VERSION: up ver to
	  1.4.2_relight for upcoming KDE 3.5.2 release

2006-03-04 17:42 +0000 [r515713]  lueck

	* branches/KDE/3.5/kdegraphics/kiconedit/Makefile.am: fix for
	  untranslatable string CCMAIL:kde-i18n-doc@kde.org

2006-03-05 11:35 +0000 [r515917]  whuss

	* branches/KDE/3.5/kdegraphics/kviewshell/kviewpart.cpp: Fix menu
	  merging on session restore, and when using drag and drop. BUG:
	  119491

2006-03-08 12:27 +0000 [r516729]  mueller

	* branches/KDE/3.5/kdegraphics/kpdf/xpdf/splash/Splash.cc: fix
	  uninitialized memory access

2006-03-11 18:16 +0000 [r517646]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/configure.in.in: use of
	  pkg-config on crazy distro that decided to remove xft-config
	  Patch by Philippe Rigault BUGS: 123417

2006-03-12 11:14 +0000 [r517830]  dang

	* branches/KDE/3.5/kdegraphics/kolourpaint/NEWS,
	  branches/KDE/3.5/kdegraphics/kolourpaint/README,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow.h,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpdefs.h,
	  branches/KDE/3.5/kdegraphics/kolourpaint/kpmainwindow_file.cpp: *
	  Printing improvements - Respect image DPI - Fit image to page if
	  image is too big - Centre image on page (disable using hidden
	  config "Print Image Centered On Page") * Fix
	  kpMainWindow::saveDefaultDocSize() debug Main bug symptoms fixed
	  due to popular comment. We still need a DPI GUI. CCMAIL:
	  108976@bugs.kde.org

2006-03-14 19:14 +0000 [r518639]  rdieter

	* branches/KDE/3.5/kdegraphics/kpdf/configure.in.in: BUG: 123417
	  CCBUG: 123417 Use KDE_PKG_CHECK_MODULES (pkg-config) for xft
	  detection.

2006-03-16 14:13 +0000 [r519199]  rdieter

	* branches/KDE/3.5/kdegraphics/kuickshow/configure.in.bot,
	  branches/KDE/3.5/kdegraphics/kuickshow/configure.in.in: BUG:
	  123418 Use KDE_PKG_CHECK_MODULES(pkgconfig) for imlib detection.

2006-03-16 18:47 +0000 [r519277]  aacid

	* branches/KDE/3.5/kdegraphics/kpdf/shell/main.cpp,
	  branches/KDE/3.5/kdegraphics/kpdf/VERSION: update version number

2006-03-17 09:28 +0000 [r519468]  pfeiffer

	* branches/KDE/3.5/kdegraphics/kuickshow/src/printing.cpp: the
	  prefix in KTempFile's c'tor is used as-is, without any further
	  prefixing passing a relative path is a bad idea then, e.g. when
	  cwd == a read-only location BUG: 123767

2006-03-17 21:34 +0000 [r519784]  coolo

	* branches/KDE/3.5/kdegraphics/kdegraphics.lsm: tagging 3.5.2

