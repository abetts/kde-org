------------------------------------------------------------------------
r854907 | lueck | 2008-08-30 17:16:27 +0200 (Sat, 30 Aug 2008) | 1 line

documentation backport fron trunk
------------------------------------------------------------------------
r855428 | lueck | 2008-08-31 21:12:55 +0200 (Sun, 31 Aug 2008) | 1 line

backport typo fix from trunk
------------------------------------------------------------------------
r855434 | lueck | 2008-08-31 21:32:44 +0200 (Sun, 31 Aug 2008) | 1 line

backport from trunk:remove obsolete menu item
------------------------------------------------------------------------
r856367 | annma | 2008-09-02 20:11:53 +0200 (Tue, 02 Sep 2008) | 3 lines

backport of r856306 
tackat, you own me 100 extra karma points!

------------------------------------------------------------------------
r857636 | ereslibre | 2008-09-06 00:08:28 +0200 (Sat, 06 Sep 2008) | 2 lines

Backport(No need to call to setupGUI after createGUI. This only makes some actions not being added. This "workaround" is no longer needed.)

------------------------------------------------------------------------
r857693 | scripty | 2008-09-06 08:35:54 +0200 (Sat, 06 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r858318 | pino | 2008-09-07 21:14:17 +0200 (Sun, 07 Sep 2008) | 3 lines

remove the custom help menu definition in the shell ui.rc file
as a consequence, make the "tip of the day" menu entry tied to the standard action name

------------------------------------------------------------------------
r858646 | mlaurent | 2008-09-08 13:38:07 +0200 (Mon, 08 Sep 2008) | 4 lines

Backport:
Fix mem leak
Fix potential crash

------------------------------------------------------------------------
r858956 | nielsslot | 2008-09-09 09:31:40 +0200 (Tue, 09 Sep 2008) | 2 lines

Backport a fix to handle 'repeat 0' correctly

------------------------------------------------------------------------
r859718 | nielsslot | 2008-09-11 09:15:10 +0200 (Thu, 11 Sep 2008) | 2 lines

Backport the handle negative numbers in a repeat command fix.

------------------------------------------------------------------------
r859732 | mlaurent | 2008-09-11 09:51:34 +0200 (Thu, 11 Sep 2008) | 3 lines

Backport:
don't crash whene there is not selection

------------------------------------------------------------------------
r860467 | habacker | 2008-09-13 11:49:00 +0200 (Sat, 13 Sep 2008) | 2 lines

applied the following patch from trunk: fix reading of UTF-8-encoded files on win32.Though linux systems don't seem to need the call to QTextStream::setEncoding("UTF-8"), it doesn't seem to do any harm, so I added the two calls without any #ifdef wrappers.

------------------------------------------------------------------------
r860812 | lueck | 2008-09-14 13:32:27 +0200 (Sun, 14 Sep 2008) | 1 line

fixed wrong year in screenshot
------------------------------------------------------------------------
r861106 | annma | 2008-09-15 09:46:53 +0200 (Mon, 15 Sep 2008) | 2 lines

backport of 861100, add Danish keyboard

------------------------------------------------------------------------
r861767 | scripty | 2008-09-17 08:53:56 +0200 (Wed, 17 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r861778 | mlaurent | 2008-09-17 09:42:34 +0200 (Wed, 17 Sep 2008) | 3 lines

Backport:
Fix export file name

------------------------------------------------------------------------
r862771 | gladhorn | 2008-09-19 21:37:33 +0200 (Fri, 19 Sep 2008) | 1 line

backport 862768 while hanging upside down
------------------------------------------------------------------------
r864312 | gladhorn | 2008-09-24 14:49:48 +0200 (Wed, 24 Sep 2008) | 1 line

backport r864311
------------------------------------------------------------------------
