<?php
	include_once ("functions.inc");
	$translation_file = "www";
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "KDE Plasma 5.10, Simple by Default, Powerful when Needed",
		'cssFile' => '/content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = 'plasma-5.10.0'; // for i18n
	$version = "5.10.0";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px; 
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}

figure {
    position: relative;
    z-index: 2;
    font-size: smaller;
    text-shadow: 2px 2px 5px grey;
}
</style>


<main class="releaseAnnouncment container">


	<h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Plasma %1", $version)?></h1>

	<?php include "./announce-i18n-bar.inc"; ?>
	<figure class="videoBlock">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/VtdTC2Mh070?rel=0" allowfullscreen='true'></iframe>
	</figure>
	
	<figure class="topImage">
		<a href="plasma-5.10/plasma-5.10.png">
			<img src="plasma-5.10/plasma-5.10-wee.png" width="600" height="338" alt="Plasma 5.10" />
		</a>
		<figcaption><?php print i18n_var("KDE Plasma %1", "5.10")?></figcaption>
	</figure>

	<p>
		<?php i18n("Tuesday, 30 May 2017.")?>
		<?php print i18n_var("Today KDE has made a new feature release of our desktop Plasma 5.10 with new features across the suite to give users an experience which lives up to our tagline: simple by default, powerful when needed.");?>
	</p>

<br clear="all" />

<h2><?php i18n("Panel Task Manager");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.10/middle-click-task-bar.webm">
<video loop="true" autoplay="true" width="350">
<source src="plasma-5.10/middle-click-task-bar.webm" type="video/webm">
[WebM Video not supported]
</video>
</a>
<figcaption><?php i18n("Middle Mouse Click to Group");?></figcaption>
</figure>

<p><?php i18n("Task Manager, the list of applications in the panel, has gained options for middle mouse click such as grouping and ungrouping applications.");?></p>

<p><?php i18n("Several other improvements here include:</p>
<ul>
<li>Places jump list actions in File manager launchers (e.g. pinned Dolphin in Task Manager now lists user places)</li>
<li>The icon size in vertical Task Managers is now configurable to support more common vertical panel usage patterns</li>
<li>Improved app identification and pinning in Task Manager for apps that rely on StartupWMClass, perl-SDL-based apps and more</li>
</ul>");?>

<br clear="all" />

<h2><?php i18n("Folder View Is the New Default Desktop");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.10/spring_loading.gif">
<img src="plasma-5.10/spring_loading.gif" style="border: 0px" width="350" height="192" alt="<?php i18n("Spring Loading in Folder View");?>" />
</a>
<figcaption><?php i18n("Folder on the Desktop by Default");?></figcaption>
</figure>

<p><?php i18n("After some years shunning icons on the desktop we have accepted the inevitable and changed to Folder View as the default desktop which brings some icons by default and allows users to put whatever files or folders they want easy access to.  Many other improvements have been made to the Folder View include:");?></p>
<ul><?php i18n("
<li><a href='https://blogs.kde.org/2017/01/31/plasma-510-spring-loading-folder-view-performance-work'>Spring Loading</a> in Folder View making drag and drop of files powerful and quick</li>
<li>More space-saving/tighter icon grid in Folder View based on much user feedback</li>
<li>Improved mouse behavior / ergonomics in Folder View for icon dnd (less surprising drop/insert location), rectangle selection (easier, less fiddly) and hover (same)</li>
<li>Revamped rename user interface in Folder View (better keyboard and mouse behavior e.g. closing the editor by clicking outside, RTL fixed, etc.)</li>
<li><em>Massively</em> improved performance in Folder View for initial listing and scrolling large folders, reduced memory usage</li>
<li>Many other bug fixes and UI improvements in Folder View, e.g. better back button history, Undo shortcut support, clickable location in the headings, etc.</li>
<li>Unified drop menu in Folder View, showing both file (Copy/Move/Link) and widget (creating a Picture widget from an image drop, etc.) drop actions</li>
<li>It is now possible to resize widgets in the desktop by dragging on their edges and moving them with Alt+left-click, just like regular windows</li>
");?>
</ul>

<br clear="all" />
<h2><?php i18n("New Features Everywhere");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.10/lock-music.png">
<img src="plasma-5.10/lock-music-wee.png" style="border: 0px" width="336" height="329" alt="<?php i18n("Lock Screen Now Has Music Controls");?>" />
</a>
<figcaption><?php i18n("Lock Screen Now Has Music Controls");?></figcaption>
<p>&nbsp;</p>
<a href="plasma-5.10/software-centre-krunner.png">
<img src="plasma-5.10/software-centre-krunner-wee.png" style="border: 0px" width="350" height="57" alt="<?php i18n("Software Centre Plasma Search");?>" />
</a>
<figcaption><?php i18n("Software Centre Plasma Search offers to install apps");?></figcaption>
<p>&nbsp;</p>
<a href="plasma-5.10/plasma-pa.png">
<img src="plasma-5.10/plasma-pa-wee.png" style="border: 0px" width="350" height="345" alt="<?php i18n("Audio Volume Device Menu");?>" />
</a>
<figcaption><?php i18n("Audio Volume Device Menu");?></figcaption>
</figure>

<p><?php i18n("There are so many other improvements throughout the desktop, here's a sample:</p>
<ul>
<li>Media controls on lock screen</li>
<li>Pause music on suspend</li>
<li>Software Centre Plasma Search (KRunner) suggests to install non-installed apps</li>
<li>File copying notifications have a context menu on previews giving access to actions such as open containing folder, copy, open with etc</li>
<li>Improved plasma-windowed (enforces applet default/minimum sizes etc)</li>
<li>'desktop edit mode', when opening toolbox reveals applet handles</li>
<li>Performance optimizations in Pager and Task Manager</li>
<li>'Often used' docs and apps in app launchers in addition to 'Recently used'</li>
<li>Panel icons (buttons for popup applets, launcher applets) now follow the Icons -> Advanced -> Panel size setting in System Settings again, so they won't take up too much space, particularly useful for wide vertical panels</li>
<li>Revamped password dialogs for network authentication</li>
<li>The security of the lock screen architecture got reworked and simplified to ensure that your system is secured when the screen is locked. On Linux systems the lock screen is put into a sandbox through the seccomp technology.</li>
<li>Plasma's window manager support for hung processes got improved. When a window is not responding any more it gets darkened to indicate that one cannot interact with it any more.</li>
<li>Support for locking and unlocking the shell from the startup script, useful especially for distributions and enterprise setups</li>
<li>Audio Volume applet has a handy menu on each device which you can use to set is as default or switch output to headphones.</li>
</ul>
");?>

<br clear="all" />
<h2><?php i18n("Improved touch screen support");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.10/lock-screen-virtual-keyboard.png">
<img src="plasma-5.10/lock-screen-virtual-keyboard-wee.png" style="border: 0px" width="350" height="197" alt="<?php i18n("Virtual keyboard on Log In and Lock Screen");?>" />
</a>
<figcaption><?php i18n("Virtual keyboard on Log In and Lock Screen");?></figcaption>
</figure>
<?php i18n("Touch Screen Support has improved in several ways:
<ul>
<li>Virtual Keyboard in lock screen
<li>Virtual Keyboard in the login screen
<li>Touch screen edge swipe gestures
<li>Left screen edge defaults to window switching
<li>Show auto-hiding panels through edge swipe gesture
</ul>");?>

<br clear="all" />
<h2><?php i18n("Working for the Future with Wayland");?></h2>
<p><?php i18n("We have put a lot of work into porting to new graphics layer Wayland, the switch is coming but we won't recommend it until it is completely transparent to the user.  There will be improved features too such as KWin now supports scaling displays by different levels if you have a HiDPI monitor and a normal DPI screen.</p>
<p>Keyboard layout support in Wayland now has all the features of X11:</p>
<ul>
<li>Layout switcher in the system tray</li>
<li>Per layout global shortcut</li>
<li>Switch layout based on a policy, either global, virtual desktop, application or per window</li>
<li>IPC interface added, so that other applications can change layout.</li>
</ul>
");?>

<br clear="all" />

<h2><?php i18n("Plymouth Boot Splash Selection");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.10/plymouth-kcm.png">
<img src="plasma-5.10/plymouth-kcm-wee.png" style="border: 0px" width="350" height="245" alt="<?php i18n("Plymouth KControl Module");?>" />
</a>
<figcaption><?php i18n("Plymouth KControl Module");?></figcaption>
</figure>

<p><?php i18n("A new System Settings module lets you download and select boot time splashes.");?></p>

<br clear="all" />

<h2><?php i18n("Bundle Packages");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.10/xdg-portal.png">
<img src="plasma-5.10/xdg-portal-wee.png" style="border: 0px" width="350" height="204" alt="<?php i18n("Selecting a file using file chooser portal, invoking openURI portal and notification portal");?>" />
</a>
<figcaption><?php i18n("Flatpak integration with xdg-desktop-portal-kde: selecting a file using file chooser portal, invoking openURI portal and notification portal");?></figcaption>
</figure>

<p><?php i18n("Experimental support for forthcoming new bundle package formats has been implemented.  Discover software centre has gained provisional backends for Flatpak and Snappy.  New plugin xdg-desktop-portal-kde has added KDE integration into Flatpak packaged applications.");?></p>

<p><?php i18n("Support for GNOME’s <a href='https://odrs.gnome.org/'>Open Desktop Ratings</a>, replacing old Ubuntu popularity contest with tons of already existing reviews and comments.");?></p> 

<br clear="all" />

	<a href="plasma-5.9.5-5.10.0-changelog.php"><?php print i18n_var("Full Plasma 5.10 changelog"); ?></a>

	<!-- // Boilerplate again -->
	<section class="row get-it">
		<article class="col-md">
			<h2><?php i18n("Live Images");?></h2>
			<p>
				<?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
			</p>
			<a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
			<a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
		</article>

		<article class="col-md">
			<h2><?php i18n("Package Downloads");?></h2>
			<p>
				<?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
			</p>
			<a href='https://community.kde.org/Plasma/Packages' class="learn-more"><?php i18n("Package download wiki page");?></a>
		</article>

		<article class="col-md">
			<h2><?php i18n("Source Downloads");?></h2>
			<p>
				<?php i18n("You can install Plasma 5 directly from source.");?>
			</p>
			<a href='http://community.kde.org/Frameworks/Building'><?php i18n("Community instructions to compile it");?></a>
			<a href='../info/plasma-5.10.0.php' class='learn-more'><?php i18n("Source Info Page");?></a>
		</article>
	</section>

	<section class="give-feedback">
		<h2><?php i18n("Feedback");?></h2>

		<p>
			<?php print i18n_var("You can give us feedback and get updates on <a href='%1'><img src='%2' /></a> <a href='%3'>Facebook</a>
			or <a href='%4'><img src='%5' /></a> <a href='%6'>Twitter</a>
			or <a href='%7'><img src='%8' /></a> <a href='%9'>Google+</a>.", "https://www.facebook.com/kde", "https://www.kde.org/announcements/facebook.gif", "https://www.facebook.com/kde", "https://twitter.com/kdecommunity", "https://www.kde.org/announcements/twitter.png", "https://twitter.com/kdecommunity", "https://plus.google.com/105126786256705328374/posts", "https://www.kde.org/announcements/googleplus.png", "https://plus.google.com/105126786256705328374/posts"); ?>
		</p>
		<p>
			<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
		</p>

		<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

		<p><?php i18n("Your feedback is greatly appreciated.");?></p>
	</section>

	<h2>
		<?php i18n("Supporting KDE");?>
	</h2>

	<p align="justify">
		<?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. <a href='%3'>supporting member programme</a>.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/"); ?>
	</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
