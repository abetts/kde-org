<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.24.0");
  $site_root = "../";
  $release = '5.24.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
July 09, 2016. KDE today announces the release
of KDE Frameworks 5.24.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("General changes");?></h3>

<ul>
<li><?php i18n("The list of supported platforms for each framework is now more explicit.
Android has been added to the list of supported platforms in all frameworks where this is the case.");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("DocumentUrlDB::del Only assert when children of dir actually exist");?></li>
<li><?php i18n("Ignore malformed Queries which have a binary operator without first argument");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Many new or improved icons");?></li>
<li><?php i18n("fix bug 364931 user-idle icon was not visible (bug 364931)");?></li>
<li><?php i18n("Add a program to convert symbolically linked files to qrc aliases");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Integrate relative library paths to APK");?></li>
<li><?php i18n("Use \"${BIN_INSTALL_DIR}/data\" for DATAROOTDIR on Windows");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Ensure extracting an archive does not install files outside the extraction folder,
for security reasons. Instead, extract such files to the root of the extraction folder.");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Cleanup KBookmarkManagerList before qApp exits, to avoid deadlocks with the DBus thread");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Deprecate authorizeKAction() in favor of authorizeAction()");?></li>
<li><?php i18n("Fix reproducibility in builds by ensuring utf-8 encoding");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("KStandardAction::showStatusbar: Return the intended action");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Make epoxy optional");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("[OS X] make kded5 an agent, and build it as a regular application");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Remove KDETranslator class, there's no kdeqt.po anymore");?></li>
<li><?php i18n("Document the replacement for use12Clock()");?></li>
</ul>

<h3><?php i18n("KDesignerPlugin");?></h3>

<ul>
<li><?php i18n("Add support for KNewPasswordWidget");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Allow KDocTools to always locate at least its own installed stuff");?></li>
<li><?php i18n("Use CMAKE_INSTALL_DATAROOTDIR to look for docbook instead of share");?></li>
<li><?php i18n("Update qt5options manpage docbook to qt 5.4");?></li>
<li><?php i18n("Update kf5options manpage docbook");?></li>
</ul>

<h3><?php i18n("KEmoticons");?></h3>

<ul>
<li><?php i18n("Move glass theme to kde-look");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Use QGuiApplication instead of QApplication");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Fix applying inherit value for outline shorthand property");?></li>
<li><?php i18n("Handle initial and inherit for border radius");?></li>
<li><?php i18n("Discard property if we caught an invalid length|percent as background-size");?></li>
<li><?php i18n("cssText must output comma separated values for those properties");?></li>
<li><?php i18n("Fix parsing background-clip in shorthand");?></li>
<li><?php i18n("Implement background-size parsing in shorthand");?></li>
<li><?php i18n("Mark properties as set when repeating patterns");?></li>
<li><?php i18n("Fix background properties inheritance");?></li>
<li><?php i18n("Fix applying Initial and Inherit for background-size property");?></li>
<li><?php i18n("Deploy the khtml kxmlgui file in a Qt resource file");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Also search catalogs for stripped variants of values in env var LANGUAGE");?></li>
<li><?php i18n("Fix parsing of env var values WRT modifier and codeset, done in wrong order");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Add support for loading and using an icontheme in a RCC file automatically");?></li>
<li><?php i18n("Document icon theme deployment on MacOS and Windows, see https://api.kde.org/frameworks/kiconthemes/html/index.html");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Allow timeout in reset_oom_protection while waiting for SIGUSR1");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("KIO: add SlaveBase::openPasswordDialogV2 for better error checking, please port your kioslaves to it");?></li>
<li><?php i18n("Fix KUrlRequester opening file dialog in wrong directory (bug 364719)");?></li>
<li><?php i18n("Fix unsafe KDirModelDirNode* casts");?></li>
<li><?php i18n("Add cmake option KIO_FORK_SLAVES to set default value");?></li>
<li><?php i18n("ShortUri filter: fix filtering of mailto:user@host");?></li>
<li><?php i18n("Add OpenFileManagerWindowJob to highlight file within a folder");?></li>
<li><?php i18n("KRun: add runApplication method");?></li>
<li><?php i18n("Add soundcloud search provider");?></li>
<li><?php i18n("Fix an alignment issue with the OS X native \"macintosh\" style");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("Add KExtraColumnsProxyModel::removeExtraColumn, will be needed by StatisticsProxyModel");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("kjs/ConfigureChecks.cmake - set HAVE_SYS_PARAM_H properly");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Make sure we have a size to offer (bug 364896)");?></li>
<li><?php i18n("Fix \"Download dialog fails when all categories missing\"");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Fix notify by taskbar");?></li>
</ul>

<h3><?php i18n("KNotifyConfig");?></h3>

<ul>
<li><?php i18n("KNotifyConfigWidget: add disableAllSounds() method (bug 157272)");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Add switch to disable KParts' handling of window titles");?></li>
<li><?php i18n("Add donate menu item to help menu of our apps");?></li>
</ul>

<h3><?php i18n("Kross");?></h3>

<ul>
<li><?php i18n("Fix name of QDialogButtonBox's enumerator \"StandardButtons\"");?></li>
<li><?php i18n("Remove the first attempt to load library because we will try libraryPaths anyway");?></li>
<li><?php i18n("Fix crash when a method exposed to Kross returns QVariant with non-relocatable data");?></li>
<li><?php i18n("Do not use C-style casts into void* (bug 325055)");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("[QueryMatch] Add iconName");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Show Scrollbar Text Preview after a delay of 250ms");?></li>
<li><?php i18n("hide preview and stuff on view content scrolling");?></li>
<li><?php i18n("set parent + toolview, I think this is needed to avoid task switcher entry in Win10");?></li>
<li><?php i18n("Remove \"KDE-Standard\" from encoding box");?></li>
<li><?php i18n("Folding preview on per default");?></li>
<li><?php i18n("Avoid dashed underline for preview &amp; avoid poisoning of line layout cache");?></li>
<li><?php i18n("Always enable \"Show preview of folded text\" option");?></li>
<li><?php i18n("TextPreview: Adjust the grooveRect-height when scrollPastEnd is enabled");?></li>
<li><?php i18n("Scrollbar preview: use groove rect if scrollbar does not use full height");?></li>
<li><?php i18n("Add KTE::MovingRange::numberOfLines() just like KTE::Range has");?></li>
<li><?php i18n("Code folding preview: set popup height so that all hidden lines fit");?></li>
<li><?php i18n("Add option to disable preview of folded text");?></li>
<li><?php i18n("Add modeline 'folding-preview' of type bool");?></li>
<li><?php i18n("View ConfigInterface: support 'folding-preview' of type bool");?></li>
<li><?php i18n("Add bool KateViewConfig::foldingPreview() and setFoldingPreview(bool)");?></li>
<li><?php i18n("Feature: Show text preview when hovering over folded code block");?></li>
<li><?php i18n("KateTextPreview: add setShowFoldedLines() and showFoldedLines()");?></li>
<li><?php i18n("Add modelines 'scrollbar-minimap' [bool], and 'scrollbar-preview' [bool]");?></li>
<li><?php i18n("Enable mini-map scrollbar by default");?></li>
<li><?php i18n("New feature: Show text preview when hovering over the scrollbar");?></li>
<li><?php i18n("KateUndoGroup::editEnd(): pass KTE::Range by const ref");?></li>
<li><?php i18n("Fix vim-mode shortcut handling, after behaviour changes in Qt 5.5 (bug 353332)");?></li>
<li><?php i18n("Autobrace: don't insert ' character in text");?></li>
<li><?php i18n("ConfigInterface: add scrollbar-minimap config key to enable/disable scrollbar mini map");?></li>
<li><?php i18n("Fix KTE::View::cursorToCoordinate() when top message widget is visible");?></li>
<li><?php i18n("Refactoring of the Emulated Command Bar");?></li>
<li><?php i18n("Fix drawing artifacts when scrolling while notifications are visible (bug 363220)");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Add a parent_window event to Plasma Window interface");?></li>
<li><?php i18n("Properly handle destroying a Pointer/Keyboard/Touch resource");?></li>
<li><?php i18n("[server] Delete dead code: KeyboardInterface::Private::sendKeymap");?></li>
<li><?php i18n("[server] Add support for setting the clipboard selection DataDeviceInterface manually");?></li>
<li><?php i18n("[server] Ensure that Resource::Private::get returns nullptr if passed a nullptr");?></li>
<li><?php i18n("[server] Add resource check in QtExtendedSurfaceInterface::close");?></li>
<li><?php i18n("[server] Unset SurfaceInterface pointer in referenced objects when being destroyed");?></li>
<li><?php i18n("[server] Fix error message in QtSurfaceExtension Interface");?></li>
<li><?php i18n("[server] Introduce a Resource::unbound signal emitted from unbind handler");?></li>
<li><?php i18n("[server] Don't assert when destroying a still referenced BufferInterface");?></li>
<li><?php i18n("Add destructor request to org_kde_kwin_shadow and org_kde_kwin_shadow_manager");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Fix reading Unihan data");?></li>
<li><?php i18n("Fix minimum size of KNewPasswordDialog (bug 342523)");?></li>
<li><?php i18n("Fix ambiguous contructor on MSVC 2015");?></li>
<li><?php i18n("Fix an alignment issue under the OS X native \"macintosh\" style (bug 296810)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("KXMLGui: Fix merge indices when removing xmlgui clients with actions in groups (bug 64754)");?></li>
<li><?php i18n("Don't warn about \"file found in compat location\" if it wasn't found at all");?></li>
<li><?php i18n("Add donate menu item to help menu of our apps");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Do not set peap label based on peap version");?></li>
<li><?php i18n("Make network manager version checks in runtime (to avoid compile vs. run-time (bug 362736)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[Calendar] Flip arrow buttons on right-to-left languages");?></li>
<li><?php i18n("Plasma::Service::operationDescription() should return a QVariantMap");?></li>
<li><?php i18n("Don't include embedded contrainers in containmentAt(pos) (bug 361777)");?></li>
<li><?php i18n("fix the color theming for the restart system icon (login screen) (bug 364454)");?></li>
<li><?php i18n("disable taskbar thumbnails with llvmpipe (bug 363371)");?></li>
<li><?php i18n("guard against invalid applets (bug 364281)");?></li>
<li><?php i18n("PluginLoader::loadApplet: restore compatibility for misinstalled applets");?></li>
<li><?php i18n("correct folder for PLASMA_PLASMOIDS_PLUGINDIR");?></li>
<li><?php i18n("PluginLoader: improve error message about plugin version compatibility");?></li>
<li><?php i18n("Fix check to keep QMenu on screen for multiscreen layouts");?></li>
<li><?php i18n("New containment type for the systray");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Fix check that CPU is valid");?></li>
<li><?php i18n("Handle reading /proc/cpuinfo for Arm processors");?></li>
<li><?php i18n("Find CPUs by subsystem rather than driver");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Mark helper exe as non-gui app");?></li>
<li><?php i18n("Allow nsspellcheck to be compiled on mac per default");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.24");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.4");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
