<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.16.0");
  $site_root = "../";
  $release = '5.16.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
November 13, 2015. KDE today announces the release
of KDE Frameworks 5.16.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Monitor lib: Use Kformat::spelloutDuration to localize time string");?></li>
<li><?php i18n("Use KDE_INSTALL_DBUSINTERFACEDIR to install dbus interfaces");?></li>
<li><?php i18n("UnindexedFileIndexer: Handle files that have been moved when baloo_file was not running");?></li>
<li><?php i18n("Remove Transaction::renameFilePath and add DocumentOperation for it.");?></li>
<li><?php i18n("Make constructors with a single parameter explicit");?></li>
<li><?php i18n("UnindexedFileIndexer: only index required parts of file");?></li>
<li><?php i18n("Transaction: add method to return timeInfo struct");?></li>
<li><?php i18n("Added exclude mimetypes to balooctl's config");?></li>
<li><?php i18n("Databases: Use QByteArray::fromRawData when passing data to a codec");?></li>
<li><?php i18n("Balooctl: Move 'status' command to its own class");?></li>
<li><?php i18n("Balooctl: Show help menu if the command is not recognized");?></li>
<li><?php i18n("Balooshow: Allow us to lookup files by their inode + devId");?></li>
<li><?php i18n("Balooctl monitor: stop if baloo dies");?></li>
<li><?php i18n("MonitorCommand: Use both the started and finished signals");?></li>
<li><?php i18n("Balooctl monitor: Move to a proper command class");?></li>
<li><?php i18n("Add dbus notification for when we start/finish indexing a file");?></li>
<li><?php i18n("FileIndexScheduler: Forcibly kill threads on exit");?></li>
<li><?php i18n("WriteTransaction commit: Avoid fetching the positionList unless required");?></li>
<li><?php i18n("WriteTransaction: Extra asserts in replaceDocument");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("isBluetoothOperational now also depends on unblocked rfkill");?></li>
<li><?php i18n("Fix determining global state of rfkill switch");?></li>
<li><?php i18n("QML API: Mark properties without notify signal as constants");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Warn instead of error if ecm_install_icons finds no icons. (bug 354610)");?></li>
<li><?php i18n("make it possible to build KDE Frameworks 5 with a plain qt 5.5.x installed from the normal qt.io installer on mac os");?></li>
<li><?php i18n("Do not unset cache variables in KDEInstallDirs. (bug 342717)");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Set default value for WheelScrollLines");?></li>
<li><?php i18n("Fix WheelScrollLines settings with Qt &gt;= 5.5 (bug 291144)");?></li>
<li><?php i18n("Switch to Noto font for Plasma 5.5");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Fixing the build against Qt 5.3");?></li>
<li><?php i18n("Moved the boost.optional include to the place that uses it");?></li>
<li><?php i18n("Replacing the boost.optional usage in continuations with a slimmer optional_view structure");?></li>
<li><?php i18n("Added support for a custom ordering of linked results");?></li>
<li><?php i18n("Allow QML to invoke activities KCM");?></li>
<li><?php i18n("Adding the support for activity deletion to activities KCM");?></li>
<li><?php i18n("New activity configuration UI");?></li>
<li><?php i18n("New configuration UI that supports adding description and wallpaper");?></li>
<li><?php i18n("Settings UI is now properly modularized");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Fix KArchive for behavior change in Qt 5.6");?></li>
<li><?php i18n("Fix memleaks, lower memory usage");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Handle proxying qInfo messages");?></li>
<li><?php i18n("Wait for async call starting helper to finish before checking the reply (bug 345234)");?></li>
<li><?php i18n("Fix variable name, otherwise there's no way the include can work");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Fix usage of ecm_create_qm_loader.");?></li>
<li><?php i18n("Fix include variable");?></li>
<li><?php i18n("Use KDE_INSTALL_FULL_ variant, so there is no ambiguity");?></li>
<li><?php i18n("Allow KConfig to use resources as fallback config files");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Make KConfigWidgets self contained, bundle the one global file in a resource");?></li>
<li><?php i18n("Make doctools optional");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KAboutData: apidoc \"is is\" -&gt; \"is\" addCredit(): ocsUserName -&gt; ocsUsername");?></li>
<li><?php i18n("KJob::kill(Quiet) should also exit the event loop");?></li>
<li><?php i18n("Add support for desktop file name to KAboutData");?></li>
<li><?php i18n("Use correct escaping character");?></li>
<li><?php i18n("Reduce some allocations");?></li>
<li><?php i18n("Make KAboutData::translators/setTranslators simple");?></li>
<li><?php i18n("Fix setTranslator example code");?></li>
<li><?php i18n("desktopparser: skip the Encoding= key");?></li>
<li><?php i18n("desktopfileparser: Address review comments");?></li>
<li><?php i18n("Allow setting service types in kcoreaddons_desktop_to_json()");?></li>
<li><?php i18n("desktopparser: Fix parsing of double and bool values");?></li>
<li><?php i18n("Add KPluginMetaData::fromDesktopFile()");?></li>
<li><?php i18n("desktopparser: Allow passing relative paths to service type files");?></li>
<li><?php i18n("desktopparser: Use more categorized logging");?></li>
<li><?php i18n("QCommandLineParser uses -v for --version so just use --verbose");?></li>
<li><?php i18n("Remove lots of duplicated code for desktop{tojson,fileparser}.cpp");?></li>
<li><?php i18n("Parse ServiceType files when reading .desktop files");?></li>
<li><?php i18n("Make SharedMimeInfo an optional requirement");?></li>
<li><?php i18n("Remove call to QString::squeeze()");?></li>
<li><?php i18n("desktopparser: avoid unnecessary utf8 decoding");?></li>
<li><?php i18n("desktopparser: Don't add another entry if entry ends in a separator");?></li>
<li><?php i18n("KPluginMetaData: Warn when a list entry is not a JSON list");?></li>
<li><?php i18n("Add mimeTypes() to KPluginMetaData");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("Improve search for drkonqui and keep it silent per default if not found");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("ConfigPropertyMap can now be queried for immutable config options using the isImmutable(key) method");?></li>
<li><?php i18n("Unbox QJSValue in config property map");?></li>
<li><?php i18n("EventGenerator: Add support for sending wheel events");?></li>
<li><?php i18n("fix lost QuickViewSharedEngine initialSize on initializing.");?></li>
<li><?php i18n("fix critical regression for QuickViewSharedEngine by commit 3792923639b1c480fd622f7d4d31f6f888c925b9");?></li>
<li><?php i18n("make pre-specified view size precede initial object size in QuickViewSharedEngine");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Make doctools optional");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Don't try to store a QDateTime in mmap'ed memory");?></li>
<li><?php i18n("Sync and adopt uriencode.cmake from kdoctools.");?></li>
</ul>

<h3><?php i18n("KDesignerPlugin");?></h3>

<ul>
<li><?php i18n("Add KCollapsibleGroupBox");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("update pt_BR entities");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Do not XOR Shift for KP_Enter (bug 128982)");?></li>
<li><?php i18n("Grab all keys for a symbol (bug 351198)");?></li>
<li><?php i18n("Do not fetch keysyms twice for every keypress");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Fix printing from KHTMLPart by correctly setting printSetting parent");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("kiconthemes now support themes embedded in qt resources inside the :/icons prefix like Qt does itself for QIcon::fromTheme");?></li>
<li><?php i18n("Add missing required dependencies");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("Recognize image/vnd.adobe.photoshop instead of image/x-psd");?></li>
<li><?php i18n("Partially revert d7f457a to prevent crash on application exit");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Make doctools optional");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Save proxy url with correct scheme");?></li>
<li><?php i18n("Ship the \"new file templates\" in the kiofilewidgets library using a .qrc (bug 353642)");?></li>
<li><?php i18n("Properly handle middle click in navigatormenu");?></li>
<li><?php i18n("Make kio_http_cache_cleaner deployable in application installer/bundles");?></li>
<li><?php i18n("KOpenWithDialog: Fix creating desktop file with empty mimetype");?></li>
<li><?php i18n("Read protocol info from plugin metadata");?></li>
<li><?php i18n("Allow local kioslave deployment");?></li>
<li><?php i18n("Add a .protocol to JSON converted");?></li>
<li><?php i18n("Fix double-emit of result and missing warning when listing hits an inaccessible folder (bug 333436)");?></li>
<li><?php i18n("Preserve relative link targets when copying symlinks. (bug 352927)");?></li>
<li><?php i18n("Using suitable icons for default folders in the user home (bug 352498)");?></li>
<li><?php i18n("Add an interface which allow plugin to show custom overlay icons");?></li>
<li><?php i18n("Make KNotifications dep in KIO (kpac) optional");?></li>
<li><?php i18n("Make doctools + wallet optional");?></li>
<li><?php i18n("Avoid kio crashes if no dbus server is running");?></li>
<li><?php i18n("Add KUriFilterSearchProviderActions, to show a list of actions for searching some text using web shortcuts");?></li>
<li><?php i18n("Move the entries for the \"Create New\" menu from kde-baseapps/lib/konq to kio (bug 349654)");?></li>
<li><?php i18n("Move konqpopupmenuplugin.desktop from kde-baseapps to kio (bug 350769)");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Use \"_timezone\" global variable for MSVC instead of \"timezone\". Fixes build with MSVC 2015.");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Fix 'KDE Partition Manager' desktop file and homepage URL");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Now that kparts no longer needs knotifications, only things that really want notifications require on this framework");?></li>
<li><?php i18n("Add description + purpose for speech + phonon");?></li>
<li><?php i18n("Make phonon dependency optional, purely internal change, like it is done for speech.");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Use deleteLater in Part::slotWidgetDestroyed().");?></li>
<li><?php i18n("Remove KNotifications dep from KParts");?></li>
<li><?php i18n("Use function to query ui_standards.rc location instead of hardcoding it, allows resource fallback to work");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("RunnerManager: Simplify plugin loading code");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("KBuildSycoca: always save, even if no change in .desktop file was noticed. (bug 353203)");?></li>
<li><?php i18n("Make doctools optional");?></li>
<li><?php i18n("kbuildsycoca: parse all the mimeapps.list files mentioned in the new spec.");?></li>
<li><?php i18n("Use largest timestamp in subdirectory as resource directory timestamp.");?></li>
<li><?php i18n("Keep MIME types separate when converting KPluginInfo to KPluginMetaData");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("highlighting: gnuplot: add .plt extension");?></li>
<li><?php i18n("fix validation hint, thanks to \"Thomas Jarosch\" &lt;thomas.jarosch@intra2net.com&gt;, add hint about the compile time validation, too");?></li>
<li><?php i18n("Don't crash when command is not available.");?></li>
<li><?php i18n("Fix bug #307107");?></li>
<li><?php i18n("Haskell highlighting variables starting with _");?></li>
<li><?php i18n("simplify git2 init, given we require recent enough version (bug 353947)");?></li>
<li><?php i18n("bundle default configs in resource");?></li>
<li><?php i18n("syntax highlighting (d-g): use default styles instead of hard-coded colors");?></li>
<li><?php i18n("better scripts search, first user local stuff, then the stuff in our resources, then all other stuff, that way the user can overwrite our shipped scripts with local ones");?></li>
<li><?php i18n("package all js stuff in resources, too, only 3 config files missing and ktexteditor could be just used as a library without any bundled files");?></li>
<li><?php i18n("next try: put all bundled xml syntax files into a resource");?></li>
<li><?php i18n("add input mode switch shortcut (bug 347769)");?></li>
<li><?php i18n("bundle xml files in resource");?></li>
<li><?php i18n("syntax highlighting (a-c): migrate to new default styles, remove hard-coded colors");?></li>
<li><?php i18n("syntax highlighting: remove hard-coded colors and use default styles instead");?></li>
<li><?php i18n("syntax highlighting: use new default styles (removes hard-coded colors)");?></li>
<li><?php i18n("Better \"Import\" default style");?></li>
<li><?php i18n("Introduce \"Save As with Encoding\" to save a file with different encoding, using the nice grouped encoding menu we have and replacing all save dialogs with the correct ones of the operating system without loosing this important feature.");?></li>
<li><?php i18n("bundle ui file into lib, using my extension to xmlgui");?></li>
<li><?php i18n("Printing again honors the selected font &amp; color schema (bug 344976)");?></li>
<li><?php i18n("Use breeze colors for saved and modified lines");?></li>
<li><?php i18n("Improved icon border default colors of scheme \"Normal\"");?></li>
<li><?php i18n("autobrace: only insert brace when next letter is empty or not alphanumeric");?></li>
<li><?php i18n("autobrace: if removing start parenthesis with backspace, remove end as well");?></li>
<li><?php i18n("autobrace: only establish connection once");?></li>
<li><?php i18n("Autobrace: eat closing parentheses under some conditions");?></li>
<li><?php i18n("Fix shortcutoverride not being forwarded to the mainwindow");?></li>
<li><?php i18n("Bug 342659 - Default \"bracket highlighting\" color is hard to see (Normal schema fixed) (bug 342659)");?></li>
<li><?php i18n("Add proper default colors for \"Current Line Number\" color");?></li>
<li><?php i18n("bracket matching &amp; auto-brackets: share code");?></li>
<li><?php i18n("bracket matching: guard against negative maxLines");?></li>
<li><?php i18n("bracket matching: just because the new range matches the old doesn't mean no update is required");?></li>
<li><?php i18n("Add the width of half a space to allow painting the cursor at EOL");?></li>
<li><?php i18n("fix some HiDPI issues in the icon border");?></li>
<li><?php i18n("fix bug #310712: remove trailing spaces also on line with cursor (bug 310712)");?></li>
<li><?php i18n("only display \"mark set\" message when vi input mode is active");?></li>
<li><?php i18n("remove &amp; from button text (bug 345937)");?></li>
<li><?php i18n("fix update of current line number color (bug 340363)");?></li>
<li><?php i18n("implement brackets insert on writing a bracket over a selection (bug 350317)");?></li>
<li><?php i18n("auto brackets (bug 350317)");?></li>
<li><?php i18n("fix alert HL (bug 344442)");?></li>
<li><?php i18n("no column scrolling with dyn word wrap on");?></li>
<li><?php i18n("remember if highlighting was set by user over sessions to not loose it on save after restore (bug 332605)");?></li>
<li><?php i18n("fix folding for tex (bug 328348)");?></li>
<li><?php i18n("fixed bug #327842: End of C-style comment is misdetected (bug 327842)");?></li>
<li><?php i18n("save/restore dyn word wrap on session save/restore (bug 284250)");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("Add a new submenu to KTextEdit to switch between spell-checking languages");?></li>
<li><?php i18n("Fix loading Sonnet default settings");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Use KDE_INSTALL_DBUSINTERFACEDIR to install dbus interfaces");?></li>
<li><?php i18n("Fixed KWallet configuration file warnings on login (bug 351805)");?></li>
<li><?php i18n("Prefix the kwallet-pam output properly");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Add collapsible container widget, KCollapsibleGroupBox");?></li>
<li><?php i18n("KNewPasswordWidget: missing color initialization");?></li>
<li><?php i18n("Introduce KNewPasswordWidget");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("kmainwindow: Pre-fill translator information when available. (bug 345320)");?></li>
<li><?php i18n("Allow to bind the contextmenu key (lower right) to shortcuts (bug 165542)");?></li>
<li><?php i18n("Add function to query standards xml file location");?></li>
<li><?php i18n("Allow kxmlgui framework to be used without any installed files");?></li>
<li><?php i18n("Add missing required dependencies");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Fix TabBar items being cramped together on initial creation, which can be observed in eg. Kickoff after Plasma start");?></li>
<li><?php i18n("Fix dropping files onto the desktop/panel not offering a selection of actions to take");?></li>
<li><?php i18n("Take QApplication::wheelScrollLines into account from ScrollView");?></li>
<li><?php i18n("Use BypassWindowManagerHint only on platform X11");?></li>
<li><?php i18n("delete old panel background");?></li>
<li><?php i18n("more readable spinner at small sizes");?></li>
<li><?php i18n("colored view-history");?></li>
<li><?php i18n("calendar: Make the entire header area clickable");?></li>
<li><?php i18n("calendar: Don't use current day number in goToMonth");?></li>
<li><?php i18n("calendar: Fix updating decade overview");?></li>
<li><?php i18n("Theme breeze icons when loaded trough IconItem");?></li>
<li><?php i18n("Fix Button minimumWidth property (bug 353584)");?></li>
<li><?php i18n("Introduce appletCreated signal");?></li>
<li><?php i18n("Plasma Breeze Icon: Touchpad add svg id elements");?></li>
<li><?php i18n("Plasma Breeze Icon: change Touchpad to 22x22px size");?></li>
<li><?php i18n("Breeze Icon: add widget icon to notes");?></li>
<li><?php i18n("A script to replace hardcoded colors with stylesheets");?></li>
<li><?php i18n("Apply SkipTaskbar on ExposeEvent");?></li>
<li><?php i18n("Don't set SkipTaskbar on every event");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.16");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.3");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
