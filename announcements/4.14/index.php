<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $release = '4.14';
  $release_full = '4.14.0';
  $page_title = i18n_noop("KDE Software Compilation 4.14");
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";
?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>
<?php i18n("August 20, 2014");?>
</p>

<font size="+1">
<?php i18n("This release is dedicated to Volker Lanz, a long time KDE member who passed away last April. Volker was the author and maintainer of KDE Partition Manager, and a regular IRC participant (Torch) providing user support and being active in the KDE Community.");?><br />
</font>

<?php
  include "../announce-i18n-bar.inc";
?>

<p>
<?php i18n("The KDE Community announces the latest major updates to KDE Applications delivering primarily improvements and bugfixes. Plasma Workspaces and the KDE Development Platform are frozen and receiving only long term support; those teams are focused on the transition to Plasma 5 and Frameworks 5.");?>
</p>

<?php showscreenshotpng("plasma-4.11.png", ""); ?>
<br />

<div style="width: 360px; float: right; padding: 1ex; margin: 1ex;"><img src="https://dot.kde.org/sites/dot.kde.org/files/mascot_20140702_konqui-framework_wee_0.png" /></div>
<?php i18n("In the past, KDE has jointly released the three major divisions of KDE software—Plasma Workspaces, KDE Development Platform and KDE Applications. The KDE Development Platform has been reworked into KDE Frameworks. The monolithic libraries that comprise the Development Platform are now independent, cross platform modules (KDE Frameworks 5) that are available to all Qt developers. Plasma Workspaces has been moved to a new technology foundation based on Qt5 and KDE Frameworks 5. With the 3 major KDE software components moving at different paces, their release schedules are now separated. For the most part, 4.14 involves KDE Applications.");?>

<h2><?php i18n("Development Platform/KDE Frameworks 5");?></h2>
<?php print i18n_var("The <a href='%1'>modular Frameworks structure</a> will have widespread benefits for KDE software. In addition, Frameworks is a substantial <a href='%2'>contribution to the Qt ecosystem</a> by making KDE technology available to all Qt developers. <a href='%3'>Inqlude, the Qt library archive</a> simplifies the search for Qt libraries, while the <a href='%4'>alpha release</a> of the <a href='%5'>Inqlude tool</a> offers a command line interface for accessing Inqlude.", "https://dot.kde.org/2013/09/25/frameworks-5", "https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers", "http://inqlude.org/", "http://rubygems.org/gems/inqlude/versions/0.7.0", "https://github.com/cornelius/inqlude");?>

<h2><?php i18n("Plasma Workspaces");?></h2>
<?php print i18n_var("<a href='%1'>Plasma 5</a> was recently released after 3 years of work; it is on its own release schedule with feature releases every three months and bugfix releases in the intervening months. The Plasma team has built a solid foundation that will support Plasma Workspaces for many years.", "https://dot.kde.org/2014/07/15/plasma-5.0");?>

<div style="width: 360px; float: right; padding: 1ex; margin: 1ex;"><img src="https://dot.kde.org/sites/dot.kde.org/files/mascot_20140702_konqui-plasma_wee_0.png" /></div>
<h2><?php i18n("KDE Applications");?></h2>
<?php print i18n_var("KDE Applications 4.14 is not about lots of &quot;new and improved stuff&quot;. Many KDE developers are focused on the Next Experience (Plasma 5) or porting to KDE Frameworks (based on <a href='%1'>Qt5</a>). Mostly, the 4.14 release is needed by aspects of our workflow (such as translations). This release offers more software stability, with little emphasis on new and less-proven stuff.", "http://qt-project.org/qt5");?>

<p>
<?php print i18n_var("There are over 200 actively maintained <a href='%1'>KDE applications</a>. Many of them are listed in the <a href='%2'>KDE userbase</a>. Wikipedia also has another <a href='%3'>list of KDE applications</a>.", "KDE applications", "https://userbase.kde.org/Applications", "http://en.wikipedia.org/wiki/List_of_KDE_applications");?>
</p>

<p>
<?php i18n("Most previous releases had highlights of new features and prominent applications. This gave some people the impression that KDE developers favored new-and-shiny over quality, which is not true. So for this announcement of the 4.14 Release, developers were asked for details—small, incremental improvements and bugfixes that might not even be noticeable to most users. These are the kinds of tasks that most developers work on, the kinds of tasks that allow beginners to make meaningful, mostly invisible contributions. Here are a few examples of the kinds of improvements that KDE developers have made in this release:");?>
<ul>
<li><a href="http://kopete.kde.org/">Kopete</a></li>
<ul>
<li><?php i18n("Support for SOCKS5 proxy in ICQ protocol (before only HTTP type was supported)");?></li>
<li><?php i18n("Support for using system proxy settings in ICQ protocol");?></li>
<li><?php print i18n_var("Support for audio calls (both protocols Google libjingle and real jingle) for *all* <a href='%1'>jabber</a> accounts, enabled by default", "http://www.jabber.org/");?></li>
<li><?php i18n("Updated libiris library from upstream which implements jabber/xmpp protocol in kopete");?></li>
<li><?php i18n("Contact property custom name and support for preferred display name type, making it possible to distinguish and choose between custom names stored on a server list and contact custom/nick name");?></li>
<li><?php print i18n_var("Wrap PGP-signed or encrypted messages into <a href='%1'>XEP-0027</a> XML block instead of the normal body of message", "http://www.xmpp.org/extensions/xep-0027.html");?></li>
<li><?php i18n("Show full range of jabber priorities in account config dialog");?></li>
</ul>
<div style="width: 360px; float: right; padding: 1ex; margin: 1ex;"><img src="https://dot.kde.org/sites/dot.kde.org/files/mascot_20140702_konqui-group_wee.png" /></div>

<li><a href="http://edu.kde.org/cantor/">Cantor</a></li>
<ul>
<li><?php print i18n_var("New <a href='%1'>Lua backend</a>", "http://oproj.tuxfamily.org/math/lua/kde/2014/08/04/cantor.html");?></li>
<li><?php print i18n_var("<a href='%1'>UTF-8</a> on LaTeX entries", "http://en.wikipedia.org/wiki/UTF-8");?></li>
<li><?php i18n("Add CTRL+Space as an alternative default code completion");?></li>
<li><?php i18n("Support to plot extension in Python 2 backend");?></li>
<li><?php i18n("Support to linear algebra extension in Python 2 backend");?></li>
<li><?php i18n("Support to packaging extension in Sage, Octave backends");?></li>
<li><?php i18n("Support to autorun scripts in Python 2, Scilab, Octave, Sage, Maxima, Qalculate and KAlgebra backends");?></li>
</ul>
<li><?php print i18n_var("<a href='%1'>Kanagram</a> got a new QML <a href='%2'>User Interface and some features</a>.", "http://edu.kde.org/kanagram/", "http://debjitmondal.blogspot.com/2014/07/brand-new-kanagram.html");?></li>
<li><?php print i18n_var("<a href='%1'>Okular</a> got bugfixes, small features and internal refactoring", "http://okular.kde.org/");?></li>
<li><a href="http://www.kde.org/applications/utilities/kate/">Kate</a></li>
<ul>
<li><?php i18n("New highlighting rules for languages; bugfixes and improvements to existing languages");?></li>
<li><?php i18n("Improved VI mode");?></li>
<li><?php i18n("Comment blocks can be folded automatically to save screen space");?></li>
<li><?php i18n("Improved support and auto-generation of dark system color schemes");?></li>
<li><?php i18n("Multiple bug fixes");?></li>
</ul>
<li><a href="http://umbrello.kde.org/">Umbrello</a></li>
<ul>
<li><?php i18n("UML2 ports on components");?></li>
<li><?php i18n("UML2 interface ball and socket notation");?></li>
<li><?php i18n("Improved C++ import (map declarations to correct namespace)");?></li>
<li><?php i18n("Crash fixes in all language importers");?></li>
<li><?php print i18n_var("Improved loading of <a href='%1'>Rose models</a>: Added support for controlled units and class diagrams", "http://en.wikipedia.org/wiki/IBM_Rational_Rose_XDE");?></li>
<li><?php print i18n_var("Support for loading <a href='%1'>ArgoUML</a> files (without diagrams)", "http://en.wikipedia.org/wiki/ArgoUML");?></li>
<li><?php print i18n_var("Support for loading <a href='%1'>Embarcadero</a> describe files (without diagrams)", "http://www.embarcadero.com/data-modeling");?></li>
<li><?php print i18n_var("Ada now can generate multiple classes per package (<a href='%1'>bugfix</a>)", "http://bugs.kde.org/336933");?></li>
<li><?php print i18n_var("New &quot;Find in diagram&quot; function (<a href='%1'>bugfix</a>)", "http://bugs.kde.org/116354");?></li>
<li><?php print i18n_var("Stabilized positions of activity pins (<a href='%1'>bugfix</a>)", "http://bugs.kde.org/335399");?></li>
<li><?php print i18n_var("Fixed sluggish UI reaction in state diagram (<a href='%1'>bugfix</a>)", "http://bugs.kde.org/337463");?></li>
<li><?php print i18n_var("Crash fixes: <a href='%1'>bugfix</a>, <a href='%2'>bugfix</a>, <a href='%3'>bugfix</a>", "http://bugs.kde.org/256716", "http://bugs.kde.org/332612", "http://bugs.kde.org/337606");?></li>
</ul>
<li><?php print i18n_var("<a href='%1'>Dolphin</a> has mostly bug fixes and small changes such as:", "http://dolphin.kde.org/");?></li>
<ul>
<li><?php i18n("Highlighting of the current item in the Places Panel is prettier.");?></li>
<li><?php i18n("&quot;Free space&quot; notification in the status bar is now always up-to-date in all windows and views.");?></li>
<li><?php i18n("Refactoring of the huge class that represents Dolphin's main window has been started to make the code more maintainable and easier to understand.");?></li>
</ul>
<li><a href="http://edu.kde.org/marble/">Marble</a>
<ul>
<li><?php i18n("Dolphin now shows thumbnails of .gpx, .kml and other file types supported by Marble");?></li>
<li><?php print i18n_var("<a href='%1'>KML</a> improvements: The list of supported KML features has been extended", "http://en.wikipedia.org/wiki/Keyhole_Markup_Language");?></li>
<li><?php print i18n_var("The new political vector map now shows governmental boundaries in different colors; a <a href='%1'>Google Summer of Code</a> project by Abhinav Gangwar.", "https://www.google-melange.com/gsoc/homepage/google/gsoc2014");?></li>
</ul>
<li><a href="https://bugs.kde.org/buglist.cgi?bug_status=RESOLVED&amp;bug_status=VERIFIED&bug_status=CLOSED&amp;chfieldfrom=2013-01-01&amp;chfieldto=Now&amp;f1=cf_versionfixedin&amp;f2=cf_versionfixedin&amp;j_top=OR&amp;o1=equals&amp;o2=equals&amp;query_format=advanced&amp;resolution=FIXED&amp;v1=4.14&amp;v2=4.14.0&amp;order=product%2Cbug_id%20DESC"><?php i18n("Many, many bugfixes");?></a></li>
</ul>

<h2><?php i18n("Support KDE");?></h2>

<a href="https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=5"><img src="images/join-the-game.png" width="231" height="120"
alt="<?php i18n("Join the Game");?>" align="left"/> </a>
<p align="justify"> <?php print i18n_var("KDE e.V.'s <a
href='%1'>Supporting Member program</a> is
open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&amp;id=5");?></p>
<p><?php print i18n_var("You can also make a one time donation at <a href='%1'>our donations page</a>.", "http://www.kde.org/community/donations/");?>

<br clear="all" />
<h4>
  <?php print i18n_var("Installing %1 Binary Packages", $release);?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php print i18n_var("Some Linux/UNIX OS vendors have kindly provided binary packages of %1 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.", $release);?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php print i18n_var("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='/info/%1.php#binary'>%1 Info Page</a>.", $release_full);?>
</p>

<h4>
  <?php print i18n_var("Compiling %1", $release);?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php print i18n_var("The complete source code for %1 may be <a href='http://download.kde.org/stable/%1/src/'>freely downloaded</a>. Instructions on compiling and installing %1 are available from the <a href='/info/%1.php'>%1 Info Page</a>.", $release_full);?>
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
