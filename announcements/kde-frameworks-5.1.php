<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Second release of KDE Frameworks 5");
  $site_root = "../";
  $release = '5.1.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
August 7th, 2014. KDE today announces the second release
of KDE Frameworks 5.  In line with the planned release policy for KDE
Frameworks this release comes one month after the initial version and
has both bugfixes and new features.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>


<p><?php i18n("
This release, versioned 5.1, comes with a number of bugfixes and new features including:
</p>

<ul>
<li>KTextEditor: Major refactorings and improvements of the vi-mode</li>
<li>KAuth: Now based on PolkitQt5-1</li>
<li>New migration agent for KWallet</li>
<li>Windows compilation fixes</li>
<li>Translation fixes</li>
<li>New install dir for KXmlGui files and for AppStream metainfo</li>
<li><a href='http://www.proli.net/2014/08/04/taking-advantage-of-opengl-from-plasma/'>Plasma Taking advantage of OpenGL</a></li>
</ul>
");?>

<br clear="all" />
<?php i18n("
<h2>Getting started</h2>
");?>
</p>
<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<ul>
<li><a href='%1'>Binary package distro install instructions</a>.<br /></li>
</ul>
Building  from source is possible using the basic <em>cmake .; make; make  install</em> commands. For a single Tier 1 framework, this is often  the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to  <a href='%2'>use kdesrc-build</a>.
", "http://community.kde.org/Frameworks/Binary_Packages", "http://kdesrc-build.kde.org/");?>
</p>
<?php print i18n_var("
Frameworks 5.1 requires Qt 5.2.  It is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
<ul>
<li><a href='%1'>KDE Frameworks 5.1 Source Info page with known bugs and security issues</a></li>
</ul>
", "http://kde.org/info/kde-frameworks-5.1.0.php");?>
</p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
