<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.29.0");
  $site_root = "../";
  $release = '5.29.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
December 12, 2016. KDE today announces the release
of KDE Frameworks 5.29.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("New framework");?></h3>

<p>This release includes Prison, a new framework for barcode generation (including QR codes).</p>

<h3><?php i18n("General");?></h3>

<p>FreeBSD was added to metainfo.yaml in all frameworks tested to work on FreeBSD.</p>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Performance improvements when writing (4 * speedup for writing out data)");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Make BINARY_ICONS_RESOURCE ON by default");?></li>
<li><?php i18n("add vnd.rar mime for shared-mime-info 1.7 (bug 372461)");?></li>
<li><?php i18n("add claws icon (bug 371914)");?></li>
<li><?php i18n("add gdrive icon instead of a generic cloud icon (bug 372111)");?></li>
<li><?php i18n("fix bug \"list-remove-symbolic use wrong image\" (bug 372119)");?></li>
<li><?php i18n("other additions and improvements");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Skip Python bindings test if PyQt isn't installed");?></li>
<li><?php i18n("Only add the test if python is found");?></li>
<li><?php i18n("Reduce the CMake minimum required");?></li>
<li><?php i18n("Add ecm_win_resolve_symlinks module");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("find QDBus, needed by appstream kpackage handler");?></li>
<li><?php i18n("Let KPackage have dependencies from packagekit &amp; appstream");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Properly sending the resource linked event");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Adapt to quickgit -&gt; cgit change");?></li>
<li><?php i18n("Fix bug if group name is not defined. Can still break under bad conditions");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Add errorString() method to provide error info");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Add timeout property (bug 363200)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("kconfig_compiler - generate code with overrides");?></li>
<li><?php i18n("Properly parse function keywords (bug 371562)");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Ensure menu actions get the intended MenuRole");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KTextToHtml: fix bug \"[1] added at the end of a hyperlink\" (bug 343275)");?></li>
<li><?php i18n("KUser: Only search for an avatar if loginName isn't empty");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("Align with KInit and don't use DISPLAY on Mac");?></li>
<li><?php i18n("Don't close all file descriptors on OS X");?></li>
</ul>

<h3><?php i18n("KDesignerPlugin");?></h3>

<ul>
<li><?php i18n("src/kgendesignerplugin.cpp - add overrides to generated code");?></li>
</ul>

<h3><?php i18n("KDESU");?></h3>

<ul>
<li><?php i18n("Unsets XDG_RUNTIME_DIR in processes run with kdesu");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Actually find FFMpeg's libpostproc");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("java: apply the names to the right buttons");?></li>
<li><?php i18n("java: set names in permission dialog");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Check properly pointer inequality from dngettext (bug 372681)");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Allow showing icons from all categories (bug 216653)");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Set environment variables from KLaunchRequest when starting new process");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Ported to categorized logging");?></li>
<li><?php i18n("Fix compilation against WinXP SDK");?></li>
<li><?php i18n("Allow uppercase checksums matching in Checksums tab (bug 372518)");?></li>
<li><?php i18n("Never stretch the last (=date) column in the file dialog (bug 312747)");?></li>
<li><?php i18n("Import and update kcontrol docbooks for code in kio from kde-runtime master");?></li>
<li><?php i18n("[OS X] make KDE's trash use the OS X trash");?></li>
<li><?php i18n("SlaveBase: add documentation about event loops and notifications and kded modules");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Add new archive management option (subdir) to knsrc");?></li>
<li><?php i18n("Consume the new error signals (set job errors)");?></li>
<li><?php i18n("Handle oddity regarding files disappearing when just created");?></li>
<li><?php i18n("Actually install the core headers, with CamelCases");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("[KStatusNotifierItem] Save / restore widget position during hide / restore it window (bug 356523)");?></li>
<li><?php i18n("[KNotification] Allow annotating notifications with URLs");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("keep installing metadata.desktop (bug 372594)");?></li>
<li><?php i18n("manually load metadata if absolute path is passed");?></li>
<li><?php i18n("Fix potential failure if package is not appstream compatible");?></li>
<li><?php i18n("Let KPackage know about X-Plasma-RootPath");?></li>
<li><?php i18n("Fix generating the metadata.json file");?></li>
</ul>

<h3><?php i18n("KPty");?></h3>

<ul>
<li><?php i18n("More utempter path searching (including /usr/lib/utempter/)");?></li>
<li><?php i18n("Add library path so utempter binary is found in Ubuntu 16.10");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Prevent Qt warnings about an unsupported clipboard mode on Mac");?></li>
<li><?php i18n("Use syntax definitions from KF5::SyntaxHighlighting");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("Don't replace window icons with the result of a failed lookup");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("[client] Fix nullptr dereference in ConfinedPointer and LockedPointer");?></li>
<li><?php i18n("[client] Install pointerconstraints.h");?></li>
<li><?php i18n("[server] Fix regression in SeatInterface::end/cancelPointerPinchGesture");?></li>
<li><?php i18n("Implementation of PointerConstraints protocol");?></li>
<li><?php i18n("[server] Reduce overhead of pointersForSurface");?></li>
<li><?php i18n("Return SurfaceInterface::size in global compositor space");?></li>
<li><?php i18n("[tools/generator] Generate enum FooInterfaceVersion on server side");?></li>
<li><?php i18n("[tools/generator] Wrap wl_fixed request args in wl_fixed_from_double");?></li>
<li><?php i18n("[tools/generator] Generate implementation of client side requests");?></li>
<li><?php i18n("[tools/generator] Generate client side resource factories");?></li>
<li><?php i18n("[tools/generator] Generate callbacks and listener on client side");?></li>
<li><?php i18n("[tools/generator] Pass this as q pointer to Client::Resource::Private");?></li>
<li><?php i18n("[tools/generator] Generate Private::setup(wl_foo *arg) on client side");?></li>
<li><?php i18n("Implementation of PointerGestures protocol");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Prevent crashing on Mac");?></li>
<li><?php i18n("Don't replace icons with the result of a failed lookup");?></li>
<li><?php i18n("KMessageWidget: fix layout when wordWrap is enabled without actions");?></li>
<li><?php i18n("KCollapsibleGroupBox: don't hide widgets, override focus policy instead");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("[KWindowInfo] Add pid() and desktopFileName()");?></li>
</ul>

<h3><?php i18n("Oxygen Icons");?></h3>

<ul>
<li><?php i18n("Add application-vnd.rar icon (bug 372461)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Check for metadata validity in settingsFileChanged (bug 372651)");?></li>
<li><?php i18n("Don't flip tabbar layout if vertical");?></li>
<li><?php i18n("Remove radialGradient4857 (bug 372383)");?></li>
<li><?php i18n("[AppletInterface] Never pull focus away from fullRepresentation (bug 372476)");?></li>
<li><?php i18n("Fix SVG icon ID prefix (bug 369622)");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("winutils_p.h: Restore compatibility with WinXP SDK");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Also search for hunspell-1.5");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Normalize XML license attribute values");?></li>
<li><?php i18n("Sync syntax definitions from ktexteditor");?></li>
<li><?php i18n("Fix folding region merging");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.29");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.5");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
