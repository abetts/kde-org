<?php

  include_once ("functions.inc");
  $translation_file = "www";
  $release = '4.12';
  $release_full = '4.12.0';
  $page_title = i18n_noop("KDE Software Compilation 4.12");
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>
<img src="screenshots/plasma-4.12.png" style="float: right; border: 0; background-image: none; margin-top: -80px;" alt="<?php i18n("The KDE Plasma Workspaces 4.12");?>" />
<br /><br /><br />
<font size="+1">
<?php i18n("December 18, 2013. The KDE Community is proud to announce the latest major updates to KDE Applications delivering new features and fixes. With the Plasma Workspaces and the KDE Platform frozen and receiving only long term support, those teams are focused on the technical transition to Frameworks 5. The upgrade in the version number for the Platform is merely for the convenience of packaging. All bug fixes and minor features developed since the release of Plasma Workspaces, Applications and Platform 4.11 have been included.");?><br />
</font>
</p>

<?php
  include "../announce-i18n-bar.inc";
?>

<p>
<?php i18n("These releases are all translated in 52 languages; we expect more languages to be added in subsequent monthly minor bugfix releases by KDE. The Documentation Team updated several application handbooks for this release.");?>
</p>

<p>
<h2><a href="applications.php"><img src="images/applications.png" class="app-icon" alt="<?php i18n("The KDE Applications 4.12");?>"/> <?php i18n("KDE Applications 4.12 Bring Huge Step Forward in Personal Information Management and Improvements All Over");?></a></h2>
<?php i18n("This release marks substantial improvements in the KDE PIM stack, giving much better performance and many new features. Kate added several features including initial Vim-macro support, and games and educational applications bring a variety of new functionality. The <a href='applications.php'>announcement for the KDE Applications 4.12</a> has more information.");?>

</p>

<p>
<h2><img src="images/platform.png" class="app-icon" alt="<?php i18n("The KDE Development Platform 4.12");?>"/> <?php i18n("KDE Platform 4.12 Becomes More Stable");?></a></h2>
<?php i18n("This release of KDE Platform 4.12 only includes bugfixes and minor optimizations and features. About 20 bugfixes as well as several optimizations have been made to various subsystems, including KNewStuff, KNotify4, file handling and more. Notably, Nepomuk received bugfixes and indexing abilities for MS Office 97 formats. A technology preview of the Next Generation KDE Platform, named KDE Frameworks 5, is coming this month. Read <a href='http://dot.kde.org/2013/09/25/frameworks-5'>this article</a> to find out what is coming.");?>
</p>
<h2><?php i18n("Spread the Word and See What's Happening: Tag as &quot;KDE&quot;");?></h2>
<p>
<?php i18n("KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.12 releases of KDE software.");?>
</p>
<h2><?php i18n("Release Parties");?></h2>
<p>
<?php i18n("As usual, KDE community members organize release parties all around the world. Several have already been scheduled and more will come later. Find <a href='http://community.kde.org/Promo/Events/Release_Parties/4.12'>a list of parties here</a>. Anyone is welcome to join! There will be a combination of interesting company and inspiring conversations as well as food and drinks. It's a great chance to learn more about what is going on in KDE, get involved, or just meet other users and contributors.");?>
</p>
<p>
<?php i18n("We encourage people to organize their own parties. They're fun to host and open for everyone! Check out <a href='http://community.kde.org/Promo/Events/Release_Parties'>tips on how to organize a party</a>.");?>
<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.12/&amp;title=KDE%20releases%20version%204.12%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.12/" data-text="#KDE releases version 4.12 of Plasma Workspaces, Applications and Platform →" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.12/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.12%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.12/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde412"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde412"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde412"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>
<h2><?php i18n("About these release announcements");?></h2>
<p>
<?php i18n("These release announcements were prepared by the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past four months.");?>
</p>

<h4><?php i18n("Support KDE");?></h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="<?php i18n("Join the Game");?>" align="left"/> </a>
<p align="justify"> <?php i18n("KDE e.V.'s new <a
href='http://jointhegame.kde.org/'>Supporting Member program</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.");?></p>
<br clear="all" />
<p>&nbsp;</p>

<?php
  include("footer.inc");
?>
