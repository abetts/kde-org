<?php
    include_once ("functions.inc");
    $translation_file = "www";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.65.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.65.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
December 14, 2019. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.65.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<p>New module: KQuickCharts -- a QtQuick module providing high-performance charts.</p>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Pixel align color-picker");?></li>
<li><?php i18n("Add new baloo icons");?></li>
<li><?php i18n("Add new preferences search icons");?></li>
<li><?php i18n("Use an eyedropper for color-picker icons (bug 403924)");?></li>
<li><?php i18n("Add \"all applications\" category icon");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("EBN extra-cmake-modules transport cleanup");?></li>
<li><?php i18n("ECMGenerateExportHeader: add NO_BUILD_SET_DEPRECATED_WARNINGS_SINCE flag");?></li>
<li><?php i18n("Explicitly use lib for systemd directories");?></li>
<li><?php i18n("Add install dir for systemd units");?></li>
<li><?php i18n("KDEFrameworkCompilerSettings: enable all Qt &amp; KF deprecation warnings");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Conditionally set SH_ScrollBar_LeftClickAbsolutePosition based on kdeglobals setting (bug 379498)");?></li>
<li><?php i18n("Set application name and version on the knshandler tool");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Fix module imports with Python3");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Install .pri file for KAuthCore");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Deprecate KonqBookmarkMenu and KonqBookmarkContextMenu");?></li>
<li><?php i18n("Move classes only used by KonqBookmarkMenu together with it");?></li>
</ul>

<h3><?php i18n("KCalendarCore");?></h3>

<ul>
<li><?php i18n("Fallback to system time zone on calendar creation with an invalid one");?></li>
<li><?php i18n("Memory Calendar: avoid code duplication");?></li>
<li><?php i18n("Use QDate as key in mIncidencesForDate in MemoryCalendar");?></li>
<li><?php i18n("Handle incidences in different time zones in MemoryCalendar");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("[KCMultiDialog] Remove most special margins handling; it's done in KPageDialog now");?></li>
<li><?php i18n("KPluginSelector: use new KAboutPluginDialog");?></li>
<li><?php i18n("Add guard for missing kirigami (bug 405023)");?></li>
<li><?php i18n("Disable the restore defaults button if the KCModule says so");?></li>
<li><?php i18n("Have KCModuleProxy take care of the defaulted state");?></li>
<li><?php i18n("Make KCModuleQml conform to the defaulted() signal");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("Refactor KHistoryComboBox::insertItems");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Document Notifiers setting");?></li>
<li><?php i18n("Only create a session config when actually restoring a session");?></li>
<li><?php i18n("kwriteconfig: add delete option");?></li>
<li><?php i18n("Add KPropertySkeletonItem");?></li>
<li><?php i18n("Prepare KConfigSkeletonItem to allow inheriting its private class");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("[KColorScheme] Make order of decoration colors match DecorationRole enum");?></li>
<li><?php i18n("[KColorScheme] Fix mistake in NShadeRoles comment");?></li>
<li><?php i18n("[KColorScheme/KStatefulBrush] Switch hardcoded numbers for enum items");?></li>
<li><?php i18n("[KColorScheme] Add items to ColorSet and Role enums for the total number of items");?></li>
<li><?php i18n("Register KKeySequenceWidget to KConfigDialogManager");?></li>
<li><?php i18n("Adjust KCModule to also channel information about defaults");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Deprecate KAboutData::fromPluginMetaData, now there is KAboutPluginDialog");?></li>
<li><?php i18n("Add a descriptive warning when inotify_add_watch returned ENOSPC (bug 387663)");?></li>
<li><?php i18n("Add test for bug \"bug-414360\" it's not a ktexttohtml bug (bug 414360)");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Include API to generically implement --replace arguments");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("EBN kdeclarative transfer protocol cleanup");?></li>
<li><?php i18n("Adapt to change in KConfigCompiler");?></li>
<li><?php i18n("make header and footer visible when they get content");?></li>
<li><?php i18n("support qqmlfileselectors");?></li>
<li><?php i18n("Allow to disable autosave behavior in ConfigPropertyMap");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Remove kdeinit dependency from kded");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("remove unused kgesturemap from kaction");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Catalan Works: Add missing entities");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Undeprecate I18N_NOOP2");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Deprecate top-level UserIcon method, no longer used");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Add new protocol for 7z archives");?></li>
<li><?php i18n("[CopyJob] When linking also consider https for text-html icon");?></li>
<li><?php i18n("[KFileWidget] Avoid calling slotOk right after the url changed (bug 412737)");?></li>
<li><?php i18n("[kfilewidget] Load icons by name");?></li>
<li><?php i18n("KRun: don't override user preferred app when opening local *.*html and co. files (bug 399020)");?></li>
<li><?php i18n("Repair FTP/HTTP proxy querying for the case of no proxy");?></li>
<li><?php i18n("Ftp ioslave: Fix ProxyUrls parameter passing");?></li>
<li><?php i18n("[KPropertiesDialog] provide a way of showing the target of a symlink (bug 413002)");?></li>
<li><?php i18n("[Remote ioslave] Add Display Name to remote:/ (bug 414345)");?></li>
<li><?php i18n("Fix HTTP proxy settings (bug 414346)");?></li>
<li><?php i18n("[KDirOperator] Add Backspace shortcut to back action");?></li>
<li><?php i18n("When kioslave5 couldn't be found in libexec-ish locations try $PATH");?></li>
<li><?php i18n("[Samba] Improve warning message about netbios name");?></li>
<li><?php i18n("[DeleteJob] Use a separate worker thread to run actual IO operation (bug 390748)");?></li>
<li><?php i18n("[KPropertiesDialog] Make creation date string mouse-selectable too (bug 413902)");?></li>
</ul>

<p>Deprecations:
* Deprecated KTcpSocket and KSsl* classes
* Remove the last traces of KSslError from TCPSlaveBase
* Port ssl_cert_errors meta data from KSslError to QSslError
* Deprecate KTcpSocket overload of KSslErrorUiData ctor
* [http kio slave] use QSslSocket instead of KTcpSocket (deprecated)
* [TcpSlaveBase] port from KTcpSocket (deprecated) to QSslSocket</p>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Fix margins of ToolBarHeader");?></li>
<li><?php i18n("Do not crash when icon's source is empty");?></li>
<li><?php i18n("MenuIcon: fix warnings when the drawer isn't initialized");?></li>
<li><?php i18n("Account for a mnemonic label to go back to \"\"");?></li>
<li><?php i18n("Fix InlineMessage actions always being placed in overflow menu");?></li>
<li><?php i18n("Fix default card background (bug 414329)");?></li>
<li><?php i18n("Icon: solve threading issue on when the source is http");?></li>
<li><?php i18n("keyboard navigation fixes");?></li>
<li><?php i18n("i18n: extract messages also from C++ sources");?></li>
<li><?php i18n("Fix cmake project command position");?></li>
<li><?php i18n("Make QmlComponentsPool one instance per engine (bug 414003)");?></li>
<li><?php i18n("Switch ToolBarPageHeader to use the icon collapse behaviour from ActionToolBar");?></li>
<li><?php i18n("ActionToolBar: Automatically change to icon-only for actions marked KeepVisible");?></li>
<li><?php i18n("Add a displayHint property to Action");?></li>
<li><?php i18n("add the dbus interface in the static version");?></li>
<li><?php i18n("Revert \"take into account dragging speed when a flick ends\"");?></li>
<li><?php i18n("FormLayout: Fix label height if wide mode is false");?></li>
<li><?php i18n("don't show the handle by default when not modal");?></li>
<li><?php i18n("Revert \"Ensure that GlobalDrawer topContent always stays on top\"");?></li>
<li><?php i18n("Use a RowLayout for laying out ToolBarPageHeader");?></li>
<li><?php i18n("Vertically center left actions in ActionTextField (bug 413769)");?></li>
<li><?php i18n("irestore dynamic watch of tablet mode");?></li>
<li><?php i18n("replace SwipeListItem");?></li>
<li><?php i18n("support actionsVisible property");?></li>
<li><?php i18n("start SwipeListItem port to SwipeDelegate");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("Deprecate KRecursiveFilterProxyModel");?></li>
<li><?php i18n("KNumberModel: gracefully handle a stepSize of 0");?></li>
<li><?php i18n("Expose KNumberModel to QML");?></li>
<li><?php i18n("Add new class KNumberModel that is a model of numbers between two values");?></li>
<li><?php i18n("Expose KDescendantsProxyModel to QML");?></li>
<li><?php i18n("Add qml import for KItemModels");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Add some friendly \"report bugs here\" links");?></li>
<li><?php i18n("Fix i18n syntax to avoid runtime errors (bug 414498)");?></li>
<li><?php i18n("Turn KNewStuffQuick::CommentsModel into a SortFilterProxy for reviews");?></li>
<li><?php i18n("Correctly set i18n arguments in one pass (bug 414060)");?></li>
<li><?php i18n("These functions are @since 5.65, not 5.64");?></li>
<li><?php i18n("Add OBS to screenrecorders (bug 412320)");?></li>
<li><?php i18n("Fix a couple of broken links, update links to https://kde.org/applications/");?></li>
<li><?php i18n("Fix translations of $GenericName");?></li>
<li><?php i18n("Show a \"Loading more...\" busy indicator when loading view data");?></li>
<li><?php i18n("Give some more pretty feedback in NewStuff::Page while the Engine is loading (bug 413439)");?></li>
<li><?php i18n("Add an overlay component for item activity feedback (bug 413441)");?></li>
<li><?php i18n("Only show DownloadItemsSheet if there's more than one download item (bug 413437)");?></li>
<li><?php i18n("Use the pointing hand cursor for the single-clickable delegates (bug 413435)");?></li>
<li><?php i18n("Fix the header layouts for EntryDetails and Page components (bug 413440)");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Make the docs reflect that setIconName should be preferred over setPixmap when possible");?></li>
<li><?php i18n("Document configuration file path on Android");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Mark BrowserRun::simpleSave properly as deprecated");?></li>
<li><?php i18n("BrowserOpenOrSaveQuestion: move AskEmbedOrSaveFlags enum from BrowserRun");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Allow triggering sort from QML");?></li>
</ul>

<h3><?php i18n("kquickcharts");?></h3>

<p>New module.
The Quick Charts module provides a set of charts that can be used from QtQuick
applications. They are intended to be used for both simple display of data as
well as continuous display of high-volume data (often referred to as plotters).
The charts use a system called distance fields for their accelerated rendering,
which provides ways of using the GPU for rendering 2D shapes without loss of
quality.</p>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("KateModeManager::updateFileType(): validate modes and reload menu of the status bar");?></li>
<li><?php i18n("Verify modes of the session config file");?></li>
<li><?php i18n("LGPLv2+ after ok by Svyatoslav Kuzmich");?></li>
<li><?php i18n("restore files pre-format");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("Deprecate kregexpeditorinterface");?></li>
<li><?php i18n("[kfinddialog] Remove usage of kregexpeditor plugin system");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("[server] Do not own dmabuf implementation");?></li>
<li><?php i18n("[server] Make double-buffered properties in xdg-shell double-buffered");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("[KSqueezedTextLabel] Add icon for \"Copy entire text\" action");?></li>
<li><?php i18n("Unify KPageDialog margin handling into KPageDialog itself (bug 413181)");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Adjust count after _GTK_FRAME_EXTENTS addition");?></li>
<li><?php i18n("Add support for _GTK_FRAME_EXTENTS");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Drop unused broken KGesture support");?></li>
<li><?php i18n("Add KAboutPluginDialog, to be used with KPluginMetaData");?></li>
<li><?php i18n("Also allow invoking session restoration logic when apps are manually launched (bug 413564)");?></li>
<li><?php i18n("Add missing property to KKeySequenceWidget");?></li>
</ul>

<h3><?php i18n("Oxygen Icons");?></h3>

<ul>
<li><?php i18n("Symlink microphone to audio-input-microphone on all sizes (bug 398160)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("move backgroundhints managment in Applet");?></li>
<li><?php i18n("use the file selector in the interceptor");?></li>
<li><?php i18n("more use of ColorScope");?></li>
<li><?php i18n("also monitor window changes");?></li>
<li><?php i18n("support for user removing background and automatic shadow");?></li>
<li><?php i18n("support file selectors");?></li>
<li><?php i18n("support qml file selectors");?></li>
<li><?php i18n("remove stray qgraphicsview stuff");?></li>
<li><?php i18n("don't delete and recreate wallpaperinterface if not needed");?></li>
<li><?php i18n("MobileTextActionsToolBar check if controlRoot is undefined before using it");?></li>
<li><?php i18n("Add hideOnWindowDeactivate to PlasmaComponents.Dialog");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("include the cmake command we are about to use");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("[TabBar] Use window color instead of button color (bug 413311)");?></li>
<li><?php i18n("bind enabled properties to the view enabled");?></li>
<li><?php i18n("[ToolTip] Base timeout on text length");?></li>
<li><?php i18n("[ComboBox] Don't dim Popup");?></li>
<li><?php i18n("[ComboBox] Don't indicate focus when popup is open");?></li>
<li><?php i18n("[ComboBox] Follow focusPolicy");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("[udisks2] fix media change detection for external optical drives (bug 394348)");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Disable ispell backend with mingw");?></li>
<li><?php i18n("Implement ISpellChecker backend for Windows &gt;= 8");?></li>
<li><?php i18n("Basic cross-compiling support for parsetrigrams");?></li>
<li><?php i18n("embed trigrams.map into shared library");?></li>
</ul>

<h3><?php i18n("Syndication");?></h3>

<ul>
<li><?php i18n("Fix Bug 383381 - Getting the feed URL from a youtube channel no longer works (bug 383381)");?></li>
<li><?php i18n("Extract code so we can fix parsing code (bug 383381)");?></li>
<li><?php i18n("atom has icon support (So we can use specific icon in akregator)");?></li>
<li><?php i18n("Convert as a real qtest apps");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Updates from CMake 3.16 final release");?></li>
<li><?php i18n("reStructuredText: Fix inline literals highlighting preceding characters");?></li>
<li><?php i18n("rst: Add support for standalone hyperlinks");?></li>
<li><?php i18n("JavaScript: move keywords from TypeScript and other improvements");?></li>
<li><?php i18n("JavaScript/TypeScript React: rename syntax definitions");?></li>
<li><?php i18n("LaTeX: fix backslash delimiter in some keywords (bug 413493)");?></li>
</ul>

<h3><?php i18n("ThreadWeaver");?></h3>

<ul>
<li><?php i18n("Use URL with transport encryption");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.65");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.11");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
