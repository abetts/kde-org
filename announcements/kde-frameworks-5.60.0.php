<?php
    include_once ("functions.inc");
    $translation_file = "www";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.60.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.60.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
July 13, 2019. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.60.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("General");?></h3>

<ul>
<li><?php i18n("Qt &gt;= 5.11 is now required, now that Qt 5.13 has been released.");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("[QueryTest] Test if independent phrases are really independent");?></li>
<li><?php i18n("[TermGenerator] Insert an empty position between independent terms");?></li>
<li><?php i18n("[QueryTest] Restructure tests to allow easier extension");?></li>
<li><?php i18n("[TermGenerator] Leave single term phrases out of the PositionDB");?></li>
<li><?php i18n("[TermGenerator] Do Term truncation prior to UTF-8 conversion");?></li>
<li><?php i18n("[PostingIterator] Move positions() method to VectorPositionInfoIterator");?></li>
<li><?php i18n("[TermGenerator] Use UTF-8 ByteArray for termList");?></li>
<li><?php i18n("[WriteTransactionTest] Clear mixup of QString and QByteArray");?></li>
<li><?php i18n("[experimental/BalooDB] Fix trivial 0 / nullptr warning");?></li>
<li><?php i18n("[PositionDbTest] Fix trivial memleak in test");?></li>
<li><?php i18n("[PendingFileQueueTest] Verify create + delete do not emit extra events");?></li>
<li><?php i18n("[PendingFileQueueTest] Verify delete + create actually works");?></li>
<li><?php i18n("[PendingFileQueue] Avoid delete + create / create + delete race");?></li>
<li><?php i18n("[PendingFileQueueTest] Use synthetic timer events to speedup test");?></li>
<li><?php i18n("[XAttrIndexer] Update DocumentTime when XAttrs are updated");?></li>
<li><?php i18n("[PendingFileQueueTest] Shorten timeouts, verify tracking time");?></li>
<li><?php i18n("[PendingFileQueue] Use more accurate calculation of remaining time");?></li>
<li><?php i18n("[ModifiedFileIndexer] Use correct mimetype for folders, delay until needed");?></li>
<li><?php i18n("[NewFileIndexer] Omit symlinks from the index");?></li>
<li><?php i18n("[ModifiedFileIndexer] Avoid shadowing XAttr changes by content changes");?></li>
<li><?php i18n("[NewFileIndexer] Use correct mimetype for folders, check excludeFolders");?></li>
<li><?php i18n("[UnindexedFileIndexer] Pick up comment, tags and rating changes");?></li>
<li><?php i18n("[UnindexedFileIndexer] Skip filetime checks for new files");?></li>
<li><?php i18n("[DocumentUrlDB] Avoid manipulation of the whole tree on trivial rename");?></li>
<li><?php i18n("[DocumentUrlDB] Catch invalid URLs early");?></li>
<li><?php i18n("[DocumentUrlDB] Remove unused 'rename' method");?></li>
<li><?php i18n("[balooctl] Streamline indexer control commands");?></li>
<li><?php i18n("[Transaction] Replace template for functor with std::function");?></li>
<li><?php i18n("[FirstRunIndexer] Use correct mimetype for folders");?></li>
<li><?php i18n("Move invariant IndexingLevel out of the loop");?></li>
<li><?php i18n("[BasicIndexingJob] Skip lookup of baloo document type for directories");?></li>
<li><?php i18n("[FileIndexScheduler] Ensure indexer is not run in suspended state");?></li>
<li><?php i18n("[PowerStateMonitor] Be conservative when determining power state");?></li>
<li><?php i18n("[FileIndexScheduler] Stop the indexer when quit() is called via DBus");?></li>
<li><?php i18n("Avoid container detach in a few places");?></li>
<li><?php i18n("Do not try to append to QLatin1String");?></li>
<li><?php i18n("Disable valgrind detection when compiling with MSVC");?></li>
<li><?php i18n("[FilteredDirIterator] Combine all suffixes into one large RegExp");?></li>
<li><?php i18n("[FilteredDirIterator] Avoid RegExp overhead for exact matches");?></li>
<li><?php i18n("[UnindexedFileIterator] Delay mimetype determination until it is needed");?></li>
<li><?php i18n("[UnindexedFileIndexer] Do not try to add nonexistent file to index");?></li>
<li><?php i18n("Detect valgrind, avoid database removal when using valgrind");?></li>
<li><?php i18n("[UnindexedFileIndexer] Loop optimizations (avoid detach, invariants)");?></li>
<li><?php i18n("Delay running UnindexedFileIndexer and IndexCleaner");?></li>
<li><?php i18n("[FileIndexScheduler] Add new state for Idle on battery");?></li>
<li><?php i18n("[FileIndexScheduler] Postpone housekeeping tasks while on battery");?></li>
<li><?php i18n("[FileIndexScheduler] Avoid emitting state changes multiple times");?></li>
<li><?php i18n("[balooctl] Clarify and extend status output");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Add MediaTransport API");?></li>
<li><?php i18n("Add LE Advertising and GATT APIs");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add id=\"current-color-scheme\" to collapse-all icons (bug 409546)");?></li>
<li><?php i18n("Add disk-quota icons (bug 389311)");?></li>
<li><?php i18n("Symlink install to edit-download");?></li>
<li><?php i18n("Change joystick settings icon to game controller (bug 406679)");?></li>
<li><?php i18n("Add edit-select-text, make 16px draw-text like 22px");?></li>
<li><?php i18n("Update KBruch icon");?></li>
<li><?php i18n("Add help-donate-[currency] icons");?></li>
<li><?php i18n("Make Breeze Dark use same Kolourpaint icon as Breeze");?></li>
<li><?php i18n("Add 22px notifications icons");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Fix a crash in KactivityTestApp when Result has strings with non-ASCII");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Do not crash if the inner file wants to be bigger than QByteArray max size");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KPluginMetaData: use Q_DECLARE_METATYPE");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("[GridDelegate] Fix gaps in corners of thumbnailArea highlight");?></li>
<li><?php i18n("get rid of blockSignals");?></li>
<li><?php i18n("[KCM GridDelegate] Silence warning");?></li>
<li><?php i18n("[KCM GridDelegate] Take into account implicitCellHeight for inner delegate height");?></li>
<li><?php i18n("Fix GridDelegate icon");?></li>
<li><?php i18n("Fix fragile comparison to i18n(\"None\") and describe behavior in docs (bug 407999)");?></li>
</ul>

<h3><?php i18n("KDE WebKit");?></h3>

<ul>
<li><?php i18n("Downgrade KDEWebKit from Tier 3 to Porting Aids");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Update pt-BR user.entities");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Fix extracting of some properties to match what was written (bug 408532)");?></li>
<li><?php i18n("Use debugging category in taglib extractor/writer");?></li>
<li><?php i18n("Format photo exposure bias value (bug 343273)");?></li>
<li><?php i18n("fix property name");?></li>
<li><?php i18n("Remove photo prefix from every exif property name (bug 343273)");?></li>
<li><?php i18n("Rename ImageMake and ImageModel properties (bug 343273)");?></li>
<li><?php i18n("[UserMetaData] Add method to query which attributes are set");?></li>
<li><?php i18n("Format focal length as millimeter");?></li>
<li><?php i18n("Format photo exposure time as rational when applicable (bug 343273)");?></li>
<li><?php i18n("Enable usermetadatawritertest for all UNIXes, not only Linux");?></li>
<li><?php i18n("Format the aperture values as F numbers (bug 343273)");?></li>
</ul>

<h3><?php i18n("KDE GUI Addons");?></h3>

<ul>
<li><?php i18n("KModifierKeyInfo: we are sharing the internal implementation");?></li>
<li><?php i18n("Remove double look-ups");?></li>
<li><?php i18n("Move to runtime the decision to use x11 or not");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Update UK Early May bank holiday for 2020 (bug 409189)");?></li>
<li><?php i18n("Fix ISO code for Hesse / Germany");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("QImage::byteCount -&gt; QImage::sizeInByes");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Fix KFileItemTest::testIconNameForUrl test to reflect different icon name");?></li>
<li><?php i18n("Fix i18n number-of-arguments error in knewfilemenu warning message");?></li>
<li><?php i18n("[ftp] Fix wrong access time in Ftp::ftpCopyGet() (bug 374420)");?></li>
<li><?php i18n("[CopyJob] Batch reporting processed amount");?></li>
<li><?php i18n("[CopyJob] Report results after finishing copy (bug 407656)");?></li>
<li><?php i18n("Move redundant logic in KIO::iconNameForUrl() into KFileItem::iconName() (bug 356045)");?></li>
<li><?php i18n("Install KFileCustomDialog");?></li>
<li><?php i18n("[Places panel] Don't show Root by default");?></li>
<li><?php i18n("Downgrade \"Could not change permissions\" dialog box to a qWarning");?></li>
<li><?php i18n("O_PATH is only available on linux. To prevent the compiler from throwing an error");?></li>
<li><?php i18n("Show feedback inline when creating new files or folders");?></li>
<li><?php i18n("Auth Support: Drop privileges if target is not owned by root");?></li>
<li><?php i18n("[copyjob] Only set modification time if the kio-slave provided it (bug 374420)");?></li>
<li><?php i18n("Cancel privilege operation for read-only target with the current user as owner");?></li>
<li><?php i18n("Add KProtocolInfo::defaultMimetype");?></li>
<li><?php i18n("Always save view settings when switching from one view mode to another");?></li>
<li><?php i18n("Restore exclusive group for sorting menu items");?></li>
<li><?php i18n("Dolphin-style view modes in the file dialog (bug 86838)");?></li>
<li><?php i18n("kio_ftp: improve error handling when copying to FTP fails");?></li>
<li><?php i18n("kioexec: change the scary debug messages for delayed deletion");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("[ActionTextField] Make action glow on press");?></li>
<li><?php i18n("support text mode and position");?></li>
<li><?php i18n("mouseover effect for breadcrumb on desktop");?></li>
<li><?php i18n("enforce a minimum height of 2 gridunits");?></li>
<li><?php i18n("Set SwipeListItem implicitHeight to be the maximum of content and actions");?></li>
<li><?php i18n("Hide tooltip when PrivateActionToolButton is pressed");?></li>
<li><?php i18n("Remove accidentally slipped back traces of cmake code for Plasma style");?></li>
<li><?php i18n("ColumnView::itemAt");?></li>
<li><?php i18n("force breeze-internal if no theme is specified");?></li>
<li><?php i18n("correct navigation on left pinned page");?></li>
<li><?php i18n("keep track of the space covered by pinned pages");?></li>
<li><?php i18n("show a separator when in left sidebar mode");?></li>
<li><?php i18n("in single column mode, pin has no effect");?></li>
<li><?php i18n("first semi working prototype of pinning");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("[KUiServerJobTracker] Handle ownership change");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("[kmoretools] Add icons to download and install actions");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Don't search for phonon on Android");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Add profile support interface for TerminalInterface");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Don't delay emission of matchesChanged indefinitely");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Add X-Flatpak-RenamedFrom as recognized key");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("fix goto line centering (bug 408418)");?></li>
<li><?php i18n("Fix bookmark icon display on icon border with low dpi");?></li>
<li><?php i18n("Fix action \"Show Icon Border\" to toggle border again");?></li>
<li><?php i18n("Fix empty pages in print preview and lines printed twice (bug 348598)");?></li>
<li><?php i18n("remove no longer used header");?></li>
<li><?php i18n("fix autoscrolling down speed (bug 408874)");?></li>
<li><?php i18n("Add default variables for variables interface");?></li>
<li><?php i18n("Make automatic spellcheck work after reloading a document (bug 408291)");?></li>
<li><?php i18n("raise default line length limit to 10000");?></li>
<li><?php i18n("WIP:Disable highlighting after 512 characters on a line");?></li>
<li><?php i18n("KateModeMenuList: move to QListView");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Include a description");?></li>
<li><?php i18n("Proof of concept of a wayland protocol to allow the keystate dataengine to work");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KPasswordLineEdit now correctly inherits its QLineEdit's focusPolicy (bug 398275)");?></li>
<li><?php i18n("Replace \"Details\" button with KCollapsibleGroupBox");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[Svg] Fix porting error from QRegExp::exactMatch");?></li>
<li><?php i18n("ContainmentAction: Fix loading from KPlugin");?></li>
<li><?php i18n("[TabBar] Remove exterior margins");?></li>
<li><?php i18n("Applet, DataEngine and containment listing methods inPlasma::PluginLoader no longer filters the plugins with X-KDE-ParentAppprovided when empty string is passed");?></li>
<li><?php i18n("Make pinch in calendar work again");?></li>
<li><?php i18n("Add disk-quota icons (bug 403506)");?></li>
<li><?php i18n("Make Plasma::Svg::elementRect a bit leaner");?></li>
<li><?php i18n("Automatically set version of desktopthemes packages to KF5_VERSION");?></li>
<li><?php i18n("Don't notify about changing to the same state it was at");?></li>
<li><?php i18n("Fix the alignment of the label of the toolbutton");?></li>
<li><?php i18n("[PlasmaComponents3] Vertically center button text as well");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Change initial size of the config dialog");?></li>
<li><?php i18n("Improve Job Dialog buttons' icons and text");?></li>
<li><?php i18n("Fix translation of actiondisplay");?></li>
<li><?php i18n("Don't show error message if sharing is cancelled by the user");?></li>
<li><?php i18n("Fix warning when reading plugin metadata");?></li>
<li><?php i18n("Redesign config pages");?></li>
<li><?php i18n("ECMPackageConfigHelpers -&gt; CMakePackageConfigHelpers");?></li>
<li><?php i18n("phabricator: Fix fallthrough in switch");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Show shortcut in menu item when specified (bug 405541)");?></li>
<li><?php i18n("Add MenuSeparator");?></li>
<li><?php i18n("Fix ToolButton remaining in a pressed state after press");?></li>
<li><?php i18n("[ToolButton] Pass custom icon size to StyleItem");?></li>
<li><?php i18n("honor visibility policy (bug 407014)");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("[Fstab] Select appropriate icon for home or root directory");?></li>
<li><?php i18n("[Fstab] Show mounted \"overlay\" filesystems");?></li>
<li><?php i18n("[UDev Backend] Narrow device queried for");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Fortran: relicense to MIT");?></li>
<li><?php i18n("Improve the Fortran fixed format syntax highlighting");?></li>
<li><?php i18n("Fortran: implement free &amp; fixed formats");?></li>
<li><?php i18n("Fix CMake COMMAND nested paren highlighting");?></li>
<li><?php i18n("Add more keywords and also support rr in gdb highlighter");?></li>
<li><?php i18n("Detect comment lines early in GDB highlighter");?></li>
<li><?php i18n("AppArmor: update syntax");?></li>
<li><?php i18n("Julia: update syntax and add constants keywords (bug 403901)");?></li>
<li><?php i18n("CMake: Highlight the standard CMake environment variables");?></li>
<li><?php i18n("Add syntax definition for ninja build");?></li>
<li><?php i18n("CMake: Support for 3.15 features");?></li>
<li><?php i18n("Jam: various improvements and fixes");?></li>
<li><?php i18n("Lua: update for Lua54 and end of function as Keyword rather than Control");?></li>
<li><?php i18n("C++: update for C++20");?></li>
<li><?php i18n("debchangelog: add Eoan Ermine");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.60");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.11");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
