<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("First release of KDE Frameworks 5");
  $site_root = "../";
  $release = '5.0.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p>
<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />
<?php i18n("
July 7th, 2014.  The KDE Community is proud to announce KDE Frameworks 5.0. Frameworks 5 is the next generation of KDE libraries, modularized and optimized for easy integration in Qt applications. The Frameworks offer a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. There are over 50 different Frameworks as part of this release providing solutions including hardware integration, file format support, additional widgets, plotting functions, spell checking and more.  Many of the Frameworks are cross platform and have minimal or no extra dependencies making them easy to build and add to any Qt application.
");?>
</p>
<p>
<?php i18n("
The  KDE Frameworks represent an effort to rework the powerful KDE Platform 4 libraries into a set of independent, cross platform modules that will be  readily available to all Qt developers to simplify, accelerate and  reduce the cost of Qt development. The individual Frameworks are  cross-platform and well documented and tested and their usage will be  familiar to Qt developers, following the style and standards set by the  Qt Project. Frameworks are developed under the proven KDE governance  model with a predictable release schedule, a clear and vendor neutral contributor process, open governance and flexible licensing (LGPL).
");?>
</p>
<p>
<?php i18n("
The Frameworks have a clear dependency structure, divided into Categories and Tiers. The Categories refer to runtime dependencies:
<ul>
<li><strong>Functional</strong> elements have no runtime dependencies.</li>
<li><strong>Integration</strong> designates code that may require runtime dependencies for integration depending on what the OS or platform offers.</li>
<li><strong>Solutions</strong> have mandatory runtime dependencies.</li>
</ul>
The <strong>Tiers</strong> refer to compile-time dependencies on other Frameworks. Tier 1 Frameworks have no dependencies within Frameworks and only need Qt and other relevant libraries. Tier 2 Frameworks can depend only on Tier 1.  Tier 3 Frameworks can depend on other Tier 3 Frameworks as well as Tier 2 and Tier 1.
");?>
</p>
<p>
<?php print i18n_var("
The transition from Platform to Frameworks has been in progress for over 3 years, guided by top KDE technical contributors.
Learn more about Frameworks 5 <a href='%1'>in this article from last year</a>.
", "http://dot.kde.org/2013/09/25/frameworks-5");?>
</p>
<?php i18n("
<h2>Highlights</h2>
");?>
</p>
<p>
<?php print i18n_var("
There are over 50 Frameworks currently available.  Browse the complete set <a href='%1'>in the online API documentation</a>. Below an impression of some of the functionality Frameworks offers to  Qt application developers.
", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<p>
<?php i18n("
<strong>KArchive</strong> offers support for many popular compression codecs in a  self-contained, featureful and easy-to-use file archiving and extracting  library. Just feed it files; there's no need to reinvent an archiving  function in your Qt-based application!
");?>
</p>
<br clear="all" />
<p>
<?php i18n("
<strong>ThreadWeaver</strong> offers a high-level API to manage threads using job- and queue-based interfaces. It allows easy scheduling of thread  execution by specifying dependencies between the threads and executing  them satisfying these dependencies, greatly simplifying the use of multiple threads.
");?>
</p>
<br clear="all" />
<p>
<?php i18n("
<strong>KConfig</strong> is a Framework to deal with storing and  retrieving configuration settings. It features a group-oriented API. It  works with INI files and XDG-compliant cascading directories. It  generates code based on XML files.
");?>
</p>
<br clear="all" />
<p>
<?php i18n("
<strong>Solid</strong> offers hardware detection and can inform an  application about storage devices and volumes, CPU, battery status,  power management, network status and interfaces, and Bluetooth. For  encrypted partitions, power and networking, running daemons are  required.
");?>
</p>
<br clear="all" />
<p>
<?php i18n("
<strong>KI18n</strong> adds Gettext support to applications, making it easier to integrate the translation workflow of Qt applications in the general translation infrastructure of many projects.
");?>
</p>
<br clear="all" />
<?php i18n("
<h2>Getting started</h2>
");?>
</p>
<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<ul>
<li><a href='%1'>Binary package distro install instructions</a>.<br /></li>
</ul>
Building  from source is possible using the basic <em>cmake .; make; make  install</em> commands. For a single Tier 1 framework, this is often  the easiest solution. People interested in contributing to frameworks  or tracking progress in development of the entire set are encouraged to  <a href='%2'>use kdesrc-build</a>.
", "http://community.kde.org/Frameworks/Binary_Packages", "http://kdesrc-build.kde.org/");?>
</p>
<?php print i18n_var("
Frameworks 5.0 requires Qt 5.2.  It represents the first in a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
<ul>
<li><a href='%1'>KDE Frameworks 5.0 Source Info page with known bugs and security issues</a></li>
</ul>
", "http://kde.org/info/kde-frameworks-5.0.0.php");?>
</p>
<?php print i18n_var("
The team is currently working on providing a detailed listing of all Frameworks and third party libraries at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>
<?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
