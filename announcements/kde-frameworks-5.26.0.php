<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.26.0");
  $site_root = "../";
  $release = '5.26.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
September 10, 2016. KDE today announces the release
of KDE Frameworks 5.26.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Add Qt5Network as a public dependency");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Fix include dir in pri file");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add missing namespace prefix definitions");?></li>
<li><?php i18n("Check SVG icons for wellformedness");?></li>
<li><?php i18n("Fix all edit-clear-location-ltr icons (bug 366519)");?></li>
<li><?php i18n("add kwin effect icon support");?></li>
<li><?php i18n("rename caps-on in input-caps-on");?></li>
<li><?php i18n("add caps icons for text input");?></li>
<li><?php i18n("add some gnome specific icons from Sadi58");?></li>
<li><?php i18n("add app icons from gnastyle");?></li>
<li><?php i18n("Dolphin, Konsole and Umbrello icons optimized for 16px, 22px, 32px");?></li>
<li><?php i18n("Updated VLC icon for 22px, 32px and 48px");?></li>
<li><?php i18n("Added app icon for Subtitle Composer");?></li>
<li><?php i18n("Fix Kleopatra new icon");?></li>
<li><?php i18n("Added app icon for Kleopatra");?></li>
<li><?php i18n("Added icons for Wine and Wine-qt");?></li>
<li><?php i18n("fix presentation word bug thanks Sadi58 (bug 358495)");?></li>
<li><?php i18n("add system-log-out icon in 32px");?></li>
<li><?php i18n("add 32px system- icons, remove colored system- icons");?></li>
<li><?php i18n("add pidgin, banshee icon support");?></li>
<li><?php i18n("remove vlc app icon due to license issue, add new VLC icon (bug 366490)");?></li>
<li><?php i18n("Add gthumb icon support");?></li>
<li><?php i18n("use HighlightedText for folder icons");?></li>
<li><?php i18n("places folder icons are now use stylesheet (highleight color)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("ecm_process_po_files_as_qm: Skip fuzzy translations");?></li>
<li><?php i18n("The default level for logging categories should be Info rather than Warning");?></li>
<li><?php i18n("Document ARGS variable in the create-apk-* targets");?></li>
<li><?php i18n("Create a test that validates projects' appstream information");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Add condition if group's platforms are not defined");?></li>
<li><?php i18n("Template: Sort platforms alphabetically");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("Bring from kdelibs the file used to generate kentities.c");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Add Donate entry to KStandardShortcut");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Add Donate standard action");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("[kpackagelauncherqml] Assume desktop file name is same as pluginId");?></li>
<li><?php i18n("Load QtQuick rendering settings from a config file and set default");?></li>
<li><?php i18n("icondialog.cpp - proper compile fix that doesn't shadow m_dialog");?></li>
<li><?php i18n("Fix crash when no QApplication is available");?></li>
<li><?php i18n("expose translation domain");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Fix Windows compilation error in kstyle.h");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("add paths for config, cache + data to general.entities");?></li>
<li><?php i18n("Made up-to-date with the English version");?></li>
<li><?php i18n("Add Space and Meta key entities to src/customization/en/user.entities");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Only require Xattr if the operating system is Linux");?></li>
<li><?php i18n("Restore Windows build");?></li>
</ul>

<h3><?php i18n("KIdleTime");?></h3>

<ul>
<li><?php i18n("[xsync] XFlush in simulateUserActivity");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("KPropertiesDialog: remove warning note from docu, the bug is gone");?></li>
<li><?php i18n("[test program] resolve relative paths using QUrl::fromUserInput");?></li>
<li><?php i18n("KUrlRequester: fix error box when selecting a file and reopening the file dialog");?></li>
<li><?php i18n("Provide a fallback if slaves don't list the . entry (bug 366795)");?></li>
<li><?php i18n("Fix creating symlink over \"desktop\" protocol");?></li>
<li><?php i18n("KNewFileMenu: when creating symlinks use KIO::linkAs instead of KIO::link");?></li>
<li><?php i18n("KFileWidget: fix double '/' in path");?></li>
<li><?php i18n("KUrlRequester: use static connect() syntax, was inconsistent");?></li>
<li><?php i18n("KUrlRequester: pass window() as parent for the QFileDialog");?></li>
<li><?php i18n("avoid calling connect(null, .....) from KUrlComboRequester");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("uncompress archives in subfolders");?></li>
<li><?php i18n("No longer allow installing to generic data folder because of potential security hole");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Get StatusNotifierWatcher property ProtocolVersion in async way");?></li>
</ul>

<h3><?php i18n("Package Framework");?></h3>

<ul>
<li><?php i18n("silence contentHash deprecation warnings");?></li>
</ul>

<h3><?php i18n("Kross");?></h3>

<ul>
<li><?php i18n("Revert \"Remove unused KF5 dependencies\"");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("remove accel clash (bug 363738)");?></li>
<li><?php i18n("fix email address highlighting in doxygen (bug 363186)");?></li>
<li><?php i18n("detect some more json files, like our own projects ;)");?></li>
<li><?php i18n("improve mime-type detection (bug 357902)");?></li>
<li><?php i18n("Bug 363280 - highlighting: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif (bug 363280)");?></li>
<li><?php i18n("Bug 363280 - highlighting: c++: #if 1 #endif #if defined (A) aaa #elif defined (B) bbb #endif");?></li>
<li><?php i18n("Bug 351496 - Python folding is not working during initial typing (bug 351496)");?></li>
<li><?php i18n("Bug 365171 - Python syntax highlighting: not working correctly for escape sequences (bug 365171)");?></li>
<li><?php i18n("Bug 344276 - php nowdoc not folded correctly (bug 344276)");?></li>
<li><?php i18n("Bug 359613 - Some CSS3 properties are not supported in syntax highlight (bug 359613)");?></li>
<li><?php i18n("Bug 367821 - wineHQ syntax: The section in a reg file isn't highlighted correctly (bug 367821)");?></li>
<li><?php i18n("Improve swap file handling if swap directory specified");?></li>
<li><?php i18n("Fix crash when reloading documents with auto-wrapped line due to line length limit (bug 366493)");?></li>
<li><?php i18n("Fix constant crashes related to the vi command bar (bug 367786)");?></li>
<li><?php i18n("Fix: Line numbers in printed documents now starts at 1 (bug 366579)");?></li>
<li><?php i18n("Backup Remote Files: Treat mounted files also as remote files");?></li>
<li><?php i18n("cleanup logic for searchbar creation");?></li>
<li><?php i18n("add highlighting for Magma");?></li>
<li><?php i18n("Allows only one level of recursion");?></li>
<li><?php i18n("Fix broken swap-file on windows");?></li>
<li><?php i18n("Patch: add bitbake support for syntax highlighting engine");?></li>
<li><?php i18n("autobrace: look at spellcheck attribute where the character was entered (bug 367539)");?></li>
<li><?php i18n("Highlight QMAKE_CFLAGS");?></li>
<li><?php i18n("Don't pop out of the main context");?></li>
<li><?php i18n("Add some executable names that are commonly used");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Add British \"stone\" unit of mass");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Move kwallet-query docbook to correct subdir");?></li>
<li><?php i18n("Fix wording an -&gt; one");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Make linux/input.h compile time optional");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Fix background of non-BMP characters");?></li>
<li><?php i18n("Add C octal escaped UTF-8 search");?></li>
<li><?php i18n("Make the default KMessageBoxDontAskAgainMemoryStorage save to QSettings");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Port to Donate standard action");?></li>
<li><?php i18n("Port away from deprecated authorizeKAction");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("fix device icon 22px icon didn't work in the old file");?></li>
<li><?php i18n("WindowThumbnail: Do GL calls in the correct thread (bug 368066)");?></li>
<li><?php i18n("Make plasma_install_package work with  KDE_INSTALL_DIRS_NO_DEPRECATED");?></li>
<li><?php i18n("add margin and padding to the start.svgz icon");?></li>
<li><?php i18n("fix stylesheet stuff in computer icon");?></li>
<li><?php i18n("add computer and laptop icon for kicker (bug 367816)");?></li>
<li><?php i18n("Fix cannot assign undefined to double warning in DayDelegate");?></li>
<li><?php i18n("fix stylesheed svgz files are not in love with me");?></li>
<li><?php i18n("rename the 22px icons to 22-22-x and the 32px icons to x for kicker");?></li>
<li><?php i18n("[PlasmaComponents TextField] Don't bother loading icons for unused buttons");?></li>
<li><?php i18n("Extra guard in Containment::corona in the special system tray case");?></li>
<li><?php i18n("When marking a containment as deleted, also mark all sub-applets as deleted - fixes system tray container configs not being deleted");?></li>
<li><?php i18n("Fix Device Notifier icon");?></li>
<li><?php i18n("add system-search to system in 32 and 22px size");?></li>
<li><?php i18n("add monochrome icons for kicker");?></li>
<li><?php i18n("Set colour scheme on system-search icon");?></li>
<li><?php i18n("Move system-search into system.svgz");?></li>
<li><?php i18n("Fix wrong or missing \"X-KDE-ParentApp\" in desktop file definitions");?></li>
<li><?php i18n("Fix API dox of Plasma::PluginLoader: mixup of applets/dataengine/services/..");?></li>
<li><?php i18n("add system-search icon for the sddm theme");?></li>
<li><?php i18n("add nepomuk 32px icon");?></li>
<li><?php i18n("update touchpad icon for the system tray");?></li>
<li><?php i18n("Remove code that can never be executed");?></li>
<li><?php i18n("[ContainmentView] Show panels when UI becomes ready");?></li>
<li><?php i18n("Don't redeclare property implicitHeight");?></li>
<li><?php i18n("use QQuickViewSharedEngine::setTranslationDomain (bug 361513)");?></li>
<li><?php i18n("add 22px and 32px plasma breeze icon support");?></li>
<li><?php i18n("remove colored system icons and add 32px monochrome ones");?></li>
<li><?php i18n("Add an optional reveal password button to TextField");?></li>
<li><?php i18n("The standard tooltips are now mirrored when in a right-to-left language");?></li>
<li><?php i18n("Performance when changing months in the calendar has been greatly improved");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Don't lowercase the language names in trigram parsing");?></li>
<li><?php i18n("Fix immediate crash on startup due to null plugin pointer");?></li>
<li><?php i18n("Handle dictionaries without correct names");?></li>
<li><?php i18n("Replace hand-curated list of script-language mappings, use proper names for languages");?></li>
<li><?php i18n("Add tool to generate trigrams");?></li>
<li><?php i18n("Unbreak language detection a bit");?></li>
<li><?php i18n("Use selected language as suggestion for detection");?></li>
<li><?php i18n("Use cached spellers in language detection, improve performance a bit");?></li>
<li><?php i18n("Improve language detection");?></li>
<li><?php i18n("Filter list of suggestions against available dictionaries, remove dupes");?></li>
<li><?php i18n("Remember to add the last trigram match");?></li>
<li><?php i18n("Check if any of the trigrams actually matched");?></li>
<li><?php i18n("Handle multiple languages with same score in trigram matcher");?></li>
<li><?php i18n("Don't check for minimum size twice");?></li>
<li><?php i18n("Prune list of languages against available languages");?></li>
<li><?php i18n("Use same minimum length everywhere in langdet");?></li>
<li><?php i18n("Sanity check that the loaded model has the correct amount of trigrams for each language");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.26");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.5");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
