<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.40.0");
  $site_root = "../";
  $release = '5.40.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
November 11, 2017. KDE today announces the release
of KDE Frameworks 5.40.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Consider DjVu files to be documents (bug 369195)");?></li>
<li><?php i18n("Fix spelling so WPS Office presentations are recognized correctly");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("add folder-stash for the stash Dolphin toolbar icon");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Fix potential mem leak. Fix logic");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("no margins for qml modules from qwidget side");?></li>
<li><?php i18n("Initialize variables (found by coverity)");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Fix icon of KStandardAction::MoveToTrash");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("fix URL detection with double urls like \"http://www.foo.bar&lt;http://foo.bar/&gt;\"");?></li>
<li><?php i18n("Use https for KDE urls");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("full docu for disableSessionManagement() replacement");?></li>
<li><?php i18n("Make kssl compile against OpenSSL 1.1.0 (bug 370223)");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Fix display name of Generator property");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("KGlobalAccel: fix support numpad keys (again)");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Correct installation of start_kdeinit when DESTDIR and libcap are used together");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Fix display of remote:/ in the qfiledialog");?></li>
<li><?php i18n("Implement support for categories on KfilesPlacesView");?></li>
<li><?php i18n("HTTP: fix error string for the 207 Multi-Status case");?></li>
<li><?php i18n("KNewFileMenu: clean up dead code, spotted by Coverity");?></li>
<li><?php i18n("IKWS: Fix possible infinite loop, spotted by Coverity");?></li>
<li><?php i18n("KIO::PreviewJob::defaultPlugins() function");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("syntax working on older Qt 5.7 (bug 385785)");?></li>
<li><?php i18n("stack the overlaysheet differently (bug 386470)");?></li>
<li><?php i18n("Show the delegate highlighted property as well when there's no focus");?></li>
<li><?php i18n("preferred size hints for the separator");?></li>
<li><?php i18n("correct Settings.isMobile usage");?></li>
<li><?php i18n("Allow applications to be somewhat convergent on a desktop-y system");?></li>
<li><?php i18n("Make sure the content of the SwipeListItem doesn't overlap the handle (bug 385974)");?></li>
<li><?php i18n("Overlaysheet's scrollview is always ointeractive");?></li>
<li><?php i18n("Add categories in gallery desktop file (bug 385430)");?></li>
<li><?php i18n("Update the kirigami.pri file");?></li>
<li><?php i18n("use the non installed plugin to do the tests");?></li>
<li><?php i18n("Deprecate Kirigami.Label");?></li>
<li><?php i18n("Port gallery example use of Labels to be consistently QQC2");?></li>
<li><?php i18n("Port Kirigami.Controls uses of Kirigami.Label");?></li>
<li><?php i18n("make the scrollarea interactive on touch events");?></li>
<li><?php i18n("Move the git find_package call to where it's used");?></li>
<li><?php i18n("default to transparent listview items");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Remove PreferCache from network requests");?></li>
<li><?php i18n("Don't detach shared pointers to private data when setting previews");?></li>
<li><?php i18n("KMoreTools: Update and fix desktopfiles (bug 369646)");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Remove check for SNI hosts when chosing whether to use legacy mode (bug 385867)");?></li>
<li><?php i18n("Only check for legacy system tray icons if we're going to make one (bug 385371)");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("use the non installed service files");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Initialize values");?></li>
<li><?php i18n("Initialize some pointer");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("API dox: fix wrong names of methods and args, add missing \since");?></li>
<li><?php i18n("Avoid (certain) crashes while executing QML scripts (bug 385413)");?></li>
<li><?php i18n("Avoid a QML crash triggered by C style indentation scripts");?></li>
<li><?php i18n("Increase size of trailing mark");?></li>
<li><?php i18n("fix some indenters from indenting on random characters");?></li>
<li><?php i18n("Fix deprecation warning");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("Initialize value");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("[client] Drop the checks for platformName being \"wayland\"");?></li>
<li><?php i18n("Don't duplicate connect to wl_display_flush");?></li>
<li><?php i18n("Wayland foreign protocol");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("fix createKMessageBox focus widget inconsistency");?></li>
<li><?php i18n("more compact password dialog (bug 381231)");?></li>
<li><?php i18n("Set KPageListView width properly");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("KKeyServer: fix handling of Meta+Shift+Print, Alt+Shift+arrowkey etc");?></li>
<li><?php i18n("Support flatpak platform");?></li>
<li><?php i18n("Use KWindowSystem's own platform detection API instead of duplicated code");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Use https for KDE urls");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("8021xSetting: domain-suffix-match is defined in NM 1.2.0 and newer");?></li>
<li><?php i18n("Support \"domain-suffix-match\" in Security8021xSetting");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("manually draw the circle arc");?></li>
<li><?php i18n("[PlasmaComponents Menu] Add ungrabMouseHack");?></li>
<li><?php i18n("[FrameSvg] Optimize updateSizes");?></li>
<li><?php i18n("Don't position a Dialog if it's of type OSD");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Improve compilation as a static plugin");?></li>
<li><?php i18n("make the radiobutton a radiobutton");?></li>
<li><?php i18n("use qstyle to paint the Dial");?></li>
<li><?php i18n("use a ColumnLayout for menus");?></li>
<li><?php i18n("fix Dialog");?></li>
<li><?php i18n("remove invalid group property");?></li>
<li><?php i18n("Fix formatting of the md file so it matches the other modules");?></li>
<li><?php i18n("behavior of combobox closer to qqc1");?></li>
<li><?php i18n("workaround for QQuickWidgets");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Add assignByDictionnary method");?></li>
<li><?php i18n("Signal if we are able to assign dictionary");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Makefile: fix regexpr matching in \"CXXFLAGS+\"");?></li>
</ul>

<h3><?php i18n("ThreadWeaver");?></h3>

<ul>
<li><?php i18n("CMake cleanup: Don't hardcode -std=c++0x");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.40");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.7");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
