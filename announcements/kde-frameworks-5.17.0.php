<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.17.0");
  $site_root = "../";
  $release = '5.17.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
December 12, 2015. KDE today announces the release
of KDE Frameworks 5.17.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Fix date filter used by timeline://");?></li>
<li><?php i18n("BalooCtl: Return after commands");?></li>
<li><?php i18n("Clean up and armour Baloo::Database::open(), handle more crash conditions");?></li>
<li><?php i18n("Add check in Database::open(OpenDatabase) to fail if db doesn't exist");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Many icons added or improved");?></li>
<li><?php i18n("use stylesheets in breeze icons (bug 126166)");?></li>
<li><?php i18n("BUG: 355902 fix and changed system-lock-screen (bug 355902 fix and changed system-lock-screen)");?></li>
<li><?php i18n("Add 24px dialog-information for GTK apps (bug 355204)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Don't warn when SVG(Z) icons are provided with multiple sizes/level of detail");?></li>
<li><?php i18n("Make sure we load translations on the main thread. (bug 346188)");?></li>
<li><?php i18n("Overhaul the ECM build system.");?></li>
<li><?php i18n("Make it possible to enable Clazy on any KDE project");?></li>
<li><?php i18n("Do not find XCB's XINPUT library by default.");?></li>
<li><?php i18n("Clean export dir before generating an APK again");?></li>
<li><?php i18n("Use quickgit for Git repository URL.");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Add plasmoid installation failed to plasma_workspace.notifyrc");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Fixed a lock on the first start of the daemon");?></li>
<li><?php i18n("Moving QAction creation to the main thread. (bug 351485)");?></li>
<li><?php i18n("Sometimes clang-format makes a bad decision (bug 355495)");?></li>
<li><?php i18n("Killing potential synchronization issues");?></li>
<li><?php i18n("Use org.qtproject instead of com.trolltech");?></li>
<li><?php i18n("Removing the usage of libkactivities from the plugins");?></li>
<li><?php i18n("KAStats config removed from the API");?></li>
<li><?php i18n("Added linking and unlinking to ResultModel");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Make kgenframeworksapidox more robust.");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Fix KCompressionDevice::seek(), called when creating a KTar on top of a KCompressionDevice.");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KAboutData: Allow https:// and other URL schemas in homepage. (bug 355508)");?></li>
<li><?php i18n("Repair MimeType property when using kcoreaddons_desktop_to_json()");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Port KDeclarative to use KI18n directly");?></li>
<li><?php i18n("DragArea delegateImage can now be a string from which an icon is automatically created");?></li>
<li><?php i18n("Add new CalendarEvents library");?></li>
</ul>

<h3><?php i18n("KDED");?></h3>

<ul>
<li><?php i18n("Unset SESSION_MANAGER envvar instead of setting it empty");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Fix some i18n calls.");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Mark m4a as readable by taglib");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Cookie dialogue: make it work as intended");?></li>
<li><?php i18n("Fix filename suggestion changing to something random when changing save-as mimetype.");?></li>
<li><?php i18n("Register DBus name for kioexec (bug 353037)");?></li>
<li><?php i18n("Update KProtocolManager after configuration change.");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("Fix KSelectionProxyModel usage in QTableView (bug 352369)");?></li>
<li><?php i18n("Fix resetting or changing the source model of a KRecursiveFilterProxyModel.");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("registerServicesByGroupingNames can define default more items");?></li>
<li><?php i18n("Make KMoreToolsMenuFactory::createMenuFromGroupingNames lazy");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Add syntax highlighting for TaskJuggler and PL/I");?></li>
<li><?php i18n("Make it possible to disable keyword-completion via the config interface.");?></li>
<li><?php i18n("Resize the tree when the completion model got reset.");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Correctly handle the case where the user deactivated us");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Fix a small artifact of KRatingWidget on hi-dpi.");?></li>
<li><?php i18n("Refactor and fix the feature introduced in bug 171343 (bug 171343)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Don't call QCoreApplication::setQuitLockEnabled(true) on init.");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Add basic plasmoid as example for developerguide");?></li>
<li><?php i18n("Add a couple of plasmoid templates for kapptemplate/kdevelop");?></li>
<li><?php i18n("[calendar] Delay the model reset until the view is ready (bug 355943)");?></li>
<li><?php i18n("Don't reposition while hiding. (bug 354352)");?></li>
<li><?php i18n("[IconItem] Don't crash on null KIconLoader theme (bug 355577)");?></li>
<li><?php i18n("Dropping image files onto a panel will no longer offer to set them as wallpaper for the panel");?></li>
<li><?php i18n("Dropping a .plasmoid file onto a panel or the desktop will install and add it");?></li>
<li><?php i18n("remove the now unused platformstatus kded module (bug 348840)");?></li>
<li><?php i18n("allow paste on password fields");?></li>
<li><?php i18n("fix positioning of edit menu, add a button to select");?></li>
<li><?php i18n("[calendar] Use ui language for getting the month name (bug 353715)");?></li>
<li><?php i18n("[calendar] Sort the events by their type too");?></li>
<li><?php i18n("[calendar] Move the plugin library to KDeclarative");?></li>
<li><?php i18n("[calendar] qmlRegisterUncreatableType needs a bit more arguments");?></li>
<li><?php i18n("Allow adding config categories dynamically");?></li>
<li><?php i18n("[calendar] Move the plugins handling to a separate class");?></li>
<li><?php i18n("Allow plugins to supply event data to Calendar applet (bug 349676)");?></li>
<li><?php i18n("check for slot existence before connecting or disconnecting (bug 354751)");?></li>
<li><?php i18n("[plasmaquick] Don't link OpenGL explicitly");?></li>
<li><?php i18n("[plasmaquick] Drop XCB::COMPOSITE and DAMAGE dependency");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.17");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.3");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
