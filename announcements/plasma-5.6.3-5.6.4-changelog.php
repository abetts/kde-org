<?php
include_once ("functions.inc");
$translation_file = "www";
$page_title = i18n_noop("Plasma 5.6.4 Complete Changelog");
$site_root = "../";
$release = 'plasma-5.6.4';
include "header.inc";
?>
<p><a href="plasma-5.6.4.php">Plasma 5.6.4</a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='breeze-plymouth' href='http://quickgit.kde.org/?p=breeze-plymouth.git'>Breeze-plymouth</a> </h3>
<ul id='ulbreeze-plymouth' style='display: block'>
	<li>Update artwork. <a href='http://quickgit.kde.org/?p=breeze-plymouth.git&amp;a=commit&amp;h=d9b1f28af5a19ae706f8cceff6800abec32e272d'>Commit.</a> </li>
	
</ul>


<h3><a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Properly allocate Qt container. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=332cf4e6a675647a578bf0d5c6b7e8f4755dd736'>Commit.</a> </li>
<li>Properly filter progress. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=397e924e34cf18f63289715209f672127eb6f5aa'>Commit.</a> </li>
<li>Don't use Qt shadowing macros for final and override. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6164e54bcb2f44ebd8075a3130fdd700c18ce2f9'>Commit.</a> </li>
<li>Use correct API. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=7fb9a969fc988a2b076573fd22871c40599d8978'>Commit.</a> </li>
<li>Make sure arguments are coherent. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=a408c5bd278ab313cdbba6f1f6e9e44e06efe467'>Commit.</a> </li>
<li>Modernize QApt backend. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6aa2f5249d40c84027df6070b6272f1c1188a97a'>Commit.</a> </li>
<li>Use move constructor. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=6c3bc9d22815a8a9a824d6026af920715c345363'>Commit.</a> </li>
<li>Modernize constructors. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0c3f33acc28244856e12ec66e30e4515efad1b10'>Commit.</a> </li>
<li>Remove unused method. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=0d99e00de9c815afdcb11291ef3348f74dbfe017'>Commit.</a> </li>
<li>Use the correct descriptions. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=efcba3838636f3d062477bd4c09aa0a386f1a6b3'>Commit.</a> </li>
<li>Don't load non-package appstream resources. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c842b4e8c6d930c14e9919648dd0944826d8082d'>Commit.</a> </li>
<li>Fix warning. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=127d8fd93b57b576d6dfb16b119ba39c30c3e9c2'>Commit.</a> </li>
<li>Make sure all elements are visible in the CategoryView. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=2f4265c58a663b3426add0fbf4edb77c11fd6da1'>Commit.</a> </li>
<li>Clean up search a bit. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=5c096de4ee384a001792f887cbb7153ff09013c7'>Commit.</a> </li>
<li>Be specific. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=c9ee14da31c1e42830a7b09ae5e50666f979aab1'>Commit.</a> </li>
<li>Add the expected properties of a top level page. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=30eae35d362a988fdb339cf0a71f9050ef26f93f'>Commit.</a> </li>
<li>Fix invokation by package name. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=65c0a6849accf9a9de7892a7d7509f7eeae6daf0'>Commit.</a> </li>
<li>Don't use a value that is no longer valid. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=9fe37e84954f0e2c61c9e4d714369f9c4b9b8341'>Commit.</a> </li>
<li>Make sure "Dummy 1" always has screenshots. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=7498a77d99b0d56c3ea00127f75d330ae6d71a26'>Commit.</a> </li>
<li>Don't try to create a component that isn't in a package. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=691a886177791f0fcf98ad6cc0e5241dddbcf7d6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362228'>#362228</a></li>
<li>Make sure kcrash is initialized for discover. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=63879411befc50bfd382d014ca2efa2cd63e0811'>Commit.</a> </li>
<li>When the search is over, let the view decide whether to jump up. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=1dad826bfc31e5c4224720c83eeed99c2ccc94fa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362087'>#362087</a></li>
<li>Make sure we don't get increasingly big delegates for the carousel. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=fba6930af0e6365acdf4707c542c5487923356fb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362059'>#362059</a></li>
<li>Fix prioritary carussel local json file look-up. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=7d9171234581a684f8cc804f61c1f38e0619e103'>Commit.</a> </li>
<li>Add an arcconfig for phab. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=f72b7377c3df241c28c0be2dfae5f3a066161de9'>Commit.</a> </li>
</ul>


<h3><a name='kde-cli-tools' href='http://quickgit.kde.org/?p=kde-cli-tools.git'>kde-cli-tools</a> </h3>
<ul id='ulkde-cli-tools' style='display: block'>
<li>Add arcconfig for phab. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=4866a1374f5c3e4011c4dff87dbe72b79b9f1c9c'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Remove highlightMask, it's outdated and unusable. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=e3614f9ab82ca3b87de7db9d3abbf593ce6ab04b'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127794'>#127794</a></li>
<li>[Converter Runner] Try QLocale::toDouble to get input value. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=54a37fc80c33c1f76faa4975868d1cc824d38dc2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127802'>#127802</a></li>
</ul>


<h3><a name='kinfocenter' href='http://quickgit.kde.org/?p=kinfocenter.git'>Info Center</a> </h3>
<ul id='ulkinfocenter' style='display: block'>
<li>Remove second list storing duplicate data. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=6ebda194ea7c54f2d2f3087132f8a7aaff6a2acc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127864'>#127864</a></li>
<li>Keep a reference to the Solid::Device whilst using it's interface. <a href='http://quickgit.kde.org/?p=kinfocenter.git&amp;a=commit&amp;h=95569a0eae884427c7f7ab11fd63ae577f0be16d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350861'>#350861</a>. Code review <a href='https://git.reviewboard.kde.org/r/127862'>#127862</a></li>
</ul>


<h3><a name='kscreen' href='http://quickgit.kde.org/?p=kscreen.git'>KScreen</a> </h3>
<ul id='ulkscreen' style='display: block'>
<li>Fix serializertest by adding missing data. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=82cd8f8294982ec70e2a500eb041310ed1ce77bc'>Commit.</a> </li>
<li>Make the serializer a bit more robust. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=94f235982af53e252dab9efb049ddcacee2a2d3c'>Commit.</a> </li>
<li>Ix crasher in kscreen kded daemon. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=338a781e71ee18c43681356b8ea74e0171c55f6c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362586'>#362586</a></li>
<li>Fix the building of tests in parallel. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=47a001d9217c94f95880120f48bccc4272a98e78'>Commit.</a> </li>
<li>Fix typo in README. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=25917abfbf7a748a96807b8f78250afc991dd7c4'>Commit.</a> </li>
<li>Fix generator autotest. <a href='http://quickgit.kde.org/?p=kscreen.git&amp;a=commit&amp;h=7f2200ab90ea3494e70f7fda29187c769239c592'>Commit.</a> </li>
</ul>


<h3><a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Skip DontCrashEmptyDecorationTest if we don't have a /dev/dri/card0 device. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=6d60f01ad96abae030d3f9aa43349c8e479f0014'>Commit.</a> </li>
</ul>


<h3><a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Validate the config before checking if it can be applied. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=5e55fd1e0e8aa8379d67101822b1ca8954d7f18f'>Commit.</a> See bug <a href='https://bugs.kde.org/362586'>#362586</a></li>
<li>Verify rotation when updating screen size in XRandR backend. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=c3cb380aa0835c268a84a4c480854b91ab9a76f2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356228'>#356228</a></li>
</ul>


<h3><a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Silence warning. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a378296d2feeb4e52c2561dba52fbdf2dfa18f68'>Commit.</a> </li>
<li>[taskmanager] Stop parsing executables as .desktop files. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=71055e2b725fbb65e16055588f86a57c373d8b47'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127571'>#127571</a></li>
<li>Add PowerDevil runner to the AppDash default set. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=823402adfa9d8bfc9c740a4e1ff87d8830293d73'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362078'>#362078</a></li>
<li>Add missing namespace. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=162c65ea1e5ca66ae3bff5d127ed3c7e72bca419'>Commit.</a> </li>
<li>Fix DND on Folder View containments with locked widgets. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=bca9fc813e4dfe6faf59d5945ac19c96c49b1e6f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/360578'>#360578</a></li>
</ul>


<h3><a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=e791e31c34c6347b7b092c9fb24bbbcd40dd7f5b'>Commit.</a> </li>
<li>Fix data validation when creating new pppoe connections. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=c2be0336226cf27263ed234f69cddd3b5214c267'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/362175'>#362175</a></li>
</ul>


<h3><a name='plasma-sdk' href='http://quickgit.kde.org/?p=plasma-sdk.git'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Correct window icon for Cuttlefish icon viewer. <a href='http://quickgit.kde.org/?p=plasma-sdk.git&amp;a=commit&amp;h=ccb2f5b19487f4510bb08a1da7916c1169492c4b'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Kuiserver: use QApplication rather than QCoreApplication. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5f6a27e7754241e744cd9e92bb812da0d7f5e274'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348123'>#348123</a>. Code review <a href='https://git.reviewboard.kde.org/r/127793'>#127793</a></li>
<li>[digital-clock] Fix display of seconds with certain locales. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a7a22de14c360fa5c975e0bae30fc22e4cd7cc43'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/127623'>#127623</a></li>
<li>Guard m_controller with a QPointer. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=f86fa3cd776a48367aa15331c1c0b102fd378d03'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361985'>#361985</a></li>
<li>Expand window->desktopentry mapping heuristics to attempt RDN matching. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e0f2adaad6bc315e00109e23f75b69c076611a97'>Commit.</a> </li>
</ul>


<?php
  include("footer.inc");
?>
