<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.44.0");
  $site_root = "../";
  $release = '5.44.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
March 10, 2018. KDE today announces the release
of KDE Frameworks 5.44.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("balooctl: Remove checkDb option (bug 380465)");?></li>
<li><?php i18n("indexerconfig: Describe some functions");?></li>
<li><?php i18n("indexerconfig: Expose canBeSearched function (bug 388656)");?></li>
<li><?php i18n("balooctl monitor: Wait for dbus interface");?></li>
<li><?php i18n("fileindexerconfig: Introduce canBeSearched() (bug 388656)");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("remove view-media-playlist from preferences icons");?></li>
<li><?php i18n("add 24px media-album-cover icon");?></li>
<li><?php i18n("add Babe QML support (22px)");?></li>
<li><?php i18n("update handle- icons for kirigami");?></li>
<li><?php i18n("add 64px media icons for elisa");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Define __ANDROID_API__");?></li>
<li><?php i18n("Fix readelf command name on x86");?></li>
<li><?php i18n("Android toolchain: add ANDROID_COMPILER_PREFIX variable, fix include path for x86 targets, extend search path for NDK dependencies");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Exit in error if the output directory is not empty (bug 390904)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Save some memory allocations by using the right API");?></li>
<li><?php i18n("Export kconf_update with tooling");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Improve KLanguageButton::insertLanguage when no name is passed");?></li>
<li><?php i18n("Add icons for KStandardActions Deselect and Replace");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Cleanup m_inotify_wd_to_entry before invalidating Entry pointers (bug 390214)");?></li>
<li><?php i18n("kcoreaddons_add_plugin: remove effectless OBJECT_DEPENDS on json file");?></li>
<li><?php i18n("Help automoc to find metadata JSON files referenced in the code");?></li>
<li><?php i18n("kcoreaddons_desktop_to_json: note the generated file in the build log");?></li>
<li><?php i18n("Bump shared-mime-info to 1.3");?></li>
<li><?php i18n("Introduce K_PLUGIN_CLASS_WITH_JSON");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Fix the build failure on armhf/aarch64");?></li>
<li><?php i18n("Kill QmlObjectIncubationController");?></li>
<li><?php i18n("disconnect render() on window change (bug 343576)");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Allen Winter is now officially the maintainer of KHolidays");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("API dox: add note about calling setApplicationDomain after QApp creation");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("[KIconLoader] Take into account devicePixelRatio for overlays");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Do not assume layout of msghdr and iovec structure (bug 391367)");?></li>
<li><?php i18n("Fix protocol selection in KUrlNavigator");?></li>
<li><?php i18n("Change qSort to std::sort");?></li>
<li><?php i18n("[KUrlNavigatorPlacesSelector] Use KFilePlacesModel::convertedUrl");?></li>
<li><?php i18n("[Drop Job] Create proper trash file on linking");?></li>
<li><?php i18n("Fix unintentional breadcrumb menu item activation (bug 380287)");?></li>
<li><?php i18n("[KFileWidget] Hide places frame and header");?></li>
<li><?php i18n("[KUrlNavigatorPlacesSelector] Put categories into submenus (bug 389635)");?></li>
<li><?php i18n("Make use of the standard KIO test helper header");?></li>
<li><?php i18n("Add Ctrl+H to the list of shortcuts for \"show/hide hidden files\" (bug 390527)");?></li>
<li><?php i18n("Add move semantics support to KIO::UDSEntry");?></li>
<li><?php i18n("Fix \"ambiguous shortcut\" issue introduced with D10314");?></li>
<li><?php i18n("Stuff the \"Couldn't find executable\" message box into a queued lambda (bug 385942)");?></li>
<li><?php i18n("Improve usability of \"Open With\" dialog by adding option to filter the application tree");?></li>
<li><?php i18n("[KNewFileMenu] KDirNotify::emitFilesAdded after storedPut (bug 388887)");?></li>
<li><?php i18n("Fix assert when cancelling the rebuild-ksycoca dialog (bug 389595)");?></li>
<li><?php i18n("Fix bug #382437 \"Regression in kdialog causes wrong file extension\" (bug 382437)");?></li>
<li><?php i18n("Faster simplejob start");?></li>
<li><?php i18n("Repair copying file to VFAT without warnings");?></li>
<li><?php i18n("kio_file: skip error handling for initial perms during file copy");?></li>
<li><?php i18n("Allow move semantics to be generated for KFileItem. The existing copy constructor, destructor and copy assignment operator are now also generated by the compiler");?></li>
<li><?php i18n("Don't stat(/etc/localtime) between read() and write() copying files (bug 384561)");?></li>
<li><?php i18n("remote: don't create entries with empty names");?></li>
<li><?php i18n("Add supportedSchemes feature");?></li>
<li><?php i18n("Use F11 as the shortcut to toggle the aside preview");?></li>
<li><?php i18n("[KFilePlacesModel] Group network shares under \"Remote\" category");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Show tool button as checked while the menu is shown");?></li>
<li><?php i18n("non interactive scroll indicators on mobile");?></li>
<li><?php i18n("Fix submenus of actions");?></li>
<li><?php i18n("Make it possible to use QQC2.Action");?></li>
<li><?php i18n("Make it possible to support exclusive action groups (bug 391144)");?></li>
<li><?php i18n("Show the text by the page action tool buttons");?></li>
<li><?php i18n("Make it possible for actions to show submenus");?></li>
<li><?php i18n("Don't have specific component position in its parent");?></li>
<li><?php i18n("Don't trigger SwipeListItem's actions unless they are exposed");?></li>
<li><?php i18n("Add an isNull() check before setting whether QIcon is a mask");?></li>
<li><?php i18n("Add FormLayout.qml to kirigami.qrc");?></li>
<li><?php i18n("fix swipelistitem colors");?></li>
<li><?php i18n("better behavior for headers and footers");?></li>
<li><?php i18n("Improve ToolBarApplicationHeader left padding and eliding behavior");?></li>
<li><?php i18n("Make sure the navigation buttons don't go under the action");?></li>
<li><?php i18n("support for header and footer properties in overlaysheet");?></li>
<li><?php i18n("Eliminate unnecessary bottom padding on OverlaySheets (bug 390032)");?></li>
<li><?php i18n("Polish ToolBarApplicationHeader appearance");?></li>
<li><?php i18n("show a close button on desktop (bug 387815)");?></li>
<li><?php i18n("not possible to close the sheet with mousewheel");?></li>
<li><?php i18n("Only multiply the icon size if Qt isn't doing it already (bug 390076)");?></li>
<li><?php i18n("take global footer into account for handle position");?></li>
<li><?php i18n("event compress the creation and destruction of scrollbars");?></li>
<li><?php i18n("ScrollView: Make the scrollbar policy public and fix it");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Add vokoscreen to KMoreTools and add it to the \"screenrecorder\" grouping");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Use QWidget to see if the window is visible");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Help automoc to find metadata JSON files referenced in the code");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("Clean up old, unreachable code");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Update krunner plugin template");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Add icons for KTextEditor Document-Export, Bookmark-Remove and Formatting Text Upppercase, Lowercase and Capitalize");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Implement releasing of client-freed output");?></li>
<li><?php i18n("[server] Properly handle the situation when the DataSource for a drag gets destroyed (bug 389221)");?></li>
<li><?php i18n("[server] Don't crash when a subsurface gets committed whose parent surface got destroyed (bug 389231)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Reset QLocale internals when we have a custom app language");?></li>
<li><?php i18n("Do not allow to configure separator actions via context menu");?></li>
<li><?php i18n("Don't show context menu if right-clicking outside (bug 373653)");?></li>
<li><?php i18n("Improve KSwitchLanguageDialogPrivate::fillApplicationLanguages");?></li>
</ul>

<h3><?php i18n("Oxygen Icons");?></h3>

<ul>
<li><?php i18n("add Artikulate icon (bug 317527)");?></li>
<li><?php i18n("add folder-games icon (bug 318993)");?></li>
<li><?php i18n("fix incorrect 48px icon for calc.template (bug 299504)");?></li>
<li><?php i18n("add media-playlist-repeat and shuffle icon (bug 339666)");?></li>
<li><?php i18n("Oxygen: add tag icons like in breeze (bug 332210)");?></li>
<li><?php i18n("link emblem-mount to media-mount (bug 373654)");?></li>
<li><?php i18n("add network icons which are available in breeze-icons (bug 374673)");?></li>
<li><?php i18n("sync oxygen with breeze-icons add icons for audio plasmoid");?></li>
<li><?php i18n("Add edit-select-none to Oxygen for Krusader (bug 388691)");?></li>
<li><?php i18n("Add rating-unrated icon (bug 339863)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("use the new value for largeSpacing in Kirigami");?></li>
<li><?php i18n("Reduce visibility of PC3 TextField placeholder text");?></li>
<li><?php i18n("Don't make Titles 20% transparent either");?></li>
<li><?php i18n("[PackageUrlInterceptor] Don't rewrite \"inline\"");?></li>
<li><?php i18n("Don't make Headings 20% transparent, to match Kirigami");?></li>
<li><?php i18n("don't put the fullrep in the popup if not collapsed");?></li>
<li><?php i18n("Help automoc to find metadata JSON files referenced in the code");?></li>
<li><?php i18n("[AppletQuickItem] Preload applet expander only if not already expanded");?></li>
<li><?php i18n("other preload microoptimizations");?></li>
<li><?php i18n("Set IconItem default to smooth=true");?></li>
<li><?php i18n("preload the expander (the dialog) too");?></li>
<li><?php i18n("[AppletQuickItem] Fix setting default preload policy if no environment variable is set");?></li>
<li><?php i18n("fix RTL appearance for ComboBox (bug https://bugreports.qt.io/browse/QTBUG-66446)");?></li>
<li><?php i18n("try to preload certain applets in a smart way");?></li>
<li><?php i18n("[Icon Item] Set filtering on FadingNode texture");?></li>
<li><?php i18n("Initialize m_actualGroup to NormalColorGroup");?></li>
<li><?php i18n("Make sure the FrameSvg and Svg instances have the right devicePixelRatio");?></li>
</ul>

<h3><?php i18n("Prison");?></h3>

<ul>
<li><?php i18n("Update links to dependencies, and mark Android as officially supported");?></li>
<li><?php i18n("Make DMTX dependency optional");?></li>
<li><?php i18n("Add QML support for Prison");?></li>
<li><?php i18n("Set minimum size on 1D barcodes as well");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Fix tier, accommodate for KIO");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Fix syntax error in previous commit, detected by launching ruqola");?></li>
<li><?php i18n("Show a radiobutton when we are showing an exclusive control (bug 391144)");?></li>
<li><?php i18n("implement MenuBarItem");?></li>
<li><?php i18n("implement DelayButton");?></li>
<li><?php i18n("New component: round button");?></li>
<li><?php i18n("take into account toolbar position");?></li>
<li><?php i18n("support colors for icons in buttons");?></li>
<li><?php i18n("support --reverse");?></li>
<li><?php i18n("icons in Menu fully functional");?></li>
<li><?php i18n("consistent shadows with the new breeze style");?></li>
<li><?php i18n("Some QStyles seem to not return sensible pixelmetrics here");?></li>
<li><?php i18n("first rough icons support");?></li>
<li><?php i18n("don't wrap around with mouse wheel");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("fix a leak and incorrect nullptr check in DADictionary");?></li>
<li><?php i18n("[UDisks] Fix auto-mount regression (bug 389479)");?></li>
<li><?php i18n("[UDisksDeviceBackend] Avoid multiple lookup");?></li>
<li><?php i18n("Mac/IOKit backend: support for drives, discs and volumes");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Use Locale::name() instead of Locale::bcp47Name()");?></li>
<li><?php i18n("Find libhunspell build by msvc");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Basic support for PHP and Python fenced code blocks in Markdown");?></li>
<li><?php i18n("Support case-insensitive WordDetect");?></li>
<li><?php i18n("Scheme highlighting: Remove hard-coded colors");?></li>
<li><?php i18n("Add syntax highlighting for SELinux CIL Policies &amp; File Contexts");?></li>
<li><?php i18n("Adding ctp file extension to the PHP syntax highlighting");?></li>
<li><?php i18n("Yacc/Bison: Fix the $ symbol and update syntax for Bison");?></li>
<li><?php i18n("awk.xml: add gawk extension keywords (bug 389590)");?></li>
<li><?php i18n("Add APKBUILD to be highlighted as a Bash file");?></li>
<li><?php i18n("Revert \"Add APKBUILD to be highlighted as a Bash file\"");?></li>
<li><?php i18n("Add APKBUILD to be highlighted as a Bash file");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.44");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.7");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
