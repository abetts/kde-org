<?php
	include_once ("functions.inc");
	$translation_file = "www";
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.11 Makes the Desktop More Powerful, Elegant and Secure",
		'cssFile' => '/content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = 'plasma-5.11.0'; // for i18n
	$version = "5.11.0";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px; 
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}

figure {
    position: relative;
    z-index: 2;
    font-size: smaller;
    /* text-shadow: 2px 2px 5px grey; */
    font-style: italic;
    color: #545454;
    text-align: right;
}
</style>

<main class="releaseAnnouncment container">


	<h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n("Plasma 5.11")?></h1>

	<?php include "./announce-i18n-bar.inc"; ?>

	<figure class="videoBlock">
		<iframe width="560" height="315" src="https://www.youtube.com/embed/nMFDrBIA0PM?rel=0" allowfullscreen='true'></iframe>
	</figure>

	
	<figure class="topImage">
		<a href="plasma-5.11/plasma-5.11.png">
			<img src="plasma-5.11/plasma-5.11-wee.png" width="600" height="338" alt="Plasma 5.11" />
		</a>
		<figcaption><?php print i18n_var("KDE Plasma %1", "5.11")?></figcaption>
	</figure>

	<p>
		<?php i18n("Tuesday, 10 Oct 2017.")?>
		<?php print i18n_var("Today KDE publishes this autumn's Plasma feature release, KDE Plasma 5.11. Plasma 5.11 brings a redesigned settings app, improved notifications, a more powerful task manager. Plasma 5.11 is the first release to contain the new “Vault”, a system to allow the user to encrypt and open sets of documents in a secure and user-friendly way, making Plasma an excellent choice for people dealing with private and confidential information.");?>
	</p>

<br clear="all" />

<h2><?php i18n("New System Settings Design");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.11/system-settings.png">
<img src="plasma-5.11/system-settings-wee.png" width="350" height="302" />
</a>
<figcaption><?php i18n("System Settings' New Design");?></figcaption>
</figure>

<p><?php i18n("The revamped System Settings user interface allows easier access to commonly used settings. It is the first step in making this often-used and complex application easier to navigate and more user-friendly. The new design is added as an option, users who prefer the older icon or tree views can move back to their preferred way of navigation.");?></p>

<br clear="all" />

<h2><?php i18n("Notification History");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.11/notification-history.png">
<img src="plasma-5.11/notification-history-wee.png" width="350" height="307" />
</a>
<figcaption><?php i18n("Notification History");?></figcaption>
</figure>

<p><?php i18n("Due to popular demand notifications optionally stores missed and expired notifications in a history. This is the first part of an ongoing effort to modernize the notification system in Plasma. This allows the user to override applications not marking their notifications as persistent, and viewing what happened in her absence.");?></p>

<br clear="all" />

<h2><?php i18n("Task Manager Improvements");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.11/task-manager-kate.png">
<img src="plasma-5.11/task-manager-kate-wee.png" width="350" height="232" />
</a>
<figcaption><?php i18n("Kate with Session Jump List Actions");?></figcaption>
</figure>

<p><?php i18n("Plasma's Task Manager lays the foundation for enabling applications to provide dynamic jump list actions. In Plasma 5.10, applications had to define additional actions added to their task entries statically. The new functions make it possible for applications to provide access to internal functions (such as a text editor's list of sessions, options to change application or document state, etc.), depending on what the application is currently doing. Moreover, rearranging windows in group popups is now possible, allowing the user to make the ordering of his opened applications more predictable. On top of all these changes, performance of the task manager has been improved for a smoother operation.");?></p>

<br clear="all" />

<h2><?php i18n("Plasma Vault");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.11/plasma-vault.png">
<img src="plasma-5.11/plasma-vault-wee.png" width="350" height="307" />
</a>
<figcaption><?php i18n("Plasma Vault Stores Your Files Securely");?></figcaption>
</figure>

<p><?php i18n("For users who often deal with sensitive, confidential and private information, the new Plasma Vault offers strong encryption features presented in a user-friendly way. Plasma Vault allows to lock and encrypt sets of documents and hide them from prying eyes even when the user is logged in. These 'vaults' can be decrypted and opened easily. Plasma Vault extends Plasma's activities feature with secure storage.");?></p>

<br clear="all" />

<h2><?php i18n("App Launcher Menu Improvements");?></h2>
<div style="float: right; width: 610px;">
<figure>
<a href="plasma-5.11/kickoff-edit-applications.png">
<img src="plasma-5.11/kickoff-edit-applications-wee.png" width="250" height="333" />
</a>
<figcaption><?php i18n("Edit Application Entries Direct from Menu.");?></figcaption>
</figure>&nbsp;&nbsp;<figure style="float: right; width: 610px;">
<a href="plasma-5.11/kicker.png">
<img src="plasma-5.11/kicker-wee.png" width="179" height="314" />
</a>
<figcaption><?php i18n("Kicker without Sidebar");?></figcaption>
</figure>
</div>

<p><?php i18n("Search results in launchers have gained features previously only available to applications listed on the menu. You no longer need to manually look for an application just to edit or uninstall it. The Kicker application launcher now hides its sidebar if no favorites are present, leading to a cleaner look. It also supports choosing an icon from the current icon theme rather than only pictures on your hard drive.  You can now have different favorites per activity.
</p><p>
The different app menus now all share which applications are listed as favourites so you don't lose your settings if you decide to change your launcher.
</p>
");?></p>

<br clear="all" />

<h2><?php i18n("Folder View");?></h2>
<p><?php i18n("
Folder View, which became the default desktop layout in Plasma 5.10, saw many improvement based on user feedback. It supports more keyboard shortcuts, such as Ctrl+A to “Select All”, and spreads icons more uniformly across the visible area to avoid unpleasant gaps on the right and bottom edges of a screen. Moreover, startup has been sped up and interacting with icons results in significantly less disk access.
");?></p>

<br clear="all" />

<h2><?php i18n("Wayland");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.11/dual-monitor-dpi-change-wayland.jpg">
<img src="plasma-5.11/dual-monitor-dpi-change-wayland-wee.jpg" width="350" height="253" />
</a>
<figcaption><?php i18n("One app window, two monitors, two DPIs");?></figcaption>
</figure>

<p><?php i18n("Wayland is the next generation display server technology making its entry in the Linux desktop. Wayland allows for improved visual quality and less overhead while providing more security with its clearer protocol semantics. A complete Wayland session's most visible feature is probably smoother graphics, leading to a cleaner and more enjoyable user experience. Plasma's Wayland support has been long in the making, and while it isn't fully there as a replacement for X11, more and more users enjoy Wayland on a daily basis.");?></p>

<p><?php i18n("A lot of work has been put into Plasma on Wayland. KWin, Plasma's Wayland compositor, can now automatically apply scaling based on the pixel density of a screen and even do so for each screen individually. This will significantly improve user experience on setups with multiple monitors, such as when a regular external monitor is connected to a modern high-resolution laptop. Moreover, legacy applications not supporting this functionality may be upscaled to remain readable.");?></p>

<p><?php i18n("Work has started to allow for a completely X-free environment, starting the Xwayland compatibility layer only when an application requires it. This will eventually result in improved security and performance as well as reduced resource consumption. Furthermore, it is now possible to use ConsoleKit2 instead of logind for setting up the Wayland session, extending the number of supported platforms.");?></p>

<p><?php i18n("Additional improvements include:");?></p>
<ul>
<?php i18n("
<li>Application identification logic, used for e.g. pinning apps in Task Manager, has been greatly improved</li>
<li>Audio indicator in Task Manager denoting an application playing sound is now available</li>
<li>Restricted window move operations are now possible</li>
<li>Window shortcuts can be also assigned to Wayland windows</li>
<li>Window title logic has been unified between X and Wayland windows</li>");?></ul>

<br clear="all" />

	<a href="plasma-5.10.5-5.11.0-changelog.php"><?php print i18n_var("Full Plasma 5.11 changelog"); ?></a>

	<!-- // Boilerplate again -->
	<section class="row get-it">
		<article class="col-md">
			<h2><?php i18n("Live Images");?></h2>
			<p>
				<?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
			</p>
			<a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
			<a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
		</article>

		<article class="col-md">
			<h2><?php i18n("Package Downloads");?></h2>
			<p>
				<?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
			</p>
			<a href='https://community.kde.org/Plasma/Packages' class="learn-more"><?php i18n("Package download wiki page");?></a>
		</article>

		<article class="col-md">
			<h2><?php i18n("Source Downloads");?></h2>
			<p>
				<?php i18n("You can install Plasma 5 directly from source.");?>
			</p>
			<a href='http://community.kde.org/Frameworks/Building'><?php i18n("Community instructions to compile it");?></a>
			<a href='../info/plasma-5.11.0.php' class='learn-more'><?php i18n("Source Info Page");?></a>
		</article>
	</section>

	<section class="give-feedback">
		<h2><?php i18n("Feedback");?></h2>

		<p>
			<?php print i18n_var("You can give us feedback and get updates on <a href='%1'><img src='%2' /></a> <a href='%3'>Facebook</a>
			or <a href='%4'><img src='%5' /></a> <a href='%6'>Twitter</a>
			or <a href='%7'><img src='%8' /></a> <a href='%9'>Google+</a>.", "https://www.facebook.com/kde", "https://www.kde.org/announcements/facebook.gif", "https://www.facebook.com/kde", "https://twitter.com/kdecommunity", "https://www.kde.org/announcements/twitter.png", "https://twitter.com/kdecommunity", "https://plus.google.com/105126786256705328374/posts", "https://www.kde.org/announcements/googleplus.png", "https://plus.google.com/105126786256705328374/posts"); ?>
		</p>
		<p>
			<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
		</p>

		<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

		<p><?php i18n("Your feedback is greatly appreciated.");?></p>
	</section>

	<h2>
		<?php i18n("Supporting KDE");?>
	</h2>

	<p align="justify">
		<?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. <a href='%3'>supporting member programme</a>.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/"); ?>
	</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
