<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.35.0");
  $site_root = "../";
  $release = '5.35.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
June 10, 2017. KDE today announces the release
of KDE Frameworks 5.35.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Improve error notification");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Pass an explicit list of arguments. This prevents QProcess from trying to handle our space containing path through a shell");?></li>
<li><?php i18n("Fix property changes being missed immediately after an object is added (bug 377405)");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("update awk mime as it's a script language (bug 376211)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("restore hidden-visibility testing with Xcode 6.2");?></li>
<li><?php i18n("ecm_qt_declare_logging_category(): more unique include guard for header");?></li>
<li><?php i18n("Add or improve \"Generated. Don't edit\" messages and make consistent");?></li>
<li><?php i18n("Add a new FindGperf module");?></li>
<li><?php i18n("Change default pkgconfig install path for FreeBSD");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Fix kactivities-stats into tier3");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Do not consider keyword Q_REQUIRED_RESULT");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Verify that whoever is calling us is actually who he says he is");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("Generate gperf output at build time");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Ensure proper per thread seeding in KRandom");?></li>
<li><?php i18n("Do not watch QRC's paths (bug 374075)");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Don't include the pid in the dbus path when on flatpak");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Consistently emit MouseEventListener::pressed signal");?></li>
<li><?php i18n("Don't leak MimeData object (bug 380270)");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Handle having spaces in the path to CMAKE_SOURCE_DIR");?></li>
</ul>

<h3><?php i18n("KEmoticons");?></h3>

<ul>
<li><?php i18n("Fix: Qt5::DBus is only privately used");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("use /usr/bin/env to locate python2");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Generate kentities gperf output at build time");?></li>
<li><?php i18n("Generate doctypes gperf output at build time");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Extend Programmer's Guide with notes about influence of setlocale()");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Addressed an issue where certain elements in applications (e.g. Dolphin's file view) would become inaccessible in high-dpi multi-screen setup (bug 363548)");?></li>
<li><?php i18n("[RenameDialog] Enforce plain text format");?></li>
<li><?php i18n("Identify PIE binaries (application/x-sharedlib) as executable files (bug 350018)");?></li>
<li><?php i18n("core: expose GETMNTINFO_USES_STATVFS in config header");?></li>
<li><?php i18n("PreviewJob: skip remote directories. Too expensive to preview (bug 208625)");?></li>
<li><?php i18n("PreviewJob: clean up empty temp file when get() fails (bug 208625)");?></li>
<li><?php i18n("Speed up detail treeview display by avoiding too many column resizes");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Use a single QNAM (and a disk cache) for HTTP jobs");?></li>
<li><?php i18n("Internal cache for provider data on initialisation");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Fix KSNIs being unable to register service under flatpak");?></li>
<li><?php i18n("Use application name instead of pid when creating SNI dbus service");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Do not export symbols from private libraries");?></li>
<li><?php i18n("Fix symbol exporting for KF5PeopleWidgets and KF5PeopleBackend");?></li>
<li><?php i18n("limit #warning to GCC");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Replace kwalletd4 after migration has finished");?></li>
<li><?php i18n("Signal completion of migration agent");?></li>
<li><?php i18n("Only start timer for migration agent if necessary");?></li>
<li><?php i18n("Check for unique application instance as early as possible");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Add requestToggleKeepAbove/below");?></li>
<li><?php i18n("Keep QIcon::fromTheme in main thread");?></li>
<li><?php i18n("Remove pid changedSignal in Client::PlasmaWindow");?></li>
<li><?php i18n("Add pid to plasma window management protocol");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KViewStateSerializer: Fix crash when view is destroyed before state serializer (bug 353380)");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Better fix for NetRootInfoTestWM in path with spaces");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Set main window as parent of stand-alone popup menus");?></li>
<li><?php i18n("When building menu hierarchies, parent menus to their containers");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Add VLC tray icon");?></li>
<li><?php i18n("Plasmoid templates: use the image which is part of the package (again)");?></li>
<li><?php i18n("Add template for Plasma QML Applet with QML extension");?></li>
<li><?php i18n("Recreate plasmashellsurf on exposed, destroy on hidden");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Haskell: highlight \"julius\" quasiquoter using Normal##Javascript rules");?></li>
<li><?php i18n("Haskell: enable hamlet highlighting for \"shamlet\" quasiquoter too");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.35");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.6");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
