<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Plasma 5.9 Kicks off 2017 in Style.");
  $site_root = "../";
  $release = 'plasma-5.9.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<figure style="float: none">
<iframe style="text-align: center" width="560" height="315" src="https://www.youtube.com/embed/lm0sqqVcotA?rel=0" frameborder="0" allowfullscreen></iframe>
<br clear="all" />
&nbsp;
<br clear="all" />
<a href="plasma-5.9/plasma-5.9.png">
<img src="plasma-5.9/plasma-5.9-wee.png" style="border: 0px" width="600" height="338" alt="<?php i18n("KDE Plasma 5.9");?>" />
</a>
<figcaption><?php i18n("KDE Plasma 5.9");?></figcaption>
</figure>


<p>
<?php i18n("Tuesday, 31 January 2017. "); ?>
<?php i18n("Today KDE releases this year’s first Plasma feature update, Plasma 5.9. While this release brings many exciting new features to your desktop, we'll continue to provide bugfixes to Plasma 5.8 LTS.
");?>
</p>

<br clear="all" />

<h2><?php i18n("Be even more productive");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.9/spectacle-notification.png">
<img src="plasma-5.9/spectacle-notification-wee.png" style="border: 0px" width="350" height="192" alt="<?php i18n("Spectacle screenshot notifications can now be dragged into e-mail composers (including web mail)");?>" />
</a>
<figcaption><?php i18n("Spectacle screenshot notifications can now be dragged into e-mail composers (including web mail)");?></figcaption>
</figure>

<p><?php i18n("In our ongoing effort to make you more productive with Plasma we added interactive previews to our notifications. This is most noticeable when you take a screenshot using Spectacle's global keyboard shortcuts (Shift+Print Scr): you can drag the resulting file from the notification popup directly into a chat window, an email composer or a web browser form, without ever having to leave the application you're currently working with. Drag and drop was improved throughout the desktop, with new drag and drop functionality to add widgets directly to the system tray. Widgets can also be added directly from the full screen Application Dashboard launcher.");?></p>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="plasma-5.9/icon-dialog.png">
<img src="plasma-5.9/icon-dialog-wee.png" style="border: 0px" width="350" height="327" alt="<?php i18n("Icon Widget Properties");?>" />
</a>
<figcaption><?php i18n("Icon Widget Properties");?></figcaption>
</figure>
<p><?php i18n("The icon widget that is created for you when you drag an application or document onto your desktop or a panel sees the return of a settings dialog: you can now change the icon, label text, working directory, and other properties. Its context menu now also sports an 'Open with' section as well as a link to open the folder the file it points to is located in.");?></p>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="plasma-5.9/mute.png">
<img src="plasma-5.9/mute.png" style="border: 0px" width="340" height="399" alt="<?php i18n("Muting from Panel Task Manager");?>" />
</a>
<figcaption><?php i18n("Muting from Panel Task Manager");?></figcaption>
</figure>

<p><?php i18n("Due to popular demand we implemented switching between windows in Task Manager using Meta + number shortcuts for heavy multi-tasking. Also new in Task Manager is the ability to pin different applications in each of your activities. And should you rather want to focus on one particular task, applications currently playing audio are marked in Task Manager similar to how it’s done in modern web browsers. Together with a button to mute the offending application, this can help you stay focused.");?></p>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="plasma-5.9/kickoff-krunner-result.png">
<img src="plasma-5.9/kickoff-krunner-result-wee.png" style="border: 0px" width="350" height="145" alt="<?php i18n("Search Actions");?>" />
</a>
<figcaption><?php i18n("Search Actions");?></figcaption>
</figure>
<p><?php i18n("The Quick Launch applet now supports jump list actions, bringing it to feature parity with the other launchers in Plasma. KRunner actions, such as “Run in Terminal” and “Open containing folder” are now also shown for the KRunner-powered search results in the application launchers.");?></p>

<p><?php i18n("A new applet was added restoring an earlier KDE 4 feature of being able to group multiple widgets together in a single widget operated by a tabbed interface. This allows you to quickly access multiple arrangements and setups at your fingertips.");?></p>

<br clear="all" />
<h2><?php i18n("More streamlined visuals");?></h2>

<figure style="float: right; width: 360px;">
<a href="plasma-5.9/breeze-scrollbar.ogv">
 <video width="350" height="240" autoplay="true">
  <source src="plasma-5.9/breeze-scrollbar.ogv" type="video/ogg" />
 </video> 
</a>
<figcaption><?php i18n("New Breeze Scrollbar Design");?></figcaption>
</figure>
<p><?php i18n("Improvements have been made to the look and feel of the Plasma Desktop and its applications. Scroll bars in the Breeze style, for instance, have transitioned to a more compact and beautiful design, giving our applications a sleek and modern look.");?></p>

<br clear="all" />
<h2><?php i18n("Global Menus");?></h2>
<figure style="float: right; width: 360px;">
<a href="plasma-5.9/global-menus-widget.png">
<img src="plasma-5.9/global-menus-widget-wee.png" style="border: 0px" width="350" height="207" alt="<?php i18n('Global Menus in a Plasma Widget');?>" />
</a>
<figcaption><?php i18n("Global Menus in a Plasma Widget");?></figcaption>
<a href="plasma-5.9/global-menus-window-bar.png">
<img src="plasma-5.9/global-menus-window-bar-wee.png" style="border: 0px" width="350" height="258" alt="<?php i18n('Global Menus in the Window Bar');?>" />
</a>
<figcaption><?php i18n("Global Menus in the Window Bar");?></figcaption>
</figure>
<p><?php i18n("Global Menus have returned.  KDE's pioneering feature to separate the menu bar from the application window allows for new user interface paradigm with either a Plasma Widget showing the menu or neatly tucked away in the window bar.");?></p>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="plasma-5.9/task-manager-tooltips.png">
<img src="plasma-5.9/task-manager-tooltips.png" style="border: 0px" width="284" height="225" alt="<?php i18n('Neater Task Manager Tooltips');?>" />
</a>
<figcaption><?php i18n("Neat Task Manager Tooltips");?></figcaption>
</figure>
<p><?php i18n("Task Manager tooltips have been redesigned to provide more information while being significantly more compact. Folder View is now able to display file emblems which are used, for example, to indicate symlinks. Overall user experience when navigating and renaming files has been greatly improved.");?></p>

<br clear="all" />
<h2><?php i18n("More powerful Look and Feel import & export");?></h2>

<figure style="float: right; width: 360px;">
 <iframe width="350" height="197" src="https://www.youtube.com/embed/RX5HwumKPP8" frameborder="0" allowfullscreen></iframe>
<figcaption><?php i18n("Look and Feel Themes");?></figcaption>
</figure>
<p><?php i18n("The global Look and Feel desktop themes now support changing the window decoration as well – the 'lookandfeelexplorer' theme creation utility will export your current window decoration to the theme you create.");?></p>

<p><?php i18n("If you install, from the KDE store, themes that depend on other artwork packs also present on the KDE store (such as Plasma themes and Icon themes) they will be automatically downloaded, in order to give you the full experience intended by the theme creator.");?></p>


<h2><?php i18n("New network configuration module");?></h2>

<br clear="all" />
<figure style="float: right; width: 360px;">
<a href="plasma-5.9/network-configuration.png">
<img src="plasma-5.9/network-configuration-wee.png" style="border: 0px" width="350" height="309" alt="<?php i18n('Network Connections Configuration');?>" />
</a>
<figcaption><?php i18n("Network Connections Configuration");?></figcaption>
</figure>
<p><?php i18n("A new configuration module for network connections has been added to System Settings, using QML and bringing a new fresh look. Design of the module is inspired by our network applet, while the configuration functionality itself is based on the previous Connection Editor. This means that although it features a new design, functionality remains using the proven codebase.");?></p>

<br clear="all" />
<h2><?php i18n("Wayland");?></h2>
<figure style="float: right; width: 360px;">
<!--
<a href="plasma-5.9/kwin-wayland.png">
<img src="plasma-5.9/kwin-wayland-wee.png" style="border: 0px" width="349" height="107" alt="<?php i18n('Plasma with Wayland Can Now Take Screenshots and Pick Colors');?>" />
</a>
<figcaption><?php i18n("Plasma with Wayland Can Now Take Screenshots and Pick Colors");?></figcaption>
<br />
-->
<iframe width="350" height="197" src="https://www.youtube.com/embed/_kQwFpZoDZY" frameborder="0" allowfullscreen></iframe>
<figcaption><?php i18n("Pointer Gesture Support");?></figcaption>
<a href="plasma-5.9/wayland-touchpad.png">
<img src="plasma-5.9/wayland-touchpad-wee.png" style="border: 0px" width="350" height="352" alt="<?php i18n('Touchpad Configuration');?>" />
</a>
<figcaption><?php i18n("Wayland Touchpad Configuration");?></figcaption>
</figure>

<p><?php i18n("Wayland has been an ongoing transitional task, getting closer to feature completion with every release. This release makes it even more accessible for enthusiastic followers to try Wayland and start reporting any bugs they might find. Notable improvements in this release include:");?></p>
<p><?php i18n("An ability to take screenshots or use a color picker.  Fullscreen users will be pleased at borderless maximized windows.");?></p>
<p><?php i18n("Pointers can now be confined by applications, gestures are supported (see video right) and relative motions used by games were added. Input devices were made more configurable and now save between sessions.  There is also a new settings tool for touchpads.");?></p>
<p><?php i18n("Using the Breeze style you can now drag applications by clicking on an empty area of the UI just like in X.  When running X applications the window icon will show up properly on the panel.  Panels can now auto-hide.  Custom color schemes can be set for windows, useful for accessibility.");?></p>

<p><a href="plasma-5.8.5-5.9.0-changelog.php">
<?php i18n("Full Plasma 5.9.0 changelog");?></a></p>

<!-- // Boilerplate again -->

<h2><?php i18n("Live Images");?></h2>

<p><?php print i18n_var("
The easiest way to try it out is with a live image booted off a
USB disk. You can find a list of <a href='%1'>Live Images with Plasma 5</a> on the KDE Community Wiki.", "https://community.kde.org/Plasma/Live_Images");?></p>

<p><?php print i18n_var("
<a href='%1'>Docker images</a> also provide a quick and easy way to test Plasma.", "https://community.kde.org/Plasma/Docker_Images");?></p>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
, $release);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
