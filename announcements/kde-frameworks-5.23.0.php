<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.23.0");
  $site_root = "../";
  $release = '5.23.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
June 13, 2016. KDE today announces the release
of KDE Frameworks 5.23.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Make it actually possible to tell providers from the url we were given");?></li>
<li><?php i18n("Provide QDebug helpers for some Attica classes");?></li>
<li><?php i18n("Fix redirection of absolute Urls (bug 354748)");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Fix using spaces in the tags kioslave (bug 349118)");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add a CMake option to build binary Qt resource out of icons dir");?></li>
<li><?php i18n("Many new and updated icons");?></li>
<li><?php i18n("update disconnect network icon for bigger diference to conntected (bug 353369)");?></li>
<li><?php i18n("update mount and unmount icon (bug 358925)");?></li>
<li><?php i18n("add some avatars from plasma-desktop/kcms/useraccount/pics/sources");?></li>
<li><?php i18n("remove chromium icon cause the default chromium icon fit's well (bug 363595)");?></li>
<li><?php i18n("make the konsole icons lighter (bug 355697)");?></li>
<li><?php i18n("add mail icons for thunderbird (bug 357334)");?></li>
<li><?php i18n("add public key icon (bug 361366)");?></li>
<li><?php i18n("remove process-working-kde cause the konqueror icons should be used (bug 360304)");?></li>
<li><?php i18n("update krusader icons according to (bug 359863)");?></li>
<li><?php i18n("rename the mic icons according D1291 (bug D1291)");?></li>
<li><?php i18n("add some script mimetype icons (bug 363040)");?></li>
<li><?php i18n("add virtual keyboard and touchpad on/off functionality for OSD");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Remove unused dependencies and translation handling");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Adding runningActivities property to the Consumer");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Major rework of the API docs generation");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Use QQuickWidget for QML KCMs (bug 359124)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Avoid skipping KAuthorized check");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Allow using new style connect syntax with KStandardAction::create()");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Print the failing plugin when notifying a cast warning");?></li>
<li><?php i18n("[kshareddatacache] Fix invalid use of &amp; to avoid unaligned reads");?></li>
<li><?php i18n("Kdelibs4ConfigMigrator: skip reparsing if nothing was migrated");?></li>
<li><?php i18n("krandom: Add testcase to catch bug 362161 (failure to auto-seed)");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("Check size of unix domain socket path before copying to it");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Support selected state");?></li>
<li><?php i18n("KCMShell import can now be used to query for whether opening a KCM is actually allowed");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Warn about KDateTimeParser::parseDateUnicode not being implemented");?></li>
<li><?php i18n("K4TimeZoneWidget: correct path for flag images");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Add commonly used entities for keys to en/user.entities");?></li>
<li><?php i18n("Update man-docbook template");?></li>
<li><?php i18n("Update book template + man template + add arcticle template");?></li>
<li><?php i18n("Call kdoctools_create_handbook only for index.docbook (bug 357428)");?></li>
</ul>

<h3><?php i18n("KEmoticons");?></h3>

<ul>
<li><?php i18n("Add emojis support to KEmoticon + Emoji One icons");?></li>
<li><?php i18n("Add support for custom emoticon sizes");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Fix potential memory leak reported by Coverity and simplify the code");?></li>
<li><?php i18n("The number of layers is determined by the number of comma-separated values in the ‘background-image’ property");?></li>
<li><?php i18n("Fix parsing background-position in shorthand declaration");?></li>
<li><?php i18n("Do not create new fontFace if there is no valid source");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Don't make KIconThemes depend on Oxygen (bug 360664)");?></li>
<li><?php i18n("Selected state concept for icons");?></li>
<li><?php i18n("Use system colors for monochrome icons");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Fix race in which the file containing the X11 cookie has the wrong permissions for a small while");?></li>
<li><?php i18n("Fix permissions of /tmp/xauth-xxx-_y");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Give clearer error message when KRun(URL) is given a URL without scheme (bug 363337)");?></li>
<li><?php i18n("Add KProtocolInfo::archiveMimetypes()");?></li>
<li><?php i18n("use selected icon mode in file open dialog sidebar");?></li>
<li><?php i18n("kshorturifilter: fix regression with mailto: not prepended when no mailer is installed");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("Set correct \"dialog\" flag for Progress Widget dialog");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Don't initialize KNS3::DownloadManager with the wrong categories");?></li>
<li><?php i18n("Extend KNS3::Entry public API");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("use QUrl::fromUserInput to construct sound url (bug 337276)");?></li>
</ul>

<h3><?php i18n("KNotifyConfig");?></h3>

<ul>
<li><?php i18n("use QUrl::fromUserInput to construct sound url (bug 337276)");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Fix associated applications for mimetypes with uppercase characters");?></li>
<li><?php i18n("Lowercase the lookup key for mimetypes, to make it case insensitive");?></li>
<li><?php i18n("Fix ksycoca notifications when the DB doesn't exist yet");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Fix default encoding to UTF-8 (bug 362604)");?></li>
<li><?php i18n("Fix color configurability of default style \"Error\"");?></li>
<li><?php i18n("Search &amp; Replace: Fix replace background color (regression introduced in v5.22) (bug 363441)");?></li>
<li><?php i18n("New color scheme \"Breeze Dark\", see https://kate-editor.org/?post=3745");?></li>
<li><?php i18n("KateUndoManager::setUndoRedoCursorOfLastGroup(): pass Cursor as const reference");?></li>
<li><?php i18n("sql-postgresql.xml improve syntax highlighting by ignoring multiline function bodies");?></li>
<li><?php i18n("Add syntax highlighting for Elixir and Kotlin");?></li>
<li><?php i18n("VHDL syntax highlighting in ktexteditor: add support for functions inside architecture statements");?></li>
<li><?php i18n("vimode: Don't crash when given a range for a nonexistent command (bug 360418)");?></li>
<li><?php i18n("Properly remove composed characters when using Indic locales");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("Fix downloading currency exchange rates (bug 345750)");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("KWalletd migration: fix error handling, stops the migration from happening on every single boot.");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("[client] Don't check resource version for PlasmaWindow");?></li>
<li><?php i18n("Introduce an initial state event into Plasma Window protocol");?></li>
<li><?php i18n("[server] Trigger error if a transient request tries to parent to itself");?></li>
<li><?php i18n("[server] Properly handle the case that a PlasmaWindow is unmapped before client bound it");?></li>
<li><?php i18n("[server] Properly handle destructor in SlideInterface");?></li>
<li><?php i18n("Add support for touch events in fakeinput protocol and interface");?></li>
<li><?php i18n("[server] Standardize the destructor request handling for Resources");?></li>
<li><?php i18n("Implement wl_text_input and zwp_text_input_v2 interfaces");?></li>
<li><?php i18n("[server] Prevent double delete of callback resources in SurfaceInterface");?></li>
<li><?php i18n("[server] Add resource nullptr check to ShellSurfaceInterface");?></li>
<li><?php i18n("[server] Compare ClientConnection instead of wl_client in SeatInterface");?></li>
<li><?php i18n("[server] Improve the handling when clients disconnect");?></li>
<li><?php i18n("server/plasmawindowmanagement_interface.cpp - fix -Wreorder warning");?></li>
<li><?php i18n("[client] Add context pointer to connects in PlasmaWindowModel");?></li>
<li><?php i18n("Many fixes related to destruction");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Use selected icon effect for current KPageView page");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("[platform xcb] Respect request icon size (bug 362324)");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Right-clicking the menu bar of an application will now longer allow bypassing");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Revert \"drop WiMAX support for NM 1.2.0+\" as it breaks ABI");?></li>
</ul>

<h3><?php i18n("Oxygen Icons");?></h3>

<ul>
<li><?php i18n("Sync weather icons with breeze");?></li>
<li><?php i18n("Add update icons");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Add cantata system tray support (bug 363784)");?></li>
<li><?php i18n("Selected state for Plasma::Svg and IconItem");?></li>
<li><?php i18n("DaysModel: reset m_agendaNeedsUpdate when plugin sends new events");?></li>
<li><?php i18n("Update audio and network icon to get a better contrast (bug 356082)");?></li>
<li><?php i18n("Deprecate downloadPath(const QString &amp;file) in favor of downloadPath()");?></li>
<li><?php i18n("[icon thumbnail] Request for preferred icon size (bug 362324)");?></li>
<li><?php i18n("Plasmoids can now tell whether widgets are locked by the user or sysadmin restrictions");?></li>
<li><?php i18n("[ContainmentInterface] Don't try to popup empty QMenu");?></li>
<li><?php i18n("Use SAX for Plasma::Svg stylesheet replacement");?></li>
<li><?php i18n("[DialogShadows] Cache access to QX11Info::display()");?></li>
<li><?php i18n("restore air plasma theme icons from KDE4");?></li>
<li><?php i18n("Reload selected color scheme on colors changed");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.23");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.4");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
