<?php
    include_once ("functions.inc");
    $translation_file = "www";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "KDE Plasma 5.16 Beta: Your Three Week Notification of a More Tidy and Composed Desktop",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = 'plasma-5.15.90'; // for i18n
    $version = "5.15.90";
?>

<script src="/js/use-ekko-lightbox.js" defer="true"></script>

<main class="releaseAnnouncment container">

    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("Plasma %1", $version)?></h1>

    <?php include "./announce-i18n-bar.inc"; ?>

<!--
    <figure class="videoBlock">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/C2kR1_n_d-g?rel=0" allowfullscreen='true'></iframe>
    </figure>
-->

    <figure class="topImage">
        <a href="plasma-5.16/plasma-5.16-full-2.png" data-toggle="lightbox">
            <img src="plasma-5.16/plasma-5.16-full-2-wee.png" height="338" width="600" style="width: 100%; max-width: 600px; height: auto;" alt="Plasma 5.16" />
        </a>
        <figcaption><?php print i18n_var("KDE Plasma %1", "5.16")?></figcaption>
    </figure>

    <p><?php i18n("Thursday, 16 May 2019.")?></p>
    <p><?php i18n("Today KDE launches the beta release of Plasma 5.16.")?></p>

    <p><?php i18n("In this release, many aspects of Plasma have been polished and 
    rewritten to provide high consistency and bring new features. There is a completely rewritten notification system supporting Do Not Disturb mode, more intelligent history with grouping, critical notifications in fullscreen apps, improved notifications for file transfer jobs, a much more usable System Settings page to configure everything, and many other things. The System and 
    Widget Settings have been refined and worked on by porting code to 
    newer Kirigami and Qt technologies and polishing the user interface. 
    And of course the VDG and Plasma team effort towards <a href=\"https://community.kde.org/Goals/Usability_%26_Productivity\">Usability & Productivity 
    goal</a> continues, getting feedback on all the papercuts in our software that make your life less 
    smooth and fixing them to ensure an intuitive and consistent workflow for your 
    daily use.")?></p>

    <p><?php i18n("For the first time, the default wallpaper of Plasma 5.16 will 
    be decided by a contest where everyone can participate and submit art. The 
    winner will receive a Slimbook One v2 computer, an eco-friendly, compact 
    machine, measuring only 12.4 x 12.8 x 3.7 cm. It comes with an i5 processor, 8 
    GB of RAM, and is capable of outputting video in glorious 4K. Naturally, your 
    One will come decked out with the upcoming KDE Plasma 5.16 desktop, your 
    spectacular wallpaper, and a bunch of other great software made by KDE. You can find 
    more information and submitted work on <a href=\"https://community.kde.org/KDE_Visual_Design_Group/Plasma_5.16_Wallpaper_Competition\">the competition wiki 
    page</a>, and you can <a href=\"https://forum.kde.org/viewtopic.php?f=312&t=160487\">submit your own wallpaper in the 
    subforum</a>.")?></p>

    <h3 id="desktop"><?php i18n("Desktop Management");?></h3>
    <figure style="float: right; text-align: right">
    <a href="plasma-5.16/notifications.png" data-toggle="lightbox">
    <img src="plasma-5.16/notifications-wee.png" style="border: 0px" width="350" height="315" alt="<?php i18n("New Notifications");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("New Notifications");?></figcaption>
    <br clear="all" />
    <a href="plasma-5.16/plasma-theme-fixes.png" data-toggle="lightbox">
    <img src="plasma-5.16/plasma-theme-fixes.png" style="border: 0px" width="281" height="251" alt="<?php i18n("Theme Engine Fixes for Clock Hands!");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Theme Engine Fixes for Clock Hands!");?></figcaption>
    <br clear="all" />
    <a href="plasma-5.16/panel-edit-mode-alternatives.png" data-toggle="lightbox">
    <img src="plasma-5.16/panel-edit-mode-alternatives-wee.png" style="border: 0px" width="350" height="183" alt="<?php i18n("Panel Editing Offers Alternatives");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Panel Editing Offers Alternatives");?></figcaption>
    <br clear="all" />
    <a href="plasma-5.16/sddm-theme.png" data-toggle="lightbox">
    <img src="plasma-5.16/sddm-theme-wee.png" style="border: 0px" width="350" height="490" alt="<?php i18n("Login Screen Theme Improved");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Login Screen Theme Improved");?></figcaption>
    </figure>

    <ul>
    <li><?php i18n("Completely rewritten notification system supporting Do Not Disturb mode, more intelligent history with grouping, critical notifications in fullscreen apps, improved notifications for file transfer jobs, a much more usable System Settings page to configure everything, and more!"); ?></li>
    <li><?php i18n("Plasma themes are now correctly applied to panels when selecting a new theme."); ?></li>
    <li><?php i18n("More options for Plasma themes: offset of analog clock hands and toggling blur behind."); ?></li>
    <li><?php i18n("All widget configuration settings have been modernized and now feature an improved UI. The Color Picker widget also improved, now allowing dragging colors from the plasmoid to text editors, palette of photo editors, etc."); ?></li>
    <li><?php i18n("The look and feel of lock, login and logout screen have been improved with new icons, labels, hover behavior, login button layout and more."); ?></li>
    <li><?php i18n("When an app is recording audio, a microphone icon will now appear in the System Tray which allows for changing and muting the volume using mouse middle click and wheel. The Show Desktop icon is now also present in the panel by default."); ?></li>
    <li><?php i18n("The Wallpaper Slideshow settings window now displays the images in the selected folders, and allows selecting and deselecting them."); ?></li>
    <li><?php i18n("The Task Manager features better organized context menus and can now be configured to move a window from a different virtual desktop to the current one on middle click."); ?></li>
    <li><?php i18n("The default Breeze window and menu shadow color are back to being pure black, which improves visibility of many things especially when using a dark color scheme."); ?></li>
    <li><?php i18n("The \"Show Alternatives...\" button is now visible in panel edit mode, use it to quickly change widgets to similar alternatives."); ?></li>
    <li><?php i18n("Plasma Vaults can now be locked and unlocked directly from Dolphin."); ?></li>
    </ul>

    <br clear="all" />
    
    <h3 id="settings"><?php i18n("Settings");?></h3>
    <figure style="float: right; text-align: right">
    <a href="plasma-5.16/color-scheme.png" data-toggle="lightbox">
    <img src="plasma-5.16/color-scheme-wee.png" style="border: 0px" width="350" height="321" alt="<?php i18n("Color Scheme");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Color Scheme");?></figcaption>
    <br clear="all"/>
    <a href="plasma-5.16/application-style.png" data-toggle="lightbox">
    <img src="plasma-5.16/application-style-wee.png" style="border: 0px" width="350" height="194" alt="<?php i18n("Application Style and Appearance Settings");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Application Style and Appearance Settings");?></figcaption>
    </figure>

    <ul>
    <li><?php i18n("There has been a general polish in all pages; the entire Appearance section has been refined, the Look and Feel page has moved to the top level, and improved icons have been added in many pages."); ?></li>

    <li><?php i18n("The Color Scheme and Window Decorations pages have been redesigned with a more consistent grid view. The Color Scheme page now supports filtering by light and dark themes, drag and drop to install themes, undo deletion and double click to apply."); ?></li>

    <li><?php i18n("The theme preview of the Login Screen page has been overhauled."); ?></li>

    <li><?php i18n("The Desktop Session page now features a \"Reboot to UEFI Setup\" option."); ?></li>

    <li><?php i18n("There is now full support for configuring touchpads using the Libinput driver on X11."); ?></li>
    </ul>

    <br clear="all" />
    
    <h3 id="settings"><?php i18n("Window Management");?></h3>
    <figure style="float: right; text-align: right">
    <a href="plasma-5.16/window-management.png" data-toggle="lightbox">
    <img src="plasma-5.16/window-management-wee.png" style="border: 0px" width="350" height="197" alt="<?php i18n("Window Management");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Window Management");?></figcaption>
    </figure>

    <ul>
    <li><?php i18n("Initial support for using Wayland with proprietary Nvidia drivers has been added. When using Qt 5.13 with this driver, graphics are also no longer distorted after waking the computer from sleep."); ?></li>

    <li><?php i18n("Wayland now features drag and drop between XWayland and Wayland native windows."); ?></li>

    <li><?php i18n("Also on Wayland, the System Settings Libinput touchpad page now allows you to configure the click method, switching between \"areas\" or \"clickfinger\"."); ?></li>

    <li><?php i18n("KWin's blur effect now looks more natural and correct to the human eye by not unnecessary darkening the area between blurred colors."); ?></li>

    <li><?php i18n("Two new default shortcuts have been added: Meta+L can now be used by default to lock the screen and Meta+D can be used to show and hide the desktop."); ?></li>

    <li><?php i18n("GTK windows now apply correct active and inactive colour scheme."); ?></li>
    </ul>

    <br clear="all" />
    
    <h3 id="plasma-nm"><?php i18n("Plasma Network Manager");?></h3>
    <figure style="float: right; text-align: right">
    <a href="plasma-5.16/plasma-nm-wireguard.png" data-toggle="lightbox">
    <img src="plasma-5.16/plasma-nm-wireguard-wee.png" style="border: 0px" width="350" height="287" alt="<?php i18n("Plasma Network Manager with Wireguard");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Plasma Network Manager with Wireguard");?></figcaption>
    </figure>
    
    <ul>
    <li><?php i18n("The Networks widget is now faster to refresh Wi-Fi networks and more reliable at doing so. It also has a button to display a search field to help you find a particular network from among the available choices. Right-clicking on any network will expose a \"Configure…\" action."); ?></li>
    <li><?php i18n("WireGuard is now compatible with NetworkManager 1.16."); ?></li>
    <li><?php i18n("One Time Password (OTP) support in Openconnect VPN plugin has been added."); ?></li>
    </ul>

    <br clear="all" />
    
    <h3 id="settings"><?php i18n("Discover");?></h3>
    <figure style="float: right; text-align: right">
    <a href="plasma-5.16/discover-update.png" data-toggle="lightbox">
    <img src="plasma-5.16/discover-update-wee.png" style="border: 0px" width="350" height="250" alt="<?php i18n("Updates in Discover");?>" />
    </a>
    <figcaption style="text-align: right"><?php i18n("Updates in Discover");?></figcaption>
    </figure>

    <ul>
    <li><?php i18n("In Discover's Update page, apps and packages now have distinct \"downloading\" and \"installing\" sections. When an item has finished installing, it disappears from the view."); ?></li>
    <li><?php i18n("Tasks completion indicator now looks better by using a real progress bar. Discover now also displays a busy indicator when checking for updates."); ?></li>
    <li><?php i18n("Improved support and reliability for AppImages and other apps that come from store.kde.org."); ?></li>
    <li><?php i18n("Discover now allows you to force quit when installation or update operations are proceeding."); ?></li>
    <li><?php i18n("The sources menu now shows the version number for each different source for that app."); ?></li>
    </ul>

    <br clear="all" />

    <a href="plasma-5.15.5-5.15.90-changelog.php"><?php print i18n_var("Full Plasma %1 changelog", "5.15.90"); ?></a>

    <!-- // Boilerplate again -->
    <section class="row get-it">
        <article class="col-md">
            <h2><?php i18n("Live Images");?></h2>
            <p>
                <?php i18n("The easiest way to try it out is with a live image booted off a USB disk. Docker images also provide a quick and easy way to test Plasma.");?>
            </p>
            <a href='https://community.kde.org/Plasma/Live_Images' class="learn-more"><?php i18n("Download live images with Plasma 5");?></a>
            <a href='https://community.kde.org/Plasma/Docker_Images' class="learn-more"><?php i18n("Download Docker images with Plasma 5");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Package Downloads");?></h2>
            <p>
                <?php i18n("Distributions have created, or are in the process of creating, packages listed on our wiki page.");?>
            </p>
            <a href='https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro' class="learn-more"><?php i18n("Get KDE Software on Your Linux Distro wiki page");?></a>
        </article>

        <article class="col-md">
            <h2><?php i18n("Source Downloads");?></h2>
            <p>
                <?php i18n("You can install Plasma 5 directly from source.");?>
            </p>
            <a href='https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source' class='learn-more'><?php i18n("Community instructions to compile it");?></a>
            <a href='../info/plasma-5.15.90.php' class='learn-more'><?php i18n("Source Info Page");?></a>
        </article>
    </section>

    <section class="give-feedback">
        <h2><?php i18n("Feedback");?></h2>

        <p class="kSocialLinks">
            <?php i18n("You can give us feedback and get updates on our social media channels:"); ?>
            <a class="shareFacebook" href="https://www.facebook.com/kde/" rel="nofollow">Post on Facebook</a>
            <a class="shareTwitter" href="https://twitter.com/kdecommunity" rel="nofollow">Share on Twitter</a>
            <a class="shareDiaspora" href="https://joindiaspora.com/people/9c3d1a454919ef06" rel="nofollow">Share on Diaspora</a>
            <a class="shareReddit" href="http://www.reddit.com/r/kde/" rel="nofollow">Share on Reddit</a>
            <a class="shareYouTube" href="https://www.youtube.com/channel/UCF3I1gf7GcbmAb0mR6vxkZQ" rel="nofollow">Share on YouTube</a>
            <a class="shareMastodon" href="https://mastodon.technology/@kde" rel="nofollow">Share on Mastodon</a>
            <a class="shareLinkedIn" href="https://www.linkedin.com/company/29561/" rel="nofollow">Share on LinkedIn</a>
            <a class="sharePeerTube" href="https://peertube.mastodon.host/accounts/kde/videos" rel="nofollow">Share on PeerTube</a>
        </p>
        <p>
            <?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?>
        </p>

        <p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>Plasma Matrix chat room</a>, <a href='%2'>Plasma-devel mailing list</a> or report issues via <a href='%3'>bugzilla</a>. If you like what the team is doing, please let them know!", "https://webchat.kde.org/#/room/#cutehmi:kde.org", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&amp;format=guided"); ?>

        <p><?php i18n("Your feedback is greatly appreciated.");?></p>
    </section>

    <h2>
        <?php i18n("Supporting KDE");?>
    </h2>

    <p align="justify">
        <?php print i18n_var("KDE is a <a href='%1'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='%2'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='%3'>Join the Game</a> initiative.", "http://www.gnu.org/philosophy/free-sw.html", "/community/donations/", "https://relate.kde.org/civicrm/contribute/transact?id=5"); ?>
    </p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

</main>
<?php
  require('../aether/footer.php');
