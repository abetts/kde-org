<?php
include_once ("functions.inc");
$translation_file = "www";
$page_title = i18n_noop("Plasma 5.5.5 Complete Changelog");
$site_root = "../";
$release = 'plasma-5.5.5';
include "header.inc";
?>
<p><a href="plasma-5.5.5.php">Plasma 5.5.5</a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='http://quickgit.kde.org/?p=bluedevil.git'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>KCM: Fix selecting save directory with file dialog. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=2df38f76cca8d6c999ee353056c2c73e1bb2867c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359373'>#359373</a></li>
<li>Sendfile: Fix exec line in desktop file. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=3c1d7709a1393ca494731f46173dc3cd9aaf0fb5'>Commit.</a> </li>
</ul>


<h3><a name='discover' href='http://quickgit.kde.org/?p=discover.git'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Polish category loading. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=994f7552a136c54cc5a2e5e4f9d52e50e5a54105'>Commit.</a> </li>
<li>Make sure qapt-check doesn't get called repeatedly. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=7ded9bf1de57f7954be552a48aa9abdb3e4db400'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126923'>#126923</a>. Fixes bug <a href='https://bugs.kde.org/347602'>#347602</a>. Fixes bug <a href='https://bugs.kde.org/358359'>#358359</a></li>
<li>--debug. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=4162a4ff72df3128df1110afe58c2853480cb622'>Commit.</a> </li>
<li>Prevent changing section as update is stateless for now. <a href='http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=cb5d6823897e79dd5eb4f27c434f34a75d9e7aa7'>Commit.</a> </li>
</ul>


<h3><a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Use Plasma grid units. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=38498c0399a4b0dc7176284367003c4e537cb346'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359728'>#359728</a></li>
</ul>


<h3><a name='khelpcenter' href='http://quickgit.kde.org/?p=khelpcenter.git'>KHelpCenter</a> </h3>
<ul id='ulkhelpcenter' style='display: block'>
<li>Fix arguments usage logic. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=1720755511827adc5325f2613910a71f0134ba34'>Commit.</a> </li>
<li>Sync View::langLookup with kio_help. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=2976d22a3f7097d7d20d957362d00aaf9afd17c3'>Commit.</a> </li>
<li>Allow redirecting from internal pages. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=1048f78a31b2e48204d403a8a3e4792724d678f2'>Commit.</a> </li>
<li>Use absolute URLs in <link> nodes as such. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=9904c89c3b3c0ebce4672052adb827b545948875'>Commit.</a> </li>
<li>Search: fix loading of search handlers. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=30a014df602a06a805d89beb5f581b1b6b5ee254'>Commit.</a> </li>
<li>Indexbuilder: make sure the index directory exists. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=43fd10af22186af9e0344cb344984de3218e1175'>Commit.</a> </li>
<li>Rework CLI and unique application handling. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=6010149a683cd82a8eab210fe67d6610d11e5eb7'>Commit.</a> </li>
<li>Fix extraction of glossary id from glossary: URLs. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=98df5351f4591df9feb5468ff9d885e7e38289b6'>Commit.</a> </li>
<li>Create glossary cache directory if not existing. <a href='http://quickgit.kde.org/?p=khelpcenter.git&amp;a=commit&amp;h=f06788ee87ba7d85e9c2deed3d78822d2bc14d42'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357187'>#357187</a></li>
</ul>


<h3><a name='kscreenlocker' href='http://quickgit.kde.org/?p=kscreenlocker.git'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>

</ul>


<h3><a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>[aurorae] Handle client palette changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=3434358acdd52ae1fbf21ce1f61bcfe56df26a1c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357311'>#357311</a>. Code review <a href='https://git.reviewboard.kde.org/r/126891'>#126891</a></li>
</ul>


<h3><a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>XRandR: handle changeOutput() without CRTC better. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=d3c0b50e3b23c4f4839f15dda3af4361939ae484'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344813'>#344813</a></li>
</ul>


<h3><a name='oxygen' href='http://quickgit.kde.org/?p=oxygen.git'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Use KDE_INSTALL_DATADIR to install lookandfeel directory. <a href='http://quickgit.kde.org/?p=oxygen.git&amp;a=commit&amp;h=c5ec6a8d72e4332b1cdf24d44cf33fb0e09dbc0f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126775'>#126775</a></li>
</ul>


<h3><a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>[Kickoff] Add removeApplicationCommand from Kicker. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1b5307e78dacea388c18c239a7f4b544686bc748'>Commit.</a> See bug <a href='https://bugs.kde.org/359837'>#359837</a></li>
<li>Use the item midpoint when calculating grid drop coords. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=c7e0335677244cf4c4c0d82701c730c4481aa02c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358745'>#358745</a>. Fixes bug <a href='https://bugs.kde.org/359755'>#359755</a></li>
<li>Fix loading of extra runners and Kicker runner result column sizing. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9b463835893572fe917ae5277b9ed4d4f27a5dd4'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/126950'>#126950</a>. Fixes bug <a href='https://bugs.kde.org/356760'>#356760</a></li>
<li>Fix rename editor positioning. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3882ebb20dbcfb4e49f2813667ca318fab283edc'>Commit.</a> </li>
<li>Do tooltip location for panel popups a la systray. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f6acdf977bdbae191ab4d349f1a246fb63f9c823'>Commit.</a> See bug <a href='https://bugs.kde.org/358894'>#358894</a></li>
<li>Cancel tooltip before opening context menu. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=017a1c426baf27c8a7e028fac9df142aeaf9fa6b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358894'>#358894</a></li>
<li>Check for selection before handling Return/Menu. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ac9aa83a13968181d7440563d603e9c39887767b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358941'>#358941</a></li>
<li>Hide folder size from tooltips. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b90b4b74dfc0d394f655db095fd3ef581ede4b85'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358896'>#358896</a></li>
<li>Scale by device pixel ratio. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6ecb70d9980b01718aa6c7fc658dd6b74e1325d5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/358892'>#358892</a></li>
<li>Fix merge screwup. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0dd09ad785e0df4047c0522a1d286fc8e5fed1ce'>Commit.</a> </li>
<li>Cleanup. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=1aa696db30482c225933f522df8972c33d798316'>Commit.</a> </li>
<li>Require Qt 5.5. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=011c7ea008710fb78b434879ea995e71225df4fd'>Commit.</a> </li>
<li>Fix entries staying highlighted after context menu closes. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=9e0a7e991dbfc862a72f21f4662e280aff8ab317'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356018'>#356018</a></li>
</ul>


<h3><a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Use proper linear scaling for the traffic monitor. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=4e41e431f914ae6478f033bbda6877dc4e9b9e23'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359802'>#359802</a></li>
</ul>


<h3><a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Fix typo. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e722b1e5f214ec1d007f9b856b97293df16ea4fb'>Commit.</a> </li>
<li>[User Switcher] Fix session switching when automatic screen locking is enabled. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=7a0096ba99d7a71ae9f45d7c0011d0ebb1eae23d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/356945'>#356945</a></li>
<li>First after merge reversal. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=60fd43857d3d304ee4d772d0542b6c507a2531bb'>Commit.</a> </li>
<li>Reset the model on list always shown/hide change. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=42a3d8accd4e494d343954ddaa916a6c618d94f3'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357627'>#357627</a>. See bug <a href='https://bugs.kde.org/352055'>#352055</a></li>
</ul>


<?php
  include("footer.inc");
?>
