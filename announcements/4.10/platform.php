<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $release = '4.10';
  $release_full = '4.10.0';
  $page_title = i18n_noop("KDE Platform 4.10 Opens Up More APIs to Qt Quick");
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<?php
  include "../announce-i18n-bar.inc";
?>
<h2><?php i18n("Plasma SDK");?></h2>
<p>
<?php i18n("This release of the KDE Development Platform sees more work on a
comprehensive SDK for Plasma. Previously separate and distinct
components, such as plasmoidviewer, plasamengineexplorer and
plasmawallpaperviewer, are now part of <a
href='http://techbase.kde.org/Projects/Plasma/PlasMate'>PlasMate</a>,
the toolset for developing Plasma widgets.");?>
</p>
<?php showscreenshot("plasmate.png", i18n_var("Plasmate forms the heart of the Plasma SDK")); ?>

<h2><?php i18n("Qt Quick Support");?></h2>
<p>
<?php i18n("The use of <a href='http://doc.qt.digia.com/qt/qtquick.html'>Qt
Quick</a> within Plasma continues to expand. Many components have been
updated to use Qt Quick exclusively for the user interface; this also
makes it easy to extend and customize Plasma Workspaces. Plasma now
also allows Containments (which are responsible for presenting widgets
on the desktop and in panels) to be written using only Qt Quick's
easy-to-learn language. This gives developers the ability to produce
custom Containments for experimentation or special use cases. With
this capability, Plasma is a valuable, universal user interface
toolkit.");?>
</p>

<h2><?php i18n("Scripting Desktop Effects");?></h2>
<p>
<?php i18n("Scripting interfaces for window effects, behavior and management make
KWin Window Management a useful tool for developers wanting to address
a particular use case. In addition, this modular approach minimizes
the size of the core of KWin. It also improves maintainability by
moving specialized code into external scripts Compared to the C++ code
they replace, scripts make it easier to write, maintain and ensure
quality of code.");?>
</p>

<h4><?php i18n("Installing the KDE Development Platform");?></h4>
<?php
  include("boilerplate.inc");
?>

<h2><?php i18n("Also Announced Today:");?></h2>
<h2><a href="plasma.php"><img src="images/plasma.png" class="app-icon" alt="<?php i18n("The KDE Plasma Workspaces 4.10");?>" width="64" height="64" /> <?php i18n("Plasma Workspaces 4.10 Improve Mobile Device Support and Receive Visual Refinement");?></a></h2>
<p>
<?php i18n("Several components of Plasma Workspaces have been ported to Qt Quick/QML framework. Stability and usability have been improved. A new print manager and Color Management support have been introduced.");?>
</p>

<h2><a href="applications.php"><img src="images/applications.png" class="app-icon" alt="<?php i18n("The KDE Applications 4.10");?>"/> <?php i18n("KDE Applications Improve Usability, Performance and Take You to Mars");?></a></h2>
<p>
<?php i18n("KDE Applications gained feature enhancements to Kate, KMail and Konsole. KDE-Edu applications saw a complete overhaul of KTouch and many other changes. KDE Games introduced the new Picmi game and improvements throughout.");?>
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

<?php
  include("footer.inc");
?>
