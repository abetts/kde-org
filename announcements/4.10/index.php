<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $release = '4.10';
  $release_full = '4.10.0';
  $page_title = i18n_noop("KDE Software Compilation 4.10");
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<?php //showscreenshot("plasma-4.10.png", "KDE Software Compilation 4.10"); ?>
<p>
<img src="screenshots/plasma-4.10.png" style="float: right; border: 0; background-image: none; margin-top: -80px;" alt="<?php i18n("The KDE Plasma Workspaces 4.10");?>" />
<!-- <img src="../../stuff/clipart/klogo-official-oxygen-128x128.png" style="float: right;" /> -->
<br /><br /><br />
<font size="+1">
<?php i18n("The KDE Community proudly announces the latest releases of Plasma Workspaces, Applications and Development Platform. With the 4.10 release, the premier collection of Free Software for home and professional use makes incremental improvements to a large number of applications, and offers the latest technologies.");?><br />
</font>
</p>

<?php
  include "../announce-i18n-bar.inc";
?>

<p>
<h2><a href="plasma.php"><img src="images/plasma.png" class="app-icon" alt="<?php i18n("The KDE Plasma Workspaces 4.10");?>" width="64" height="64" /> <?php i18n("Plasma Workspaces 4.10 Improve Mobile Device Support and Receive Visual Refinement");?></a></h2>
<?php i18n("Several components of Plasma Workspaces have been ported to the Qt Quick/QML framework. Stability and usability have been improved. A new print manager and Color Management support have been introduced.");?>
</p>

<p>
<h2><a href="applications.php"><img src="images/applications.png" class="app-icon" alt="<?php i18n("The KDE Applications 4.10");?>"/> <?php i18n("KDE Applications Improve Usability, Performance and Take You to Mars");?></a></h2>
<?php i18n("KDE Applications gained feature enhancements to Kate, KMail and Konsole. KDE-Edu applications saw a complete overhaul of KTouch and many other changes. KDE Games introduced the new Picmi game and improvements throughout.");?>

</p>

<p>
<h2><a href="platform.php"><img src="images/platform.png" class="app-icon" alt="<?php i18n("The KDE Development Platform 4.10");?>"/> <?php i18n("KDE Platform 4.10 Opens Up More APIs to Qt Quick");?></a></h2>
<?php i18n("This release makes it easier to contribute to KDE with a Plasma SDK (Software Development Kit), the ability to write Plasma widgets and widget collections in the Qt Markup Language (QML), changes in the libKDEGames library, and new scripting capabilities in window manager KWin.");?>
</p>
<p><br />
<?php i18n("The KDE Quality team <a href='http://www.sharpley.org.uk/blog/kde-testing'>organized a testing program</a> for this release, assisting developers by identifying legitimate bugs and carefully testing the applications. Thanks to their work, KDE innovation and quality go hand in hand. If you are interested in the Quality Team and their work, check out <a href='http://community.kde.org/Get_Involved/Quality'>The Quality Team wikipage</a>.");?><br />
</p>
<h2><?php i18n("Spread the Word and See What's Happening: Tag as &quot;KDE&quot;");?></h2>
<p>
<?php i18n("KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.10 releases of KDE software.");?>
</p>
<h2><?php i18n("Release Parties");?></h2>
<p>
<?php i18n("As usual, KDE community members organize release parties all around the world. Several have already been scheduled and more will come later. Find <a href='http://community.kde.org/Promo/Events/Release_Parties/4.10'>a list of parties here</a>. Anyone is welcome to join! There will be a combination of interesting company and inspiring conversations as well as food and drinks. It's a great chance to learn more about what is going on in KDE, get involved, or just meet other users and contributors.");?>
</p>
<p>
<?php i18n("We encourage people to organize their own parties. They're fun to host and open for everyone! Check out <a href='http://community.kde.org/Promo/Events/Release_Parties'>tips on how to organize a party</a>.");?>
<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.10/&amp;title=KDE%20releases%20version%204.10%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.10/" data-text="#KDE releases version 4.10 of Plasma Workspaces, Applications and Platform →" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.10/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.10%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.10/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde410"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde410"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde410"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde410"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>
<h2><?php i18n("About these release announcements");?></h2>
<p>
<?php i18n("These release announcements were prepared by Devaja Shah, Jos Poortvliet, Carl Symons, Sebastian Kügler and other members of the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past six months.");?>
</p>

<h4><?php i18n("Support KDE");?></h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="<?php i18n("Join the Game");?>" align="left"/> </a>
<p align="justify"> <?php i18n("KDE e.V.'s new <a
href='http://jointhegame.kde.org/'>Supporting Member program</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.");?></p>
<br clear="all" />
<p>&nbsp;</p>

<?php
  include("footer.inc");
?>
