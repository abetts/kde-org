<?php

  $release = '4.10';
  $release_full = '4.10.0';
  $page_title = "KDE platvorm 4.10 viib rohkem API-sid üle Qt Quickile";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Teistes keeltes:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<h2>Plasma SDK</h2>
<p>
KDE arendusplatvormi käesolevas väljalaskes leiab senisest märksa täielikuma Plasma SDK. Varasemad eraldi komponendid, näiteks plasmoidviewer, plasmaengineexplorer ja plasmawallpaperviewer, kuuluvad nüüd kõik Plasma vidinate arendamise tööriistakomplekti <a href="http://techbase.kde.org/Projects/Plasma/PlasMate">Plasmate</a> koosseisu.
</p>
<?php showscreenshot("plasmate.png", "Plasmate kujutab endast Plasma SDK südant"); ?>

<h2>Qt Quicki toetus</h2>
<p>
<a href="http://doc.qt.digia.com/qt/qtquick.html">Qt Quicki</a> kasutamine Plasmas aina areneb. Paljud koostisosad on uuendatud, et need pruugiksid kasutajaliideses ainult Qt Quicki, mis ühtlasi muudab hõlpsaks Plasma töötsoonide laiendamise ja kohandamise. Plasma võimaldab nüüd kirjutada ka konteinereid (mille ülesanne on esitada vidinaid töölaual ja paneelidel) ainult Qt Quicki hõlpsasti omandatavat arenduskeelt kasutades. See lubab arendajatel pakkuda katsetamiseks või erijuhtudeks kohandatud konteinereid. Sel moel on Plasma muutunud väärtuslikuks ja universaalseks kasutajaliidese tööriistakomplektiks.
</p>

<h2>Töölauaefektid skriptidega</h2>
<p>
Töölaua efektide, käitumise ja haldamise skriptimisliidesd muudavad KWini aknahalduse arendajate silmis tulusaks tööriistaks, mille abil lahendada konkreetseid erijuhtumeid. Lisaks vähendab modulaarne lähenemine tublisti KWini põhiosa suurust. Paraneb ka hooldatavus, kui spetsiaalseteks juhtumiteks mõeldud kood jätta väliste skriptide hooleks. Võrreldes C++ koodiga, mida skriptid asendavad, on koodi kirjutamine, hooldamine ja kvaliteedi tagamine palju kergem.
</p>

<h4>KDE arendusplatvormi paigaldamine</h4>
<?php
  include("boilerplate-et.inc");
?>

<h2>Täna ilmusid veel:</h2>
<h2><a href="plasma-et.php"><img src="images/plasma.png" class="app-icon" alt="KDE Plasma töötsoonid 4.10" width="64" height="64" /> Plasma töötsoonid 4.10 parandavad mobiilsete seadmete toetust ja näevad kaunimad välja</a></h2>
<p>
Mitmed Plasma töötsoonide komponendid porditi Qt Quick/QML-i raamistikule. Stabiilsus ja kasutatavus on paranenud. Lisandunud on uus trükkimishaldur ja värvihalduse toetus.
</p>

<h2><a href="applications-et.php"><img src="images/applications.png" class="app-icon" alt="KDE rakendused 4.10"/> KDE rakendused: hõlpsamad kasutada, parema jõudlusega ja viivad lausa Marsile</a></h2>
<p>
KDE rakendustes on eriti tuntavaid täiendusi saanud Kate, KMail ja Konsool. KDE õpirakendustes on põhjalikult muudetud KTouchi, aga muutusi on teisigi. KDE mängud pakuvad uut mängu Picmi ning mitmeid mängimist parandamist täiustusi.
</p>

<?php
  include($site_root . "/contact/about_kde-et.inc");
?>

<h4>Kontaktisikud</h4>

<?php
  include($site_root . "/contact/press_contacts-et.inc");
?>

<?php
  include("footer-et.inc");
?>