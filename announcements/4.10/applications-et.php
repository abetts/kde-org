<?php

  $release = '4.10';
  $release_full = '4.10.0';
  $page_title = "KDE rakendused: hõlpsamad kasutada, parema jõudlusega ja viivad lausa Marsile";
  $site_root = "..";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Teistes keeltes:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<h2>Kate märguanded pole enam nii pealetükkivad</h2>
<p>
KDE võimas tekstiredaktor Kate sai täiustusi paljudes valdades. Tänu <a href="http://ev.kde.org">KDE e.V.</a> spondeerimisel peetud intensiivsetele <a href="http://dot.kde.org/2012/11/24/katekdevelop-october-sprint-whats-new-kate">koodikirjutamispäevadele oktoobris</a> sai Kate tunduvalt parema märguannete süsteemi, lisavõimalusena kerimisribana kasutatava niinimetatud minikaardi, uue projektihalduse plugina, valmisvärviskeemid, tunduvaid parandusi skriptiliideses ja veel palju muud. Tõsise vigade parandamise pingutusega suudeti avatud veateadete hulk viia 850lt 60ni. Koodikirjutamispäevade kõrval nähti vaeva uue kiiravamise võimaluse ja muude vajalike omaduste kallal. Kõik parandused toovad kasu ka Kate kasutamisele teistes rakendustes, sealhulgas kergekaalulisemale tekstiredaktorile KWrite ning KDE võimsale arenduskeskkonnale KDevelop.
</p>
<?php showscreenshot("kate-notifications.png", "Kate uued passiivsed märguanded ei sega enam nii palju töötegemist"); ?>

<h2>Konsooli täiustused</h2>
<p>
Konsooli jõudsid tagasi KDE 3 ajast tuntud ekraani trükkimise ja signaalide saatmise võimalused, samuti saab muuta ridade vahet ning Control-klahvi käitumist teksti lohistamise ajal. Nüüd on toetatud xtermi 1006 hiirelaiendused ning käsurea võib puhastada, enne kui kasutada järjehoidjaid mõne käsu andmiseks.
</p>
<?php showscreenshot("konsole-top.png", "Konsool sai uusi võimalusi"); ?>

<h2>Tükkrenderdamine parandab Okulari jõudlust</h2>
<p>
KDE universaalne dokumendinäitaja Okular sai hulga täiustusi. Uusimate omaduste hulka kuulub näiteks meetod, mida nimetatakse tükkrenderdamiseks (<a href="http://tsdgeos.blogspot.com/2012/11/okular-tiled-rendering-merged-to-master.html">tiled rendering</a>), mis annab Okularile võimaluse suurendada dokumente senisest rohkem ja kiiremini ning samal ajal ometi vähendada varasemaga võrreldes mälukasutust. Põimitud video käitlemist on täiustatud. Annotatsioonide loomine ja muutmine on kasutajasõbralikum ning nüüd käitub ka tahvelarvuti annotatsiooni loomisel samamoodi nagu hiirega nii-öelda päriarvuti. Taustal tegutseva meetodi (QTabletEvent) suure täpsuse tõttu on ka vabakäeannotatsioonid palju kenamad. Üks uus omadus on hõlpus liikumine ajaloos edasi- ja tagasinupuga. Dokumendinäitaja puutesõbralik versioon Okular Active kuulub nüüd KDE tuumikrakenduste hulka. See on Plasma Active'i uus e-raamatute lugemise rakendus, mida on optimeeritud puutetundlikke seadmeid silmas pidades. Okular Active toetab väga paljusid e-raamatu vorminguid.
</p>
<?php showscreenshot("okular-tiled-rendering.png", "Tükkrenderdamine Okularis: kiirem suurendamine vähema mäluga"); ?>

<h2>Gwenview sai tegevuste toe</h2>
<p>
KDE pildinäitaja Gwenview võib uhkustada täiustatud pisipiltide loomise ja käsitsemise ning tegevuste toetamisega. Rakendus toetab JPG- ja PNG-failide värvikorrektsiooni, kohandudes koostöös KWiniga konkreetse monitori värviprofiilile, mis lubab fotosid ja graafikat eri masinates ühesugusena näha. Gwenview piltide importija töötab nüüd rekursiivselt, näidates kõiki saadaolevaid pilte nii kataloogi sees kui ka selle alamkataloogides. Täpsemalt kõneleb sellest <a href="http://agateau.com/2012/12/04/changes-in-gwenview-for-kde-sc-4.10">Gwenview hooldaja Aurélien Gateau ajaveeb</a>.
</p>
<?php showscreenshot("gwenview.png", "KDE tõhus pildinäitaja Gwenview"); ?>
<p>
KDE vestlusrakendus Kopete sai tõelise "mittehäirimise" režiimi, mis tühistab täielikult visuaalsed ja helilised märguanded.
</p>
<h2>Kontacti jõudlus paranes</h2>
<p>
KDE PIM-i rakendustes parandati palju vigu ja lisati hulk täiendusi. Suur töö otsinguga tegeleva taustaprogrammi kallal tõi kaasa tohutult paranenud e-kirjade indekseerimise ja hankimise, mis omakorda tähendab töökindlamaid rakendusi ja väiksemat ressursikasutust.
</p>
<?php showscreenshot("kontact.png", "Kontacti grupitöö klient"); ?>
<p>
KMail suudab nüüd automaatselt muuta kirjadele lisatud piltide suurust, mida saab seadistada rakenduse seadistustes. Samuti pakub KMail teksti automaatset korrigeerimist, muu hulgas sõnade asendamist ja lausete algustähtede muutmist suureks. Seadistused ja sõnaloendid on ühised Calligra Wordsiga ning neid saab igati muuta. Laienenud on HTML-kirjade koostamise toetus: lisada saab tabeleid, määrata nende ridade ja veergude omadusi ning lahtreid liita. Nüüd on toetatud ka piltide kindlaks määratud suurused, samuti võib HTML-koodi otse sisestada. Paranenud on samuti HTML-kirjade "lihtteksti" osa, sealhulgas HTML-siltide teisendamine lihttekstiks. Muude KMaili täiustuste seas võib ära mainida hiljuti kasutatud failide avamist kirjakoostajas, uute kontaktide lisamist otse KMailist ning visiitkaartide (vCard) lisamist kirjadele.<br />
Importimisnõustaja toetab nüüd seadistuste importimist Operast, seadistuste ja andmete importimist Claws Mailist ja Balsast ning siltide importimist Thunderbirdist ja Claws Mailist.
</p>
<?php showscreenshot("kontact-mail.png", "Kontacti e-posti klient"); ?>

<h2>Suured täiustused mängudes</h2>
<p>
KDE mängud ja õpirakendused on üle elanud suuri muutusi. KDE mängud on tunduvalt paremad põhiteekide täiustamise tõttu, mis tagab märksa sujuvama mängimise. Välimuselt ja sisultki uus on masinakirja õppimise rakendus KTouch ning etemaks on mitmes mõttes muutunud Marble, mis vaid kindlustab enda tugevat kohta vaba tarkvara maailma kaardirakenduste seas.
</p>
<p>
Käesolevas väljalaskes on samuti üks uus mäng. <a href="http://games.kde.org/game.php?game=picmi">Picmi</a> kujutab endast ühele inimesele mõeldud loogikamängu. Selle eesmärk on märkida lahtreid vastavalt mängulaua serval olevatele arvudele, et luua algselt varjatud muster või pilt. Picmi pakub kaht mängimisrežiimi: juhuslikud mängud luuakse vastavalt valitud raskusastmele, kuid võib läbi mängida ka valmismõistatusi.
</p>
<?php showscreenshot("picmi.png", "Uus mäng Picmi"); ?>
<h2>Sudokude trükkimine</h2>
<p>


Parandusi on saanud teisedki KDE mängud ja õpirakendused, muu hulgas suudab KSudoku nüüd mõistatusi välja trükkida, nii et neid saab lahendada ka mujal kui arvutis. KGoldrunner on ümber kirjutatud uute KDE mängude teekide peale: mäng ise ja välimuski on sama, aga kõik toimib kenamini ja sujuvamalt. Hüpleva kuubiku mäng (KJumpingCube) võimaldab nüüd muuta käikude kiirust ning animeerib mitmeosalised käigud, et neid oleks lihtsam mõista. Kasutajaliidest on täiustatud ja mängida saab kas Kepleri või Newtoni vastu. Väiksema mängulaua korral saab kasutada lihtsustatud mängustiile. KAlgebra kasutajaliidest on veidi parandatud, Pairs pakub aga võimalust kasutada teemaredaktorit.
</p>
<?php showscreenshot("ksudoku.png", "Sudokusid saab trükkida ja paberil lahendada"); ?>
<p>
Graafiteooria arenduskeskkonna Rocs arendajad muutsid omajagu kasutajaliidest ja seadistustedialoogi, et neid oleks hõlpsam kasutada. Samuti on nüüd toetatud järgmised failitüübid: TGF, DOT/Graphviz (import/eksport) ja TikZ/PGF (ainult eksport).
</p>

<h2>Kuule ja Marsile: kosmosesondid kõigile näha</h2>
<p>
<a href="http://marble.kde.org/">Virtuaalne gloobus Marble</a> jätkab tungimist kosmosesse. <a href="http://tu-dresden.de/en">Dresdeni tehnikaülikooli</a> tudeng René Küttner võttis Marble'i ette <a href="http://sophia.estec.esa.int/socis2012/">ESA SoCiS 2012 programmi</a> raames. <a href="http://www.esa.int/ESA">Euroopa Kosmoseagentuur</a> käivitas ka programmi "The Summer of Code in Space" ning sellegi puhul ostus Marble väljavalituks.
</p>
<p>
<?php showscreenshot("marble-indonesia.png", "Töölauagloobus Marble"); ?>

René töötas välja viisi, kuidas näidata teiste planeetide ümber tiirlevaid kosmosesonde Marble'is. Nii võib Marble näidata selliste kosmoseekspeditsioonide asukohti ja orbiite, nagu <a href="http://www.esa.int/Our_Activities/Space_Science/Mars_Express">Mars Express</a>, <a href="http://www.esa.int/Our_Activities/Space_Science/Venus_Express">Venus Express</a> ja <a href="http://www.esa.int/Our_Activities/Space_Science/SMART-1">SMART-1</a>. Näha on ka Marsi kuude Phobose ja Deimose asukohad. Samuti täiustas ta Maa satelliitide teekonna kuvamist. Mõningaid lisandunud omadusi näitab <a href="http://www.youtube.com/watch?v=K_VA0XtvjYk">see video</a>. Projekti rahastamise eest tuleb tänada <a href="http://www.esa.int/ESA">ESA-t</a> ja <a href="http://sophia.estec.esa.int/socis2012/?q=sponsors">SoCiS-e sponsoreid</a>.
</p>
<?php showscreenshot("marble-mars.png", "Marble Marsil"); ?>
Sellest projektist ja muudest Marble'i uudistest annab põhjalikuma ülevaate <a href="http://marble.kde.org/changelog.php#v15">Marble'i visuaalne muudatuste logi</a>.

<h2>Masinakirja õppimine mõnusaks</h2>
<p>
KDE masinakirja õppimise rakendus Ktouch on põhjalikult ümber kirjutatud. Nüüd on sel selge, elegantne ja elav kasutajaliides, mis muudab masinakirja õppimise ja harjutamise nii mõnusaks, kui see vähegi olla saab. <a href="http://blog.sebasgo.net/blog/2012/11/28/november-update-for-ktouch/">Uus kasutajaliides</a> vähendab varasemat keerukust ning annab õppurile abi värvide ja mittesegavate animatsioonidega. Paljud uued omadused aitavad õppimist ja harjutamist veelgi parandada: uus õppetunniredaktor kasutab kvaliteedikontrolli, kasutaja saab jälgida enda edenemist ja leida üles nõrgad kohad, üldine välimus on köitev ja vastab ekraani suurusele, vihjeid ja konkreetseid osutusi, mida ja kuidas paremaks muuta, näeb palju selgemini.
</p>
<?php showscreenshot("ktouch.png", "Masinakiri selgeks KTouchiga"); ?>

<h4>KDE rakenduste paigaldamine</h4>
<?php
  include("boilerplate-et.inc");
?>

<h2>Täna ilmusid veel:</h2>
<h2><a href="plasma-et.php"><img src="images/plasma.png" class="app-icon" alt="KDE Plasma töötsoonid 4.10" width="64" height="64" /> Plasma töötsoonid 4.10 parandavad mobiilsete seadmete toetust ja näevad kaunimad välja</a></h2>
<p>
Mitmed Plasma töötsoonide komponendid porditi Qt Quick/QML-i raamistikule. Stabiilsus ja kasutatavus on paranenud. Lisandunud on uus trükkimishaldur ja värvihalduse toetus.
</p>

<h2><a href="platform-et.php"><img src="images/platform.png" class="app-icon" alt="KDE arendusplatvorm 4.10"/> KDE platvorm 4.10 viib rohkem API-sid üle Qt Quickile</a></h2>
<p>
Käesolev väljalase muudab veel lihtsamaks arendada KDE-d Plasma tarkvaraarenduse tööriistakomplektiga (SDK), kirjutada Plasma vidinaid ja vidinakogusid Qt märkekeeles (QML). Samuti võib märkida muudatusi teegis libKDEGames ja uusi skriptimisvõimalusi aknahalduris KWin.
</p>

<?php
  include($site_root . "/contact/about_kde-et.inc");
?>

<h4>Kontaktisikud</h4>

<?php
  include($site_root . "/contact/press_contacts-et.inc");
?>

<?php
  include("footer-et.inc");
?>