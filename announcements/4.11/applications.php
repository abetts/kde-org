<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $release = '4.11';
  $release_full = '4.11.0';
  $page_title = i18n_noop("KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over");
  $site_root = "..";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<?php
  include "../announce-i18n-bar.inc";
?>

<p>
<?php i18n("August 14, 2013");?>
</p>

<p>
<?php i18n("The Dolphin file manager brings many small fixes and optimizations in this release. Loading large folders has been sped up and requires up to 30&#37; less memory. Heavy disk and CPU activity is prevented by only loading previews around the visible items. There have been many more improvements: for example, many bugs that affected expanded folders in Details View were fixed, no &quot;unknown&quot; placeholder icons will be shown any more when entering a folder, and middle clicking an archive now opens a new tab with the archive contents, creating a more consistent  experience overall.");?>
</p>

<?php showscreenshot("send-later.png", i18n_var("The new send-later work flow in Kontact")); ?>

<h2><?php i18n("Kontact Suite Improvements");?></h2>

<p>
<?php i18n("The Kontact Suite has once again seen significant focus on stability, performance and memory usage. Importing folders, switching between maps, fetching mail, marking or moving large numbers of messages and startup time have all been improved in the last 6 months. See <a href='http://blogs.kde.org/2013/07/18/memory-usage-improvements-411'>this blog</a> for details. The <a href='http://www.aegiap.eu/kdeblog/2013/07/news-in-kdepim-4-11-archive-mail-agent/'>archive functionality has seen many bug fixes</a> and there have also been improvements in the ImportWizard, allowing importing of settings from the Trojitá mail client and better importing from various other applications. Find more information <a href='http://www.progdan.cz/2013/07/whats-new-in-the-akonadi-world/'>here</a>.");?>
</p>

<?php showscreenshot("kmail-archive-agent.png", i18n_var("The archive agent manages storing email in compressed form")); ?>

<p>
<?php i18n("This release also comes with some significant new features. There is a <a href='http://www.aegiap.eu/kdeblog/2013/05/news-in-kdepim-4-11-header-theme-33-grantlee-theme-generator-headerthemeeditor/'>new theme editor for email headers</a> and email images can be resized on the fly. The <a href='http://www.aegiap.eu/kdeblog/2013/07/new-in-kdepim-4-11-send-later-agent/'>Send Later feature</a> allows scheduling the sending of emails on a specific date and time, with the added possibility of repeated sending according to a specified interval. KMail Sieve filter support (an IMAP feature allowing filtering on the server) has been improved, users can generate sieve filtering scripts <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-improve-sieve-support-22/'>with an easy-to-use interface</a>. In the security area, KMail introduces automatic 'scam detection', <a href='http://www.aegiap.eu/kdeblog/2013/04/news-in-kdepim-4-11-scam-detection/'>showing a warning</a> when mails contain typical phishing tricks.  You now receive an <a href='http://www.aegiap.eu/kdeblog/2013/06/news-in-kdepim-4-11-new-mail-notifier/'>informative notification</a> when new mail arrives. and last but not least, the Blogilo blog writer comes with a much-improved QtWebKit-based HTML editor.");?>
</p>

<h2><?php i18n("Extended Language Support for Kate");?></h2>

<p>
<?php i18n("Advanced text editor Kate introduces new plugins: Python (2 and 3), JavaScript & JQuery, Django and XML. They introduce features like static and dynamic autocompletion, syntax checkers, inserting of code snippets and the ability to automatically indent XML with a shortcut. But there is more for Python friends: a python console providing in-depth information on an opened source file. Some small UI improvements have also been done, including <a href='http://kate-editor.org/2013/04/02/kate-search-replace-highlighting-in-kde-4-11/'>new passive notifications for the search functionality</a>, <a href='http://kate-editor.org/2013/03/16/kate-vim-mode-papercuts-bonus-emscripten-qt-stuff/'>optimizations to the VIM mode</a> and <a href='http://kate-editor.org/2013/03/27/new-text-folding-in-kate-git-master/'>new text folding functionality</a>.");?>
</p>

<?php showscreenshot("kstars.png", i18n_var("KStars shows interesting upcoming events visible from your location")); ?>

<h2><?php i18n("Other Application Improvements");?></h2>

<p>
<?php i18n("In the area of games and education several smaller and larger new features and optimizations have arrived. Prospective touch typists might enjoy the right-to-left support in KTouch while the star-gazer's friend, KStars, now has a tool which shows interesting events coming up in your area. Math tools Rocs, Kig, Cantor and KAlgebra all got attention, supporting more backends and calculations. And the KJumpingCube game now has features larger board sizes, new skill levels, faster responses and an improved user interface.");?>
</p>

<p>
<?php i18n("The Kolourpaint simple painting application can deal with the WebP image format and the universal document viewer Okular has configurable review tools and introduces undo/redo support in forms and annotations. The JuK audio tagger/player supports playback and metadata editing of the new Ogg Opus audio format (however, this requires that the audio driver and TagLib also support Ogg Opus).");?>
</p>


<h4><?php i18n("Installing KDE Applications");?></h4>
<?php
  include("boilerplate.inc");
?>

<h2><?php i18n("Also Announced Today:");?></h2>
<h2><a href="plasma.php"><img src="images/plasma.png" class="app-icon" alt="<?php i18n("The KDE Plasma Workspaces 4.11");?>" width="64" height="64" /> <?php i18n("Plasma Workspaces 4.11 Continues to Refine User Experience");?></a></h2>
<p>
<?php i18n("Gearing up for long term maintenance, Plasma Workspaces delivers further improvements to basic functionality with a smoother taskbar, smarter battery widget and improved sound mixer. The introduction of KScreen brings intelligent multi-monitor handling to the Workspaces, and large scale performance improvements combined with small usability tweaks make for an overall nicer experience.");?>
</p>

<h2><a href="platform.php"><img src="images/platform.png" class="app-icon" alt="<?php i18n("The KDE Development Platform 4.11");?>"/> <?php i18n("KDE Platform 4.11 Delivers Better Performance");?></a></h2>
<p>
<?php i18n("This release of KDE Platform 4.11 continues to focus on stability. New features are being implemented for our future KDE Frameworks 5.0 release, but for the stable release we managed to squeeze in optimizations for our Nepomuk framework.");?>
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

<?php
  include("footer.inc");
?>
