<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $release = '4.11';
  $release_full = '4.11.0';
  $page_title = i18n_noop("KDE Software Compilation 4.11");
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>
<img src="screenshots/plasma-4.11.png" style="float: right; border: 0; background-image: none; margin-top: -80px;" alt="<?php i18n("The KDE Plasma Workspaces 4.11");?>" />
<br /><br /><br />
<font size="+1">
<?php i18n("August 14, 2013. The KDE Community is proud to announce the latest major updates to the Plasma Workspaces, Applications and Development Platform delivering new features and fixes while readying the platform for further evolution. The Plasma Workspaces 4.11 will receive long term support as the team focuses on the technical transition to Frameworks 5. This then presents the last combined release of the Workspaces, Applications and Platform under the same version number.");?><br />
</font>
</p>

<?php
  include "../announce-i18n-bar.inc";
?>

<p>
<?php i18n("This release is dedicated to the memory of <a href='http://en.wikipedia.org/wiki/Atul_Chitnis'>Atul 'toolz' Chitnis</a>, a great Free and Open Source Software champion from India. Atul led the Linux Bangalore and FOSS.IN conferences since 2001 and both were landmark events in the Indian FOSS scene. KDE India was born at the first FOSS.in in December 2005. Many Indian KDE contributors started out at these events. It was only because of Atul's encouragement that the KDE Project Day at FOSS.IN was always a huge success. Atul left us on June 3rd after fighting a battle with cancer. May his soul rest in peace. We are grateful for his contributions to a better world.");?>
</p>

<p>
<?php i18n("These releases are all translated in 54 languages; we expect more languages to be added in subsequent monthly minor bugfix releases by KDE. The Documentation Team updated 91 application handbooks for this release.");?>
</p>

<p>
<h2><a href="plasma.php"><img src="images/plasma.png" class="app-icon" alt="<?php i18n("The KDE Plasma Workspaces 4.11");?>" width="64" height="64" /> <?php i18n("Plasma Workspaces 4.11 Continues to Refine User Experience");?></a></h2>
<?php i18n("Gearing up for long term maintenance, Plasma Workspaces delivers further improvements to basic functionality with a smoother taskbar, smarter battery widget and improved sound mixer. The introduction of KScreen brings intelligent multi-monitor handling to the Workspaces, and large scale performance improvements combined with small usability tweaks make for an overall nicer experience.");?>
</p>

<p>
<h2><a href="applications.php"><img src="images/applications.png" class="app-icon" alt="<?php i18n("The KDE Applications 4.11");?>"/> <?php i18n("KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over");?></a></h2>
<?php i18n("This release marks massive improvements in the KDE PIM stack, giving much better performance and many new features. Kate improves the productivity of Python and Javascript developers with new plugins, Dolphin became faster and the educational applications bring various new features.");?>

</p>

<p>
<h2><a href="platform.php"><img src="images/platform.png" class="app-icon" alt="<?php i18n("The KDE Development Platform 4.11");?>"/> <?php i18n("KDE Platform 4.11 Delivers Better Performance");?></a></h2>
<?php i18n("This release of KDE Platform 4.11 continues to focus on stability. New features are being implemented for our future KDE Frameworks 5.0 release, but for the stable release we managed to squeeze in optimizations for our Nepomuk framework.");?>
</p>
<p><br />
<?php i18n("When upgrading, please observe the <a href='http://community.kde.org/KDE_SC/4.11_Release_Notes'>release notes</a>.");?><br />
</p>
<h2><?php i18n("Spread the Word and See What's Happening: Tag as &quot;KDE&quot;");?></h2>
<p>
<?php i18n("KDE encourages people to spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, twitter, identi.ca. Upload screenshots to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with &quot;KDE&quot;. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the 4.11 releases of KDE software.");?>
</p>
<h2><?php i18n("Release Parties");?></h2>
<p>
<?php i18n("As usual, KDE community members organize release parties all around the world. Several have already been scheduled and more will come later. Find <a href='http://community.kde.org/Promo/Events/Release_Parties/4.11'>a list of parties here</a>. Anyone is welcome to join! There will be a combination of interesting company and inspiring conversations as well as food and drinks. It's a great chance to learn more about what is going on in KDE, get involved, or just meet other users and contributors.");?>
</p>
<p>
<?php i18n("We encourage people to organize their own parties. They're fun to host and open for everyone! Check out <a href='http://community.kde.org/Promo/Events/Release_Parties'>tips on how to organize a party</a>.");?>
<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.11/&amp;title=KDE%20releases%20version%204.11%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.11/" data-text="#KDE releases version 4.11 of Plasma Workspaces, Applications and Platform →" data-via="kdecommunity" data-hashtags="linux,opensource">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.11/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.11%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.11/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde411"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde411"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde411"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde411"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>
<h2><?php i18n("About these release announcements");?></h2>
<p>
<?php i18n("These release announcements were prepared by Jos Poortvliet, Sebastian Kügler, Markus Slopianka, Burkhard Lück, Valorie Zimmerman, Maarten De Meyer, Frank Reininghaus, Michael Pyne, Martin Gräßlin and other members of the KDE Promotion Team and the wider KDE community. They cover highlights of the many changes made to KDE software over the past six months.");?>
</p>

<h4><?php i18n("Support KDE");?></h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="<?php i18n("Join the Game");?>" align="left"/> </a>
<p align="justify"> <?php i18n("KDE e.V.'s new <a
href='http://jointhegame.kde.org/'>Supporting Member program</a> is
now open.  For &euro;25 a quarter you can ensure the international
community of KDE continues to grow making world class Free
Software.");?></p>
<br clear="all" />
<p>&nbsp;</p>

<?php
  include("footer.inc");
?>
