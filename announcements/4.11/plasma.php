<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $release = '4.11';
  $release_full = '4.11.0';
  $page_title = i18n_noop("Plasma Workspaces 4.11 Continues to Refine User Experience");
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<?php
  include "../announce-i18n-bar.inc";
?>

<p>
<?php i18n("August 14, 2013");?>
</p>

<?php showscreenshot("empty-desktop.png", i18n_var("KDE Plasma Workspaces 4.11")); ?>

<p>
<?php i18n("In the 4.11 release of Plasma Workspaces, the taskbar – one of the most used Plasma widgets –  <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'>has been ported to QtQuick</a>. The new taskbar, while retaining the look and functionality of its old counterpart, shows more consistent and fluent behavior. The port also resolved a number of long standing bugs. The battery widget (which previously could adjust the brightness of the screen) now also supports keyboard brightness, and can deal with multiple batteries in peripheral devices, such as your wireless mouse and keyboard. It shows the battery charge for each device and warns when one is running low. The Kickoff menu now shows recently installed applications for a few days. Last but not least, notification popups now sport a configure button where one can easily change the settings for that particular type of notification.");?>
</p>
<?php showscreenshot("notifications.png", i18n_var("Improved notification handling")); ?>

<p>
<?php i18n("KMix, KDE's sound mixer, received significant performance and stability work as well as <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>full media player control support</a> based on the MPRIS2 standard. ");?>
</p>

<?php showscreenshot("battery-applet.png", i18n_var("The redesigned battery applet in action")); ?>


<h2><?php i18n("KWin Window Manager and Compositor");?></h2>
<p>
<?php i18n("Our window manager, KWin, has once again received significant updates, moving away from legacy technology and incorporating the 'XCB' communication protocol. This results in smoother, faster window management. Support for OpenGL 3.1 and OpenGL ES 3.0 has been introduced as well. This release also incorporates first experimental support for the X11 successor Wayland. This allows to use KWin with X11 on top of a Wayland stack. For more information on how to use this experimental mode see <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>this post</a>. The KWin scripting interface has seen massive improvements, now sporting configuration UI support, new animations and graphical effects and many smaller improvements. This release brings better multi-screen awareness (including an edge glow option for 'hot corners'), improved quick tiling (with configurable tiling areas) and the usual slew of bug fixes and optimizations. See <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>here</a> and <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>here</a> for more details.");?>
</p>

<h2><?php i18n("Monitor Handling and Web Shortcuts");?></h2>

<p>
<?php i18n("The monitor configuration in System Settings has been <a href='http://www.afiestas.org/kscreen-1-0-released/'>replaced with the new KScreen tool</a>. KScreen brings more intelligent multi-monitor support to Plasma Workspaces, automatically configuring new screens and remembering settings for monitors manually configured. It sports an intuitive, visually-oriented interface and handles re-arranging monitors through simple drag and drop.");?>
</p>

<?php showscreenshot("kscreen.png", i18n_var("The new KScreen monitor handling")); ?>

<p>
<?php i18n("Web Shortcuts, the easiest way to quickly find what you're looking for on the web, have been cleaned up and improved. Many were updated to use securely encrypted (TLS/SSL) connections, new web shortcuts were added and a few obsolete shortcuts removed. The process of adding your own web shortcuts has been improved as well. Find more details <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>here</a>.");?>
</p>

<p>
<?php i18n("This release marks the end of Plasma Workspaces 1, part of the KDE SC 4 feature series. To ease the transition to the next generation this release will be supported for at least two years. Focus of feature development will shift to Plasma Workspaces 2 now, performance improvements and bugfixing will concentrate on the 4.11 series.");?>
</p>


<h4><?php i18n("Installing Plasma");?></h4>
<?php
  include("boilerplate.inc");
?>
<h2><?php i18n("Also Announced Today:");?></h2>

<h2><a href="applications.php"><img src="images/applications.png" class="app-icon" alt="<?php i18n("The KDE Applications 4.11");?>"/> <?php i18n("KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over");?></a></h2>
<p>
<?php i18n("This release marks massive improvements in the KDE PIM stack, giving much better performance and many new features. Kate improves the productivity of Python and Javascript developers with new plugins, Dolphin became faster and the educational applications bring various new features.");?>
</p>

<h2><a href="platform.php"><img src="images/platform.png" class="app-icon" alt="<?php i18n("The KDE Development Platform 4.11");?>"/> <?php i18n("KDE Platform 4.11 Delivers Better Performance");?></a></h2>
<p>
<?php i18n("This release of KDE Platform 4.11 continues to focus on stability. New features are being implemented for our future KDE Frameworks 5.0 release, but for the stable release we managed to squeeze in optimizations for our Nepomuk framework.");?>
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>

<?php
  include("footer.inc");
?>
