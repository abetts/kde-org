<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("KDE Ships Plasma 5.2 Beta");
  $site_root = "../";
  $release = 'plasma-5.1.95';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p>
<?php i18n("Tuesday, 13 January 2015.
Today KDE releases a beta for Plasma 5.2.  This release adds a number
of new components and improves the existing desktop.  We welcome all
testers to find and help fix the bugs before our stable release in two
weeks' time.
");?>
</p>

<div style="text-align: center">
<a href="plasma-5.2/full-screen.png">
<img src="plasma-5.2/full-screen-wee.png" style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="800" height="451" alt="<?php i18n("Plasma 5.2");?>" />
</a>
</div>

<h2><?php i18n("New Components");?></h2>

<div style="float: right; margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center ">
<a href="plasma-5.2/kscreen.png">
<img style="border: 0px;" src="plasma-5.2/kscreen-wee.png" width="400" height="394" alt="<?php i18n("Dual monitor setup");?>" />
</a><br />
KScreen dual monitor setup
</div>

<p>This release of Plasma comes with some new components to make your desktop even more complete:</p>

<p><strong>BlueDevil</strong>: a range of desktop components to manage Bluetooth devices.  It'll set up your mouse, keyboard, send &amp; receive files and you can browse for devices.</p>

<p><strong>KSSHAskPass</strong>: if you access computers with ssh keys but those keys have passwords this module will give you a graphical UI to enter those passwords.</p>

<p><strong>Muon</strong>: install and manage software and other addons for your computer.</p>

<p><strong>Login theme configuration (SDDM)</strong>: SDDM is now the login manager of choice for Plasma and this new System Settings module allows you to configure the theme.</p>

<p><strong>KScreen</strong>: getting its first release for Plasma 5 is the System Settings module to set up multiple monitor support.</p>

<p><strong>GTK Application Style</strong>: this new module lets you configure themeing of applications from Gnome.</p>

<p><strong>KDecoration</strong>: this new library makes it easier and more reliable to make themes for KWin, Plasma's window manager.</p>

<h2><?php i18n("Work in Progress");?></h2>

<p>These modules have problem which makes it unlikely they will be in the stable release but we welcome testing.</p>

<p><strong>Touchpad settings</strong>: feeling left handed? You can use this new System Settings module to set up your touchpad however you like.</p>

<p><strong>User Manager</strong>: a module for System Settings to create and administer user accounts.</p>

<h2><?php i18n("Other highlights");?></h2>

<p>Undo changes to Plasma desktop layout</p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="plasma-5.2/output.gif" width="608" height="384" alt="<?php i18n("Undo desktop changes");?>" /><br />
Undo changes to desktop layout
</div>

<p>Smarter sorting of results in KRunner, press Alt-space to easily search through your computer</p>

<div style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="plasma-5.2/krunner.png" style="margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="744" height="182" alt="<?php i18n("Smart sorting in KRunner");?>" /><br />
Smart sorting in KRunner
</div>

<p>Breeze window decoration theme adds a new look to your desktop and is now used by default</p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px" src="plasma-5.2/window_decoration.png" style="float: right; margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="720" height="56" alt="<?php i18n("New Breeze Window Decoration");?>" /><br />
New Breeze Window Decoration
</div>

<p>The artists in the visual design group have been hard at work on many new Breeze icons</p>

<p>They are have added a new white mouse cursor theme for Breeze.

<p>New plasma widgets: 15 puzzle, web browser, show desktop</p>

<div style="margin-left: -2ex; padding: 1ex; border: 0px; background-image: none; font-size: smaller; text-align: center">
<img style="border: 0px;" src="plasma-5.2/new_plasmoid.png" style="float: right; margin-left: -2ex; padding: 1ex; border: 0; background-image: none; " width="563" height="316" alt="<?php i18n("Web browser plasmoid");?>" /><br />
Web browser plasmoid
</div>

<p>Audio Player controls in KRunner, press Alt-Space and type next to change music track</p>

<p>The Kicker alternative application menu can install applications from the menu and adds menu editing features.</p>

<p>Over 300 bugs fixed throughout Plasma modules.</p>

<p><a href="plasma-5.1.2-5.1.95-changelog.php">5.2 beta full changelog</a></p>

<!-- // Boilerplate again -->

<!--
<h2><?php i18n("Live Images");?></h2>

<p><?php i18n("
The easiest way to try it out is the with a live image booted off a
USB disk.  Images are available for development versions of <a
href='http://cdimage.ubuntu.com/kubuntu-plasma5/'>Kubuntu Plasma 5</a>.
");?></p>
-->

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
, $release);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<p><?php print i18n_var("You can provide feedback either via the <a
href='%1'>#Plasma IRC channel</a>, <a
href='%2'>Plasma-devel
mailing list</a> or report issues via <a
href='%3'>bugzilla</a>. Plasma
5 is also <a
href='%4'>discussed on the KDE
Forums</a>. Your feedback is greatly appreciated. If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided",  "http://forum.kde.org/viewforum.php?f=289");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
