<?php
    include_once ("functions.inc");
    $translation_file = "www";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.66.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.66.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
January 11, 2020. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.66.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("All frameworks");?></h3>

<ul>
<li><?php i18n("Port from QRegExp to QRegularExpression");?></li>
<li><?php i18n("Port from qrand to QRandomGenerator");?></li>
<li><?php i18n("Fix compilation with Qt 5.15 (e.g. endl is now Qt::endl, QHash insertMulti now requires using QMultiHash...)");?></li>
</ul>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Don't use a verified nullptr as a data source");?></li>
<li><?php i18n("Support multiple children elements in comment elements");?></li>
<li><?php i18n("Set a proper agent string for Attica requests");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Correctly report if baloo_file is unavailable");?></li>
<li><?php i18n("Check cursor_open return value");?></li>
<li><?php i18n("Initialise QML monitor values");?></li>
<li><?php i18n("Move URL parsing methods from kioslave to query object");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Add Battery1 interface");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Change XHTML icon to be a purple HTML icon");?></li>
<li><?php i18n("Merge headphones and zigzag in the center");?></li>
<li><?php i18n("Add application/x-audacity-project icon (bug 415722)");?></li>
<li><?php i18n("Add 32px preferences-system");?></li>
<li><?php i18n("Add application/vnd.apple.pkpass icon (bug 397987)");?></li>
<li><?php i18n("icon for ktimetracker using the PNG in the app repo, to be replaced with real breeze SVG");?></li>
<li><?php i18n("add kipi icon, needs redone as a breeze theme svg [or just kill off kipi]");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("[android] Fix apk install target");?></li>
<li><?php i18n("Support PyQt5 compiled with SIP 5");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Remove ColorSchemeFilter from KStyle");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Display fully qualified class/namespace name as page header (bug 406588)");?></li>
</ul>

<h3><?php i18n("KCalendarCore");?></h3>

<ul>
<li><?php i18n("Improve README.md to have an Introduction section");?></li>
<li><?php i18n("Make incidence geographic coordinate also accessible as a property");?></li>
<li><?php i18n("Fix RRULE generation for timezones");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Deprecate KCModuleContainer");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("Fix invalid cast to enum by changing the type to int rather than enum");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("Deprecate KPixmapProvider");?></li>
<li><?php i18n("[KHistoryComboBox] Add method to set an icon provider");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("kconfig EBN transport protocol cleanup");?></li>
<li><?php i18n("Expose getter to KConfigWatcher's config");?></li>
<li><?php i18n("Fix writeFlags with KConfigCompilerSignallingItem");?></li>
<li><?php i18n("Add a comment pointing to the history of Cut and Delete sharing a shortcut");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Rename \"Configure Shortcuts\" to \"Configure Keyboard Shortcuts\" (bug 39488)");?></li>
</ul>

<h3><?php i18n("KContacts");?></h3>

<ul>
<li><?php i18n("Align ECM and Qt setup with Frameworks conventions");?></li>
<li><?php i18n("Specify ECM dependency version as in any other framework");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Add KPluginMetaData::supportsMimeType");?></li>
<li><?php i18n("[KAutoSaveFile] Use QUrl::path() instead of toLocalFile()");?></li>
<li><?php i18n("Unbreak build w/ PROCSTAT: add missing impl. of KProcessList::processInfo");?></li>
<li><?php i18n("[KProcessList] Optimize KProcessList::processInfo (bug 410945)");?></li>
<li><?php i18n("[KAutoSaveFile] Improve the comment in tempFileName()");?></li>
<li><?php i18n("Fix KAutoSaveFile broken on long path (bug 412519)");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("[KeySequenceHelper] Grab actual window when embedded");?></li>
<li><?php i18n("Add optional subtitle to grid delegate");?></li>
<li><?php i18n("[QImageItem/QPixmapItem] Don't lose precision during calculation");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Partial fix for accentuated characters in file name on Windows");?></li>
<li><?php i18n("Remove unrequired private declarations for taglibextractor");?></li>
<li><?php i18n("Partial solution to accept accentuated characters on windows");?></li>
<li><?php i18n("xattr: fix crash on dangling symlinks (bug 414227)");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Set breeze as default theme when reading from configuration file");?></li>
<li><?php i18n("Deprecate the top-level IconSize() function");?></li>
<li><?php i18n("Fix centering scaled icons on high dpi pixmaps");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("pic: Fix Invalid-enum-value undefined behaviour");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("[KFilePlacesModel] Fix supported scheme check for devices");?></li>
<li><?php i18n("Embed protocol data also for Windows version of trash ioslave");?></li>
<li><?php i18n("Adding support for mounting KIOFuse URLs for applications that don't use KIO (bug 398446)");?></li>
<li><?php i18n("Add truncation support to FileJob");?></li>
<li><?php i18n("Deprecate KUrlPixmapProvider");?></li>
<li><?php i18n("Deprecate KFileWidget::toolBar");?></li>
<li><?php i18n("[KUrlNavigator] Add RPM support to krarc: (bug 408082)");?></li>
<li><?php i18n("KFilePlaceEditDialog: fix crash when editing the Trash place (bug 415676)");?></li>
<li><?php i18n("Add button to open the folder in filelight to view more details");?></li>
<li><?php i18n("Show more details in warning dialog shown before starting a privileged operation");?></li>
<li><?php i18n("KDirOperator: Use a fixed line height for scroll speed");?></li>
<li><?php i18n("Additional fields such as deletion time and original path are now shown in the file properties dialog");?></li>
<li><?php i18n("KFilePlacesModel: properly parent tagsLister to avoid memleak. Introduced with D7700");?></li>
<li><?php i18n("HTTP ioslave: call correct base class in virtual_hook(). The base of HTTP ioslave is TCPSlaveBase, not SlaveBase");?></li>
<li><?php i18n("Ftp ioslave: fix 4 character time interpreted as year");?></li>
<li><?php i18n("Re-add KDirOperator::keyPressEvent to preserve BC");?></li>
<li><?php i18n("Use QStyle for determining icon sizes");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("ActionToolBar: Only show the overflow button if there are visible items in the menu (bug 415412)");?></li>
<li><?php i18n("Don't build and install app templates on android");?></li>
<li><?php i18n("Don't hardcode the margin of the CardsListView");?></li>
<li><?php i18n("Add support for custom display components to Action");?></li>
<li><?php i18n("Let the other components grow if there's more things on the header");?></li>
<li><?php i18n("Remove dynamic item creation in DefaultListItemBackground");?></li>
<li><?php i18n("reintroduce the collapse button (bug 415074)");?></li>
<li><?php i18n("Show application window icon on AboutPage");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("Add KColumnHeadersModel");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Added tests for Math.exp()");?></li>
<li><?php i18n("Added tests for various assignment operators");?></li>
<li><?php i18n("Test special cases of multiplicate operators (*, / and %)");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Ensure the dialog title is correct with an uninitialised engine");?></li>
<li><?php i18n("Don't show the info icon on the big preview delegate (bug 413436)");?></li>
<li><?php i18n("Support archive installs with adoption commands (bug 407687)");?></li>
<li><?php i18n("Send along the config name with requests");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Expose enum to the metaobject compiler");?></li>
</ul>

<h3><?php i18n("KQuickCharts");?></h3>

<ul>
<li><?php i18n("Also correct the shader header files");?></li>
<li><?php i18n("Correct license headers for shaders");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Deprecate KServiceTypeProfile");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Add \"line-count\" property to the ConfigInterface");?></li>
<li><?php i18n("Avoid unwanted horizontal scrolling (bug 415096)");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("[plasmashell] Update docs for panelTakesFocus to make it generic");?></li>
<li><?php i18n("[plasmashell] Add signal for panelTakesFocus changing");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("KActionCollection: provide a changed() signal as a replacement for removed()");?></li>
<li><?php i18n("Adjust keyboard shortcut configuration window's title");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Manager: add support for AddAndActivateConnection2");?></li>
<li><?php i18n("cmake: Consider NM headers as system includes");?></li>
<li><?php i18n("Sync Utils::securityIsValid with NetworkManager (bug 415670)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[ToolTip] Round position");?></li>
<li><?php i18n("Enable wheel events on Slider {}");?></li>
<li><?php i18n("Sync QWindow flag WindowDoesNotAcceptFocus to wayland plasmashell interface (bug 401172)");?></li>
<li><?php i18n("[calendar] Check out of bounds array access in QLocale lookup");?></li>
<li><?php i18n("[Plasma Dialog] Use QXcbWindowFunctions for setting window types Qt WindowFlags doesn't know");?></li>
<li><?php i18n("[PC3] Complete plasma progress bar animation");?></li>
<li><?php i18n("[PC3] Only show progress bar indicator when the ends won't overlap");?></li>
<li><?php i18n("[RFC] Fix Display Configuration icon margins (bug 400087)");?></li>
<li><?php i18n("[ColorScope] Work with plain QObjects again");?></li>
<li><?php i18n("[Breeze Desktop Theme] Add monochrome user-desktop icon");?></li>
<li><?php i18n("Remove default width from PlasmaComponents3.Button");?></li>
<li><?php i18n("[PC3 ToolButton] Have the label take into account complementary color schemes (bug 414929)");?></li>
<li><?php i18n("Added background colors to active and inactive icon view (bug 370465)");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Use standard ECMQMLModules");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("[ToolTip] Round position");?></li>
<li><?php i18n("Update size hint when font changes");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Display first / in mounted storage access description");?></li>
<li><?php i18n("Ensure mounted nfs filesystems matches their fstab declared counterpart (bug 390691)");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("The signal done is deprecated in favour of spellCheckDone, now correctly emitted");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("LaTeX: fix brackets in some commands (bug 415384)");?></li>
<li><?php i18n("TypeScript: add \"bigint\" primitive type");?></li>
<li><?php i18n("Python: improve numbers, add octals, binaries and \"breakpoint\" keyword (bug 414996)");?></li>
<li><?php i18n("SELinux: add \"glblub\" keyword and update permissions list");?></li>
<li><?php i18n("Several enhancements to gitolite syntax definition");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.66");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.12");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
