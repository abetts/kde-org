<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.21.0");
  $site_root = "../";
  $release = '5.21.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
April 09, 2016. KDE today announces the release
of KDE Frameworks 5.21.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<p><?php i18n("New framework: KActivitiesStats, a library for accessing the usage statistics data collected by the KDE activity manager.");?></p>

<h3><?php i18n("All frameworks");?></h3>

<p><?php i18n("Qt &gt;= 5.4 is now required, i.e. Qt 5.3 is no longer supported.");?></p>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Add const variant to getter method");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Centralize batch size in config");?></li>
<li><?php i18n("Remove code blocking indexing of text/plain files without .txt extension (bug 358098)");?></li>
<li><?php i18n("Check both, filename and filecontent to determine mimetype (bug 353512)");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("ObexManager: Split error messages for missing objects");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("add breeze lokalize icons");?></li>
<li><?php i18n("sync app icons between breeze and breeze dark");?></li>
<li><?php i18n("update theme icons and remove application-system icon fix kicker groups");?></li>
<li><?php i18n("add xpi support for firefox addons (bug 359913)");?></li>
<li><?php i18n("update okular icon with the right one");?></li>
<li><?php i18n("add ktnef app icon support");?></li>
<li><?php i18n("add kmenueditor, kmouse and knotes icon");?></li>
<li><?php i18n("change audio volume muted icon to use - for mute instead of only red color (bug 360953)");?></li>
<li><?php i18n("add djvu mimetype support (bug 360136)");?></li>
<li><?php i18n("add link instead of double entry");?></li>
<li><?php i18n("add ms-shortcut icon for gnucash (bug 360776)");?></li>
<li><?php i18n("change wallpaper background to generic one");?></li>
<li><?php i18n("update icons to use an generic wallpaper");?></li>
<li><?php i18n("add the icon for konqueror (bug 360304)");?></li>
<li><?php i18n("add process-working icon for progress animation in KDE (bug 360304)");?></li>
<li><?php i18n("add software install icon and update update icon with the right color");?></li>
<li><?php i18n("add add and remove emblem icons for dolphin select, add mount icon");?></li>
<li><?php i18n("Remove stylesheet from analogclock and kickerdash applet icons");?></li>
<li><?php i18n("sync breeze and breeze dark (bug 360294)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Fix _ecm_update_iconcache to only update the install location");?></li>
<li><?php i18n("Revert \"ECMQtDeclareLoggingCategory: Include &lt;QDebug&gt; with the generated file\"");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Fallback to QCommonStyle implementation of standardIcon");?></li>
<li><?php i18n("Set a default menu close timeout");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Removed compiler checks now that all frameworks require c++11");?></li>
<li><?php i18n("Removed QML ResourceModel as it is superseeded by KAStats::ResultModel");?></li>
<li><?php i18n("Inserting into empty QFlatSet returned an invalid iterator");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("Simplify code (qCount -&gt; std::count, homegrown isprint -&gt; QChar::isPrint)");?></li>
<li><?php i18n("encoding detection: fix crash in wrong usage of isprint (bug 357341)");?></li>
<li><?php i18n("Fix crash due to uninitialized variable (bug 357341)");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("KCompletionBox: force frameless window and don't set focus");?></li>
<li><?php i18n("KCompletionBox should *not* be a tooltip");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Add support for get QStandardPaths locations inside desktop files");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Fix kcoreaddons_desktop_to_json() on windows");?></li>
<li><?php i18n("src/lib/CMakeLists.txt - fix linking to a Threads library");?></li>
<li><?php i18n("Add stubs to allow compilation on Android");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Avoid introspecting a DBus interface when we don't use it");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("uniform use of std::numeric_limits");?></li>
<li><?php i18n("[DeclarativeDragArea] Don't override \"text\" of mime data");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Fix obsolete link in kdebugdialog5 docbook");?></li>
<li><?php i18n("Don't leak Qt5::Network as required lib for the rest of the ConfigureChecks");?></li>
</ul>

<h3><?php i18n("KDESU");?></h3>

<ul>
<li><?php i18n("Set feature macros to enable building on musl libc");?></li>
</ul>

<h3><?php i18n("KEmoticons");?></h3>

<ul>
<li><?php i18n("KEmoticons: fix crash when loadProvider fails for some reason");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Make kglobalaccel5 properly killable, fixing super slow shutdown");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Use qt system locale langs as fallback on non UNIX");?></li>
</ul>

<h3><?php i18n("KInit");?></h3>

<ul>
<li><?php i18n("Clean up and refactor the xcb port of klauncher");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("FavIconsCache: sync after write, so other apps see it, and to avoid crash on destruction");?></li>
<li><?php i18n("Fix many threading issues in KUrlCompletion");?></li>
<li><?php i18n("Fix crash in rename dialog (bug 360488)");?></li>
<li><?php i18n("KOpenWithDialog: improve window title and description text (bug 359233)");?></li>
<li><?php i18n("Allow for better cross-platform deployment of io slaves by bundling protocol info in plugin meta data");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("KSelectionProxyModel: Simplify row removal handling, simplify deselection logic");?></li>
<li><?php i18n("KSelectionProxyModel: Recreate mapping on removal only if needed (bug 352369)");?></li>
<li><?php i18n("KSelectionProxyModel: Only clear firstChild mappings for top-level");?></li>
<li><?php i18n("KSelectionProxyModel: Ensure proper signalling when removing last selected");?></li>
<li><?php i18n("Make DynamicTreeModel searchable by display role");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Do not crash if .desktop files are missing or broken");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Handle left-button clicking on legacy systray icons (bug 358589)");?></li>
<li><?php i18n("Only use X11BypassWindowManagerHint flag on platform X11");?></li>
</ul>

<h3><?php i18n("Package Framework");?></h3>

<ul>
<li><?php i18n("After installing a package, load it");?></li>
<li><?php i18n("if the package exists and up to date don't fail");?></li>
<li><?php i18n("Add Package::cryptographicHash(QCryptographicHash::Algorithm)");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Set the contact uri as person uri in PersonData when no person exists");?></li>
<li><?php i18n("Set a name for the database connection");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("Import runner template from KAppTemplate");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Fix new kbuildsycoca warning, when a mimetype inherits from an alias");?></li>
<li><?php i18n("Fix handling of x-scheme-handler/* in mimeapps.list");?></li>
<li><?php i18n("Fix handling of x-scheme-handler/* in mimeapps.list parsing (bug 358159)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Revert \"Open/Save config page: Use term \"Folder\" instead of \"Directory\"\"");?></li>
<li><?php i18n("enforce UTF-8");?></li>
<li><?php i18n("Open/Save config page: Use term \"Folder\" instead of \"Directory\"");?></li>
<li><?php i18n("kateschemaconfig.cpp: use correct filters with open/save dialogs (bug 343327)");?></li>
<li><?php i18n("c.xml: use default style for control flow keywords");?></li>
<li><?php i18n("isocpp.xml: use default style \"dsControlFlow\" for control flow keywords");?></li>
<li><?php i18n("c/isocpp: add more C standard types");?></li>
<li><?php i18n("KateRenderer::lineHeight() returns an int");?></li>
<li><?php i18n("printing: use font size from selected printing schema (bug 356110)");?></li>
<li><?php i18n("cmake.xml speedup: Use WordDetect instead of RegExpr");?></li>
<li><?php i18n("Change tab width to 4 instead of 8");?></li>
<li><?php i18n("Fix changing the current line number color");?></li>
<li><?php i18n("Fix selecting completion item with the mouse (bug 307052)");?></li>
<li><?php i18n("Add syntax highlighting for gcode");?></li>
<li><?php i18n("Fix the MiniMap selection background painting");?></li>
<li><?php i18n("Fix encoding for gap.xml (use UTF-8)");?></li>
<li><?php i18n("Fix nested comment blocks (bug 358692)");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Take content margins into account when calculating size hints");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Fix editing toolbars loses plugged actions");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("ConnectionSettings: Initialize gateway ping timeout");?></li>
<li><?php i18n("New TunSetting and Tun connection type");?></li>
<li><?php i18n("Create devices for all known types");?></li>
</ul>

<h3><?php i18n("Oxygen Icons");?></h3>

<ul>
<li><?php i18n("Install index.theme to same directory it always was in");?></li>
<li><?php i18n("Install into oxygen/base/ so icons move from apps don't clash with version installed by those apps");?></li>
<li><?php i18n("Replicate symlinks from breeze-icons");?></li>
<li><?php i18n("Add new emblem-added and emblem-remove icons for sync with breeze");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[calendar] Fix calendar applet not clearing selection when hiding (bug 360683)");?></li>
<li><?php i18n("update audio icon to use stylesheet");?></li>
<li><?php i18n("update audio mute icon (bug 360953)");?></li>
<li><?php i18n("Fixing the force-creation of applets when plasma is immutable");?></li>
<li><?php i18n("[Fading Node] Don't mix opacity separately (bug 355894)");?></li>
<li><?php i18n("[Svg] Don't reparse configuration in response to Theme::applicationPaletteChanged");?></li>
<li><?php i18n("Dialog: Set SkipTaskbar/Pager states before showing window (bug 332024)");?></li>
<li><?php i18n("Reintroduce busy property in Applet");?></li>
<li><?php i18n("Make sure PlasmaQuick export file is properly found");?></li>
<li><?php i18n("Don't import an nonexistent layout");?></li>
<li><?php i18n("Make it possible for an applet to offer a test object");?></li>
<li><?php i18n("Replace QMenu::exec with QMenu::popup");?></li>
<li><?php i18n("FrameSvg: Fix dangling pointers in sharedFrames when theme changes");?></li>
<li><?php i18n("IconItem: Schedule pixmap update when window changes");?></li>
<li><?php i18n("IconItem: Animate active and enabled change even with animations disabled");?></li>
<li><?php i18n("DaysModel: Make update a slot");?></li>
<li><?php i18n("[Icon Item] Don't animate from previous pixmap when it has been invisible");?></li>
<li><?php i18n("[Icon Item] Don't call loadPixmap in setColorGroup");?></li>
<li><?php i18n("[Applet] Don't overwrite \"Persistent\" flag of Undo notification");?></li>
<li><?php i18n("Allowing to override plasma mutability setting on containment creation");?></li>
<li><?php i18n("Add icon/titleChanged");?></li>
<li><?php i18n("Remove QtScript dependency");?></li>
<li><?php i18n("Header of plasmaquick_export.h is in plasmaquick folder");?></li>
<li><?php i18n("Install some plasmaquick headers");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.21");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.4");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
