<?php
    include_once ("functions.inc");
    $translation_file = "www";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.64.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.64.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
November 10, 2019. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.64.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Add some std::move in setter functions");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Make it compile against qt5.15");?></li>
<li><?php i18n("Use propertymap to store properties in Baloo::Result");?></li>
<li><?php i18n("Add standalone conversion functions for PropertyMap to Json and vice versa");?></li>
<li><?php i18n("[Database] Rework handling environment flags");?></li>
<li><?php i18n("Replace recursion in FilteredDirIterator with loop iteration");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Center-align the non-square 64px audio mimetype icons (bug 393550)");?></li>
<li><?php i18n("Delete remnants of nepomuk icon");?></li>
<li><?php i18n("Move colorful 32px help-about icon into actions (bug 396626)");?></li>
<li><?php i18n("Improve draw icons (bug 399665)");?></li>
<li><?php i18n("Delete nepomuk icon");?></li>
<li><?php i18n("Fill middle mouse button area");?></li>
<li><?php i18n("Add folder-recent, extend hand of clock in folder-temp");?></li>
<li><?php i18n("Use a more correct and appropriate visual metaphor for \"get hot new stuff\" icon (bug 400500)");?></li>
<li><?php i18n("Update elisa icon");?></li>
<li><?php i18n("Use css instead of scss as output format");?></li>
<li><?php i18n("Fix incorrect rendering of 22px edit-opacity icon");?></li>
<li><?php i18n("Add edit-opacity icons (bug 408283)");?></li>
<li><?php i18n("Icons for windy weather (bug 412718)");?></li>
<li><?php i18n("Fix incorrect margins in 16/22px media icons");?></li>
<li><?php i18n("Use the text rather than highlight color for rating/star emblem");?></li>
<li><?php i18n("Add draw-arrow icons (bug 408283)");?></li>
<li><?php i18n("Add draw-highlight action icons (bug 408283)");?></li>
<li><?php i18n("Add PATH/LD_LIBRARY_PATH to qrcAlias invocation");?></li>
<li><?php i18n("Add applications-network icon for renaming Internet category to Network");?></li>
<li><?php i18n("Add edit-line-width icons (bug 408283)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Don't set C/C++ standards if already set");?></li>
<li><?php i18n("Use modern way to set the C/CXX standard");?></li>
<li><?php i18n("Raise CMake requirements to 3.5");?></li>
<li><?php i18n("ECMAddQch: support PREDEFINED_MACROS/BLANK_MACROS with blanks &amp; quotes");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Add standard icons to support to all entries in QDialogButtonBox (bug 398973)");?></li>
<li><?php i18n("ensure winId() not called on non-native widgets (bug 412675)");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("tests: fix macos build failure");?></li>
<li><?php i18n("Windows MSVC compile fix");?></li>
<li><?php i18n("Add a utility accessor to get a QUrl from a ResultSet::Result");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Fix memory leak in KXzFilter::init");?></li>
<li><?php i18n("Fix null pointer reference when extraction fails");?></li>
<li><?php i18n("decodeBCJ2: Fix assert with broken files");?></li>
<li><?php i18n("KXzFilter::Private: remove unused props");?></li>
<li><?php i18n("K7Zip: Fix memory use in readAndDecodePackedStreams");?></li>
</ul>

<h3><?php i18n("KCalendarCore");?></h3>

<ul>
<li><?php i18n("Add libical version too");?></li>
<li><?php i18n("Explicitly define the Journal copy ctor");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Conditionally show navigation buttons in the header for multi-page KCMs");?></li>
<li><?php i18n("don't use a custom header height (bug 404396)");?></li>
<li><?php i18n("add extra include");?></li>
<li><?php i18n("Fix memory leak of KQuickAddons::ConfigModule objects (bug 412998)");?></li>
<li><?php i18n("[KCModuleLoader] Show error when QML fails to load");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("kconfig_compiler: Move the KSharedConfig::Ptr when using them");?></li>
<li><?php i18n("Make it compile against qt5.15 without deprecated method");?></li>
<li><?php i18n("Expose isImmutable to introspection (e.g. QML)");?></li>
<li><?php i18n("Add convenience for defaults/dirty states to KCoreConfigSkeleton");?></li>
<li><?php i18n("Make kconfig_compiler generate ctors with the optional parent arg");?></li>
<li><?php i18n("Make preferences() a public function");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Avoid overloading KCModule::changed");?></li>
</ul>

<h3><?php i18n("KContacts");?></h3>

<ul>
<li><?php i18n("Install translations");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("KProcessInfoList -- add proclist backend for FreeBSD");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Use compile time checked connect");?></li>
<li><?php i18n("Make the settingChanged() slot protected");?></li>
<li><?php i18n("Get KQuickAddons::ConfigModule to expose if we're in the defaults state");?></li>
<li><?php i18n("Grab the keyboard when KeySequenceItem is recording");?></li>
<li><?php i18n("Add ManagedConfigModule");?></li>
<li><?php i18n("Remove outdated comment about [$e] expansion");?></li>
<li><?php i18n("[ConfigModule] Expose mainUi component status and error string");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("KLocale api docs: make it easier to find how to port code away from it");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("man: use &lt;arg&gt; instead of &lt;group&gt;");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Fix crash in writer collection and cleanup");?></li>
</ul>

<h3><?php i18n("KHTML");?></h3>

<ul>
<li><?php i18n("Extend KHtmlView::print() to use a predefined QPrinter instance (bug 405011)");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Add KLocalizedString::untranslatedText");?></li>
<li><?php i18n("Replace all qWarning and related calls with categorised logging");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Fix usage of the new deprecation macros for assignIconsToContextMenu");?></li>
<li><?php i18n("Deprecate KIconTheme::assignIconsToContextMenu");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Const &amp; signature of new introduced SlaveBase::configValue");?></li>
<li><?php i18n("Port to the QSslError variant of KSslInfoDialog");?></li>
<li><?php i18n("Port KSSLD internals from KSslError to QSslError");?></li>
<li><?php i18n("Make non-ignorable SSL errors explicit");?></li>
<li><?php i18n("auto-enable KIO_ASSERT_SLAVE_STATES also for from-git builds");?></li>
<li><?php i18n("Port (most of) KSslInfoDialog from KSslError to QSslError");?></li>
<li><?php i18n("kio_http: avoid double Content-Type and Depth when used by KDAV");?></li>
<li><?php i18n("Port the KSSLD D-Bus interface from KSslError to QSslError");?></li>
<li><?php i18n("Replace usage of SlaveBase::config()-&gt;readEntry by SlaveBase::configValue");?></li>
<li><?php i18n("Remove two unused member variables using KSslError");?></li>
<li><?php i18n("Avoid sending KDirNotify::emitFilesAdded when the emptytrashjob finishes");?></li>
<li><?php i18n("Deprecate the KTcpSocket-based variant of SslUi::askIgnoreSslErrors");?></li>
<li><?php i18n("Treat \"application/x-ms-dos-executable\" as executable on all platforms (bug 412694)");?></li>
<li><?php i18n("Replace usage of SlaveBase::config() by SlaveBase::mapConfig()");?></li>
<li><?php i18n("ftptest: replace logger-colors with logger");?></li>
<li><?php i18n("[SlaveBase] Use QMap instead of KConfig to store ioslave config");?></li>
<li><?php i18n("Port KSslErrorUiData to QSslError");?></li>
<li><?php i18n("exclude ioslaves directory from api docs");?></li>
<li><?php i18n("ftptest: mark overwrite without overwrite flag no longer failing");?></li>
<li><?php i18n("ftptest: refactor the daemon startup into its own helper function");?></li>
<li><?php i18n("[SslUi] Add api docs for askIgnoreSslErrors()");?></li>
<li><?php i18n("consider ftpd not required for the time being");?></li>
<li><?php i18n("port ftp slave to new error reporting system");?></li>
<li><?php i18n("fix proxy setting loading");?></li>
<li><?php i18n("Implement KSslCertificateRule with QSslError instead of KSslError");?></li>
<li><?php i18n("Port (most of) the interface of KSslCertificateRule to QSslError");?></li>
<li><?php i18n("Port KSslCertificateManager to QSslError");?></li>
<li><?php i18n("Update test kfileplacesviewtest following D7446");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Ensure that GlobalDrawer topContent always stays on top (bug 389533)");?></li>
<li><?php i18n("highlight on mouseover only when mode than one page (bug 410673)");?></li>
<li><?php i18n("Rename Okular Active to Okular Mobile");?></li>
<li><?php i18n("items have active focus on tab when they aren't in a view (bug 407524)");?></li>
<li><?php i18n("Allow contextualActions to flow into the header toolbar");?></li>
<li><?php i18n("Fix incorrect Credits model for Kirigami.AboutPage");?></li>
<li><?php i18n("Don't show context drawer if all actions are invisible");?></li>
<li><?php i18n("Fix Kirigami template image");?></li>
<li><?php i18n("keep containers devoid of deleted items");?></li>
<li><?php i18n("limit size of the drag margins");?></li>
<li><?php i18n("Fix showing menu toolbutton when no drawer is available");?></li>
<li><?php i18n("Disable dragging global drawer when in menu mode");?></li>
<li><?php i18n("Show menu items tooltip text");?></li>
<li><?php i18n("Do not warn about LayoutDirection in SearchField");?></li>
<li><?php i18n("Properly check enabled state of Action for ActionToolBar buttons");?></li>
<li><?php i18n("Use MenuItem's action property directly in ActionMenuItem");?></li>
<li><?php i18n("Allow the global drawer to become a menu if desired");?></li>
<li><?php i18n("Be more explicit about action property types");?></li>
</ul>

<h3><?php i18n("KItemViews");?></h3>

<ul>
<li><?php i18n("[RFC] Unify style of new Kirigami.ListSectionHeader and CategoryDrawer");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Better message for String.prototype.repeat(count) range errors");?></li>
<li><?php i18n("Simplify parsing of numeric literals");?></li>
<li><?php i18n("Parse JS binary literals");?></li>
<li><?php i18n("Detect truncated hex and octal literals");?></li>
<li><?php i18n("Support new standard way of specifying octal literals");?></li>
<li><?php i18n("Collection of regression tests taken from khtmltests repository");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Ensure that the changedEntries property is correctly propagated");?></li>
<li><?php i18n("Fix KNSCore::Cache fetching when initialising Engine (bug 408716)");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("[KStatusNotifierItem] Allow left click when menu is null (bug 365105)");?></li>
<li><?php i18n("Remove Growl support");?></li>
<li><?php i18n("Add and enable Notification Center support in macOS");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Unbreak build: limit DISABLE_DEPRECATED for KService to &lt; 5.0");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Make it compile against qt5.15 without deprecated method");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("KateModeMenuList: improve word wrap");?></li>
<li><?php i18n("fix crash (bug 413474)");?></li>
<li><?php i18n("more ok's arrived, more v2+");?></li>
<li><?php i18n("more ok's arrived, more v2+");?></li>
<li><?php i18n("add hint to copyright header");?></li>
<li><?php i18n("no non-trivial code lines remain here beside for people that agreed to v2+ =&gt; v2+");?></li>
<li><?php i18n("no non-trivial code lines remain here of authors no longer responding, v2+");?></li>
<li><?php i18n("more files v2+, as got OK by authors, see kwrite-devel@kde.org");?></li>
<li><?php i18n("more files v2+, as got OK by authors, see kwrite-devel@kde.org");?></li>
<li><?php i18n("more files v2+, as got OK by authors, see kwrite-devel@kde.org");?></li>
<li><?php i18n("more files v2+, as got OK by authors, see kwrite-devel@kde.org");?></li>
<li><?php i18n("more files v2+, as got OK by authors, see kwrite-devel@kde.org");?></li>
<li><?php i18n("more files v2+, as got OK by authors, see kwrite-devel@kde.org");?></li>
<li><?php i18n("katedocument.h is v2+ already");?></li>
<li><?php i18n("ok'd by loh.tar, sars, lgplv2+");?></li>
<li><?php i18n("relicense to lgplv2+, got ok of sven + michal");?></li>
<li><?php i18n("relicense to lgplv2+, got ok of sven + michal");?></li>
<li><?php i18n("all files with SPDX-License-Identifier are lgplv2+");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("update license, dh is in lgplv2+ section of relicensecheck.pl");?></li>
<li><?php i18n("update license, dh is in lgplv2+ section of relicensecheck.pl");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("lgplv2.1+ =&gt; lgplv2+");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("this header contains no gpl v2 only logic since long");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, add SPDX-License-Identifier");?></li>
<li><?php i18n("clarify license, 'michalhumpula' =&gt; ['gplv23', 'lgplv23', 'gplv2+', 'lgplv2+', '+eV' ]");?></li>
<li><?php i18n("add missing s (bug 413158)");?></li>
<li><?php i18n("KateModeMenuList: force the vertical position above the button");?></li>
<li><?php i18n("better: self-contained headers");?></li>
<li><?php i18n("group includes for semantics");?></li>
<li><?php i18n("sort includes");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("Remove call to no longer needed KIconTheme::assignIconsToContextMenu");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("FakeInput: add support for keyboard key press and release");?></li>
<li><?php i18n("Fix non-integer scale copy on creation of OutputChangeSet");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("fix default shortcut detection");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Add support for SAE authentication used by WPA3");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("map disabledTextColor to ColorScope");?></li>
<li><?php i18n("add DisabledTextColor to Theme");?></li>
<li><?php i18n("[PC3/button] Elide text always");?></li>
<li><?php i18n("Improve panel options menu entries");?></li>
<li><?php i18n("[icons/media.svg] Add 16 &amp; 32px icons, update style");?></li>
<li><?php i18n("[PlasmaComponents3] Fix checkable toolbutton background");?></li>
</ul>

<h3><?php i18n("Prison");?></h3>

<ul>
<li><?php i18n("Fix memory handling in datamatrix");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("i18n: Add ellipsis to action items (X-Purpose-ActionDisplay)");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Do not assign combobox currentIndex as it breaks binding");?></li>
<li><?php i18n("Listen to the application style changing");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Don't build static library when BUILD_TESTING=OFF");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("VHDL: all keywords are insensitive (bug 413409)");?></li>
<li><?php i18n("Add string escape characters to PowerShell syntax");?></li>
<li><?php i18n("Modelines: fix end of comment");?></li>
<li><?php i18n("Meson: more built-in functions and add built-in member functions");?></li>
<li><?php i18n("debchangelog: add Focal Fossa");?></li>
<li><?php i18n("Updates from CMake 3.16");?></li>
<li><?php i18n("Meson: Add a comment section for comment/uncomment with Kate");?></li>
<li><?php i18n("TypeScript: update grammar and fixes");?></li>
</ul>

<h3><?php i18n("ThreadWeaver");?></h3>

<ul>
<li><?php i18n("Make it compile against qt5.15");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.64");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.11");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
