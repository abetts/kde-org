
<h3>
<a href="applications.php">
KDE Applications 4.8 Offer Faster, More Scalable File Management
</a>
</h3>

<p>
<a href="applications.php">
<img src="images/applications.png" class="app-icon" alt="The KDE Applications 4.8"/>
</a>
KDE applications released today include <a href="http://userbase.kde.org/Dolphin">Dolphin</a> with its new display engine, new <a href="http://userbase.kde.org/Kate">Kate</a> features and improvements, <a href="http://userbase.kde.org/Gwenview">Gwenview</a> with functional and visual improvements. KDE Telepathy reaches first beta milestone. <a href="http://userbase.kde.org/Marble/en">Marble's</a> new features keep arriving, among which are: Elevation Profile, satellite tracking and <a href="http://userbase.kde.org/Plasma/Krunner">Krunner</a> integration. Read the complete <a href="applications.php">'KDE Applications Announcement'</a>.
<br /><br />
<br /><br />
</p>
