<?php
    include_once ("functions.inc");
    $translation_file = "www";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.69.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.69.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
April 05, 2020. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.69.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("[SearchStore] Use categorized logging");?></li>
<li><?php i18n("[QueryParser] Fix broken detection of end quote");?></li>
<li><?php i18n("[EngineQuery] Provide toString(Term) overload for QTest");?></li>
<li><?php i18n("[EngineQuery] Remove unused position member, extend tests");?></li>
<li><?php i18n("[SearchStore] Avoid long lines and function nesting");?></li>
<li><?php i18n("[baloosearch] Bail out early if specified folder is not valid");?></li>
<li><?php i18n("[MTimeDB] Consolidate time interval handling code");?></li>
<li><?php i18n("[AdvancedQueryParser] Test if quoted phrases are passed correctly");?></li>
<li><?php i18n("[Term] Provide toString(Term) overload for QTest");?></li>
<li><?php i18n("[ResultIterator] Remove unneeded SearchStore forward declaration");?></li>
<li><?php i18n("[QueryTest] Make phrase test case data driven and extend");?></li>
<li><?php i18n("[Inotify] Start the MoveFrom expire timer at most once per inotify batch");?></li>
<li><?php i18n("[UnindexedFileIndexer] Only mark file for content indexing when needed");?></li>
<li><?php i18n("[Inotify] Call QFile::decode only in a single place");?></li>
<li><?php i18n("[QML] Correctly watch for unregistration");?></li>
<li><?php i18n("[FileIndexScheduler] Update the content index progress more often");?></li>
<li><?php i18n("[FileIndexerConfig] Replace config QString,bool pair with dedicated class");?></li>
<li><?php i18n("[QML] Set the remaining time in the monitor more reliably");?></li>
<li><?php i18n("[TimeEstimator] Correct batch size, remove config reference");?></li>
<li><?php i18n("[FileIndexScheduler] Emit change to LowPowerIdle state");?></li>
<li><?php i18n("[Debug] Improve readability of positioninfo debug format");?></li>
<li><?php i18n("[Debug] Correct output of *::toTestMap(), silence non-error");?></li>
<li><?php i18n("[WriteTransactionTest] Test removal of positions only");?></li>
<li><?php i18n("[WriteTransaction] Extend position test case");?></li>
<li><?php i18n("[WriteTransaction] Avoid growing m_pendingOperations twice on replace");?></li>
<li><?php i18n("[FileIndexScheduler] Cleanup firstRun handling");?></li>
<li><?php i18n("[StorageDevices] Fix order of notification connect and initialization");?></li>
<li><?php i18n("[Config] Remove/deprecate disableInitialUpdate");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Fix broken symlinks");?></li>
<li><?php i18n("Move corner fold to top right in 24 icons");?></li>
<li><?php i18n("Make find-location show a magnifier on a map, to be different to mark-location (bug 407061)");?></li>
<li><?php i18n("Add 16px LibreOffice icons");?></li>
<li><?php i18n("Fix configure when xmllint is not present");?></li>
<li><?php i18n("Fix stylesheet linking in 8 icons");?></li>
<li><?php i18n("Fix some stylesheet colors in 2 icon files");?></li>
<li><?php i18n("Fix symlinks to incorrect icon size");?></li>
<li><?php i18n("Add input-dialpad and call-voicemail");?></li>
<li><?php i18n("Add buho icon");?></li>
<li><?php i18n("Add calindori icon in the new pm style");?></li>
<li><?php i18n("Add nota icon");?></li>
<li><?php i18n("[breeze-icons] fix shadow in some user (applets/128) icons");?></li>
<li><?php i18n("Add call-incoming/missed/outgoing");?></li>
<li><?php i18n("Handle busybox's sed like GNU sed");?></li>
<li><?php i18n("Add transmission-tray-icon");?></li>
<li><?php i18n("Improve pixel alignment and margins of keepassxc systray icons");?></li>
<li><?php i18n("Revert \"[breeze-icons] Add telegram-desktop tray icons\"");?></li>
<li><?php i18n("Add small icons for KeePassXC");?></li>
<li><?php i18n("[breeze-icons] add TeamViewer tray icons");?></li>
<li><?php i18n("Add edit-reset");?></li>
<li><?php i18n("Change document-revert style to be more like edit-undo");?></li>
<li><?php i18n("Icons for emoji categories");?></li>
<li><?php i18n("Add flameshot tray icons");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Fix type namespace requirement");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Decouple KBookmarksMenu from KActionCollection");?></li>
</ul>

<h3><?php i18n("KCalendarCore");?></h3>

<ul>
<li><?php i18n("Fix fallback to vCalendar loading on iCalendar load failure");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("listen to passiveNotificationRequested");?></li>
<li><?php i18n("workaround to never make applicationitem resize itself");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("[KConfigGui] Check font weight when clearing styleName property");?></li>
<li><?php i18n("KconfigXT: Add a value attribute to Enum field choices");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("kdirwatch: fix a recently introduced crash (bug 419428)");?></li>
<li><?php i18n("KPluginMetaData: handle invalid mimetype in supportsMimeType");?></li>
</ul>

<h3><?php i18n("KCrash");?></h3>

<ul>
<li><?php i18n("Move setErrorMessage definition out of the linux ifdef");?></li>
<li><?php i18n("Allow providing an error message from the application (bug 375913)");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Check correct file for sandbox detection");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Introduce api for passive notifications");?></li>
<li><?php i18n("[KCM Controls GridDelegate] Use <code>ShadowedRectangle</code>");?></li>
<li><?php i18n("[kcmcontrols] Respect header/footer visibility");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Use bold italic at 100% for sect4 titles, and bold 100% for sect5 titles (bug 419256)");?></li>
<li><?php i18n("Update the list of the Italian entities");?></li>
<li><?php i18n("Use the same style for informaltable as for table (bug 418696)");?></li>
</ul>

<h3><?php i18n("KIdleTime");?></h3>

<ul>
<li><?php i18n("Fix infinite recursion in xscreensaver plugin");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("Port the HDR plugin from sscanf() to QRegularExpression. Fixes FreeBSD");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("New class KIO::CommandLauncherJob in KIOGui to replace KRun::runCommand");?></li>
<li><?php i18n("New class KIO::ApplicationLauncherJob in KIOGui to replace KRun::run");?></li>
<li><?php i18n("File ioslave: use better setting for sendfile syscall (bug 402276)");?></li>
<li><?php i18n("FileWidgets: Ignore Return events from KDirOperator (bug 412737)");?></li>
<li><?php i18n("[DirectorySizeJob] Fix sub-dirs count when resolving symlinks to dirs");?></li>
<li><?php i18n("Mark KIOFuse mounts as Probably slow");?></li>
<li><?php i18n("kio_file: honour KIO::StatResolveSymlink for UDS_DEVICE_ID and UDS_INODE");?></li>
<li><?php i18n("[KNewFileMenu] Add extension to proposed filename (bug 61669)");?></li>
<li><?php i18n("[KOpenWithDialog] Add generic name from .desktop files as a tooltip (bug 109016)");?></li>
<li><?php i18n("KDirModel: implement showing a root node for the requested URL");?></li>
<li><?php i18n("Register spawned applications as an independent cgroups");?></li>
<li><?php i18n("Add \"Stat\" prefix to StatDetails Enum entries");?></li>
<li><?php i18n("Windows: Add support for file date creation");?></li>
<li><?php i18n("KAbstractFileItemActionPlugin: Add missing quotes in code example");?></li>
<li><?php i18n("Avoid double fetch and temporary hex encoding for NTFS attributes");?></li>
<li><?php i18n("KMountPoint: skip swap");?></li>
<li><?php i18n("Assign an icon to action submenus");?></li>
<li><?php i18n("[DesktopExecParser] Open {ssh,telnet,rlogin}:// urls with ktelnetservice (bug 418258)");?></li>
<li><?php i18n("Fix exitcode from kioexec when executable doesn't exist (and --tempfiles is set)");?></li>
<li><?php i18n("[KPasswdServer] replace foreach with range/index-based for");?></li>
<li><?php i18n("KRun's KProcessRunner: terminate startup notification on error too");?></li>
<li><?php i18n("[http_cache_cleaner] replace foreach usage with QDir::removeRecursively()");?></li>
<li><?php i18n("[StatJob] Use A QFlag to specify the details returned by StatJob");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Hotfix for D28468 to fix broken variable refs");?></li>
<li><?php i18n("get rid of the incubator");?></li>
<li><?php i18n("disable mousewheel completely in outside flickable");?></li>
<li><?php i18n("Add property initializer support to PagePool");?></li>
<li><?php i18n("Refactor of OverlaySheet");?></li>
<li><?php i18n("Add ShadowedImage and ShadowedTexture items");?></li>
<li><?php i18n("[controls/formlayout] Don't attempt to reset implicitWidth");?></li>
<li><?php i18n("Add useful input method hints to password field by default");?></li>
<li><?php i18n("[FormLayout] Set compression timer interval to 0");?></li>
<li><?php i18n("[UrlButton] Disable when there is no URL");?></li>
<li><?php i18n("simplify header resizing (bug 419124)");?></li>
<li><?php i18n("Remove export header from static install");?></li>
<li><?php i18n("Fix about page with Qt 5.15");?></li>
<li><?php i18n("Fix broken paths in kirigami.qrc.in");?></li>
<li><?php i18n("Add \"veryLongDuration\" animation duration");?></li>
<li><?php i18n("fix multi row notifications");?></li>
<li><?php i18n("don't depend on window active for the timer");?></li>
<li><?php i18n("Support multiple stacked Passive Notifications");?></li>
<li><?php i18n("Fix enabling border for ShadowedRectangle on item creation");?></li>
<li><?php i18n("check for window existence");?></li>
<li><?php i18n("Add missing types to qrc");?></li>
<li><?php i18n("Fix undefined check in global drawer menu mode (bug 417956)");?></li>
<li><?php i18n("Fallback to a simple rectangle when using software rendering");?></li>
<li><?php i18n("Fix color premultiply and alpha blending");?></li>
<li><?php i18n("[FormLayout] Propagate FormData.enabled also to label");?></li>
<li><?php i18n("Add a ShadowedRectangle item");?></li>
<li><?php i18n("alwaysVisibleActions property");?></li>
<li><?php i18n("don't create instances when the app is quitting");?></li>
<li><?php i18n("Don't emit palette changes if the palette didn't change");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("[KSortFilterProxyModel QML] Make invalidateFilter public");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Fix layout in DownloadItemsSheet (bug 419535)");?></li>
<li><?php i18n("[QtQuick dialog] Port to UrlBUtton and hide when there's no URL");?></li>
<li><?php i18n("Switch to using Kirigami's ShadowedRectangle");?></li>
<li><?php i18n("Fix update scenarios with no explicit downloadlink selected (bug 417510)");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("New class KNotificationJobUiDelegate");?></li>
</ul>

<h3><?php i18n("KNotifyConfig");?></h3>

<ul>
<li><?php i18n("Use libcanberra as primary means of previewing the sound (bug 418975)");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("New class PartLoader as replacement to KMimeTypeTrader for parts");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("KAutostart: Add static method to check start condition");?></li>
<li><?php i18n("KServiceAction: store parent service");?></li>
<li><?php i18n("Properly read the X-Flatpak-RenamedFrom string list from desktop files");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Make it compile against qt 5.15");?></li>
<li><?php i18n("fix folding crash for folding of single line folds (bug 417890)");?></li>
<li><?php i18n("[VIM Mode] Add g&lt;up&gt; g&lt;down&gt; commands (bug 418486)");?></li>
<li><?php i18n("Add MarkInterfaceV2, to s/QPixmap/QIcon/g for symbols of marks");?></li>
<li><?php i18n("Draw inlineNotes after drawing word wrap marker");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("[xdgoutput] Only send initial name and description if set");?></li>
<li><?php i18n("Add XdgOutputV1 version 2");?></li>
<li><?php i18n("Broadcast application menu to resources when registering them");?></li>
<li><?php i18n("Provide an implementation for the tablet interface");?></li>
<li><?php i18n("[server] Don't make assumptions about the order of damage_buffer and attach requests");?></li>
<li><?php i18n("Pass a dedicated fd to each keyboard for the xkb  keymap (bug 381674)");?></li>
<li><?php i18n("[server] Introduce SurfaceInterface::boundingRect()");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("New class KFontChooserDialog (based on KFontDialog from KDELibs4Support)");?></li>
<li><?php i18n("[KCharSelect] Do not simplify single characters in search (bug 418461)");?></li>
<li><?php i18n("If we readd items we need to clear it first. Otherwise we will see duplicate list");?></li>
<li><?php i18n("Update kcharselect-data to Unicode 13.0");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Fix EWMH non-compliance for NET::{OnScreenDisplay,CriticalNotification}");?></li>
<li><?php i18n("KWindowSystem: deprecate KStartupInfoData::launchedBy, unused");?></li>
<li><?php i18n("Expose application menu via KWindowInfo");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Added Page element");?></li>
<li><?php i18n("[pc3/busyindicator] Hide when not running");?></li>
<li><?php i18n("Update window-pin, Add more sizes, Remove redundant edit-delete");?></li>
<li><?php i18n("Create a new TopArea element using widgets/toparea svg");?></li>
<li><?php i18n("Added plasmoid heading svg");?></li>
<li><?php i18n("Make highlighted property work for roundbutton");?></li>
</ul>

<h3><?php i18n("Prison");?></h3>

<ul>
<li><?php i18n("Also expose the true minimum size to QML");?></li>
<li><?php i18n("Add a new set of barcode size functions");?></li>
<li><?php i18n("Simplify minimum size handling");?></li>
<li><?php i18n("Move barcode image scaling logic to AbstractBarcode");?></li>
<li><?php i18n("Add API to check whether a barcode is one- or two-dimensional");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("[Dialog] Use <code>ShadowedRectangle</code>");?></li>
<li><?php i18n("Fix sizing of CheckBox and RadioButton (bug 418447)");?></li>
<li><?php i18n("Use <code>ShadowedRectangle</code>");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("[Fstab] Ensure uniqueness for all filesystem types");?></li>
<li><?php i18n("Samba: Ensure to differentiate mounts sharing the same source (bug 418906)");?></li>
<li><?php i18n("hardware tool: define syntax via syntax arg");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Fix Sonnet autodetect failing on Indian langs");?></li>
<li><?php i18n("Create ConfigView an unmanaged ConfigWidget");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("LaTeX: fix math parentheses in optional labels (bug 418979)");?></li>
<li><?php i18n("Add Inno Setup syntax, including embedded Pascal scripting");?></li>
<li><?php i18n("Lua: add # as additional deliminator to activate auto-completion with <code>#something</code>");?></li>
<li><?php i18n("C: remove ' as digit separator");?></li>
<li><?php i18n("add some comment about the skip offset stuff");?></li>
<li><?php i18n("optimize dynamic regex matching (bug 418778)");?></li>
<li><?php i18n("fix regex rules wrongly marked as dynamic");?></li>
<li><?php i18n("extend indexer to detect dynamic=true regexes that have no place holders to adapt");?></li>
<li><?php i18n("Add Overpass QL highlighting");?></li>
<li><?php i18n("Agda: keywords updated to 2.6.0 and fix float points");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.69");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.12");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
