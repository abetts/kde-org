<?php
    include_once ("functions.inc");
    $translation_file = "www";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.57.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.57.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
April 13, 2019. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.57.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Attica");?></h3>

<ul>
<li><?php i18n("Accept any HTTP status between 100 and 199 as benign");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("[DocumentIdDB] Silence non-error debug message, warn on errors");?></li>
<li><?php i18n("[baloosearch] Allow specifying a time when using e.g. mtime");?></li>
<li><?php i18n("[indexcleaner] Avoid removing included folders below excluded ones");?></li>
<li><?php i18n("[MTimeDB] Fix lookup for the LessEqual range");?></li>
<li><?php i18n("[MTimeDB] Fix lookup when time range should return empty set");?></li>
<li><?php i18n("Correct asserts/error handling in MTimeDB");?></li>
<li><?php i18n("Protect against invalid parents in the IdTreeDB");?></li>
<li><?php i18n("Remove document from MTimeDB/DocumentTimeDB even when timestamp is 0");?></li>
<li><?php i18n("Be more precise with mimetype detection (bug 403902)");?></li>
<li><?php i18n("[timeline] Canonicalize Url");?></li>
<li><?php i18n("[timeline] Fix missing/misplaced SlaveBase::finished() calls");?></li>
<li><?php i18n("[balooshow] Several extensions basic file information output");?></li>
<li><?php i18n("[timeline] Fix warning, add missing UDS entry for \".\"");?></li>
<li><?php i18n("[balooctl] Reduce nesting level for addOption arguments, cleanup");?></li>
<li><?php i18n("React to config updates inside indexer (bug 373430)");?></li>
<li><?php i18n("Fix regression when opening DB in read-write mode (bug 405317)");?></li>
<li><?php i18n("[balooctl] Cleanup trailing whitespace");?></li>
<li><?php i18n("[engine] Unbreak code, revert renaming of Transaction::abort()");?></li>
<li><?php i18n("Harmonize handling of underscore in query parser");?></li>
<li><?php i18n("Baloo engine: treat every non-success code as a failure (bug 403720)");?></li>
</ul>

<h3><?php i18n("BluezQt");?></h3>

<ul>
<li><?php i18n("Move Media interface into Adapter");?></li>
<li><?php i18n("Manager: Don't require Media1 interface for initialization (bug 405478)");?></li>
<li><?php i18n("Device: Check object path in interfaces removed slot (bug 403289)");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add \"notifications\" and \"notifications-disabled\" icons (bug 406121)");?></li>
<li><?php i18n("make start-here-kde also available start-here-kde-plasma");?></li>
<li><?php i18n("Sublime Merge Icon");?></li>
<li><?php i18n("Give applications-games and input-gaming more contrast with Breeze Dark");?></li>
<li><?php i18n("Make 24px go-up actually 24px");?></li>
<li><?php i18n("Add preferences-desktop-theme-applications and preferences-desktop-theme-windowdecorations icons");?></li>
<li><?php i18n("Add symlinks from \"preferences-desktop-theme\" to \"preferences-desktop-theme-applications\"");?></li>
<li><?php i18n("Remove preferences-desktop-theme in preparation to making it a symlink");?></li>
<li><?php i18n("Add collapse/expand-all, window-shade/unshade (bug 404344)");?></li>
<li><?php i18n("Improve consistency of window-* and add more");?></li>
<li><?php i18n("Make go-bottom/first/last/top look more like media-skip*");?></li>
<li><?php i18n("Change go-up/down-search symlinks target to go-up/down");?></li>
<li><?php i18n("Improve pixel grid alignment of go-up/down/next/previous/jump");?></li>
<li><?php i18n("Change media-skip* and media-seek* style");?></li>
<li><?php i18n("Enforce new muted icon style in all action icons");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Re-enable the setting of QT_PLUGIN_PATH");?></li>
<li><?php i18n("ecm_add_wayland_client_protocol: Improve error messages");?></li>
<li><?php i18n("ECMGeneratePkgConfigFile: make all vars dependent on ${prefix}");?></li>
<li><?php i18n("Add UDev find module");?></li>
<li><?php i18n("ECMGeneratePkgConfigFile: add variables used by pkg_check_modules");?></li>
<li><?php i18n("Restore FindFontconfig backward compatibility for plasma-desktop");?></li>
<li><?php i18n("Add Fontconfig find module");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("use more appropriate plasma-specific icon for plasma category");?></li>
<li><?php i18n("use plasma icon as icon for plasma notification category");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Update URLs to use https");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Fix crash in KArchive::findOrCreate with broken files");?></li>
<li><?php i18n("Fix uninitialized memory read in KZip");?></li>
<li><?php i18n("Add Q_OBJECT to KFilterDev");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("[KCModuleLoader] Pass args to created KQuickAddons::ConfigModule");?></li>
<li><?php i18n("Pass focus to child searchbar when KPluginSelector is focused (bug 399516)");?></li>
<li><?php i18n("Improve the KCM error message");?></li>
<li><?php i18n("Add runtime guard that pages are KCMs in KCMultiDialog (bug 405440)");?></li>
</ul>

<h3><?php i18n("KCompletion");?></h3>

<ul>
<li><?php i18n("Don't set a null completer on a non-editable combobox");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Add Notify capability to revertToDefault");?></li>
<li><?php i18n("point readme to the wiki page");?></li>
<li><?php i18n("kconfig_compiler: new kcfgc args HeaderExtension &amp; SourceExtension");?></li>
<li><?php i18n("[kconf_update] move from custom logging tech to qCDebug");?></li>
<li><?php i18n("Remove reference from const KConfigIniBackend::BufferFragment &amp;");?></li>
<li><?php i18n("KCONFIG_ADD_KCFG_FILES macro: ensure a change of File= in kcfg is picked up");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Fix \"* foo *\" we don't want to bold this string");?></li>
<li><?php i18n("Fix Bug 401996 - clicking contact web url =&gt; uncomplete url is selected (bug 401996)");?></li>
<li><?php i18n("Print strerror when inotify fails (typical reason: \"too many open files\")");?></li>
</ul>

<h3><?php i18n("KDBusAddons");?></h3>

<ul>
<li><?php i18n("Convert two old-style connects to new-style");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("[GridViewKCM] Fix implicit width calculation");?></li>
<li><?php i18n("move the gridview in a separate file");?></li>
<li><?php i18n("Avoid fractionals in GridDelegate sizes and alignments");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Remove find modules provided by ECM");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Update Ukrainian translation");?></li>
<li><?php i18n("Catalan updates");?></li>
<li><?php i18n("it entities: update URLs to use https");?></li>
<li><?php i18n("Update URLs to use https");?></li>
<li><?php i18n("Use Indonesian translation");?></li>
<li><?php i18n("Update design to look more similar to kde.org");?></li>
<li><?php i18n("Add necessary files to use native Indonesian language for all Indonesian docs");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Implement support for writing rating information for taglib writer");?></li>
<li><?php i18n("Implement more tags for taglib writer");?></li>
<li><?php i18n("Rewrite taglib writer to use property interface");?></li>
<li><?php i18n("Test ffmpeg extractor using mime type helper (bug 399650)");?></li>
<li><?php i18n("Propose Stefan Bruns as KFileMetaData maintainer");?></li>
<li><?php i18n("Declare PropertyInfo as QMetaType");?></li>
<li><?php i18n("Safeguard against invalid files");?></li>
<li><?php i18n("[TagLibExtractor] Use the correct mimetype in case of inheritance");?></li>
<li><?php i18n("Add a helper to determine actual supported parent mime type");?></li>
<li><?php i18n("[taglibextractor] Test extraction of properties with multiple values");?></li>
<li><?php i18n("Generate header for new MimeUtils");?></li>
<li><?php i18n("Use Qt function for string list formatting");?></li>
<li><?php i18n("Fix number localization for properties");?></li>
<li><?php i18n("Verify mimetypes for all existing sample files, add some more");?></li>
<li><?php i18n("Add helper function to determine mime type based on content and extension (bug 403902)");?></li>
<li><?php i18n("Add support for extracting data from ogg and ts files (bug 399650)");?></li>
<li><?php i18n("[ffmpegextractor] Add Matroska Video test case (bug 403902)");?></li>
<li><?php i18n("Rewrite the taglib extractor to use the generic PropertyMap interface (bug 403902)");?></li>
<li><?php i18n("[ExtractorCollection] Load extractor plugins lazily");?></li>
<li><?php i18n("Fix extraction of aspect ratio property");?></li>
<li><?php i18n("Increase precision of frame rate property");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Sort the polish holidays categories");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Report human-readable error if Qt5Widgets is required but is not found");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Fix padding icon that doesn't exactly match the requested size (bug 396990)");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("ora:kra: qstrcmp -&gt; memcmp");?></li>
<li><?php i18n("Fix RGBHandler::canRead");?></li>
<li><?php i18n("xcf: Don't crash with files with unsupported layer modes");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Replace currentDateTimeUtc().toTime_t() with currentSecsSinceEpoch()");?></li>
<li><?php i18n("Replace QDateTime::to_Time_t/from_Time_t with to/fromSecsSinceEpoch");?></li>
<li><?php i18n("Improve executable dialog buttons' icons (bug 406090)");?></li>
<li><?php i18n("[KDirOperator] Show Detailed Tree View by default");?></li>
<li><?php i18n("KFileItem: call stat() on demand, add SkipMimeTypeDetermination option");?></li>
<li><?php i18n("KIOExec: fix error when the remote URL has no filename");?></li>
<li><?php i18n("KFileWidget In saving single file mode an enter/return press on the KDirOperator triggers slotOk (bug 385189)");?></li>
<li><?php i18n("[KDynamicJobTracker] Use generated DBus interface");?></li>
<li><?php i18n("[KFileWidget] When saving, highlight filename after clicking existing file also when using double-click");?></li>
<li><?php i18n("Don't create thumbnails for encrypted Vaults (bug 404750)");?></li>
<li><?php i18n("Fix WebDAV directory renaming if KeepAlive is off");?></li>
<li><?php i18n("Show list of tags in PlacesView (bug 182367)");?></li>
<li><?php i18n("Delete/Trash confirmation dialogue: Fix misleading title");?></li>
<li><?php i18n("Display the correct file/path in \"too bit for fat32\" error message (bug 405360)");?></li>
<li><?php i18n("Phrase error message with GiB, not GB (bug 405445)");?></li>
<li><?php i18n("openwithdialog: use recursive flag in proxy filter");?></li>
<li><?php i18n("Remove URLs being fetched when listing job is completed (bug 383534)");?></li>
<li><?php i18n("[CopyJob] Treat URL as dirty when renaming file as conflict resolution");?></li>
<li><?php i18n("Pass local file path to KFileSystemType::fileSystemType()");?></li>
<li><?php i18n("Fix upper/lower case rename on case insensitive fs");?></li>
<li><?php i18n("Fix \"Invalid URL: QUrl(\"some.txt\")\" warnings in Save dialog (bug 373119)");?></li>
<li><?php i18n("Fix crash while moving files");?></li>
<li><?php i18n("Fix NTFS hidden check for symlinks to NTFS mountpoints (bug 402738)");?></li>
<li><?php i18n("Make file overwrite a bit safer (bug 125102)");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("fix listItems implicitWidth");?></li>
<li><?php i18n("shannon entropy to guess monochrome icon");?></li>
<li><?php i18n("Prevent context drawer from disappearing");?></li>
<li><?php i18n("remove actionmenuitembase");?></li>
<li><?php i18n("don't try to get the version on static builds");?></li>
<li><?php i18n("[Mnemonic Handling] Replace only first occurrence");?></li>
<li><?php i18n("sync when any model property updates");?></li>
<li><?php i18n("use icon.name in back/forward");?></li>
<li><?php i18n("fix toolbars for layers");?></li>
<li><?php i18n("Fix errors in kirigami example files");?></li>
<li><?php i18n("Add a SearchField and PasswordField component");?></li>
<li><?php i18n("fix handle icons (bug 404714)");?></li>
<li><?php i18n("[InlineMessage] Do not draw shadows around the message");?></li>
<li><?php i18n("immediately layout on order changed");?></li>
<li><?php i18n("fix breadcrumb layout");?></li>
<li><?php i18n("never show toolbar when the current item asks not to");?></li>
<li><?php i18n("manage back/forward in the filter as well");?></li>
<li><?php i18n("support back/forward mouse buttons");?></li>
<li><?php i18n("Add lazy instantiation for submenus");?></li>
<li><?php i18n("fix toolbars for layers");?></li>
<li><?php i18n("kirigami_package_breeze_icons: Search among size 16 icons as well");?></li>
<li><?php i18n("Fix Qmake based build");?></li>
<li><?php i18n("get the attached property of the proper item");?></li>
<li><?php i18n("fix logic when to show the toolbar");?></li>
<li><?php i18n("possible to disable toolbar for layer's pages");?></li>
<li><?php i18n("always show global toolbar on global modes");?></li>
<li><?php i18n("signal Page.contextualActionsAboutToShow");?></li>
<li><?php i18n("a bit of space to the right of the title");?></li>
<li><?php i18n("relayout when visibility changes");?></li>
<li><?php i18n("ActionTextField: Properly place actions");?></li>
<li><?php i18n("topPadding and BottomPadding");?></li>
<li><?php i18n("text on images always need to be white (bug 394960)");?></li>
<li><?php i18n("clip overlaysheet (bug 402280)");?></li>
<li><?php i18n("avoid parenting OverlaySheet to ColumnView");?></li>
<li><?php i18n("use a qpointer for the theme instance (bug 404505)");?></li>
<li><?php i18n("hide breadcrumb on pages that don't want a toolbar (bug 404481)");?></li>
<li><?php i18n("don't try to override the enabled property (bug 404114)");?></li>
<li><?php i18n("Possibility for custom header and footer in ContextDrawer (bug 404978)");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("[KUiServerJobTracker] Update destUrl before finishing the job");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Switch URLs to https");?></li>
<li><?php i18n("Update link to fsearch project");?></li>
<li><?php i18n("Handle unsupported OCS commands, and don't over-vote (bug 391111)");?></li>
<li><?php i18n("New location for KNSRC files");?></li>
<li><?php i18n("[knewstuff] Remove qt5.13 deprecated method");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("[KStatusNotifierItem] Send desktop-entry hint");?></li>
<li><?php i18n("Allow to set custom hints for notifications");?></li>
</ul>

<h3><?php i18n("KNotifyConfig");?></h3>

<ul>
<li><?php i18n("Allow selecting only supported audio files (bug 405470)");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Fix finding the host tools targets file in the Android docker environment");?></li>
<li><?php i18n("Add cross-compilation support for kpackagetool5");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Add X-GNOME-UsesNotifications as recognized key");?></li>
<li><?php i18n("Add bison minimum version of 2.4.1 due to %code");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Fix: apply correctly the text colors of the chosen scheme (bug 398758)");?></li>
<li><?php i18n("DocumentPrivate: Add option \"Auto Reload Document\" to View menu (bug 384384)");?></li>
<li><?php i18n("DocumentPrivate: Support to set dictionary on block selection");?></li>
<li><?php i18n("Fix Words &amp; Chars String on katestatusbar");?></li>
<li><?php i18n("Fix Minimap with QtCurve style");?></li>
<li><?php i18n("KateStatusBar: Show lock icon on modified label when in read-only mode");?></li>
<li><?php i18n("DocumentPrivate: Skip auto quotes when these looks already balanced (bug 382960)");?></li>
<li><?php i18n("Add Variable interface to KTextEditor::Editor");?></li>
<li><?php i18n("relax code to only assert in debug build, work in release build");?></li>
<li><?php i18n("ensure compatibility with old configs");?></li>
<li><?php i18n("more use of generic config interface");?></li>
<li><?php i18n("simplify QString KateDocumentConfig::eolString()");?></li>
<li><?php i18n("transfer sonnet setting to KTextEditor setting");?></li>
<li><?php i18n("ensure now gaps in config keys");?></li>
<li><?php i18n("convert more things to generic config interface");?></li>
<li><?php i18n("more use of the generic config interface");?></li>
<li><?php i18n("generic config interface");?></li>
<li><?php i18n("Don't crash on malformed syntax highlighting files");?></li>
<li><?php i18n("IconBorder: Accept drag&amp;drop events (bug 405280)");?></li>
<li><?php i18n("ViewPrivate: Make deselection by arrow keys more handy (bug 296500)");?></li>
<li><?php i18n("Fix for showing argument hint tree on non-primary screen");?></li>
<li><?php i18n("Port some deprecated method");?></li>
<li><?php i18n("Restore the search wrapped message to its former type and position (bug 398731)");?></li>
<li><?php i18n("ViewPrivate: Make 'Apply Word Wrap' more comfortable (bug 381985)");?></li>
<li><?php i18n("ModeBase::goToPos: Ensure jump target is valid (bug 377200)");?></li>
<li><?php i18n("ViInputMode: Remove unsupported text attributes from the status bar");?></li>
<li><?php i18n("KateStatusBar: Add dictionary button");?></li>
<li><?php i18n("add example for line height issue");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Make KFontRequester consistent");?></li>
<li><?php i18n("Update kcharselect-data to Unicode 12.0");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Send blur/background contrast in device pixels (bug 404923)");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("WireGuard: make marshalling/demarshalling of secrets from map to work");?></li>
<li><?php i18n("Add missing support for WireGuard into base setting class");?></li>
<li><?php i18n("Wireguard: handle private key as secrets");?></li>
<li><?php i18n("Wireguard: peers property should be NMVariantMapList");?></li>
<li><?php i18n("Add Wireguard connection type support");?></li>
<li><?php i18n("ActiveConnecton: add stateChangedReason signal where we can see the reason of state change");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[AppletInterface] Check for corona before accessing it");?></li>
<li><?php i18n("[Dialog] Don't forward hover event when there is nowhere to forward it to");?></li>
<li><?php i18n("[Menu] Fix triggered signal");?></li>
<li><?php i18n("Reduce the importance of some debug information so actual warnings can be seen");?></li>
<li><?php i18n("[PlasmaComponents3 ComboBox] Fix textColor");?></li>
<li><?php i18n("FrameSvgItem: catch margin changes of FrameSvg also outside own methods");?></li>
<li><?php i18n("Add Theme::blurBehindEnabled()");?></li>
<li><?php i18n("FrameSvgItem: fix textureRect for tiled subitems to not shrink to 0");?></li>
<li><?php i18n("Fix breeze dialog background with Qt 5.12.2 (bug 405548)");?></li>
<li><?php i18n("Remove crash in plasmashell");?></li>
<li><?php i18n("[Icon Item] Also clear image icon when using Plasma Svg (bug 405298)");?></li>
<li><?php i18n("textfield height based only on clear text (bug 399155)");?></li>
<li><?php i18n("bind alternateBackgroundColor");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Add KDE Connect SMS plugin");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("the plasma desktop style supports icon coloring");?></li>
<li><?php i18n("[SpinBox] Improve mouse wheel behavior");?></li>
<li><?php i18n("add a bit of padding in ToolBars");?></li>
<li><?php i18n("fix RoundButton icons");?></li>
<li><?php i18n("scrollbar based padding on all delegates");?></li>
<li><?php i18n("look for a scrollview to take its scrollbar for margins");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Allow building without UDev on Linux");?></li>
<li><?php i18n("Only get clearTextPath when used");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Add syntax definition for Elm language to syntax-highlighting");?></li>
<li><?php i18n("AppArmor &amp; SELinux: remove one indentation in XML files");?></li>
<li><?php i18n("Doxygen: don't use black color in tags");?></li>
<li><?php i18n("Allow line end context switches in empty lines (bug 405903)");?></li>
<li><?php i18n("Fix endRegion folding in rules with beginRegion+endRegion (use length=0) (bug 405585)");?></li>
<li><?php i18n("Add extensions to groovy highlighting (bug 403072)");?></li>
<li><?php i18n("Add Smali syntax highlighting file");?></li>
<li><?php i18n("Add \".\" as weakDeliminator in Octave syntax file");?></li>
<li><?php i18n("Logcat: fix dsError color with underline=\"0\"");?></li>
<li><?php i18n("fix highlighter crash for broken hl file");?></li>
<li><?php i18n("guard target link libraries for older CMake version (bug 404835)");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.57");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.10");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
