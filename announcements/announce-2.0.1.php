<?php
  $page_title = "KDE 2.0.1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>
<P>DATELINE DECEMBER 5, 2000</P>
<P>FOR IMMEDIATE RELEASE</P>
<H3 ALIGN="center">New KDE Release for Linux Desktop</H3>
<P><STRONG>New Version of Leading Desktop for Linux and
Other UNIXes Ships</STRONG></P>
<P>December 5, 2000 (The INTERNET).  The <A href="/">KDE
Team</A> today announced the release of KDE 2.0.1, a powerful, modular,
Internet-enabled desktop.  KDE 2.0.1 is a translation and bug-fix release
and follows six weeks after the release of KDE 2.0, which constitutes the
next generation of the
<A HREF="../awards">award-winning</A> KDE 1
series. KDE is the work product of hundreds of dedicated developers
originating from over 30 countries.
</P>
<P>
The primary goals of the 2.0.1 release are to improve documentation
and provide additional language translations for the user interface.  As
a result of the dedicated efforts of hundreds of translators, KDE 2 is now
available in 33 languages and dialects, including the addition of Japanese in
this release.  Code development is currently focused
on the branch that will lead to KDE 2.1, scheduled for release in first quarter
2001, so only a few significant code fixes and improvements are included in this
release.  A
<A HREF="./changelogs/changelog2_0to2_0_1.php">list of
these changes</A> and a <A HREF="../info/2.0.1.php">FAQ about
the release</A> are available at the KDE
<A href="/">website</A>.
</P>
<P>
KDE 2.0.1 includes the core KDE libraries, the core desktop environment,
the KOffice suite, as well as the over 100 applications from the other
standard base KDE packages: Administration, Games, Graphics, Multimedia,
Network, Personal Information Management (PIM), Toys and Utilities.
</P>
<P>
All of KDE 2.0.1 is available for free under an Open Source license. 
Likewise,
<A HREF="http://www.trolltech.com/">Trolltech's</A> Qt 2.2.2, the GUI
toolkit on which KDE is based,
is also available for free under two Open Source licenses:  the
<A HREF="http://www.trolltech.com/products/download/freelicense/license.html">Q
Public License</A> and the <A HREF="http://www.gnu.org/copyleft/gpl.html">GNU
General Public License</A>.
</P>
<P>
More information about KDE 2 is available on the
<A HREF="../info/2.0.1.php">KDE 2.0.1 Info Page</A>, an
evolving FAQ about the release, in a
<A HREF="http://devel-home.kde.org/~granroth/LWE2000/index.html">slideshow
presentation</A> and on
<A href="/">KDE's web site</A>, including a number of
<A HREF="http://www.kde.org/screenshots/kde2shots.html">screenshots</A>, <A HREF="http://developer.kde.org/documentation/kde2arch.html">developer information</A> and
a developer's
<A HREF="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html?rev=2.3">KDE 1 - KDE 2 porting guide</A>.
</P>
<P>
<H4>Downloading and Compiling KDE</H4>
</P>
<P>
The source packages for KDE 2.0.1 are available for free download at
<A HREF="http://ftp.kde.org/stable/2.0.1/distribution/tar/generic/src/">http://ftp.kde.org/stable/2.0.1/distribution/tar/generic/src/</A> or in the
equivalent directory at one of the many KDE ftp server
<A HREF="../mirrors/ftp.php">mirrors</A>.  KDE 2.0.1 requires
qt-2.2.1, which is available from the above locations under the name
<A HREF="http://ftp.kde.org/stable/2.0/distribution/tar/generic/src/qt-x11-2.2.1.tar.gz">qt-x11-2.2.1.tar.gz</A>,
although
<A HREF="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.2.2.tar.gz">qt-2.2.2</A>
is recommended.  KDE 2.0.1 will not work with versions of Qt older than 2.2.1.
</P>
<P>
For further instructions on compiling and installing KDE, please consult
the <A HREF="http://developer.kde.org/build/index.html">installation
instructions</A> and, if you encounter problems, the
<A HREF="http://developer.kde.org/build/index.html">compilation FAQ</A>.
</P>
<P>
<H4>Installing Binary Packages</H4>
</P>
<P>
Some distributors choose to provide binary packages of KDE for certain
versions of their distribution.  Some of these binary packages for KDE 2.0.1
will be available for free download under
<A HREF="http://ftp.kde.org/stable/2.0.1/distribution/">http://ftp.kde.org/stable/2.0.1/distribution/</A>
or under the equivalent directory at one of the many KDE ftp server
<A HREF="../mirrors/ftp.php">mirrors</A>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution.
</P>
<P>KDE 2.0.1 requires qt-2.2.1, the free version of which is available
from the above locations usually under the name qt-x11-2.2.1, although
qt-2.2.2 is recommended.  KDE 2.0.1 will not work with versions of Qt
older than 2.2.1.
<P>
At the time of this release, pre-compiled packages are available for:
</P>
<UL>
<LI><A HREF="http://kde.tdyc.com/debian/dists/potato/">Debian GNU/Linux 2.2 (potato)</A></LI>
<!--
<LI><A HREF="http://ftp.kde.org/stable/2.0.1/distribution/rpm/ppc-glibc21/">LinuxPPC (glibc 2.1)</A></LI>
-->
<LI><A HREF="http://ftp.kde.org/stable/2.0.1/distribution/rpm/Mandrake/7.2/">Mandrake 7.2</A></LI>
<LI><A HREF="http://ftp.kde.org/stable/2.0.1/distribution/rpm/Caldera/">Caldera OpenLinux 2.4</A></LI>
<LI><A HREF="http://ftp.kde.org/stable/2.0.1/distribution/rpm/RedHat/7.0/i386/">RedHat Linux 7.0 (i386)</A>, <A HREF="http://ftp.kde.org/stable/2.0.1/distribution/rpm/RedHat/7.0/alpha/">RedHat Linux 7.0 (Alpha)</A>, <A HREF="http://ftp.kde.org/stable/2.0.1/distribution/rpm/RedHat/7.0/sparc/">RedHat Linux 7.0 (Sparc)</A>, <A HREF="http://ftp.kde.org/stable/2.0.1/distribution/rpm/RedHat/6.x/i386/">RedHat Linux 6.x (i386)</A>, <A HREF="http://ftp.kde.org/stable/2.0.1/distribution/rpm/RedHat/6.x/alpha/">RedHat Linux 6.x (Alpha)</A> and <A HREF="http://ftp.kde.org/stable/2.0.1/distribution/rpm/RedHat/6.x/sparc/">RedHat Linux 6.x (Sparc)</A></LI>
<LI><A HREF="http://ftp.kde.org/stable/2.0.1/distribution/rpm/SuSE/7.0-i386/">SuSE Linux 7.0 (i386)</A>, <A HREF="http://ftp.kde.org/stable/2.0.1/distribution/rpm/SuSE/7.0-sparc/">SuSE Linux 7.0 (Sparc)</A> and <A HREF="http://ftp.kde.org/stable/2.0.1/distribution/rpm/SuSE/6.4-i386/">SuSE Linux 6.4 (i386)</A></LI>
<LI><A HREF="http://ftp.kde.org/stable/2.0.1/distribution/tar/tru64/">Tru64 Systems</A></LI>
</UL>
<P>
Please check the servers periodically for pre-compiled packages for other
distributions.  More binary packages will become available over the
coming days and weeks.
</P>
<P>
<H4>About KDE</H4>
</P>
<P>
KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.
</P>
<P>
For more information about KDE, please visit KDE's
<A HREF="../whatiskde/">web site</A>.
</P>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<FONT SIZE=2>
<EM>Trademarks Notices.</EM>
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
Trolltech and Qt are trademarks of Trolltech AS.
MS Windows, Internet Explorer and Windows Explorer are trademarks or registered trademarks of Microsoft Corporation.
Netscape and Netscape Communicator are trademarks or registered trademarks of Netscape Communications Corporation in the United States and other countries and JavaScript is a trademark of Netscape Communications Corporation.
Java is a trademark of Sun Microsystems, Inc.
Flash is a trademark or registered trademark of Macromedia, Inc. in the United States and/or other countries.
RealAudio and RealVideo are trademarks or registered trademarks of RealNetworks, Inc.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.
<BR>
<HR NOSHADE SIZE=1 WIDTH="90%" ALIGN="center">
<TABLE BORDER=0 CELLPADDING=8 CELLSPACING=0>
<TR><TH COLSPAN=2 ALIGN="left">
Press Contacts:
</TH></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
United&nbsp;States:
</TD><TD NOWRAP>
Kurt Granroth<BR>
g&#0114;&#97;&#110;&#114;o&#0116;&#00104;&#064;&#107;de&#x2e;&#x6f;rg<BR>
(1) 480 732 1752<BR>&nbsp;<BR>
Andreas Pour<BR>
p&#x6f;ur&#x40;&#x6b;d&#101;&#x2e;&#111;&#0114;&#103;<BR>
(1) 718 456 1165
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (French and English):
</TD><TD NOWRAP>
David Faure<BR>
&#102;au&#x72;&#x65;&#0064;&#00107;&#0100;e.org<BR>
(44) 1225 837409
</TD></TR>
<TR VALIGN="top"><TD ALIGN="right" NOWRAP>
Europe (English and German):
</TD><TD NOWRAP>
Martin Konold<BR>
&#107;&#111;n&#0111;ld&#00064;kde&#046;o&#00114;g<BR>
(49) 179 2252249
</TD></TR>
</TABLE>
<?php
  include "footer.inc"
?>
