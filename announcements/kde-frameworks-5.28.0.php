<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.28.0");
  $site_root = "../";
  $release = '5.28.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
November 15, 2016. KDE today announces the release
of KDE Frameworks 5.28.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("New framework: syntax-highlighting");?></h3>

<?php i18n("Syntax highlighting engine for Kate syntax definitions");?>

<?php i18n("This is a stand-alone implementation of the Kate syntax highlighting engine. It's meant as a building block for text editors as well as for simple highlighted text rendering (e.g. as HTML), supporting both integration with a custom editor as well as a ready-to-use QSyntaxHighlighter sub-class.");?>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("update kstars action icons (bug 364981)");?></li>
<li><?php i18n("Breeze Dark is listed as Breeze in System Settings wrong .themes file (bug 370213)");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Make KDECMakeSettings work with KDE_INSTALL_DIRS_NO_DEPRECATED");?></li>
<li><?php i18n("Don't require the python bindings dependencies for ECM");?></li>
<li><?php i18n("Add the PythonModuleGeneration module");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Ignoring link status when sorting UsedResources and LinkedResources model");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("[CSS] reverse changes done by doxygen 1.8.12");?></li>
<li><?php i18n("Add doxygenlayout file");?></li>
<li><?php i18n("Update way of defining group names");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Make sure we can do more than one request");?></li>
<li><?php i18n("Make sure we get to know about progress by reading the program output");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Make sure we don't break compilation with past broken units");?></li>
<li><?php i18n("Don't be fatal on File field not being properly parsed");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Display bad url");?></li>
<li><?php i18n("Load user avatars from AccountsServicePath if it exists (bug 370362)");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("[QtQuickRendererSettings] Fix default to be empty instead of \"false\"");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Make the France flag actually use all the pixmap");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Fix 'checkXML5 generates html files in workdir for valid docbooks' (bug 371987)");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("Support non integer scale factors in kiconengine (bug 366451)");?></li>
</ul>

<h3><?php i18n("KIdleTime");?></h3>

<ul>
<li><?php i18n("Disabled spamming the console output with 'waiting for' messages");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("imageformats/kra.h - overrides for KraPlugin capabilities() and create()");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Fix HTTP date format sent by kio_http to always use the C locale (bug 372005)");?></li>
<li><?php i18n("KACL: fix memory leaks detected by ASAN");?></li>
<li><?php i18n("Fix memory leaks in KIO::Scheduler, detected by ASAN");?></li>
<li><?php i18n("Removed duplicate clear button (bug 369377)");?></li>
<li><?php i18n("Fix editing autostart entries when /usr/local/share/applications doesn't exist (bug 371194)");?></li>
<li><?php i18n("[KOpenWithDialog] Hide TreeView header");?></li>
<li><?php i18n("Sanitize the symlink name buffer size (bug 369275)");?></li>
<li><?php i18n("Properly finish DropJobs when triggered is not emitted (bug 363936)");?></li>
<li><?php i18n("ClipboardUpdater: fix another crash on Wayland (bug 359883)");?></li>
<li><?php i18n("ClipboardUpdater: fix crash on Wayland (bug 370520)");?></li>
<li><?php i18n("Support non integer scale factors in KFileDelegate (bug 366451)");?></li>
<li><?php i18n("kntlm: Distinguish between NULL and empty domain");?></li>
<li><?php i18n("Don't show overwrite dialog if file name is empty");?></li>
<li><?php i18n("kioexec: use friendly filenames");?></li>
<li><?php i18n("Fix focus ownership if url is changed before showing the widget");?></li>
<li><?php i18n("Major performance improvement when turning previews off in the file dialog (bug 346403)");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("Add python bindings");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Export FunctionObjectImp, used by khtml's debugger");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Separate sort roles and filters");?></li>
<li><?php i18n("Make it possible to query installed entries");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Don't deref an object we haven't referenced when notification has no action");?></li>
<li><?php i18n("KNotification will no longer crash when using it in a QGuiApplication and no notification service is running (bug 370667)");?></li>
<li><?php i18n("Fix crashes in NotifyByAudio");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Make sure we're looking both for json and desktop metadata");?></li>
<li><?php i18n("Guard against Q_GLOBAL_STATIC being destroyed at app shutdown");?></li>
<li><?php i18n("Fix dangling pointer in KPackageJob (bug 369935)");?></li>
<li><?php i18n("Remove discovery associated to a key when removing a definition");?></li>
<li><?php i18n("Generate the icon into the appstream file");?></li>
</ul>

<h3><?php i18n("KPty");?></h3>

<ul>
<li><?php i18n("Use ulog-helper on FreeBSD instead of utempter");?></li>
<li><?php i18n("search harder for utempter using basic cmake prefix as well");?></li>
<li><?php i18n("workaround find_program ( utempter ...) failure(s)");?></li>
<li><?php i18n("use ECM path to find utempter binary, more reliable than simple cmake prefix");?></li>
</ul>

<h3><?php i18n("KRunner");?></h3>

<ul>
<li><?php i18n("i18n: handle strings in kdevtemplate files");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Breeze Dark: Darken current-line background color for better readability (bug 371042)");?></li>
<li><?php i18n("Sorted Dockerfile instructions");?></li>
<li><?php i18n("Breeze (Dark): Make comments a bit lighter for better readability (bug 371042)");?></li>
<li><?php i18n("Fix CStyle and C++/boost indenters when automatic brackets enabled (bug 370715)");?></li>
<li><?php i18n("Add modeline 'auto-brackets'");?></li>
<li><?php i18n("Fix inserting text after end of file (rare case)");?></li>
<li><?php i18n("Fix invalid xml highlighting files");?></li>
<li><?php i18n("Maxima: Remove hard-coded colors, fix itemData Label");?></li>
<li><?php i18n("Add OBJ, PLY and STL syntax definitions");?></li>
<li><?php i18n("Add syntax highlighting support for Praat");?></li>
</ul>

<h3><?php i18n("KUnitConversion");?></h3>

<ul>
<li><?php i18n("New Thermal and Electrical Units and Unit Convenience Function");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("If Gpgmepp is not found, try to use KF5Gpgmepp");?></li>
<li><?php i18n("Use Gpgmepp from GpgME-1.7.0");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Improved relocatability of CMake export");?></li>
<li><?php i18n("[tools] Fix generation of wayland_pointer_p.h");?></li>
<li><?php i18n("[tools] Generate eventQueue methods only for global classes");?></li>
<li><?php i18n("[server] Fix crash on updating focused keyboard surface");?></li>
<li><?php i18n("[server] Fix possible crash on creation of DataDevice");?></li>
<li><?php i18n("[server] Ensure we have a DataSource on the DataDevice in setSelection");?></li>
<li><?php i18n("[tools/generator] Improve resource destruction on server side");?></li>
<li><?php i18n("Add request to have focus in a PlasmaShellSurface of Role Panel");?></li>
<li><?php i18n("Add auto-hiding panel support to PlasmaShellSurface interface");?></li>
<li><?php i18n("Support passing generic QIcon through PlasmaWindow interface");?></li>
<li><?php i18n("[server] Implement the generic window property in QtSurfaceExtension");?></li>
<li><?php i18n("[client] Add methods to get ShellSurface from a QWindow");?></li>
<li><?php i18n("[server] Send pointer events to all wl_pointer resources of a client");?></li>
<li><?php i18n("[server] Don't call wl_data_source_send_send if DataSource is unbound");?></li>
<li><?php i18n("[server] Use deleteLater when a ClientConnection gets destroyed (bug 370232)");?></li>
<li><?php i18n("Implement support for the relative pointer protocol");?></li>
<li><?php i18n("[server] Cancel previous selection from SeatInterface::setSelection");?></li>
<li><?php i18n("[server] Send key events to all wl_keyboard resources of a client");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("move kcharselect-generate-datafile.py to src subdir");?></li>
<li><?php i18n("Import kcharselect-generate-datafile.py script with history");?></li>
<li><?php i18n("Remove outdated section");?></li>
<li><?php i18n("Add Unicode copyright and permission notice");?></li>
<li><?php i18n("Fix warning: Missing override");?></li>
<li><?php i18n("Add symbol SMP blocks");?></li>
<li><?php i18n("Fix \"See also\" references");?></li>
<li><?php i18n("Add missing Unicode blocks; improve ordering (bug 298010)");?></li>
<li><?php i18n("add character categories to the data file");?></li>
<li><?php i18n("update the Unicode categories in the data file generation script");?></li>
<li><?php i18n("adjust the data file generation file to be able to parse the unicode 5.2.0 data files");?></li>
<li><?php i18n("forward port fix for generating translations");?></li>
<li><?php i18n("let the script to generate the data file for kcharselect also write a translation dummy");?></li>
<li><?php i18n("Add the script to generate the data file for KCharSelect");?></li>
<li><?php i18n("new KCharSelect application (using kcharselect widget from kdelibs now)");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Improved relocatability of CMake export");?></li>
<li><?php i18n("Add support for desktopFileName to NETWinInfo");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Allow using new style connect in KActionCollection::add<a href=\"\">Action</a>");?></li>
</ul>

<h3><?php i18n("ModemManagerQt");?></h3>

<ul>
<li><?php i18n("Fix include dir in pri file");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("Fix include dir in pri file");?></li>
<li><?php i18n("Fix moc error due to Q_ENUMS being used in a namespace, with Qt branch 5.8");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("make sure OSD doesn't have Dialog flag (bug 370433)");?></li>
<li><?php i18n("set context properties before reloading the qml (bug 371763)");?></li>
<li><?php i18n("Don't reparse the metadata file if it's already loaded");?></li>
<li><?php i18n("Fix crash in qmlplugindump when no QApplication is available");?></li>
<li><?php i18n("Don't show \"Alternatives\" menu by default");?></li>
<li><?php i18n("New bool to use activated signal as toggle of expanded (bug 367685)");?></li>
<li><?php i18n("Fixes for building plasma-framework with Qt 5.5");?></li>
<li><?php i18n("[PluginLoader] Use operator&lt;&lt; for finalArgs instead of initializer list");?></li>
<li><?php i18n("use kwayland for shadows and dialog positioning");?></li>
<li><?php i18n("Remaining missing icons and network improvements");?></li>
<li><?php i18n("Move availableScreenRect/Region up to AppletInterface");?></li>
<li><?php i18n("Don't load containment actions for embedded containments (system trays)");?></li>
<li><?php i18n("Update applet alternatives menu entry visibility on demand");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Fix unstable ordering of query results yet again");?></li>
<li><?php i18n("Add a CMake option to switch between HAL and UDisks managers on FreeBSD");?></li>
<li><?php i18n("Make UDisks2 backend compile on FreeBSD (and, possibly, other UNIXes)");?></li>
<li><?php i18n("Windows: Don't display error dialogs (bug 371012)");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.28");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.5");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
