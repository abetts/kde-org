<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "Delovna okolja Plasma 4.9 – osnovne izboljšave";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Drugi jeziki:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE z veseljem najavlja takojšnjo razpoložljivost različice 4.9 delovnih okolij Plasma Desktop in Plasma Netbook. Nova različica je doživela izboljšave obstoječih zmožnosti in prinaša večje nove zmožnosti.
</p>
<h2>Upravljalnik datotek Dolphin</h2>
<p>
KDE-jev zmogljiv upravljalnik datotek Dolphin sedaj premore gumba za pomikanje nazaj in naprej ter preimenovanje imen datotek znotraj vrstice. Dolphin po novem lahko prikazuje tudi metapodatke kot so ocene, oznake, avtorja, dimenzije slik, velikosti datotek in podobno. Po teh metapodatkih je možno tudi razvrščanje in združevanje v skupine. Dodan je bil vstavek za sistem upravljanja z izvorno kodo Mercurial, ki se pridružuje že obstoječi podpori za git, Subversion in CVS. Uporabniški vmesnik programa je doživel nekaj manjših izboljšav, med katerimi so boljše podokno z mesti ter izboljšano iskanje in usklajevanje trenutnega mesta s podoknom terminala.
<div align="center" class="screenshot">
<a href="screenshots/kde49-dolphin_.png"><img src="screenshots/kde49-dolphin_thumb.png" /></a>
</div>
</p>
<h2>Posnemovalnik terminala Konsole</h2>
<p>
Iz programa Konsole lahko sedaj s pomočjo KDE-jevih spletnih bližnjic poiščete izbrano besedilo. Ko neko mapo povlečete in spustite v okno Konsole, je sedaj v priročnem meniju na voljo nova možnost »Spremeni mapo v«. Uporabniki imate po novem več nadzora pri organiziranju oken terminala, saj lahko <strong>zavihek z vleko odcepite</strong> od okna in tako ustvarite novo okno. Obstoječe zavihke je moč podvojiti, tako da uporabijo isti profil. Pri zagonu Konsole je mogoče nadzorovati vidnost menijske vrstice in vrstice z zavihki. Če vam gre pisanje skriptov dobro od rok, lahko z njimi spreminjate imena zavihkov.
<div align="center" class="screenshot">
<a href="screenshots/kde49-konsole1.png"><img src="screenshots/kde49-konsole1-cropped.png" /></a></div>
<div align="center" class="screenshot">
<a href="screenshots/kde49-konsole2.png"><img src="screenshots/kde49-konsole2-cropped.png" /></a></div>
</p>
<h2>Upravljalnik oken KWin</h2>
<p>
Veliko je bilo narejenega na KDE-jevem upravljalniku oken KWin. Med izboljšavami so tako majhne spremembe, kot sta dvigovanje oken med preklapljanjem in pomoč za nastavitve, ki so specifične za posamezno okno; kot tudi večje spremembe, na primer izboljšane nastavitve preklapljanja med okni in pohitritve grafičnih učinkov. KWin je po novem bolje povezan z Dejavnostmi, dodana je bila na primer možnost nastavljanja pravila za prikaz posameznih oken v izbranih dejavnostih. V splošnem je bilo veliko dela posvečenega izboljšanju kakovosti in pohitritvam.
<div align="center" class="screenshot">
<a href="screenshots/kde49-window-behaviour_settings.png"><img src="screenshots/kde49-window-behaviour_settings_thumb.png" /></a></div>
</p>
<h2>Dejavnosti</h2>
<p>
Dejavnosti so sedaj veliko temeljiteje integrirane v delovno okolje. V Dolphinu, Konquerorju in gradniku Prikaz map lahko sedaj datoteke povežete z izbranimi dejavnostmi. Prikaz map lahko tako na primer prikazuje le datoteke, ki so povezane s trenutno dejavnostjo. Nova je možnost šifriranja zasebnih dejavnosti.
<div align="center" class="screenshot">
<a href="screenshots/kde49-link-files-to-activities.png"><img src="screenshots/kde49-link-files-to-activities-cropped.png"/></a></div>
</p>
<p>
Dodana je bila podpora za protokol MPRIS2, ki omogoča oddaljeno upravljanje večpredstavnostnih predvajalnikov in prikaz podatkov o predvajani vsebini. Z mešalnikom KMix lahko zato sedaj nadzorujete posamezne zvočne tokove in iz različnih gradnikov za namizje, kakršen je »Sedaj se predvaja«, upravljate s predvajalniki. Podpora za MPRIS je bila dodana v predvajalnik zvoka JuK in predvajalnik videa Dragon.
</p>
<p>
V delovnih okoljih je tudi več manjših sprememb, na primer prenos več gradnikov in drugih delov okolij na novo tehnologijo Qt QML. Izboljšani minipredvajalnik za Plasmo vključuje pogovorno okno z lastnostmi skladbe in izboljšano filtriranje. Meni za zaganjanje programov Kickoff je sedaj moč uporabljati tudi s tipkovnico. Gradnik za upravljanje z omrežnimi povezavami je doživel izboljšave prikaza in uporabnosti. Veliko sprememb je doživel tudi gradnik za podatke o javnem prevozu.
</p>

<h4>Namestitev okolij Plasma</h4>
<?php
  include("boilerplate-sl.inc");
?>

<h2>Druge današnje najave:</h2>

<h2><a href="applications-sl.php"><img src="images/applications.png" class="app-icon" alt="The KDE Applications 4.9"/> Novi in izboljšani KDE-jevi programi 4.9</a></h2>
<p>
Med nove ali izboljšane KDE-jeve programe  spadajo, Okular, Kopete, Kontact, izobraževalni programi in igre. Za podrobnosti si oglejte <a href="applications-sl.php">najavo za KDE-jeve programe</a>.
</p>
<h2><a href="platform-sl.php"><img src="images/platform.png" class="app-icon" alt="The KDE Development Platform 4.9"/> KDE-jeva razvojna platforma 4.9</a></h2>
<p>
Današnja izdaja KDE-jeve razvojne platforme vsebuje popravke, druge izboljšave kakovosti in omrežnih zmožnosti ter priprave na ogrodje KDE Frameworks 5. Za podrobnosti si oglejte <a href="platform-sl.php">najavo za KDE-jevo razvojno platformo</a>.
</p>

<?php
  include("footer.inc");
?>
