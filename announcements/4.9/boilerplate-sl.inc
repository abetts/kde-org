<!-- header goes into the specific page -->
<p align="justify">
KDE-jeva programska oprema, ki vključuje vse knjižnice in programe, je prosto na voljo pod licencami za svobodno in odprtokodno prgramsko opremo. KDE-jeva programska oprema teče na raznih strojnih konfiguracijah in procesorskih arhitekturah kot so MIPS, ARM in x86. Deluje tudi na več operacijskih sistemih in s poljubnim upravljalnikom oken ali v poljubnem namiznem okolju. Poleg inačic za GNU/Linux in ostale operacijske sisteme temelječe na UNIX-u lahko večino programov dobite tudi za Windows (oglejte si stran projekta <a href="http://windows.kde.org/">KDE-jevih programov za Windows</a>) in Mac OS X (<a href="http://mac.kde.org/">KDE-jevi programi za Mac</a>). Na spletu je moč najti tudi razne preizkusne inačice KDE-jevih programov za mobilne platforme kot sta na primer MeeGo in Symbian. Te niso podprte. <a href="http://plasma-active.org/">Plasma Active</a> je uporabniška izkušnja za širši spekter naprav kot so tablični računalniki in druga mobilna strojna oprema.
<br />
KDE-jevo programsko opremo je v obliki izvorne kode in raznih binarnih formatih moč dobiti s strani <a
href="http://download.kde.org/stable/<?php echo $release_full;?>/">download.kde.org</a>, na <a href="http://www.kde.org/download/cdrom.php">zgoščenki</a> ali kot del <a href="http://www.kde.org/download/distributions.php">večjih distribucij sistema GNU/Linux ali sistemov UNIX</a>.
</p>
<p align="justify">
  <a name="packages"><em>Paketi</em></a>.
  Nekateri ponudniki operacijskega sistema GNU/Linux in sistemov UNIX so dali na razpolago binarne pakete različice <?php echo $release_full;?> za nekatere različice svojih distribucij. V drugih primeri so to storili prostovoljci. <br />
  Nekateri binarni paketi so za prost prenos na voljo s KDE-jeve strani <a
href="http://download.kde.org/binarydownload.html?url=/stable/<?php echo $release_full;?>/">download.kde.org</a>.
  Dodatni binarni paketi in posodobitve obstoječih bodo na voljo v prihodnjih tednih.
<a name="package_locations"><em>Lokacije paketov</em></a>.
Za trenuten seznam razpoložljivih binarnih paketov, o katerih je bila skupnost KDE obveščena, obiščite <a href="/info/<?php echo $release_full;?>.php"><?php echo $release;?>to spletno stran</a>.
</p>
<p align="justify">
  <a name="source_code"></a>
  Celotno izvorno kodo za različico <?php echo $release_full;?> lahko <a href="/info/<?php echo $release_full;?>.php">prosto prenesete</a>.
Navodila za prevajanje izvorne kode in nameščanje KDE-jeve programske opreme različice <?php echo $release_full;?> so na voljo na
<a href="/info/<?php echo $release_full;?>.php#binary"><?php echo $release_full;?>tej spletni strani</a>.
</p>

<h4>
Sistemske zahteve
</h4>
<p align="justify">
Da bi ta različica delovala kar najbolje, priporočamo uporabo najnovejše različice knjižnice Qt. To je potrebno za stabilnost in hitrost, saj so bile nekatere izboljšave opravljene na nivoju ogrodja Qt.<br />
Za čimboljše doživetje pri uporabi vseh zmožnosti KDE-jevih programov priporočamo tudi uporabo najnovejših gonilnikov za grafično kartico. Najnovejši gonilniki lahko izboljšajo funkcionalnost, hitrost in stabilnost.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Stiki za medije</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>
