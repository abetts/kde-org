<!-- header goes into the specific page -->
<p align="justify">
O software do KDE, incluindo todas as bibliotecas e seus aplicativos, está disponível livremente sob licenças de Código Aberto. O software KDE é executado em várias configurações de hardware e arquiteturas de CPU como a ARM e a x86, sistemas operacionais e funciona com qualquer tipo de gerenciador de janelas e ambiente de desktop. Além do Linux e outros sistemas operacionais baseados em UNIX, você pode encontrar versões para o Microsoft Windows da maioria dos aplicativos do KDE no site <a href="http://windows.kde.org">Software KDE no Windows</a> e para versões do Apple Mac OS X no site <a href="http://mac.kde.org/">Software KDE no Mac</a>. Compilações experimentais dos aplicativos do KDE para diversas plataformas móveis como o MeeGo, MS Windows Mobile e Symbian podem ser encontradas na Web mas elas não são atualmente suportadas. O <a href="http://plasma-active.org">Plasma Active</a> é uma experiência de usuário para um grande espectro de dispositivos, como tablet e outros dispositivos móveis.
<br />
O software KDE pode ser obtido em código ou vários formatos binários a partir de <a
href="http://download.kde.org/stable/<?php echo $release_full;?>/">http://download.kde.org</a> e pode
também ser obtido em <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
ou com qualquer dos <a href="http://www.kde.org/download/distributions.php">principais 
sistemas GNU/Linux e UNIX</a> distribuídos hoje.
</p>
<p align="justify">
  <a name="packages"><em>Packages</em></a>.
  Alguns distribuidores do Linux/UNIX gentilmente fornecem pacotes binários do <?php echo $release_full;?> 
para algumas versões de suas distribuições, e em outros casos voluntários da comunidade têm
feito isto. <br />
  Alguns destes pacotes binários estão disponíveis para download livre no servidor KDE <a
href="http://download.kde.org/binarydownload.html?url=/stable/<?php echo $release_full;?>/">http://download.kde.org</a>.
  Pacotes binários adicionais, bem como atualizações dos pacotes disponíveis, se tornarão disponíveis
nas próximas semanas.
<a name="package_locations"><em>Localizações dos pacotes</em></a>.
Para uma lista atual dos pacotes binários disponíveis os quais a Equipe de Lançamento do KDE foi informada,
por favor visite a <a href="/info/<?php echo $release_full;?>.php"><?php echo $release;?> Página de
Informações</a>.
</p>
<p align="justify">
  <a name="source_code"></a>
  O código-fonte completo para o <?php echo $release_full;?> pode ser <a href="/info/<?php echo $release_full;?>.php">baixado livremente</a>.
Instruções para compilação e instalação do software KDE <?php echo $release_full;?>
  estão disponíveis na <a href="/info/<?php echo $release_full;?>.php#binary"><?php echo $release_full;?> Página de Informações</a>.
</p>

<h4>
Requisitos do Sistema
</h4>
<p align="justify">
Para obter o melhor destes lançamentos, nós recomendamos usar uma versão recente do Qt, seja a 4.7.4 ou a 4.8.0. Isto é necessário para garantir uma experiência estável e com boa performance, bem como algumas melhorias feitas no software do KDE que foram construídas com base no framework do Qt.<br />
Para obter um uso completo das capacidades do software KDE, nós também recomendamos usar os últimos drivers gráficos para o seu sistema, uma vez que isso aprimora a experiência do usuário de maneira substancial, tanto nas funcionalidades opcionais como na performance e estabilidade geral.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
?>
