<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "KDE-jeva razvojna platforma 4.9 – boljša povezljivost, preprostejše razvijanje";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Drugi jeziki:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE s ponosom najavlja izid razvojne platforme KDE Platform 4.9. Ta je osnova za delovna okolja Plasma in KDE-jeve programe. Skupnost KDE trenutno razvija naslednjo večjo izdajo KDE-jeve razvojne platforme, ki bo imenovana »KDE Frameworks 5«. Le-ta bo sicer v veliki meri združljiva na ravni izvorne kode in bo temeljila na ogrodju Qt 5. Ponujala bo jasno razdelitev na posamezne dele, kar bo olajšalo uporabo le določenih delov platforme in zmanjšalo medsebojne odvisnosti. To pomeni, da je bila v preteklih šestih mesecih KDE-jeva razvojna platforma 4 v večji meri zamrznjena. Večina dela je bila posvečenega odpravljanju napak in pohitritvam.
</p>
<p>
Napredovalo je ločevanje QGraphicsView od Plasme, s čimer se pripravlja prehod na Plasmo, ki bo povsem temeljila na tehnologiji Qt QML, ter izboljšave v podpori za Qt Quick v KDE-jevih knjižnicah in delovnih okoljih Plasma.
</p>
<p>
Nekaj dela v KDE-jevih knjižnicah je bilo opravljenega na področju omrežnega povezovanja. Dostopanje do virov, deljenih prek omrežja, je hitrejše za vse KDE-jeve programe. NFS, Samba in SSHFS povezave ter KIO nič več ne štejejo datotek in map, kar prinaša pohitritve. Protokol HTTP je ravno tako hitrejši, saj so bile odpravljene nepotrebne povratne izmenjave. Pohitritev je še najbolj opazna v programih, ki uporabljajo dostop do omrežja. KOrganizer je na primer hitrejši za približno 20 %. Izboljšano je tudi shranjevanje gesel za omrežne vire.
</p>
<p>
Odpravljene so bile večje napake v Nepomuku in Sopranu, zaradi česar je namizno iskanje hitrejše in zanesljivejše. Zmogljivostne izboljšave in povečanje stabilnosti sta bila glavna cilja teh projektov v tej izdaji.
</p>

<h4>Nameščanje KDE-jeve razvojne platforme</h4>
<?php
  include("boilerplate-sl.inc");
?>

<h2>Druge današnje najave:</h2>
<h2><a href="plasma-sl.php"><img src="images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.9" width="64" height="64" />  Delovna okolja KDE Plasma 4.9 – osnovne izboljšave</a></h2>
<p>
Med poudarke glede delovnih okolij Plasma spadajo precejšnje izboljšave upravljalnika datotek Dolphin, posnemovalnika terminala Konsole, dejavnosti in upravljalnika oken KWin. Za podrobnosti si oglejte <a href="plasma-sl.php">najavo za delovna okolja Plasma</a>.
</p>
<h2><a href="applications-sl.php"><img src="images/applications.png" class="app-icon" alt="The KDE Applications 4.9"/> Novi in izboljšani KDE-jevi programi 4.9</a></h2>
<p>
Med nove ali izboljšane KDE-jeve programe  spadajo, Okular, Kopete, Kontact, izobraževalni programi in igre. Za podrobnosti si oglejte <a href="applications-sl.php">najavo za KDE-jeve programe</a>.
</p>
<?php
  include("footer.inc");
?>
