<?php

  $release = '4.9';
  $release_full = '4.9.0';
  $page_title = "KDE-jevi programi 4.9 so boljši in stabilnejši";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();

</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Drugi jeziki:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>

<p>
KDE z veseljem najavlja izid izboljšanih različic priljubljenih programov. Med te spadajo mnoga osnovna orodja in programi, kot so Okular, Kopete, Kontact ter izobraževalni programi in igre.
</p>
<p>
KDE-jev pregledovalnik dokumentov Okular lahko sedaj shrani in natisne zabeležke za dokumente PDF. Iskanje, zaznamki in izbiranje besedila so doživeli izboljšave. V načinu za predstavitve bo sedaj Okular preprečil ugasnitev zaslona ali prehod prenosnika v mirovanje. Okular po novem zna predvajati tudi video posnetke, ki so vgrajeni v dokumente PDF. Pregledovalnik slik Gwenview prinaša nov celozaslonski način za brskanje in precej popravkov in manjših izboljšav.
</p>
<p>
JuK, KDE-jev privzeti glasbeni predvajalnik prinaša podporo za objavljanje na last.fm, pridobivanje ovitkov z last.fm in podporo za protokol MPRIS2. Prikazati zna tudi ovitke, ki so vgrajeni v datotakah MP4 in AAC. KDE-jev predvajalnik videa Dragon je prav tako dobil podporo za MPRIS2
</p>
<p>
Vsestranski program za klepet Kopete zna po novem združiti vse stike brez povezave v svojo skupino in prikazati spremembe besedila stanja v oknih za klepet.
</p>
<p>
Program za računalniško podprto prevajanje Lokalize ima izboljšano iskanje ohlapnih prevodov in datotek. Z njim je sedaj možno prevajati tudi datoteke TS ogordja Qt. Umbrelo, modelirnik za UML, je dobil podporo za samodejno razporejanje diagramov in izvažanje risb v zapisu Graphviz dot. Urejevalnik binarnih datotek Okteta prinaša profile prikaza in upravljalnik zanje.
</p>
<h2>Zbrirka Kontact</h2>
<p>
Najcelovitejša zbirka programov za upravljanje osebnih podatkov Kontact je prejela veliko popravkov in pohitritev. Dodan je bil čarovnik za uvažanje nastavitev, e-pošte, filtrov, koledarjev in imenika iz programov Thunderbird ter Evolution. Novo je tudi orodje za ustvarjanje varnostnoh kopij e-pošte, nastavitev in metapodatkov. Vrnil se je program KTnef, ki je samostojen pregledovalnik prilog TNEF. Viri za Googlove storitve omogočajo uporabnikom dostop do stikov in koledarjev.
</p>
<h2>KDE-jevi izobraževalni programi</h2>

<p>
V zbirko izobraževalnih programov KDE Education je bila dodana igra za treniranje spomina Pairs. Rocs, program za učenje in raziskovanje teorije grafov, ima več izboljšav. Algoritme je sedaj moč izvajati po korakih, sistem za razveljavljanje deluje bolje, podprti pa so tudi prekrivni grafi. Namizni planetarij KStars ima izboljšano razvrščanje po času prehoda poldnevnika in času opazovanja ter boljšo podporo za pridobivanje slik projekta <a href="http://en.wikipedia.org/wiki/Digitized_Sky_Survey">Digitalized Sky Survey</a>.
<div align="center" class="screenshot">
<a href="screenshots/kde49-pairs.png"><img src="screenshots/kde49-pairs-thumb.png" /></a></div>
</p>
<p>
Nova različica namiznega globusa Marble vsebuje pohitritve in podporo za večjedrne procesorje.Izboljšave je doživel tudi uporabniški vmesnik. Med razširitvami za usmerjanje do cilja je tudi OSRM (Open Source Routing Machine), podpora za usmerjanje pešcev in kolesarjev ter zmožnost za usmerjanje in iskanje podatkov brez medmrežne povezave. Marble lahko prikazuje tudi položaje letal v simulatorju FlightGear.
</p>
<h2>KDE-jeve igre</h2>
<p>
Veliko izboljšav je doživel Kajongg, KDE-jeva igra mahjongga. Med izbojšavami so namigi za igro, izboljšana umetna inteligenca računalniških igralcev in klepet z igralci na istem strežniku (en izmed njih je kajongg.org). KGoldrunner ima več novih stopenj (prispeval jih je Gabriel Miltschitzky), KPatience pa shrani tudi zgodovino igre. KSudoku prikazuje boljše namige ter ponuja sedem novih dvorazsežnih ugank in tri nove trorazsežne uganke.
<div align="center" class="screenshot">
<a href="screenshots/kde49-ksudoku-3d-samurai.png"><img src="screenshots/kde49-ksudoku-3d-samurai-thumb.png" /></a></div>
</p>

<h4>Nameščanje KDE-jevih programov</h4>
<?php
  include("boilerplate-sl.inc");
?>

<h2>Druge današnje najave:</h2>
<h2><a href="plasma-sl.php"><img src="images/plasma.png" class="app-icon" alt="The KDE Plasma Workspaces 4.9" width="64" height="64" /> Delovna okolja Plasma 4.9 – osnovne izboljšave</a></h2>
<p>
Med poudarke glede delovnih okolij Plasma spadajo precejšnje izboljšave upravljalnika datotek Dolphin, posnemovalnika terminala Konsole, dejavnosti in upravljalnika oken KWin. Za podrobnosti si oglejte <a href="plasma-sl.php">najavo za delovna okolja Plasma</a>.
</p>
<h2><a href="platform-sl.php"><img src="images/platform.png" class="app-icon" alt="The KDE Development Platform 4.9"/> KDE-jeva razvojna platforma 4.9</a></h2>
<p>
Današnja izdaja KDE-jeve razvojne platforme vsebuje popravke, druge izboljšave kakovosti in omrežnih zmožnosti ter priprave na ogrodje KDE Frameworks 5. Za podrobnosti si oglejte <a href="platform-sl.php">najavo za KDE-jevo razvojno platformo</a>.
</p>
<?php
  include("footer.inc");
?>
