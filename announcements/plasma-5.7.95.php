<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("KDE at 20: Plasma 5.8 LTS Beta. Here for the Long Term.");
  $site_root = "../";
  $release = 'plasma-5.7.95';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<style>
figure {text-align: center; float: right; margin: 0px;}
figure img {padding: 1ex; border: 0px; background-image: none;}
figure video {padding: 1ex; border: 0px; background-image: none;}
figcaption {font-style: italic;}
</style>

<!-- video
<figure style="float: none">
<iframe style="text-align: center" width="560" height="315" src="https://www.youtube.com/embed/A9MtFqkRFwQ?rel=0" frameborder="0" allowfullscreen></iframe>
</figure>
<br clear="all" />
-->

<figure style="float: none">
<a href="plasma-5.8/plasma-5.8.png">
<img src="plasma-5.8/plasma-5.8-wee.png" style="border: 0px" width="600" height="375" alt="<?php i18n("KDE Plasma 5.8 LTS");?>" />
</a>
<figcaption><?php i18n("KDE Plasma 5.8 LTS Beta");?></figcaption>
</figure>


<p>
<?php i18n("Thursday, 15 September 2016. "); ?>
<?php i18n("Today KDE releases a beta of its first Long Term Support edition of its flagship desktop software, Plasma.  This marks the point where the developers and designers are happy to recommend Plasma for the widest possible audience be they enterprise or non-techy home users.  If you tried a KDE desktop previously and have moved away, now is the time to re-assess, Plasma is simple by default, powerful when needed.
");?>
</p>

<br clear="all" />
<h2>Plasma's Comprehensive Features</h2>

<p>Take a look at what Plasma offers, a comprehensive selection of features unparalleled in any desktop software.</p>

<h3>Desktop Widgets</h3>
<figure style="float: right">
<a href="plasma-5.8/plasma-5.8-widgets.png">
<img src="plasma-5.8/plasma-5.8-widgets-wee.png" style="border: 0px" width="350" height="196" alt="<?php i18n("Desktop Widgets");?>" />
</a>
<figcaption><?php i18n("Desktop Widgets");?></figcaption>
</figure>

<p>Cover your desktop in useful widgets to keep you up to date with weather, amused with comics or helping with calculations.</p>

<br clear="all" />
<h3>Get Hot New Stuff</h3>
<figure style="float: right">
<a href="plasma-5.8/plasma-5.8-hotnewstuff.png">
<img src="plasma-5.8/plasma-5.8-hotnewstuff-wee.png" style="border: 0px" width="350" height="241" alt="<?php i18n("Get Hot New Stuff");?>" />
</a>
<figcaption><?php i18n("Get Hot New Stuff");?></figcaption>
</figure>

<p>Download wallpapers, window style, widgets, desktop effects and dozens of other resources straight to your desktop.  We work with the new <a href="http://store.kde.org">KDE Store</a> to bring you a wide selection of addons for you to install.</p>

<br clear="all" />
<h3>Desktop Search</h3>
<figure style="float: right">
<a href="plasma-5.8/plasma-5.8-search-launch.png">
<img src="plasma-5.8/plasma-5.8-search-launch-wee.png" style="border: 0px" width="350" height="417" alt="<?php i18n("Desktop Search");?>" />
</a>
<figcaption><?php i18n("Desktop Search");?></figcaption>
</figure>

<p>Plasma will let you easily search your desktop for applications, folders, music, video, files... everything you have.</p>

<br clear="all" />
<h3>Unified Look</h3>
<figure style="float: right">
<a href="plasma-5.8/plasma-5.8-toolkits.png">
<img src="plasma-5.8/plasma-5.8-toolkits-wee.png" style="border: 0px" width="350" height="196" alt="<?php i18n("Unified Look");?>" />
</a>
<figcaption><?php i18n("Unified Look");?></figcaption>
</figure>

<p>Plasma's default Breeze theme has a unified look across all the common programmer toolkits - Qt 4 &amp; 5, GTK 2 &amp; 3, even LibreOffice.</p>

<br clear="all" />
<h3>Phone Integration</h3>
<figure style="float: right">
<a href="plasma-5.8/plasma-5.8-kdeconnect.png">
<img src="plasma-5.8/plasma-5.8-kdeconnect-wee.png" style="border: 0px" width="350" height="337" alt="<?php i18n("Phone Integration");?>" />
</a>
<figcaption><?php i18n("Phone Integration");?></figcaption>
</figure>
<p>Using KDE Connect you'll be notified on your desktop of text message, can easily transfer files, have your music silenced during calls and even use your phone as a remote control.</p>

<br clear="all" />
<h3>Infinitely Customisable</h3>
<figure style="float: right">
<a href="plasma-5.8/plasma-5.8-custom.jpg">
<img src="plasma-5.8/plasma-5.8-custom-wee.jpg" style="border: 0px" width="350" height="218" alt="<?php i18n("Infinitely Customisable");?>" />
</a>
<figcaption><?php i18n("Infinitely Customisable");?></figcaption>
</figure>
<p>Plasma is simple by default but you can customise it however you like with new widgets, panels, screens and styles.</p>

<br clear="all" />
<h2>New in Plasma 5.8</h2>
<h3>Unified Boot to Shutdown Artwork</h3>
<figure style="float: right">
<a href="plasma-5.8/plasma-5.8-boot.png">
<img src="plasma-5.8/plasma-5.8-boot-wee.png" style="border: 0px" width="348" height="153" alt="<?php i18n("Unified Boot to Shutdown Artwork");?>" />
</a>
<figcaption><?php i18n("Unified Boot to Shutdown Artwork");?></figcaption>
</figure>

<p>This release brings an all-new login screen design giving you a complete Breeze startup to shutdown experience. The layout has been tidied up and is more suitable for workstations that are part of a domain or company network. While it is much more streamlined, it also allows for greater customizability: for instance, all Plasma wallpaper plugins, such as slideshows and animated wallpapers, can now be used on the lock screen.</p>

<br clear="all" />
<h3>Right-to-Left Language Support</h3>
<figure style="float: right">
<a href="plasma-5.8/plasma-5.8-reverse.png">
<img src="plasma-5.8/plasma-5.8-reverse-wee.png" style="border: 0px" width="325" height="299" alt="<?php i18n("Right-to-Left Language Support");?>" />
</a>
<figcaption><?php i18n("Right-to-Left Language Support");?></figcaption>
</figure>
<p>Support for Semitic right-to-left written languages, such as Hebrew and Arabic, has been greatly improved. Contents of panels, the desktop, and configuration dialogs are mirrored in this configuration. Plasma’s sidebars, such as widget explorer, window switcher, activity manager, show up on the right side of the screen.</p>

<br clear="all" />
<h3>Improved Applets</h3>
<figure style="float: right">
<a href="plasma-5.8/plasma-5.8-media-controls.png">
<img src="plasma-5.8/plasma-5.8-media-controls.png" style="border: 0px" width="331" height="370" alt="<?php i18n("Context Menu Media Controls");?>" />
</a>
<figcaption><?php i18n("Context Menu Media Controls");?></figcaption>
</figure>
<p>The virtual desktop switcher (“Pager”) and window list applets have been rewritten, using the new task manager back-end we introduced in Plasma 5.7. This allows them to use the same dataset as the task manager and improves their performance while reducing memory consumption. The virtual desktop switcher also acquired an option to show only the current screen in multi-screen setups and now shares most of its code with the activity switcher applet.</p>

<p>Task manager gained further productivity features in this release. Media controls that were previously available in task manager tooltips only are now accessible in the context menus as well. In addition to bringing windows to the front during a drag and drop operation, dropping files onto task manager entries themselves will now open them in the associated application. Lastly, the popup for grouped windows can now be navigated using the keyboard and text rendering of its labels has been improved.</p>

<br clear="all" />
<h3>Simplified Global Shortcuts</h3>
<figure style="float: right">
<a href="plasma-5.8/plasma-5.8-global-shortcuts.png">
<img src="plasma-5.8/plasma-5.8-global-shortcuts-wee.png" style="border: 0px" width="350" height="204" alt="<?php i18n("Global Shortcuts Setup");?>" />
</a>
<figcaption><?php i18n("Global Shortcuts Setup");?></figcaption>
</figure>

<p>Global shortcuts configuration has been simplified to focus on the most common task, that is launching applications. Building upon the jump list functionality added in previous releases, global shortcuts can now be configured to jump to specific tasks within an application.</p>

<p>Thanks to our Wayland effort, we can finally offer so-called “modifier-only shortcuts”, enabling you to open the application menu by just pressing the Meta key. Due to popular demand, this feature also got backported to the X11 session.

<br clear="all" />
<h3>Other improvements</h3>
<figure style="float: right">
<a href="plasma-5.8/plasma-5.8-discover.png">
<img src="plasma-5.8/plasma-5.8-discover-wee.png" style="border: 0px" width="350" height="281" alt="<?php i18n("Plasma Discover's new UI");?>" />
</a>
<figcaption><?php i18n("Plasma Discover's new UI");?></figcaption>
</figure>

<p>This release sees many bugfixes in multi-screen support and, together with Qt 5.6.1, should significantly improve your experience with docking stations and projectors.</p>

<p>KWin, Plasma’s window manager, now allows compositing through llvmpipe, easing the deployment on exotic hardware and embedded devices. Now that there is a standardized and widely-used interface for applications to request turning off compositing, the “Unredirect Fullscreen” option has been removed. It often lead to stability issues and because of that was already disabled for many drivers.</p>

<p>Now that <a href="https://dot.kde.org/2016/08/10/kdes-kirigami-ui-framework-gets-its-first-public-release">Kirigami</a>, our set of versatile cross-platform UI components, has been released, we’re pleased to bring you a revamped version of Plasma Discover based on Kirigami.</p>

<p>We have new default fonts, the Noto font from Google covers all scripts available in the Unicode standard while our new monospace font Hack is perfect for coders and terminal users.</p>

<br clear="all" />
<h3>We’re in Wayland!</h3>
<figure style="float: right">
<a href="plasma-5.8/plasma-5.8-wayland.png">
<img src="plasma-5.8/plasma-5.8-wayland-wee.png" style="border: 0px" width="350" height="196" alt="<?php i18n("Plasma on Wayland Now with GTK+ support");?>" />
</a>
<figcaption><?php i18n("Plasma on Wayland Now with GTK+ support");?></figcaption>
</figure>

<p>Plasma on Wayland has come a long way in the past months. While our long term support promise does not apply to the fast-evolving Wayland stack, we think it is ready to be tested by a broader audience. There will still be minor glitches and missing features, but we are now at a point where we can ask you to give it a try and report bugs. Notable improvements in this release include:</p>
<ul>
<li>Support for xdg-shell, i.e. GTK+ applications are now supported</li>
<li>Much improved touch screen support</li>
<li>Support for touchpad gestures – the infrastructure is there, there aren't any gestures by default yet</li>
<li>The “Sliding Popups” effect is now supported</li>
<li>Clipboard contents are synced between X11 and Wayland applications</li>
</ul>

<p><a href="plasma-5.7.5-5.7.95-changelog.php">
<?php i18n("Full Plasma 5.7.95 LTS changelog");?></a></p>

<!-- // Boilerplate again -->

<h2><?php i18n("Live Images");?></h2>

<p><?php print i18n_var("
The easiest way to try it out is with a live image booted off a
USB disk. You can find a list of <a href='%1'>Live Images with Plasma 5</a> on the KDE Community Wiki.
", "https://community.kde.org/Plasma/LiveImages");?></p>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
, $release);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
