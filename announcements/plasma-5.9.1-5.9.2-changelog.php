<?php
	require('../aether/config.php');

	$pageConfig = array_merge($pageConfig, [
		'title' => "Plasma 5.9.2 Complete Changelog",
		'cssFile' => 'content/home/portal.css'
	]);

	require('../aether/header.php');
	$site_root = "../";
	$release = "5.9.2";
?>

<style>
main {
	padding-top: 20px;
	}

.videoBlock {
	background-color: #334545;
	border-radius: 2px;
	text-align: center;
}

.videoBlock iframe {
	margin: 0px auto;
	display: block;
	padding: 0px;
	border: 0;
}

.topImage {
	text-align: center
}

.releaseAnnouncment h1 a {
	color: #6f8181 !important;
}

.releaseAnnouncment h1 a:after {
	color: #6f8181;
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	vertical-align: middle;
	margin: 0px 5px;
}

.releaseAnnouncment img {
	border: 0px;
}

.get-it {
	border-top: solid 1px #eff1f1;
	border-bottom: solid 1px #eff1f1;
	padding: 10px 0px 20px;
	margin: 10px 0px;
}

.releaseAnnouncment ul {
	list-style-type: none;
	padding-left: 40px;
}
.releaseAnnouncment ul li {
	position: relative;
}

.releaseAnnouncment ul li:before {
	content: ">";
	font-family: "glyph";
	font-size: 60%;
	position: absolute;
	top: .8ex;
	left: -20px;
	font-weight: bold;
	color: #3bb566;
}

.give-feedback img {
	padding: 0px; 
	margin: 0px;
	height: 2ex;
	width: auto;
	vertical-align: middle;
}
</style>

<main class="releaseAnnouncment container">

<p><a href="plasma-5.9.2.php">Plasma 5.9.2</a> Complete Changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>

<h3><a name='discover' href='https://commits.kde.org/discover'>Discover</a> </h3>
<ul id='uldiscover' style='display: block'>
<li>Make sure we don't show warnings unless it's absolutely necessary. <a href='https://commits.kde.org/discover/d79cb42b7d77dd0c6704a3a40d85d06105de55b4'>Commit.</a> </li>
<li>Make sure the Remove button doesn't overlap the text. <a href='https://commits.kde.org/discover/e417b9ed230859e29c4d076b7db7bebdd293477f'>Commit.</a> </li>
<li>Improve local packages display on local packages. <a href='https://commits.kde.org/discover/1a5a9b5f1fa6ead2e3c4f50f1fb0b16e1ac14394'>Commit.</a> See bug <a href='https://bugs.kde.org/376276'>#376276</a></li>
</ul>


<h3><a name='kde-gtk-config' href='https://commits.kde.org/kde-gtk-config'>KDE GTK Config</a> </h3>
<ul id='ulkde-gtk-config' style='display: block'>
<li>Properly retrieve all boolean properties. <a href='https://commits.kde.org/kde-gtk-config/557e20b961fe478ecf7c8a0192830eaf0894098b'>Commit.</a> </li>
<li>Don't break compatibility with old configs. <a href='https://commits.kde.org/kde-gtk-config/0a7fafc7e2f455a1c4f7ae9a104abce007374572'>Commit.</a> </li>
<li>Fix gtk-primary-button-warps-slider with GTK 2. <a href='https://commits.kde.org/kde-gtk-config/4a06c4d34f2dd8859058618b1dbe9dbd6df9e9b8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376273'>#376273</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Lazy QUrl::fromUserInput in web browser. <a href='https://commits.kde.org/kdeplasma-addons/912ad00230e361f7482d5ddbcb1a830ff397e03e'>Commit.</a> </li>
</ul>


<h3><a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a> </h3>
<ul id='ulkscreenlocker' style='display: block'>
<li>Fix crash in Screen Locker KCM on teardown. <a href='https://commits.kde.org/kscreenlocker/51c4d6c8db8298dbd471fa91de6cb97bf8b4287a'>Commit.</a> See bug <a href='https://bugs.kde.org/373628'>#373628</a></li>
</ul>


<h3><a name='kwin' href='https://commits.kde.org/kwin'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>[autotests] Add test case for quick tiling on X11. <a href='https://commits.kde.org/kwin/8b4f284249f1ff6a52b7148c0536164c42ceaa73'>Commit.</a> See bug <a href='https://bugs.kde.org/376155'>#376155</a></li>
<li>Avoid a crash on Kwin decoration KCM teardown. <a href='https://commits.kde.org/kwin/29179f115c81ff7128bb5430cb27ef7b65f46b33'>Commit.</a> See bug <a href='https://bugs.kde.org/373628'>#373628</a></li>
<li>Support creation of PlasmaShellSurface prior to ShellSurface. <a href='https://commits.kde.org/kwin/8edd0336e67d81e93dfe1418dbe2409e2b03d405'>Commit.</a> </li>
</ul>


<h3><a name='oxygen' href='https://commits.kde.org/oxygen'>Oxygen</a> </h3>
<ul id='uloxygen' style='display: block'>
<li>Look and feel - Window decoration. <a href='https://commits.kde.org/oxygen/dee50cfd16e67014d9c839c554549e0f36f2101a'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129867'>#129867</a></li>
</ul>


<h3><a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Fix crash when invoking Present Windows with the group dialog open. <a href='https://commits.kde.org/plasma-desktop/61489110c597ad4e4cb69b004d30dbda8b4110ef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376205'>#376205</a>. Fixes bug <a href='https://bugs.kde.org/376234'>#376234</a></li>
<li>Fix crash when invoking Present Windows with the group dialog open. <a href='https://commits.kde.org/plasma-desktop/dc17f78ebcc76600040a8d2ef8c57b9a41a8d06e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376205'>#376205</a>. Fixes bug <a href='https://bugs.kde.org/376234'>#376234</a></li>
<li>Use proper version for baloo. <a href='https://commits.kde.org/plasma-desktop/07c05cc9f3df35dea4c72cb9c9581cc0b531e1d4'>Commit.</a> </li>
<li>[Folder View] Don't show script execution prompt on desktop:/. <a href='https://commits.kde.org/plasma-desktop/c2f74cc8c8dd7b814518057be62cea79f930719c'>Commit.</a> </li>
<li>[Folder View] Support extracting files to sub-directories with drag and drop from Ark. <a href='https://commits.kde.org/plasma-desktop/32c7d2d060c7a942f67737cec63f50dc85e0a6a7'>Commit.</a> </li>
<li>Fix discover desktopid in favorties. <a href='https://commits.kde.org/plasma-desktop/ecfcc8ea6e494fa118e2d613c59b539bd0a74d2e'>Commit.</a> </li>
<li>[ContextMenu] Ungrab mouse in taskmanager. <a href='https://commits.kde.org/plasma-desktop/c97c6024ca3276966258f82f3181a23bb4220daa'>Commit.</a> </li>
<li>[taskmanager] Uniform tasmanager tooltips to systray ones. <a href='https://commits.kde.org/plasma-desktop/4c7022b936da587aa081cddbd419e32597718954'>Commit.</a> </li>
<li>[Task Manager] Don't import QtQuick 2.7. <a href='https://commits.kde.org/plasma-desktop/5bbedd2c9f61dace6ffc62d993d03adff3063b4d'>Commit.</a> See bug <a href='https://bugs.kde.org/376199'>#376199</a></li>
<li>Reverse TaskManager DragDrop to blacklist Plasma instead of whitelisting URLs. <a href='https://commits.kde.org/plasma-desktop/8709b7edd5d17d2369e8ca4d261ec186387fa20e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375871'>#375871</a></li>
</ul>


<h3><a name='plasma-integration' href='https://commits.kde.org/plasma-integration'>plasma-integration</a> </h3>
<ul id='ulplasma-integration' style='display: block'>
<li>Do not treat filename in selection as URL. <a href='https://commits.kde.org/plasma-integration/e70f8134a2bc4b3647e245c05f469aeed462a246'>Commit.</a> See bug <a href='https://bugs.kde.org/376365'>#376365</a></li>
<li>QtQuick-based interfaces will now more reliably adapt to color scheme changes at runtime. <a href='https://commits.kde.org/plasma-integration/ab3298b3f5f728765d5afa8830aa7793140617c8'>Commit.</a> </li>
</ul>


<h3><a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a> </h3>
<ul id='ulplasma-sdk' style='display: block'>
<li>Update to KDevplatform API change. <a href='https://commits.kde.org/plasma-sdk/ad6e3b7f937a846ec8a5532bb62f6bf2e923b49b'>Commit.</a> </li>
</ul>


<h3><a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Fix manually reordering launchers. <a href='https://commits.kde.org/plasma-workspace/dda4d42e891a386c747fbe0e5698ca019921d3a1'>Commit.</a> </li>
<li>TaskManager: add icon to cache after painfully querying it over XCB/NETWM. <a href='https://commits.kde.org/plasma-workspace/1f2a372fd840d56b545971bfc9b0610c3ce61ea5'>Commit.</a> </li>
<li>Pass args to DataEngine superclass constructor. <a href='https://commits.kde.org/plasma-workspace/278cca4e25d9b4e17b98bfde683a78ecc830fb2e'>Commit.</a> </li>
<li>Turn the NotificationItem in a MouseArea. <a href='https://commits.kde.org/plasma-workspace/4c7335230f90b288fa875ba5d1a589e14bbc86fe'>Commit.</a> </li>
<li>A minimized config dialog on the desktop (e.g. wallpaper settings) will now be un-minimized when trying to open it multiple times. <a href='https://commits.kde.org/plasma-workspace/92601876d18c0974aa524a84c3765f279c581ed9'>Commit.</a> </li>
<li>Match QtQuick import to minimum Qt version. <a href='https://commits.kde.org/plasma-workspace/1e9a3875e5bda036bd2060aaedc57932edface72'>Commit.</a> See bug <a href='https://bugs.kde.org/376199'>#376199</a></li>
<li>[Clipboard plasmoid] Fix line breaks. <a href='https://commits.kde.org/plasma-workspace/ee26a139378535042411a345fea7d73efc79a73f'>Commit.</a> </li>
<li>[System Tray] Part Revert "Trigger context menu on press" as this breaks xembedsniproxy. <a href='https://commits.kde.org/plasma-workspace/27b1030756002e91b60ba51483efe9c2c477d16e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375930'>#375930</a></li>
<li>Rename Application Menu to Global Menu to avoid conflict. <a href='https://commits.kde.org/plasma-workspace/2bef1c1b1f10d31413388e459fdbca206872238d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375478'>#375478</a></li>
<li>[kioslave/remote] Fix porting bugs. <a href='https://commits.kde.org/plasma-workspace/566bbf7ba6bc43b5f00f43667603b9b50efea0cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376131'>#376131</a></li>
<li>[Icon Applet] More sensible minimum height. <a href='https://commits.kde.org/plasma-workspace/f0eb59ed931492159a67b2b17b4618fd40fa9d11'>Commit.</a> </li>
<li>Make services disqualification much stricter. <a href='https://commits.kde.org/plasma-workspace/7b1dc9a4bb039ff5ff2a5b71a2c6bc687ad1db72'>Commit.</a> </li>
<li>[System Tray Containment] Drop useless Q_INVOKABLE from .cpp file. <a href='https://commits.kde.org/plasma-workspace/172ab88346683fcc0f1dff6b05b3a50b85659e98'>Commit.</a> </li>
<li>[System Tray Containment] Ungrab mouse before opening context menu. <a href='https://commits.kde.org/plasma-workspace/bcb9ede5caabac3d5086dbcdcccfd7bc44211704'>Commit.</a> </li>
</ul>

	</main>

<?php
  	require('../aether/footer.php');
