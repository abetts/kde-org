<?php

  $release = '4.7';
  $release_full = '4.7.0';
  $page_title = "Uued KDE rakendused, töötsoonid ja arendusplatvorm pakuvad uusi võimalusi ja suuremat stabiilsust";
  $site_root = "../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<script type="text/javascript">
(function() {
var s = document.createElement('SCRIPT'), s1 = document.getElementsByTagName('SCRIPT')[0];
s.type = 'text/javascript';
s.async = true;
s.src = 'http://widgets.digg.com/buttons.js';
s1.parentNode.insertBefore(s, s1);
})();
</script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<p>Teistes keeltes:
<?php
  include "../announce-i18n-bar.inc";
?>
</p>
<p>
KDE-l on rõõm teatada uusimatest väljalasetest, mis uuendavad oluliselt KDE Plasma töötsoone, KDE rakendusi ja kogu KDE tarkvarale alust moodustavat KDE platvormi. Kõigi nende väljalasete versioon 4.7 pakub rohkelt uusi võimalusi ning paranenud stabiilsust ja jõudlust.
</p>
<?php
  centerThumbScreenshot("general-desktop.png", "Plasma ja rakendused 4.7");
?>

<?php
include("trailer-plasma-et.inc");
include("trailer-applications-et.inc");
include("trailer-platform-et.inc");

?>

<h3>
Kiirsuhtlus otse töölaual
</h3>
<p>
KDE-Telepathy meeskonnal on rõõm teatada KDE uue kiirsuhtluslahenduse ajaloolisest esmaväljalaskest. See on küll alles algusjärgus, aga juba praegu võib luua igat laadi kontosid, kaasa arvatud GTalk ja Facebook Chat, ning tarvitada neid täiesti igapäevaselt. Vestlusliides võimaldab valida väga erinevate välimuste vahel, pakkudes selleks Adiumi teemasid. Soovi korral võib Plasma olekuvidina seada otse paneelile ja määrata sealt oma võrguolekut. Et projekt ei ole veel piisavalt küps kuuluma KDE suurde perekonda, pakendatakse ja levitatakse seda ülejäänud KDE komponentidest eraldi. KDE-Telepathy 0.1.0 lähtekoodi leiab saidilt <a href="http://download.kde.org/download.php?url=unstable/telepathy-kde/0.1.0/src/">download.kde.org</a>. Paigaldamisjuhised leiab saidilt <a href="http://community.kde.org/Real-Time_Communication_and_Collaboration/Installing_stable_release">community.kde.org</a>.
</p>

<h3>
Stabiilsus ja uued võimalused
</h3>
<p>
Lisaks paljudele uutele omadustele, mida on kirjeldatud väljalasketeadetes, on KDE kaastöötajad pärast KDE tarkvara viimast suuremat väljalaset suutnud sulgeda üle 12 000 veateate (sealhulgas üle 2000 unikaalse veateate). Nii võib kindlalt öelda, et meie tarkvara on täna stabiilsem kui eales varem.
</p>

<h3>
Levitage sõna ja jälgige toimuvat: silt "KDE" on oluline
</h3>
<p>
KDE julgustab kõiki levitama sõna sotsiaalvõrgustikes. Edastage lugusid uudistesaitidele, kasutage selliseid kanaleid, nagu delicious, digg, reddit, twitter, identi.ca. Laadige ekraanipilte üles sellistesse teenustesse, nagu Facebook, Flickr, ipernity ja Picasa, ning postitage neid sobivatesse gruppidesse. Looge ekraanivideoid ja laadige need YouTube'i, Blip.tv-sse, Vimeosse või mujale. Ärge unustage lisada üleslaaditud materjalile silti "KDE", et kõik võiksid vajaliku materjali hõlpsamini üles leida ja KDE meeskond saaks koostada aruandeid KDE <?php echo $release;?> väljalasketeate kajastamise kohta.
Kõike seda, mis toimub seoses väljalaskega sotsiaalvõrgustikes, võite jälgida KDE kogukonna otsevoos. See sait koondab reaalajas kõik, mis toimub identi.ca-s, twitteris, youtube'is, flickris, picasawebis, ajaveebides ja paljudes teistes sotsiaalvõrgustikes. Otsevoo leiab aadressilt <a href="http://buzz.kde.org">buzz.kde.org</a>.

<div align="center">
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a class="DiggThisButton DiggCompact" href="http://digg.com/submit?url=http%3A//kde.org/announcements/4.7/&amp;title=KDE%20releases%20version%204.7%20of%20Plasma%20Workspaces,%20Applications%20and%20Platform" rev="news, tech_news"></a>
	</td>
	<td>
		<a href="http://twitter.com/share" class="twitter-share-button" data-url="https://www.kde.org/announcements/4.7/" data-text="#KDE releases version 4.7 of Plasma Workspaces, Applications and Platform → https://www.kde.org/announcements/4.7/ #kde47" data-count="horizontal" data-via="kdecommunity">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
	</td>
	<td>
		<script type="text/javascript" src="http://www.reddit.com/static/button/button1.js?url=http%3A//kde.org/announcements/4.7/"></script>
	</td>
	<td>
		<iframe src="http://www.facebook.com/plugins/like.php?app_id=225109044193701&amp;href=http%3A%2F%2Fkde.org%2Fannouncements%2F4.7%2F&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>
	</td>
	<td>
		<g:plusone size="medium" href="https://www.kde.org/announcements/4.7/"></g:plusone>
	</td>
	<td>
	</td>
</tr>
</table>
<table border="0" cellspacing="2" cellpadding="2" class="social">
<tr>
	<td>
		<a href="http://identi.ca/search/notice?q=kde47"><img src="buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
	</td>
	<td>
		<a href="http://www.flickr.com/photos/tags/kde47"><img src="buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
	</td>
	<td>
		<a href="http://www.youtube.com/results?search_query=kde47"><img src="buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
	</td>
	<td>
		<a href="http://delicious.com/tag/kde47"><img src="buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
	</td>
</tr>
</table>
<span style="font-size: 6pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></span>
</div>
</p>

<h3>
Väljalasketeadetest
</h3><p>
Käesolevad väljalasketeated koostasid Algot Runeman, Dennis Nienhüser, Dominik Haumann, Jos Poortvliet, Markus Slopianka, Martin Klapetek, Nick Pantazis, Sebastian K&uuml;gler, Stuart Jarvis, Vishesh Handa, Vivek Prakash, Carl Symons ja teised KDE propageerimismeeskonna ning laiemagi kogukonna liikmed. Eesti keelde tõlkis need Marek Laane. Väljalasketeated kajastavad ainult üksikuid muudatusi kogu arendustööst, mida KDE tarkvara on viimasel poolel aastal üle elanud.
</p>


<h4>KDE toetamine</h4>


<a href="http://jointhegame.kde.org/"><img src="images/join-the-game.png" width="231" height="120"
alt="Astuge mängu" align="left"/> </a>
<p align="justify"> KDE e.V. uus <a
href="http://jointhegame.kde.org/">toetajaliikme programm</a> on
nüüd avatud. 25 euro eest kvartalis võite omalt poolt kindlustada,
et KDE rahvusvaheline kogukond kasvab ja areneb ning suudab valmistada
maailmaklassiga vaba tarkvara.</p>
<br clear="all" />
<p>&nbsp;</p>
<?php
  include("footer.inc");
?>
