<?php

  $release = '4.7';
  $release_full = '4.7.0';
  $page_title = "Plasma Workspaces become more portable thanks to KWin";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<p>
KDE is happy to announce the immediate availability of version 4.7 of both the Plasma Desktop and Plasma Netbook Workspaces. The Plasma Workspaces have seen improvements to existing functionality, as well as the introduction of significant new features. In particular, these include new interface design methods better suited to touchscreen and mobile devices.
</p>
<?php
centerThumbScreenshot("plasma.png", "Plasma Desktop 4.7");
?>

<p>
Lots of visual polishing took place with an update to the <strong>Oxygen icons</strong>, and improved consistency between panel items such as clock and notification areas. Recognizing the modular nature of KDE software and the ability to mix and match applications from many different sources, KDE has also improved the Oxygen GTK themes, making applications from GNOME (and other applications using GTK+) blend seamlessly with KDE applications in your Plasma Workspace.
<?php
centerThumbScreenshot("oxygen-icons.png", "Oxygen Icons have been touched up");
?>

</p><p>
The Plasma Workspaces window manager, KWin, has received extensive cleanup of its code and can now run on <strong>OpenGL ES</strong> supporting hardware, making it better suited for mobile devices, and also improvements for desktop users. KWin has a new shadow system, improving support for users of older hardware or hardware for which driver support for OpenGL is limited  (Xrender backend). KWin's performance improves noticably thanks to numerous optimizations in certain painting operations.
</p><p>
Plasma's Activities have seen many improvements, the Activity Manager now takes a more prominent place in the default panel in Plasma Desktop. Activities enhance the users' workflows by providing smart ways of grouping applications, widgets and documents.
<?php
centerThumbScreenshot("plasma-activities.png", "Plasma's Activities take a more prominent role in 4.7");
?>

<!--
Plasma widgets based on the new capabilities of QtQuick have also started to appear, for example, the Battery Monitor, Device Notifier, and Lock/Logout widgets.
-->
</p><p>
The Plasma Workspaces now also offer much improved network management, including experimental support for NetworkManager 0.9 as well as Bluetooth tethering, 3G, VPN, MAC spoofing and other advanced networking options.
<?php
centerThumbScreenshot("plasma-networkmanagement.png", "Network Management in Plasma 4.7");
?>

</p><p>
Navigating through applications and recent files is easier with the addition of breadcrumbs to the Kickoff application launcher, helping users to see where they are and quickly back up to higher menu levels.
Many other usability and functionality improvements have been made to the Workspaces. For example, Konsole no longer blocks the removal of USB storage devices and KMix has improved PulseAudio support.
<?php
centerThumbScreenshot("kickoff.png", "Kickoff's new breadcrumbs ease navigation");
?>

</p>

<h4>Installing Plasma</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Announced Today:</h2>

<?php

include("trailer-applications.inc");
include("trailer-platform.inc");

include("footer.inc");

?>
