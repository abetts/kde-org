<?php

  $release = '4.7';
  $release_full = '4.7.0';
  $page_title = "Improved Multimedia and Semantic Capabilities in KDE Platform";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";

?>

<p>
KDE proudly announces the release of KDE Platform 4.7. The foundation for the Plasma Workspaces and the KDE Applications has seen significant improvements in many areas. Besides the introduction of new technology, a lot of work has been done on improving the performance and stability of the underlying structure.
</p>
<?php
centerThumbScreenshot("systemsettings.png", "KDE Platform 4.7");
?>

<p>
The latest version of Phonon, our media framework, now comes with many new features such as Zeitgeist support. We have also worked on back-ends: The VLC-based back-end is now considered stable and is the preferred back-end for multiplatform use, while the back-end based on GStreamer is now also considered stable on Linux platforms. The xine back-end is no longer maintained.
</p><p>
The semantic desktop components of the KDE Frameworks have improved functionality and stability. Nepomuk has undergone massive internal changes making it more stable and faster, with richer APIs for applications. Strigi analyzers now read meta-data in their own process, resolving over 35 crash-related bugs in Dolphin and Konqueror.
</p><p>
KWin now features an option for application developers to suspend compositing when a fullscreen application calls for it. This should increase performance for OpenGL games and GPU-accelerated video playback.
</p><p>
<strong>KDM</strong>, the display manager, has gained <a href="http://ksmanis.wordpress.com/2011/04/21/hello-planet-and-grub2-support-for-kdm/">GRUB2 support</a>. A user with multiple operating systems in their GRUB2 menu can now choose which one to reboot to in the shutdown dialog by holding down the mouse button on their reboot choice.
<?php
centerThumbScreenshot("kdm.png", "KDM supports the GRUB2 boot manager");
?>

</p><p>
<strong>KIO Proxy</strong>, KDE's system-wide proxy support continues to be overhauled with fixes and features including SOCKS proxy support, functions for obtaining multiple proxy URL addresses and support for obtaining system proxy information on the Windows and Mac platforms.
</p><p>
KwebkitPart, integrating WebKit into KDE applications, has improved ad-blocking support, making browsing with a KDE WebKit browser a more pleasant experience.
</p>



<h4>Installing the KDE Development Platform</h4>
<?php
  include("boilerplate.inc");
?>

<h2>Also Announced Today:</h2>
<?php

include("trailer-plasma.inc");
include("trailer-applications.inc");

include("footer.inc");
?>
