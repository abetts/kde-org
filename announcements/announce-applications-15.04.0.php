<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("KDE Ships KDE Applications 15.04.0");
  $site_root = "../";
  $release = "applications-15.04.0";
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n_var("April 15, 2015. Today KDE released KDE Applications 15.04. With this release a total of 72 applications have been ported to <a href='%1'>KDE Frameworks 5</a>. The team is striving to bring the best quality to your desktop and these applications. So we're counting on you to send your feedback.", "https://dot.kde.org/2013/09/25/frameworks-5");?>
</p>

<p align="justify">
<?php print i18n_var("With this release there are several new additions to the KDE Frameworks 5-based applications list, including <a href='%1'>KHangMan</a>, <a href='%2'>Rocs</a>, <a href='%3'>Cantor</a>, <a href='%4'>Kompare</a>, <a href='%5'>Kdenlive</a>, <a href='%6'>KDE Telepathy</a> and <a href='%7'>some KDE Games</a>.", "https://www.kde.org/applications/education/khangman/", "https://www.kde.org/applications/education/rocs/", "https://www.kde.org/applications/education/cantor/", "https://www.kde.org/applications/development/kompare", "https://kdenlive.org/", "https://userbase.kde.org/Telepathy", "https://games.kde.org/");?>
</p>

<p align="justify">
<?php print i18n_var("Kdenlive is one of the best non-linear video editing software available. It recently finished its <a href='%1'>incubation process</a> to become an official KDE project and was ported to KDE Frameworks 5. The team behind this masterpiece decided that Kdenlive should be released together with KDE Applications. Some new features are the autosaving function of new projects and a fixed clip stabilization.", "https://community.kde.org/Incubator");?>
</p>

<p align="justify">
<?php print i18n_var("KDE Telepathy is the tool for instant messaging. It was ported to KDE Frameworks 5 and Qt5 and is a new member of the KDE Applications releases. It is mostly complete, except the audio and video call user interface is still missing.");?>
</p>

<p align="justify">
<?php i18n("Where possible KDE uses existing technology as has been done with the new KAccounts which is also used in SailfishOS and Canonical's Unity. Within KDE Applications, it's currently used only by KDE Telepathy. But in the future, it will see wider usage by applications such as Kontact and Akonadi.");?>
</p>

<p align="justify">
<?php print i18n_var("In the <a href='%1'>KDE Education module</a>, Cantor got some new features around its Python support: a new Python 3 backend and new Get Hot New Stuff categories. Rocs has been turned upside down: the graph theory core has been rewritten, data structure separation removed and a more general graph document as central graph entity has been introduced as well as a major revisit of the scripting API for graph algorithms which now provides only one unified API. KHangMan was ported to QtQuick and given a fresh coat of paint in the process. And Kanagram received a new 2-player mode and the letters are now clickable buttons and can be typed like before.", "https://edu.kde.org/");?>
</p>

<p align="justify">
<?php print i18n_var("Besides the usual bug fixes <a href='%1'>Umbrello</a> got some usability and stability improvements this time. Furthermore the Find function can now be limited by category: class, interface, package, operations, or attributes.", "https://www.kde.org/applications/development/umbrello/");?>
</p>

<p align="justify">
<?php print i18n_var("You can find the full list of changes <a href='%1'>here</a>.", "fulllog_applications-15.04.0.php");?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the KDE Applications 15.04 release. Report bugs. Encourage others to join the KDE Community. Or <a href='%1'>support the nonprofit organization behind the KDE community</a>.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5"); ?>
</p>

<p align="justify">
<?php i18n("Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, and twitter. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with 'KDE'. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 15.04 release."); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 15.04 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 15.04 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 15.04");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 15.04 may be <a href='http://download.kde.org/stable/applications/15.04.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-15.04.0.php'>KDE Applications 15.04.0 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
