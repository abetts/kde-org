<?php

  $page_title = "KDE 4.0 Released";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

<!-- Other languages translations  -->
Also available in:
<a href="index-bn_IN.php">Bengali (India)</a>
<a href="index-ca.php">Catalan</a>
<a href="http://www.kdecn.org/announcements/4.0/index.php">Chinese</a>
<a href="index-cz.php">Czech</a>
<a href="index-nl.php">Dutch</a>
<a href="http://fr.kde.org/announcements/4.0/">French</a>
<a href="http://www.kde.de/infos/ankuendigungen/40/">German</a>
<a href="index-gu.php">Gujarati</a>
<a href="index-he.php">Hebrew</a>
<a href="index-hi.php">Hindi</a>
<a href="index-it.php">Italian</a>
<a href="index-lv.php">Latvian</a>
<a href="index-ml.php">Malayalam</a>
<a href="index-mr.php">Marathi</a>
<a href="index-fa.php">Persian</a>
<a href="index-pl.php">Polish</a>
<a href="index-pa.php">Punjabi</a>
<a href="index-pt_BR.php">Portuguese (Brazilian)</a>
<a href="index-ro.php">Romanian</a>
<a href="index-ru.php">Russian</a>
<a href="index-sl.php">Slovenian</a>
<a href="index-es.php">Spanish</a>
<a href="index-sv.php">Swedish</a>
<a href="index-ta.php">Tamil</a>

<h3 align="center">
   KDE Project Ships Fourth Major Version of cutting edge Free Software Desktop
</h3>
<p align="justify">
  <strong>
    With the fourth major version, the KDE Community marks the beginning of the KDE 4 era.
  </strong>
</p>
<p align="justify">
January 11, 2008 (The INTERNET).
</p>

<p>
The KDE Community is thrilled to announce the immediate availability of
<a href="http://www.kde.org/announcements/4.0/">KDE 4.0</a>. This significant
release marks both the end of the long and intensive development cycle
leading up to KDE 4.0 and the beginning of the KDE 4 era.
</p>

<?php
    screenshot("desktop_thumb.jpg", "desktop.jpg", "center",
"The KDE 4.0 desktop", "375");
?>

<p>
The KDE 4 <strong>Libraries</strong> have seen major improvements in almost all areas.
The Phonon multimedia framework provides platform independent multimedia support to all
KDE applications, the Solid hardware integration framework makes interacting with
(removable) devices easier and provides tools for better power management.
<p />
The KDE 4 <strong>Desktop</strong> has gained some major new capabilities. The Plasma desktop shell
offers a new desktop interface, including panel, menu and widgets on the desktop
as well as a dashboard function. KWin, the KDE Window manager, now supports advanced
graphical effects to ease interaction with your windows.
<p />
Lots of KDE <strong>Applications</strong> have seen improvements as well. Visual updates through
vector-based artwork, changes in the underlying libraries, user interface
enhancements, new features, even new applications -- you name it, KDE 4.0 has it.
Okular, the new document viewer and Dolphin, the new file manager are only two
applications that leverage KDE 4.0's new technologies.
<p />
<img src="images/oxybann.png" align="right" hspace="5"/>
The Oxygen <strong>Artwork</strong> team provides a breath of fresh air on the desktop.
Nearly all the user-visible parts of the KDE desktop and applications have been given a
facelift. Beauty and consistency are two of the basic concepts behind Oxygen.
</p>



<h3>Desktop</h3>
<ul>
	<li>Plasma is the new desktop shell. Plasma provides a panel, a menu and other
	  intuitive means to interact with the desktop and applications.
	</li>
	<li>KWin, KDE's proven window manager, now supports advanced compositing features.
	  Hardware accelerated painting takes care of a smoother and more intuitive interaction
	  with windows.
	</li>
	<li>Oxygen is the KDE 4.0 artwork. Oxygen provides a consistent, easy on the eye and
	    beautiful artwork concept.
	</li>
</ul>
Learn more about KDE's new desktop interface in the <a href="guide.php">KDE 4.0 
Visual Guide</a>.

<h3>Applications</h3>
<ul>
	<li>Konqueror is KDE's established web browser. Konqueror is light-weight, well integrated,
	    and supports the newest standards such as CSS 3.</li>
	<li>Dolphin is KDE's new file manager. Dolphin has been developed with usability in mind 
		and is an easy-to-use, yet powerful tool.
	</li>
	<li>With System Settings, a new control center interface has been introduced. The
	    KSysGuard system monitor makes it easy to monitor and control system resources
	    and activity.
	</li>
	<li>Okular, the KDE 4 document viewer, supports many formats.
	    Okular is one of the many KDE 4 applications that has been improved
	    in collaboration with the <a href="http://openusability.org">OpenUsability Project</a>.
	</li>
	<li>Educational Applications are among the first applications that have been ported and
	    developed using KDE 4 technology. Kalzium, a graphical periodic table of elements
	    and the Marble Desktop Globe are only two of many gems among the educational
	    applications. Read more about Educational Applications in the 
		<a href="education.php">Visual Guide</a>.
	</li>
	<li>Lots of the KDE Games have been updated. KDE Games such as KMines, a minesweeper game
	    and KPat, a patience game have had facelifts. Thanks to new vector artwork and
	    graphical capabilities, the games have been made more resolution independent.
	</li>
</ul>
Some applications are introduced in more detail in the <a href="applications.php">KDE 4.0 
Visual Guide</a>.

<?php
screenshot("dolphin-systemsettings-kickoff_thumb.jpg", "dolphin-systemsettings-kickoff.jpg", "center",
	"Filemanager, System Settings and Menu in action", "375");
?>

<h3>Libraries</h3>
<p>
<ul>
	<li>Phonon offers applications multimedia capabilities such as playing audio and video.
	    Internally, Phonon makes use of various backends, switchable at runtime. 
		The default backend for KDE 4.0 will be the Xine backend supplying outstanding support 
		for various formats. Phonon also allows the user to choose output devices based on the 
		type of multimedia.
	</li>
	<li>The Solid hardware integration framework integrates fixed and removable devices
	    into KDE applications. Solid also interfaces with the underlying system's
	    power management capabilities, handles network connectivity and integration of
	    Bluetooth devices. Internally, Solid combines the powers of HAL, NetworkManager and
		the BlueZ bluetooth stack, but those components are replacable without breaking 
		applications to provide maximum portability.
	</li>
	<li>KHTML is the webpage rendering engine used by Konqueror, KDE's web browser. KHTML is
	    light-weight and supports modern standards such as CSS 3. KHTML was also the first
	    engine to pass the famous Acid 2 test. 
	</li>
	<li>The ThreadWeaver library, which comes with kdelibs, provides a high-level interface
	    to make better use of today's multi-core systems, making KDE applications feel smoother
	    and more efficiently using resources available on the system.
	</li>
	<li>Being built on Trolltech's Qt 4 library, KDE 4.0 can make use of the advanced visual
	    capabilities and smaller memory footprint of this library. kdelibs provides an outstanding
		extension of the Qt library, adding large amounts of high-level functionality and convenience to
		the developer.
	</li>
</ul>
</p>
<p>KDE's <a href="http://techbase.kde.org">TechBase</a> knowledge library has more information
about the KDE libraries.</p>


<h4>Take a guided tour...</h4>
<p>
The <a href="guide.php">KDE 4.0 Visual Guide</a> provides a quick overview of various new
and improved KDE 4.0 technologies. Illustrated with many screenshots, it walks you
through the different parts of KDE 4.0 and shows some of the exciting new technologies and
improvements for the user. New features of the <a href="desktop.php">desktop</a> get
you started, <a href="applications.php">applications</a> such as System Settings, Okular the
document viewer and Dolphin the file manager are introduced. 
<a href="education.php">Educational applications</a> are shown as well as 
<a href="games.php">Games</a>.
</p>


<h4>Give it a spin...</h4>
<p>
For those interested in getting packages to test and contribute, several
distributions have notified us that they will have KDE 4.0 packages available
at or soon after the release. The complete and current list can be found on the
<a href="http://www.kde.org/info/4.0.php">KDE 4.0 Info Page</a>, where you
can also find links to the source code, information about compiling, security
and other issues.
</p>
<p>
The following distributions have notified us of the availability of packages or Live CDs for
KDE 4.0:

<?php
include "../../info/binary-4.0.inc"
?>

</p>

<h2>About KDE 4</h2>
<p>
KDE 4.0 is the innovative Free Software desktop containing lots of applications
for every day use as well as for specific purposes. Plasma is a new desktop shell developed for
KDE 4, providing an intuitive interface to interact with the desktop and
applications. The Konqueror web browser integrates the web with the desktop. The Dolphin file manager,
the Okular document reader and the System Settings control center complete the basic desktop set.
<br />
KDE is built
on the KDE Libraries which provide easy access to resources on the network by means of KIO and
advanced visual capabilities through Qt4. Phonon and Solid, which are also part of the KDE Libraries
add a multimedia framework and better hardware integration to all KDE applications.
</p>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Press Contacts</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
