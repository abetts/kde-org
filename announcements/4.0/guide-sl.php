<?php
  $page_title = "Slikovni vodič po KDE 4.0";
  $site_root = "../../";
  include "header.inc";
  include "helperfunctions.inc";
?>
<p>
Namizje in programi v KDE 4.0 si zaslužijo podrobnejši ogled. Sledeče strani vsebujejo
ogled KDE 4.0 in primerov programov. Dodanih je mnogo zaslonskih posnetkov komponent.
Pomnite, da je predstavljen le del tega, kar vam ponuja KDE 4.0
</p>
<div align="left">
<table width="400" border="0" cellspacing="20" cellpadding="8">
<tr>
        <td width="32">
                <a href="desktop-sl.php"><img src="images/desktop-32.png" /></a>
        </td>
        <td>
                <a href="desktop-sl.php"><strong>Namizje</strong>: Plasma, KRunner, KickOff in KWin</a>
        </td>
</tr>
<tr>
        <td>
                <a href="applications-sl.php"><img src="images/applications-32.png" /></a>
        </td>
        <td>
                <a href="applications-sl.php"><strong>Programi</strong>: Dolphin, Okular, Gwenview, Sistemske nastavitve, Konzola</a>
        </td>
</tr>
<tr>
        <td>
                <a href="education-sl.php"><img src="images/education-32.png" /></a>
        </td>
        <td>
                <a href="education-sl.php"><strong>Izobraževalni programi</strong>: Kalzium, Parley, Marble, Blinken, KStars in 
KTouch</a>
        </td>
</tr>
<tr>
        <td>
                <a href="games-sl.php"><img src="images/games-32.png" /></a>
        </td>
        <td>
                <a href="games-sl.php"><strong>Igre</strong>: KGoldrunner, KFourInLine, LSkat, KJumpingCube, KSudoku in Konquest</a>
        </td>
</tr>
</table>
</div>
<p>
<br /><br /><br />
<em>Slikovni vodič sta sestavila Sebastian K&uuml;gler in Jos Poortvliet.</em><br />
<em>V slovenščino ga je prevedel Jure Repinc</em>
</p>
<?php
  include("footer.inc");
?>
