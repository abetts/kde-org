<?php
    include_once ("functions.inc");
    $translation_file = "www";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.67.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.67.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
February 02, 2020. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.67.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("General");?></h3>

<ul>
<li><?php i18n("Port away from many Qt 5.15 deprecated methods, this reduces the number of warnings during the build.");?></li>
</ul>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Migrate config from KConfig to KConfigXt in order to allow KCM to use it");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("create Breeze style Kate icon based on new design by Tyson Tan");?></li>
<li><?php i18n("Change VLC icon to be more like official VLC icons");?></li>
<li><?php i18n("add ktrip icon from ktrip repo");?></li>
<li><?php i18n("Add icon for application/sql");?></li>
<li><?php i18n("Cleanup and add 22px media repeat icons");?></li>
<li><?php i18n("Add icon for text/vnd.kde.kcrash-report");?></li>
<li><?php i18n("Turn application/x-ms-shortcut into an actual shortcut icon");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Add missing Import Env Variable");?></li>
<li><?php i18n("ECMAddAppIcon: Add sc in regex to extract extension from valid names");?></li>
<li><?php i18n("ECMAddQch: support &amp; document K_DOXYGEN macro usage");?></li>
</ul>

<h3><?php i18n("Framework Integration");?></h3>

<ul>
<li><?php i18n("Drop unused dependency QtDBus");?></li>
</ul>

<h3><?php i18n("KActivitiesStats");?></h3>

<ul>
<li><?php i18n("Fix broken SQL query in allResourcesQuery");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Remove files that Windows cannot handle");?></li>
<li><?php i18n("Ensure to store resource uri without a trailing slash");?></li>
</ul>

<h3><?php i18n("KDE Doxygen Tools");?></h3>

<ul>
<li><?php i18n("Unbreak module imports for Python2");?></li>
<li><?php i18n("Hardcode utf-8 as filesystem encoding with Python2 to help api.kde.org");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("prefer the new kcm plugins to the old");?></li>
<li><?php i18n("KCModuleQml: Ensure defaulted is emitted with the current configModule-&gt;representsDefaults on load");?></li>
<li><?php i18n("Show button respecting what is declared by KCModule");?></li>
<li><?php i18n("Update KPluginSelector to allow KCM to show good state for reset, apply and default button");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Refactor KConfigXT");?></li>
<li><?php i18n("Fix python bindings build after ebd14f29f8052ff5119bf97b42e61f404f223615");?></li>
<li><?php i18n("KCONFIG_ADD_KCFG_FILES: regenerate also on new version of kconfig_compiler");?></li>
<li><?php i18n("Allow to also pass a target instead of list of sources to KCONFIG_ADD_KCFG_FILES");?></li>
<li><?php i18n("Add KSharedConfig::openStateConfig for storing state information");?></li>
<li><?php i18n("Fix Python bindings compilation after 7ab8275bdb56882692846d046a5bbeca5795b009");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("KStandardAction: add method for SwitchApplicationLanguage action creation");?></li>
<li><?php i18n("[KColorSchemeManager] Don't list duplicates");?></li>
<li><?php i18n("[KColorschemeManager] Add option to reenable following global theme");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("demote plugin load errors from warning to debug level + reword");?></li>
<li><?php i18n("Document how to filter by servicetype the right way");?></li>
<li><?php i18n("Add perlSplit() overload taking a QRegularExpression and deprecate the QRegExp one");?></li>
<li><?php i18n("Add mime type for backtraces saved from DrKonqi");?></li>
<li><?php i18n("Add utility text function KShell::tildeCollapse");?></li>
<li><?php i18n("KPluginMetaData: add initialPreference() getter");?></li>
<li><?php i18n("desktoptojson: also convert InitialPreference key");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Correctly compute bottom margin for grid delegates with subtitles");?></li>
<li><?php i18n("[ConfigModule] Say which package is invalid");?></li>
</ul>

<h3><?php i18n("KHolidays");?></h3>

<ul>
<li><?php i18n("Update holidays and add flagdays and namedays for Sweden");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("ki18n_wrap_ui: error when file doesn't exist");?></li>
<li><?php i18n("[Kuit] Revert changes in parseUiMarker()");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Add missing renamed event when a destination file already existed");?></li>
<li><?php i18n("KFilePlacesModel: On new profile in recent show only recentlyused:/ based entries by default");?></li>
<li><?php i18n("Add KFileCustomDialog constructor with a startDir parameter");?></li>
<li><?php i18n("Fix QRegularExpression::wildcardToRegularExpression() usage");?></li>
<li><?php i18n("Allow to handle apps with Terminal=True in their desktop file, handle their associated mimetype properly (bug 410506)");?></li>
<li><?php i18n("KOpenWithDialog: Allow to return a newly created KService created associated to a mimetype");?></li>
<li><?php i18n("Add KIO::DropJobFlag to allow manually showing the menu (bug 415917)");?></li>
<li><?php i18n("[KOpenWithDialog] Hide collapsible group box when all options inside are hidden (bug 415510)");?></li>
<li><?php i18n("Revert effective removal of KUrlPixmapProvider from API");?></li>
<li><?php i18n("SlaveBase::dispatchLoop: Fix timeout calculation (bug 392768)");?></li>
<li><?php i18n("[KDirOperator] Allow renaming files from the context menu (bug 189482)");?></li>
<li><?php i18n("Upstream Dolphin's file rename dialog (bug 189482)");?></li>
<li><?php i18n("KFilePlaceEditDialog: move logic into isIconEditable()");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("Clip the flickable parent item (bug 416877)");?></li>
<li><?php i18n("Remove header top margin from private ScrollView");?></li>
<li><?php i18n("proper size hint for the gridlayout (bug 416860)");?></li>
<li><?php i18n("use attached property for isCurrentPage");?></li>
<li><?php i18n("Get rid of a couple of warnings");?></li>
<li><?php i18n("try to keep the cursor in window when typing in an OverlaySheet");?></li>
<li><?php i18n("properly expand fillWidth items in mobile mode");?></li>
<li><?php i18n("Add active, link, visited, negative, neutral and positive background colors");?></li>
<li><?php i18n("Expose ActionToolBar's overflow button icon name");?></li>
<li><?php i18n("Use QQC2 Page as base for Kirigami Page");?></li>
<li><?php i18n("Specify where the code is coming from as the URL");?></li>
<li><?php i18n("Don't anchor AbstractApplicationHeader blindly");?></li>
<li><?php i18n("emit pooled after the properties have been reassigned");?></li>
<li><?php i18n("add reused and pooled signals like TableView");?></li>
</ul>

<h3><?php i18n("KJS");?></h3>

<ul>
<li><?php i18n("Abort machine run once a timeout signal has been seen");?></li>
<li><?php i18n("Support ** exponentiation operator from ECMAScript 2016");?></li>
<li><?php i18n("Added shouldExcept() function that works based on a function");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Unbreak the KNSQuick::Engine::changedEntries functionality");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("Add new signal for default action activation");?></li>
<li><?php i18n("Drop dependency to KF5Codecs by using the new stripRichText function");?></li>
<li><?php i18n("Strip richtext on Windows");?></li>
<li><?php i18n("Adapt to Qt 5.14 Android changes");?></li>
<li><?php i18n("Deprecate raiseWidget");?></li>
<li><?php i18n("Port KNotification from KWindowSystem");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Adjust metainfo.yaml to new tier");?></li>
<li><?php i18n("Remove legacy plugin loading code");?></li>
</ul>

<h3><?php i18n("KQuickCharts");?></h3>

<ul>
<li><?php i18n("Fix Qt version check");?></li>
<li><?php i18n("Register QAbstractItemModel as anonymous type for property assignments");?></li>
<li><?php i18n("Hide the line of a line chart if its width is set to 0");?></li>
</ul>

<h3><?php i18n("Kross");?></h3>

<ul>
<li><?php i18n("addHelpOption already adds by kaboutdata");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Support multiple values in XDG_CURRENT_DESKTOP");?></li>
<li><?php i18n("Deprecate allowAsDefault");?></li>
<li><?php i18n("Make \"Default Applications\" in mimeapps.list the preferred applications (bug 403499)");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Revert \"improve word completion to use highlighting to detect word boundaries\" (bug 412502)");?></li>
<li><?php i18n("import final breeze icon");?></li>
<li><?php i18n("Message-related methods: Use more member-function-pointer-based connect");?></li>
<li><?php i18n("DocumentPrivate::postMessage: avoid multiple hash lookups");?></li>
<li><?php i18n("fix Drag&amp;copy function (by using Ctrl Key) (bug 413848)");?></li>
<li><?php i18n("ensure we have a quadratic icon");?></li>
<li><?php i18n("set proper Kate icon in about dialog for KatePart");?></li>
<li><?php i18n("inline notes: correctly set underMouse() for inline notes");?></li>
<li><?php i18n("avoid use of old mascot ATM");?></li>
<li><?php i18n("Variable expansion: Add variable PercentEncoded (bug 416509)");?></li>
<li><?php i18n("Fix crash in variable expansion (used by external tools)");?></li>
<li><?php i18n("KateMessageWidget: remove unused event filter installation");?></li>
</ul>

<h3><?php i18n("KTextWidgets");?></h3>

<ul>
<li><?php i18n("Drop KWindowSystem dependency");?></li>
</ul>

<h3><?php i18n("KWallet Framework");?></h3>

<ul>
<li><?php i18n("Revert readEntryList() to use QRegExp::Wildcard");?></li>
<li><?php i18n("Fix QRegularExpression::wildcardToRegularExpression() usage");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("[KMessageWidget] Subtract the correct margin");?></li>
<li><?php i18n("[KMessageBox] Only allow selecting text in the dialog box using the mouse (bug 416204)");?></li>
<li><?php i18n("[KMessageWidget] Use devicePixelRatioF for animation pixmap (bug 415528)");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("[KWindowShadows] Check for X connection");?></li>
<li><?php i18n("Introduce shadows API");?></li>
<li><?php i18n("Deprecate KWindowEffects::markAsDashboard()");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("Use KStandardAction convenience method for switchApplicationLanguage");?></li>
<li><?php i18n("Allow programLogo property to be a QIcon, too");?></li>
<li><?php i18n("Remove ability to report bugs against arbitrary stuff from a static list");?></li>
<li><?php i18n("Remove compiler information from bug report dialog");?></li>
<li><?php i18n("KMainWindow: fix autoSaveSettings to catch QDockWidgets being shown again");?></li>
<li><?php i18n("i18n: Add more semantic context strings");?></li>
<li><?php i18n("i18n: Split translations for strings \"Translation\"");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Fixed tooltip corners and removed useless color attributes");?></li>
<li><?php i18n("Removed hardcoded colors in background SVGs");?></li>
<li><?php i18n("Fix the size and pixel alignment of checkboxes and radiobuttons");?></li>
<li><?php i18n("Update breeze theme shadows");?></li>
<li><?php i18n("[Plasma Quick] Add WaylandIntegration class");?></li>
<li><?php i18n("Same behavior for scrollbar as the desktop style");?></li>
<li><?php i18n("Make use of KPluginMetaData where we can");?></li>
<li><?php i18n("Add edit mode menu item to desktop widget context menu");?></li>
<li><?php i18n("Consistency: colored selected buttons");?></li>
<li><?php i18n("Port endl to \n Not necessary to flush as QTextStream uses QFile which flush when it's deleted");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("Fix QRegularExpression::wildcardToRegularExpression() usage");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Remove scrollbar related workarounds from list delegates");?></li>
<li><?php i18n("[TabBar] Remove frame");?></li>
<li><?php i18n("Add active, link, visited, negative, neutral and positive background colors");?></li>
<li><?php i18n("use hasTransientTouchInput");?></li>
<li><?php i18n("always round x and y");?></li>
<li><?php i18n("support mobile mode scrollbar");?></li>
<li><?php i18n("ScrollView: Do not overlay scrollbars over contents");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Add signals for udev events with actions bind and unbind");?></li>
<li><?php i18n("Clarify referencing of DeviceInterface (bug 414200)");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Updates nasm.xml with the latest instructions");?></li>
<li><?php i18n("Perl: Add 'say' to keyword list");?></li>
<li><?php i18n("cmake: Fix <code>CMAKE_POLICY_*_CMP&amp;lt;N&amp;gt;</code> regex and add special args to <code>get_cmake_property</code>");?></li>
<li><?php i18n("Add GraphQL highlighting definition");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.67");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.12");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
