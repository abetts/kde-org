<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.10.0");
  $site_root = "../";
  $release = '5.10.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
May 08, 2015. KDE today announces the release
of KDE Frameworks 5.10.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("(no changelog provided)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Generate QML-proof classes using the kconfigcompiler");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("New cmake macro kcoreaddons_add_plugin to create KPluginLoader-based plugins more easily.");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("Fix crash in texture cache.");?></li>
<li><?php i18n("and other fixes");?></li>
</ul>

<h3><?php i18n("KGlobalAccel");?></h3>

<ul>
<li><?php i18n("Add new method globalShortcut which retrieves the shortcut as defined in global settings.");?></li>
</ul>

<h3><?php i18n("KIdleTime");?></h3>

<ul>
<li><?php i18n("Prevent kidletime from crashing on platform wayland");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Added KPropertiesDialog::KPropertiesDialog(urls) and KPropertiesDialog::showDialog(urls).");?></li>
<li><?php i18n("Asynchronous QIODevice-based data fetch for KIO::storedPut and KIO::AccessManager::put.");?></li>
<li><?php i18n("Fix conditions with QFile::rename return value (bug 343329)");?></li>
<li><?php i18n("Fixed KIO::suggestName to suggest better names (bug 341773)");?></li>
<li><?php i18n("kioexec: Fixed path for writeable location for kurl (bug 343329)");?></li>
<li><?php i18n("Store bookmarks only in user-places.xbel (bug 345174)");?></li>
<li><?php i18n("Duplicate RecentDocuments entry if two different files have the same name");?></li>
<li><?php i18n("Better error message if a single file is too large for the trash (bug 332692)");?></li>
<li><?php i18n("Fix KDirLister crash upon redirection when the slot calls openURL");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("New set of classes, called KMoreTools and related. KMoreTools helps to add hints about external tools which are potentially not yet installed. Furthermore, it makes long menus shorter by providing a main and more section which is also user-configurable.");?></li>
</ul>

<h3><?php i18n("KNotifications");?></h3>

<ul>
<li><?php i18n("Fix KNotifications when used with Ubuntu's NotifyOSD (bug 345973)");?></li>
<li><?php i18n("Don't trigger notification updates when setting the same properties (bug 345973)");?></li>
<li><?php i18n("Introduce LoopSound flag allowing notifications to play sound in a loop if they need it (bug 346148)");?></li>
<li><?php i18n("Don't crash if notification doesn't have a widget");?></li>
</ul>

<h3><?php i18n("KPackage");?></h3>

<ul>
<li><?php i18n("Add a KPackage::findPackages function similar to KPluginLoader::findPlugins");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Use KPluginFactory for instantiating the plugins, instead of KService (kept for compatibility).");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("Fix wrong splitting of entry path (bug 344614)");?></li>
</ul>

<h3><?php i18n("KWallet");?></h3>

<ul>
<li><?php i18n("Migration agent now also check old wallet is empty before starting (bug 346498)");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("KDateTimeEdit: Fix so user input actually gets registered. Fix double margins.");?></li>
<li><?php i18n("KFontRequester: fix selecting monospaced fonts only");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Don't depend on QX11Info in KXUtils::createPixmapFromHandle (bug 346496)");?></li>
<li><?php i18n("new method NETWinInfo::xcbConnection() -&gt; xcb_connection_t*");?></li>
</ul>

<h3><?php i18n("KXmlGui");?></h3>

<ul>
<li><?php i18n("Fix shortcuts when secondary shortcut set (bug 345411)");?></li>
<li><?php i18n("Update list of bugzilla products/components for bug reporting (bug 346559)");?></li>
<li><?php i18n("Global shortcuts: allow configuring also the alternate shortcut");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("The installed headers are now organized like all other frameworks.");?></li>
</ul>

<h3><?php i18n("Plasma framework");?></h3>

<ul>
<li><?php i18n("PlasmaComponents.Menu now supports sections");?></li>
<li><?php i18n("Use KPluginLoader instead of ksycoca for loading C++ dataengines");?></li>
<li><?php i18n("Consider visualParent rotation in popupPosition (bug 345787)");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Don't try to highlight if there is no spell checker found. This would lead to an infinite loop with rehighlighRequest timer firing constanty.");?></li>
</ul>

<h3><?php i18n("Frameworkintegration");?></h3>

<ul>
<li><?php i18n("Fix native file dialogs from widgets QFileDialog:
** File dialogs opened with exec() and without parent were opened, but any user-interaction was blocked in a way that no file could be selected nor the dialog closed.
** File dialogs opened with open() or show() with parent were not opened at all.");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.10");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.2");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
