
<h3>
<a href="plasma-et.php">
Plasma töötsoonid annavad juhtohjad kasutaja kätte
</a>
</h3>

<p>
<a href="plasma-et.php">
<img src="images/plasma.png" class="app-icon" alt="KDE Plasma töötsoonid 4.6.0" width="64" height="64" />
</a>

<b>KDE Plasma töötsoonidele</b> lisandus uus tegevuste süsteem, mille abil on palju hõlpsam seostada rakendusi konkreetse tegevusega, näiteks tööülesannete või koduste tegemistega. Uuendatud toitehaldus pakub uusi võimalusi, aga ka senisest lihtsamat seadistamist. Plasma töötsooni aknahaldur KWin sai skriptide kasutamise võimaluse ning töötsoonid tervikuna mitmeid visuaalseid parandusi. Mobiilsetele seadmetele kohandatud <b>Plasma Netbook</b> on senisest tunduvalt kiirem ning seda on lihtsam kasutada ka puuteekraaniga seadmete puhul. Täpsemalt kõneleb kõigest <a href="plasma-et.php">KDE Plasma töötsoonide 4.6 teadaanne</a>.
</p>
