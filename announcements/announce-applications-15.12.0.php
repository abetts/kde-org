<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("KDE Ships KDE Applications 15.12.0");
  $site_root = "../";
  $release = "applications-15.12.0";
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<p align="justify">
<?php print i18n_var("December 16, 2015. Today KDE released KDE Applications 15.12.");?>
</p>


<p align="justify">
<?php print i18n_var("KDE is excited to announce the release of KDE Applications 15.12, the December 2015 update to KDE Applications. This release brings one new application and feature additions and bug fixes across the board to existing applications. The team strives to always bring the best quality to your desktop and these applications, so we're counting on you to send your feedback.");?>
</p>

<p align="justify">
<?php print i18n_var("In this release, we've updated a whole host of applications to use the new <a href='%1'>KDE Frameworks 5</a>, including <a href='%2'>Artikulate</a>, <a href='%3'>Krfb</a>, <a href='%4'>KSystemLog</a>, <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> and more of the KDE Games, apart from the KDE Image Plugin Interface and its support libraries. This brings the total number of applications that use KDE Frameworks 5 to 126.", "https://dot.kde.org/2013/09/25/frameworks-5", "https://edu.kde.org/applications/all/artikulate/", "https://www.kde.org/applications/system/krfb/", "https://www.kde.org/applications/system/ksystemlog/");?>
</p>

<h3 style="clear:both;" ><?php print i18n_var("A Spectacular New Addition");?></h3>
<p align="justify">
<?php print i18n_var("After 14 years of being a part of KDE, KSnapshot has been retired and replaced with a new screenshot application, Spectacle.");?>
</p>
<p align="justify">
<?php print i18n_var("With new features and a completely new UI, Spectacle makes taking screenshots as easy and unobtrusive as it can ever be. In addition to what you could do with KSnapshot, with Spectacle you can now take composite screenshots of pop-up menus along with their parent windows, or take screenshots of the entire screen (or the currently active window) without even starting Spectacle, simply by using the keyboard shortcuts Shift+PrintScreen and Meta+PrintScreen respectively.");?>
</p>
<p align="justify">
<?php print i18n_var("We've also aggressively optimised application start-up time, to absolutely minimise the time lag between when you start the application and when the image is captured.");?>
</p>

<h3><?php print i18n_var("Polish Everywhere");?></h3>
<p align="justify">
<?php print i18n_var("Many of the applications have undergone significant polishing in this cycle, in addition to stability and bug fixes.");?>
</p>

<figure style="float: right; margin: 0px">
<a href="https://kdenlive.org/sites/kdenlive.org/data/kdenlive_15-12.png">
<img src="https://kdenlive.org/sites/kdenlive.org/data/kdenlive_15-12.png" width="400" height="225" />
</a>
<figcaption style="word-wrap: normal"><center><?php print i18n_var("Kdenlive with its polished User Interface");?></center></figcaption>
</figure>

<p align="justify">
<?php print i18n_var("<a href='%1'>Kdenlive</a>, the non-linear video editor, has seen major fixes to its User Interface. You can now copy and paste items on your timeline, and easily toggle transparency in a given track. The icon colours automatically adjust to the main UI theme, making them easier to see.", "https://userbase.kde.org/Kdenlive");?>
</p>

<p style="clear:both;" />
<figure style="float: right; margin: 0px">
<a href="ark1512.png">
<img src="ark1512.png" width="400" height="299" />
</a>
<figcaption style="word-wrap: normal"><center><?php print i18n_var("Ark can now show ZIP comments");?></center></figcaption>
</figure>

<p align="justify">
<?php print i18n_var("<a href='%1'>Ark</a>, the archive manager, can now display comments embedded in ZIP and RAR archives. We've improved support for Unicode characters in file names in ZIP archives, and you can now detect corrupt archives and recover as much data as possible from them. You can also open archived files in their default application, and we've fixed drag-and-drop to the desktop as well previews for XML files.", "https://www.kde.org/applications/utilities/ark/");?>

<h3 style="clear:both;"><?php print i18n_var("All Play and No Work");?></h3>

<figure style="float: right; margin: 0px">
<a href="ksudoku1512.png">
<img src="ksudoku1512.png" width="400" height="287" />
</a>
<figcaption style="word-wrap: normal"><center><?php print i18n_var("KSudoku new Welcome Screen (on a Mac OS X)");?></center></figcaption>
</figure>

<p align="justify">
<?php print i18n_var("The KDE Games developers have been working hard for the past few months to optimise our games for a smoother and richer experience, and we've got a lot of new stuff in this area for our users to enjoy.");?>
</p>

<p align="justify">
<?php print i18n_var("In <a href='%1'>KGoldrunner</a>, we've added two new sets of levels, one allowing digging while falling and one not. We've added solutions for several existing sets of levels. For an added challenge, we now disallow digging while falling in some older sets of levels.", "https://www.kde.org/applications/games/kgoldrunner/");?>
</p>

<p align="justify">
<?php print i18n_var("In <a href='%1'>KSudoku</a>, you can now print Mathdoku and Killer Sudoku puzzles. The new multi-column layout in KSudoku's Welcome Screen makes it easier to see more of the many puzzle types available, and the columns adjust themselves automatically as the window is resized. We've done the same to Palapeli, making it easier to see more of your jigsaw puzzle collection at once.", "https://www.kde.org/applications/games/ksudoku/");?>
</p>

<p align="justify" style="clear:both;">
<?php print i18n_var("We've also included stability fixes for games like KNavalBattle, Klickety, <a href='%1'>KShisen</a> and <a href='%2'>KTuberling</a>, and the overall experience is now greatly improved. KTuberling, Klickety and KNavalBattle have also been updated to use the new KDE Frameworks 5.", "https://www.kde.org/applications/games/kshisen/", "https://www.kde.org/applications/games/ktuberling/");?>
</p>

<h3 style="clear:both;"><?php print i18n_var("Important Fixes");?></h3>

<p align="justify">
<?php print i18n_var("Don't you simply hate it when your favorite application crashes at the most inconvinient time? We do too, and to remedy that we've worked very hard at fixing lots of bugs for you, but probably we've left some sneak, so don't forget to <a href='%1'>report them</a>!", "https://bugs.kde.org");?>
</p>

<p align="justify">
<?php print i18n_var("In our file manager <a href='%1'>Dolphin</a>, we've included various fixes for stability and some fixes to make scrolling smoother. In <a href='%2'>Kanagram</a>, we've fixed a bothersome issue with white text on white backgrounds. In <a href='%3'>Kate</a>, we've attempted to fix a crash that was occurring on shutdown, in addition to cleaning up the UI and adding in a new cache cleaner.", "https://www.kde.org/applications/system/dolphin/", "https://www.kde.org/applications/education/kanagram/", "https://www.kde.org/applications/utilities/kate/");?>
</p>

<p align="justify">
<?php print i18n_var("The <a href='%1'>Kontact Suite</a> has seen tons of added features, big fixes and performance optimisations. In fact, there's been so much development this cycle that we've bumped the version number to 5.1. The team is hard at work, and is looking forward to all your feedback.", "https://userbase.kde.org/Kontact");?>
</p>

<h3 style="clear:both;"><?php print i18n_var("Moving Forward");?></h3>

<p align="justify">
<?php print i18n_var("As part of the effort to modernise our offerings, we've dropped some applications from KDE Applications and are no longer releasing them as of KDE Applications 15.12");?>
</p>

<p align="justify">
<?php print i18n_var("We've dropped 4 applications from the release - Amor, KTux, KSnapshot and SuperKaramba. As noted above, KSnapshot has been replaced by Spectacle, and Plasma essentially replaces SuperKaramba as a widget engine. We've dropped standalone screensavers because screen locking is handled very differently in modern desktops.");?>
</p>

<p align="justify">
<?php print i18n_var("We've also dropped 3 artwork packages (kde-base-artwork, kde-wallpapers and kdeartwork); their content had not changed in long time.");?>
</p>

<h3 style="clear:both;"><?php print i18n_var("Full Changelog");?></h3>

<p align="justify">
<?php print i18n_var("You can find the full list of changes <a href='%1'>here</a>.", "fulllog_applications-15.12.0.php");?>
</p>

<h4>
<?php i18n("Spread the Word");?>
</h4>
<p align="justify">
<?php print i18n_var("Non-technical contributors are an important part of KDE’s success. While proprietary software companies have huge advertising budgets for new software releases, KDE depends on people talking with other people. Even for those who are not software developers, there are many ways to support the KDE Applications 15.12 release. Report bugs. Encourage others to join the KDE Community. Or <a href='%1'>support the nonprofit organization behind the KDE community</a>.", "https://relate.kde.org/civicrm/contribute/transact?reset=1&id=5"); ?>
</p>

<p align="justify">
<?php i18n("Please spread the word on the Social Web. Submit stories to news sites, use channels like delicious, digg, reddit, and twitter. Upload screenshots of your new set-up to services like Facebook, Flickr, ipernity and Picasa, and post them to appropriate groups. Create screencasts and upload them to YouTube, Blip.tv, and Vimeo. Please tag posts and uploaded materials with 'KDE'. This makes them easy to find, and gives the KDE Promo Team a way to analyze coverage for the KDE Applications 15.12 release."); ?>
</p>

<!-- // Boilerplate again -->

<h4>
  <?php i18n("Installing KDE Applications 15.12 Binary Packages");?>
</h4>
<p align="justify">
  <em><?php i18n("Packages");?></em>.
  <?php i18n("Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 15.12 for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.");?>
</p>

<p align="justify">
  <a name="package_locations"></a><em><?php i18n("Package Locations");?></em>.
  <?php i18n("For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/KDE_Applications/Binary_Packages'>Community Wiki</a>.");?>
</p>

<h4>
  <?php i18n("Compiling KDE Applications 15.12");?>
</h4>
<p align="justify">
  <a name="source_code"></a>
  <?php i18n("The complete source code for KDE Applications 15.12 may be <a href='http://download.kde.org/stable/applications/15.12.0/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications-15.12.0.php'>KDE Applications 15.12.0 Info Page</a>.");?>
</p>

<h4>
  <?php i18n("Supporting KDE");?>
</h4>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4><?php i18n("Press Contacts");?></h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
