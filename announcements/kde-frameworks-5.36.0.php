<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.36.0");
  $site_root = "../";
  $release = '5.36.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="//dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
July 08, 2017. KDE today announces the release
of KDE Frameworks 5.36.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<p>Changelog for KDE Frameworks 5.36.0</p>

<p>All frameworks: Option to build &amp; install QCH file with the public API dox</p>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Use FindInotify.cmake to decide whether inotify is available");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Do not depend on bash unnecessarily, and do not validate icons by default");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("FindQHelpGenerator: avoid picking up Qt4 version");?></li>
<li><?php i18n("ECMAddQch: fail hard if needed tools are not present, to avoid surprises");?></li>
<li><?php i18n("Drop perl as dep for ecm_add_qch, not needed/used");?></li>
<li><?php i18n("Scan the whole install folder for qml dependencies");?></li>
<li><?php i18n("New: ECMAddQch, for generating qch &amp; doxygen tag files");?></li>
<li><?php i18n("Fix KDEInstallDirsTest.relative_or_absolute_usr, avoid Qt paths being used");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Check error status after every PolKitAuthority usage");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Emit errors when keditbookmarks is missing (bug 303830)");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Fix for CMake 3.9");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Use FindInotify.cmake to decide whether inotify is available");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("KKeySequenceItem: make it possible to record Ctrl+Num+1 as a shortcut");?></li>
<li><?php i18n("Start drag with press and hold on touch events (bug 368698)");?></li>
<li><?php i18n("Don't rely on QQuickWindow delivering QEvent::Ungrab as mouseUngrabEvent (as it no longer does in Qt 5.8+) (bug 380354)");?></li>
</ul>

<h3><?php i18n("KDELibs 4 Support");?></h3>

<ul>
<li><?php i18n("Search for KEmoticons, which is a dependency per the CMake config.cmake.in (bug 381839)");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("Add an extractor using qtmultimedia");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("Make sure that the tsfiles target is generated");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("More details about deploying icon themes on Mac &amp; MSWin");?></li>
<li><?php i18n("Change panel icon size default to 48");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("[KNewFileMenu] Hide \"Link To Device\" menu if it would be empty (bug 381479)");?></li>
<li><?php i18n("Use KIO::rename instead of KIO::moveAs in setData (bug 380898)");?></li>
<li><?php i18n("Fix drop menu position on Wayland");?></li>
<li><?php i18n("KUrlRequester: Set NOTIFY signal to textChanged() for text property");?></li>
<li><?php i18n("[KOpenWithDialog] HTML-escape file name");?></li>
<li><?php i18n("KCoreDirLister::cachedItemForUrl: don't create the cache if it didn't exist");?></li>
<li><?php i18n("Use \"data\" as filename when copying data urls (bug 379093)");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Fix incorrect error detection for missing knsrc files");?></li>
<li><?php i18n("Expose and use Engine's page size variable");?></li>
<li><?php i18n("Make it possible to use QXmlStreamReader to read a KNS registry file");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("Added kpackage-genericqml.desktop");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Fix cpu usage spiking after showing vi command bar (bug 376504)");?></li>
<li><?php i18n("Fix jumpy scrollbar-dragging when mini-map is enabled");?></li>
<li><?php i18n("Jump to the clicked scrollbar position when minim-map is enabled (bug 368589)");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Update kcharselect-data to Unicode 10.0");?></li>
</ul>

<h3><?php i18n("KXMLGUI");?></h3>

<ul>
<li><?php i18n("KKeySequenceWidget: make it possible to record Ctrl+Num+1 as a shortcut (bug 183458)");?></li>
<li><?php i18n("Revert \"When building menu hyerarchies, parent menus to their containers\"");?></li>
<li><?php i18n("Revert \"use transientparent directly\"");?></li>
</ul>

<h3><?php i18n("NetworkManagerQt");?></h3>

<ul>
<li><?php i18n("WiredSetting: wake on lan properties were backported to NM 1.0.6");?></li>
<li><?php i18n("WiredSetting: metered property was backported to NM 1.0.6");?></li>
<li><?php i18n("Add new properties to many settings classes");?></li>
<li><?php i18n("Device: add device statistics");?></li>
<li><?php i18n("Add IpTunnel device");?></li>
<li><?php i18n("WiredDevice: add information about required NM version for s390SubChannels property");?></li>
<li><?php i18n("TeamDevice: add new config property (since NM 1.4.0)");?></li>
<li><?php i18n("Wired device: add s390SubChannels property");?></li>
<li><?php i18n("Update introspections (NM 1.8.0)");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("Make sure size is final after showEvent");?></li>
<li><?php i18n("Fix vlc tray icon margins and color scheme");?></li>
<li><?php i18n("Set Containments to have focus within the view (bug 381124)");?></li>
<li><?php i18n("generate the old key before updating enabledborders (bug 378508)");?></li>
<li><?php i18n("show show password button also if empty text (bug 378277)");?></li>
<li><?php i18n("Emit usedPrefixChanged when prefix is empty");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("cmake: build udisks2 backend on FreeBSD only when enabled");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Highlight .julius files as JavaScript");?></li>
<li><?php i18n("Haskell: Add all language pragmas as keywords");?></li>
<li><?php i18n("CMake: OR/AND not highlighted after expr in () (bug 360656)");?></li>
<li><?php i18n("Makefile: Remove invalid keyword entries in makefile.xml");?></li>
<li><?php i18n("indexer: Improve error reporting");?></li>
<li><?php i18n("HTML syntax file version update");?></li>
<li><?php i18n("Angular modifiers in HTML attributes added");?></li>
<li><?php i18n("Update test reference data following the changes of the previous commit");?></li>
<li><?php i18n("Bug 376979 - angle brackets in doxygen comments break syntax highlighting");?></li>
</ul>

<h3><?php i18n("ThreadWeaver");?></h3>

<ul>
<li><?php i18n("Work-around MSVC2017 compiler bug");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.36");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.6");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
