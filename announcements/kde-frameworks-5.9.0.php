<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("Release of KDE Frameworks 5.9.0");
  $site_root = "../";
  $release = '5.9.0';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="http://dot.kde.org/sites/dot.kde.org/files/KDE_QT.jpg" width="320" height="176" style="float: right" />

<p><?php i18n(" 
April 10, 2015. KDE today announces the release
of KDE Frameworks 5.9.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are 60 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see <a
href='http://kde.org/announcements/kde-frameworks-5.0.php'>the
Frameworks 5.0 release announcement</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<p><?php i18n("New module: ModemManagerQt (Qt wrapper for ModemManager API)");?></p>

<p><?php print i18n_var("Note that Plasma-NM 5.2.x needs a patch in order to build and work with this version of ModemManagerQt. <a href='%1'>plasma-nm patch</a>.", "http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commitdiff&amp;h=4d72cb7966edda33bc72c77fc2a126844fc1f134");?> </p>

<p><?php i18n("Alternatively, upgrade to Plasma-NM 5.3 Beta when upgrading to ModemManagerQt 5.9.0.");?></p>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Implemented forgetting a resource");?></li>
<li><?php i18n("Build fixes");?></li>
<li><?php i18n("Added a plugin to register events for KRecentDocument notifications");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Respect KZip::extraField setting also when writing central header entries");?></li>
<li><?php i18n("Remove two erroneous asserts, happening when disk is full, bug 343214");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Fix build with Qt 5.5");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Use new json-based plugin system. KCMs are searched under kcms/. For now a desktop file still needs to be installed under kservices5/ for compatibility");?></li>
<li><?php i18n("Load and wrap the QML-only version of kcms if possible");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Fix assert when using KSharedConfig in a global object destructor.");?></li>
<li><?php i18n("kconfig_compiler: add support for CategoryLoggingName in *.kcfgc files, to generate qCDebug(category) calls.");?></li>
</ul>

<h3><?php i18n("KI18n");?></h3>

<ul>
<li><?php i18n("preload the global Qt catalog when using i18n()");?></li>
</ul>

<h3><?php i18n("KIconThemes");?></h3>

<ul>
<li><?php i18n("KIconDialog can now be shown using the regular QDialog show() and exec() methods");?></li>
<li><?php i18n("Fix KIconEngine::paint to handle different devicePixelRatios");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("Enable KPropertiesDialog to show free space information of remote file systems as well (e.g. smb)");?></li>
<li><?php i18n("Fix KUrlNavigator with high DPI pixmaps");?></li>
<li><?php i18n("Make KFileItemDelegate handle non default devicePixelRatio in animations");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("KRecursiveFilterProxyModel: reworked to emit the right signals at the right time");?></li>
<li><?php i18n("KDescendantsProxyModel: Handle moves reported by the source model.");?></li>
<li><?php i18n("KDescendantsProxyModel: Fix behavior when a selection is made while resetting.");?></li>
<li><?php i18n("KDescendantsProxyModel: Allow constructing and using KSelectionProxyModel from QML.");?></li>
</ul>

<h3><?php i18n("KJobWidgets");?></h3>

<ul>
<li><?php i18n("Propagate error code to JobView DBus interface");?></li>
</ul>

<h3><?php i18n("KNotifications");?></h3>

<ul>
<li><?php i18n("Added an event() version that takes no icon and will use a default one");?></li>
<li><?php i18n("Added an event() version that takes StandardEvent eventId and QString iconName");?></li>
</ul>

<h3><?php i18n("KPeople");?></h3>

<ul>
<li><?php i18n("Allow extending action metadata by using predefined types");?></li>
<li><?php i18n("Fix model not being properly updated after removing a contact from Person");?></li>
</ul>

<h3><?php i18n("KPty");?></h3>

<ul>
<li><?php i18n("Expose to world whether KPty has been built with utempter library");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("Add kdesrc-buildrc highlighting file");?></li>
<li><?php i18n("syntax: added support for binary integer literals in the PHP highlighting file");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Make KMessageWidget animation smooth with high Device Pixel Ratio");?></li>
</ul>

<h3><?php i18n("KWindowSystem");?></h3>

<ul>
<li><?php i18n("Add a dummy Wayland implementation for KWindowSystemPrivate");?></li>
<li><?php i18n("KWindowSystem::icon with NETWinInfo not bound to platform X11.");?></li>
</ul>

<h3><?php i18n("KXmlGui");?></h3>

<ul>
<li><?php i18n("Preserve translation domain when merging .rc files");?></li>
<li><?php i18n("Fix runtime warning QWidget::setWindowModified: The window title does not contain a '[*]' placeholder");?></li>
</ul>

<h3><?php i18n("KXmlRpcClient");?></h3>

<ul>
<li><?php i18n("Install translations");?></li>
</ul>

<h3><?php i18n("Plasma framework");?></h3>

<ul>
<li><?php i18n("Fixed stray tooltips when temporary owner of tooltip disappeared or became empty");?></li>
<li><?php i18n("Fix TabBar not properly laid out initially, which could be observed in eg. Kickoff");?></li>
<li><?php i18n("PageStack transitions now use Animators for smoother animations");?></li>
<li><?php i18n("TabGroup transitions now use Animators for smoother animations");?></li>
<li><?php i18n("Make Svg,FrameSvg work qith QT_DEVICE_PIXELRATIO");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Refresh the battery properties upon resume");?></li>
</ul>

<h3><?php i18n("Buildsystem changes");?></h3>

<ul>
<li><?php i18n("Extra CMake Modules (ECM) is now versioned like KDE Frameworks, therefore it is now 5.9, while it was 1.8 previously.");?></li>
<li><?php i18n("Many frameworks have been fixed to be useable without searching for their private dependencies.
I.e. applications looking up a framework only need its public dependencies, not the private ones.");?></li>
<li><?php i18n("Allow configuration of SHARE_INSTALL_DIR, to handle multi-arch layouts better");?></li>
</ul>

<h3><?php i18n("Frameworkintegration");?></h3>

<ul>
<li><?php i18n("Fix possible crash when destroying a QSystemTrayIcon (triggered by e.g. Trojita), bug 343976");?></li>
<li><?php i18n("Fix native modal file dialogs in QML, bug 334963");?></li>
</ul>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Binary package distro install instructions</a>.<br />
", "http://community.kde.org/Frameworks/Binary_Packages");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.9");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.2");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>review board</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://git.reviewboard.kde.org/groups/kdeframeworks/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<p><?php print i18n_var("You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.");?>
</p>
<!-- // Boilerplate again -->
<h4>
  <?php i18n("Supporting KDE");?>
</h4>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our new <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h4><?php i18n("Press Contacts");?></h4>
<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
