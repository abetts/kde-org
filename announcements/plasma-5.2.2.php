<?php
  include_once ("functions.inc");
  $translation_file = "www";
  $page_title = i18n_noop("KDE Ships Plasma 5.2.2, Bugfix Release for March");
  $site_root = "../";
  $release = 'plasma-5.2.2';
  include "header.inc";
?>

<?php
  include "./announce-i18n-bar.inc";
?>

<div style="text-align: center; float: right">
<a href="plasma-5.2/full-screen.png">
<img src="plasma-5.2/full-screen-wee2.png" style="padding: 1ex; border: 0; background-image: none; " width="400" height="226" alt="<?php i18n("Plasma 5.2");?>" />
</a>
</div>

<p>
<?php i18n("Tue, 24 Mar 2015.
Today KDE releases a bugfix update to Plasma 5, versioned 5.2.2.  
<a
href='https://www.kde.org/announcements/plasma-5.2.0.php'>Plasma 5.2</a>
was released in January with many feature refinements and new modules to complete the desktop experience.
");?>
</p>

<p>
<?php i18n("
This release adds a month's worth of new
translations and fixes from KDE's contributors.  The bugfixes are
typically small but important and include:
");?>
</p>

<ul>

<li><?php i18n("Translated documentation is now available");?></li>
<li><?php i18n("Create gtk-3.0 directory before putting the settings file into it. <a href='http://quickgit.kde.org/?p=breeze.git&a=commit&h=8614a8245741a1282a75a36cb7c67d181ec435a0'>Commit.");?></a> </li>
<li><?php i18n("KScreen: fix rounding error in updateRootProperties(). <a href='http://quickgit.kde.org/?p=kscreen.git&a=commit&h=a2d488623344e968e3e65627824e7ae369247094'>Commit.");?></a> </li>
<li><?php i18n("KWin: Fix installation of GHNS material. <a href='http://quickgit.kde.org/?p=kwin.git&a=commit&h=9bddd0fe8a7a909e0704ce215666d4a8a23a8307'>Commit.");?></a> </li>
<li><?php i18n("Muon: Wait to fetch the KNS backend until we have OCS providers. <a href='http://quickgit.kde.org/?p=muon.git&a=commit&h=a644be96c4c446b904a194278f35bb0e0540aabc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344840'>#344840</a>");?></li>
<li><?php i18n("Plasma Desktop: Extract UI messages. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&a=commit&h=12f750d497c5f2def14d89ad669057e13197b6f8'>Commit.");?></a> </li>
<li><?php i18n("Plasma Networkmanager: Make sure SSID will be displayed properly when using non-ASCII characters. <a href='http://quickgit.kde.org/?p=plasma-nm.git&a=commit&h=cbd1e7818471ae382fb25881c138e0228a44dac4'>Commit.</a> See bug <a href='https://bugs.kde.org/342697'>#342697</a>");?></li>

</ul>


<a href="plasma-5.2.1-5.2.2-changelog.php">
<?php i18n("Full Plasma 5.2.2 changelog");?></a>

<!-- // Boilerplate again -->

<h2><?php i18n("Live Images");?></h2>

<p><?php print i18n_var("
The easiest way to try it out is the with a live image booted off a
USB disk. You can find a list of <a href='%1'>Live Images with Plasma 5</a> at KDE Community Wiki.
", "https://community.kde.org/Plasma/LiveImages");?></p>

<h2><?php i18n("Package Downloads");?></h2>

<p><?php i18n("Distributions have created, or are in the process
of creating, packages listed on our wiki page.
");?></p>

<ul>
<li>
<?php print i18n_var("<a
href='https://community.kde.org/Plasma/Packages'>Package
download wiki page</a>"
, $release);?>
</li>
</ul>

<h2><?php i18n("Source Downloads");?></h2>

<p><?php i18n("You can install Plasma 5 directly from source. KDE's
community wiki has <a
href='http://community.kde.org/Frameworks/Building'>instructions to compile it</a>.
Note that Plasma 5 does not co-install with Plasma 4, you will need
to uninstall older versions or install into a separate prefix.
");?>
</p>

<ul>
<li>
<?php print i18n_var("
<a href='../info/%1.php'>Source Info Page</a>
", $release);?>
</li>
</ul>

<h2><?php i18n("Feedback");?></h2>

<?php print i18n_var("You can give us feedback and get updates on %1 or %2 or %3.", "<a href='https://www.facebook.com/kde'><img style='border: 0px; padding: 0px; margin: 0px' src='facebook.gif' width='32' height='32' /></a> <a href='https://www.facebook.com/kde'>Facebook</a>", "<a href='https://twitter.com/kdecommunity'><img style='border: 0px; padding: 0px; margin: 0px' src='twitter.png' width='32' height='32' /></a> <a href='https://twitter.com/kdecommunity'>Twitter</a>", "<a href='https://plus.google.com/105126786256705328374/posts'><img style='border: 0px; padding: 0px; margin: 0px' src='googleplus.png' width='30' height='30' /></a> <a href='https://plus.google.com/105126786256705328374/posts'>Google+</a>" );?>

<p>
<?php print i18n_var("Discuss Plasma 5 on the <a href='%1'>KDE Forums Plasma 5 board</a>.", "https://forum.kde.org/viewforum.php?f=289");?></a>
</p>

<p><?php print i18n_var("You can provide feedback direct to the developers via the <a href='%1'>#Plasma IRC channel</a>,
<a href='%2'>Plasma-devel mailing list</a> or report issues via
<a href='%3'>bugzilla</a>.  If you like what the
team is doing, please let them know!", "irc://#plasma@freenode.net", "https://mail.kde.org/mailman/listinfo/plasma-devel", "https://bugs.kde.org/enter_bug.cgi?product=plasmashell&format=guided");?></p>

<p><?php i18n("Your feedback is greatly appreciated.");?></p>

<h2>
  <?php i18n("Supporting KDE");?>
</h2>

<p align="justify">
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Supporting KDE page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative. </p>");?>

<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h2><?php i18n("Press Contacts");?></h2>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
