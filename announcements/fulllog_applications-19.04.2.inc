<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Hide]</a></h3>
<ul id='ulakonadi' style='display: block'>
<li>Add some missing dependencies in KF5AkonadiConfig.cmake. <a href='http://commits.kde.org/akonadi/7cc80667611e67204a4648cd81e711f0f5ab998a'>Commit.</a> </li>
<li>Fix QTimer being leaked in MonitorPrivate. <a href='http://commits.kde.org/akonadi/c72272da442266e1b47a42ca93a7c9ad360c59bd'>Commit.</a> </li>
<li>Port ExternalPartStorage::self to C++11 singleton syntax. <a href='http://commits.kde.org/akonadi/c1ead4aed3d5665ed60f6b8ee865f740aabb5be0'>Commit.</a> </li>
<li>Autotests: fix leak of FakeServerData and FakeSession. <a href='http://commits.kde.org/akonadi/c86274601bd57a33688a848a22b7633752d20895'>Commit.</a> </li>
<li>Fix FakeAkonadiServerCommands being leaked. <a href='http://commits.kde.org/akonadi/3cfff0ce478ce8c88b2b602f979b75025546a3bb'>Commit.</a> </li>
<li>Fix leaking of requests in ItemRetriever::exec(). <a href='http://commits.kde.org/akonadi/548c54a1b25ca6575a647ec6df4b4e143fce4f10'>Commit.</a> </li>
<li>Fix another null-sender warning in itemretrievertest. <a href='http://commits.kde.org/akonadi/0492ddd7e2ae0ba588412f3f196ceefd62231edd'>Commit.</a> </li>
<li>Monitornotificationtest: create FakeItemCache on stack. <a href='http://commits.kde.org/akonadi/9982a897ddcea81ebc74783667be2f23ed1ad578'>Commit.</a> </li>
<li>Properly cleanup in notificationmanagertest. <a href='http://commits.kde.org/akonadi/df4f5a139eac306e146e763723a281d91089d9c5'>Commit.</a> </li>
<li>Fix QObject::connect warning in itemretrievertest. <a href='http://commits.kde.org/akonadi/6422cb494f234b46261ee7f477346ed7e6880376'>Commit.</a> </li>
<li>Remove dead code in NotificationManager. <a href='http://commits.kde.org/akonadi/52a55252fcb12fd77dde96db7d6cfe8e7f4c50b5'>Commit.</a> </li>
<li>Itemsynctest: comment out the checks that make no sense. <a href='http://commits.kde.org/akonadi/79450d177d0e6624811d03de7f42fe3cf5680985'>Commit.</a> </li>
<li>Fix memory leaks in attributefactorytest. <a href='http://commits.kde.org/akonadi/e51a2db1857e9df561f65576e7308ddbfa976246'>Commit.</a> </li>
<li>Fix memory leak of ItemRetrievalJobFactory in ItemRetrievalManager. <a href='http://commits.kde.org/akonadi/c2c92eb9aaa88f67ed86300499f24eb072323db7'>Commit.</a> </li>
<li>Fix previous commit, we can't use ecm_add_test because we do add_test ourselves. <a href='http://commits.kde.org/akonadi/ec9d24cf12ad49b7def6a142bfd0aebbb9fb9410'>Commit.</a> </li>
<li>Ensure that qsqlite3.so is found in the builddir while running tests. <a href='http://commits.kde.org/akonadi/633beafad07375f89ced560e48e72e3131df475c'>Commit.</a> </li>
<li>Akonadi: port to ecm_add_test. <a href='http://commits.kde.org/akonadi/f1c8dda3b6061c6f3277d4730f13f8dc3f1cbb00'>Commit.</a> </li>
<li>Fix memory leak due to getTestMonitor(). <a href='http://commits.kde.org/akonadi/66b24c882f24ea44190a039bf89e82b32d5677d6'>Commit.</a> </li>
<li>Akonadi: fix compilation with clang. <a href='http://commits.kde.org/akonadi/24b0e5a62fa7c22be6e8bb30eebe40daa1a1059d'>Commit.</a> </li>
<li>Fix memory leaks (found by ASAN). <a href='http://commits.kde.org/akonadi/ef516b67afcf7aeca57f2e4fb99a8c4a33268df2'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-calendar' href='https://cgit.kde.org/akonadi-calendar.git'>akonadi-calendar</a> <a href='#akonadi-calendar' onclick='toggle("ulakonadi-calendar", this)'>[Hide]</a></h3>
<ul id='ulakonadi-calendar' style='display: block'>
<li>Fix the akonadi-calendar dependencies. <a href='http://commits.kde.org/akonadi-calendar/d18ba909b43e83194fd976ef21ce5ad018f1c8f5'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-contacts' href='https://cgit.kde.org/akonadi-contacts.git'>akonadi-contacts</a> <a href='#akonadi-contacts' onclick='toggle("ulakonadi-contacts", this)'>[Hide]</a></h3>
<ul id='ulakonadi-contacts' style='display: block'>
<li>Fix the akonadi-contacts dependencies. <a href='http://commits.kde.org/akonadi-contacts/5b3c2557bac1a0ea1cb9a60e38e2d9c0da471d2d'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-mime' href='https://cgit.kde.org/akonadi-mime.git'>akonadi-mime</a> <a href='#akonadi-mime' onclick='toggle("ulakonadi-mime", this)'>[Hide]</a></h3>
<ul id='ulakonadi-mime' style='display: block'>
<li>Fix the akonadi-mime dependencies. <a href='http://commits.kde.org/akonadi-mime/0fbe4700cc9afc5f7474046f0476fc00d2a0cb0f'>Commit.</a> </li>
</ul>
<h3><a name='akonadi-notes' href='https://cgit.kde.org/akonadi-notes.git'>akonadi-notes</a> <a href='#akonadi-notes' onclick='toggle("ulakonadi-notes", this)'>[Hide]</a></h3>
<ul id='ulakonadi-notes' style='display: block'>
<li>Fix the akonadi-notes dependencies. <a href='http://commits.kde.org/akonadi-notes/bbaa8676daef93d01331ce187d3fbdf7ba8344ea'>Commit.</a> </li>
</ul>
<h3><a name='akregator' href='https://cgit.kde.org/akregator.git'>akregator</a> <a href='#akregator' onclick='toggle("ulakregator", this)'>[Hide]</a></h3>
<ul id='ulakregator' style='display: block'>
<li>Fix crashes with Qt 5.12. <a href='http://commits.kde.org/akregator/cfb973c74fece37d5b52e10dda011c3c9c1be86a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371511'>#371511</a></li>
<li>Add DesktopEntry to notifyrc. <a href='http://commits.kde.org/akregator/34d4b059cd09a4e448666e440048809e50622b67'>Commit.</a> </li>
</ul>
<h3><a name='analitza' href='https://cgit.kde.org/analitza.git'>analitza</a> <a href='#analitza' onclick='toggle("ulanalitza", this)'>[Hide]</a></h3>
<ul id='ulanalitza' style='display: block'>
<li>Improve debugging information. <a href='http://commits.kde.org/analitza/efa8a897edb18caf6e9a047a81ac685479ab41ae'>Commit.</a> </li>
<li>Fix evaluation of matrixes. <a href='http://commits.kde.org/analitza/fe1d6c22afb5fd7f3799b40d723a6b4e5fd2d087'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Hide]</a></h3>
<ul id='ulark' style='display: block'>
<li>Get columns to show from first file entry. <a href='http://commits.kde.org/ark/3ad7526bf9ce4348a4ed3c6d1f30d32daf5bb529'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406135'>#406135</a></li>
<li>Stop crashing when invoking batch mode without urls. <a href='http://commits.kde.org/ark/f1444e45f810920a86898c3f1943ed1ce57f3026'>Commit.</a> </li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Elide tab titles left so key information at the end of the string doesn't get cut off. <a href='http://commits.kde.org/dolphin/d863dd9ad46b3a291751bcb9d6fd3930c4ea8afb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406569'>#406569</a></li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Fix build with exiv2-0.27.1. <a href='http://commits.kde.org/gwenview/172560b845460b6121154f88221c855542219943'>Commit.</a> </li>
</ul>
<h3><a name='juk' href='https://cgit.kde.org/juk.git'>juk</a> <a href='#juk' onclick='toggle("uljuk", this)'>[Hide]</a></h3>
<ul id='uljuk' style='display: block'>
<li>Add 'override' decl to fix compiler warnings, fix drag-and-drop. <a href='http://commits.kde.org/juk/dc2d9c8e0fe64ff6059b0f5ca927ff338e7eeca4'>Commit.</a> See bug <a href='https://bugs.kde.org/392705'>#392705</a></li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Hide]</a></h3>
<ul id='ulkalarm' style='display: block'>
<li>Include function names in debug messages. <a href='http://commits.kde.org/kalarm/94023fdb35dd8d4cf63bcf857320207cb954c3d1'>Commit.</a> </li>
<li>Fix CLang compile warnings. <a href='http://commits.kde.org/kalarm/90d9ceefd6e7024f024b7e774637d091fd964797'>Commit.</a> </li>
<li>Port KWindowSystem and X11 code to Qt5. <a href='http://commits.kde.org/kalarm/a40cc7aa97699bb7eb263f5bb1f274c93e512fb8'>Commit.</a> </li>
<li>Update change log. <a href='http://commits.kde.org/kalarm/72593f0f5213878e93c239aaa3cc803385c5d338'>Commit.</a> </li>
<li>Update change log. <a href='http://commits.kde.org/kalarm/59b999a7d00e5358b257116710a5aa2dbf0b166e'>Commit.</a> </li>
<li>Fix calendar configuration dialog not appearing. <a href='http://commits.kde.org/kalarm/11f27d5426985adece3fa25631d1f85ec68268e3'>Commit.</a> </li>
</ul>
<h3><a name='kcalcore' href='https://cgit.kde.org/kcalcore.git'>kcalcore</a> <a href='#kcalcore' onclick='toggle("ulkcalcore", this)'>[Hide]</a></h3>
<ul id='ulkcalcore' style='display: block'>
<li>Remove bogus TODO, this is a private header already so it's fine. <a href='http://commits.kde.org/kcalcore/964ec882c3becad593cc967bc53570ef52e39a0b'>Commit.</a> </li>
</ul>
<h3><a name='kdenetwork-filesharing' href='https://cgit.kde.org/kdenetwork-filesharing.git'>kdenetwork-filesharing</a> <a href='#kdenetwork-filesharing' onclick='toggle("ulkdenetwork-filesharing", this)'>[Hide]</a></h3>
<ul id='ulkdenetwork-filesharing' style='display: block'>
<li>Properly enable and disable the table view in response to the guest checkbox. <a href='http://commits.kde.org/kdenetwork-filesharing/447735830b0749f0d896dd9408c3287f66d496e9'>Commit.</a> </li>
<li>Expand the permissions comboboxes to fill available space. <a href='http://commits.kde.org/kdenetwork-filesharing/cb09bb89b823a67341a1f29aec67ccec79d66c7c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/382085'>#382085</a></li>
<li>Don't ever disable the OK button when the user is trying to remove a share. <a href='http://commits.kde.org/kdenetwork-filesharing/27ebbf47a81608fea27512fc08be8676411e71a8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407698'>#407698</a></li>
<li>Enable and disable the label too. <a href='http://commits.kde.org/kdenetwork-filesharing/9390bb1b148aa8f7e0cab4ff22a2b52d888ca8b7'>Commit.</a> </li>
<li>Enable and disable UI controls properly based on sharing status. <a href='http://commits.kde.org/kdenetwork-filesharing/29c9fc1d09b92649c48b966b1df4ae3277e56634'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407500'>#407500</a></li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Fix copy effect or split does not keep disabled state. <a href='http://commits.kde.org/kdenlive/a8874a4d1324d9965fde811443e429cb0fa65925'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408242'>#408242</a></li>
<li>Fix various keyframe related issues. <a href='http://commits.kde.org/kdenlive/b10d53bf09df465cf401a05c1e7b83351d660da2'>Commit.</a> </li>
<li>Fix error in composition index for 1st track. <a href='http://commits.kde.org/kdenlive/e71e7609b5b47a461f9863307f1a4a0df30d1411'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/408081'>#408081</a></li>
<li>Fix audio recording not added to timeline. <a href='http://commits.kde.org/kdenlive/504181807b2663a3ca561d8af044061076cb9b3d'>Commit.</a> </li>
<li>Fix guides in render widget. <a href='http://commits.kde.org/kdenlive/99cf7c0ab6057681229395e001f2258473038074'>Commit.</a> </li>
<li>Fix timeline selection/focus broken by recent commit. <a href='http://commits.kde.org/kdenlive/613235326d2168350378e8968f6677ddbdb8ff78'>Commit.</a> </li>
<li>Fix fade in broken on cut clips. <a href='http://commits.kde.org/kdenlive/4cff4653a82860c99321d45eaf083e14136e346f'>Commit.</a> </li>
<li>Revert audio capture to wav (should fix Windows issue #214). <a href='http://commits.kde.org/kdenlive/7c03f9008ad46731d4c18bcce5c9cddef551b890'>Commit.</a> </li>
<li>Fix automask bugs (initial zone incorrect and not displayed on monitor). <a href='http://commits.kde.org/kdenlive/ecf1a8da8429663124e21d36cc8ded8d975efe45'>Commit.</a> </li>
<li>Fix timeline unresponsive after deleting all clips. <a href='http://commits.kde.org/kdenlive/7e5dabd42a2dc0bbf686d50ddf72ac3d9518524d'>Commit.</a> </li>
<li>Properly load colors & icons (Fix #112). <a href='http://commits.kde.org/kdenlive/43a5b6df1206d4e891d065bad6b5689135d92b9d'>Commit.</a> </li>
<li>Fix clip grab state not updated on deselection. <a href='http://commits.kde.org/kdenlive/3ae09e5707273e1d21cac69f6fdd11c9a62300d7'>Commit.</a> </li>
<li>Add speed info to clip tooltip. <a href='http://commits.kde.org/kdenlive/7099b8acf41315de9c27067a6fbdfdfef99fc4f5'>Commit.</a> </li>
<li>Allow shortcut for change speed action. <a href='http://commits.kde.org/kdenlive/d364bac90ec6ad3cf72b72c628bd647abf2d1361'>Commit.</a> </li>
<li>Fix copy / paste track issue. <a href='http://commits.kde.org/kdenlive/28633d7a75c95865ce070e7914119320085868fa'>Commit.</a> </li>
<li>Fix slideshow clips on Windows. <a href='http://commits.kde.org/kdenlive/7db21278c5a38e4187abf48a9901d624cfb0466f'>Commit.</a> </li>
<li>Fix windows icons. <a href='http://commits.kde.org/kdenlive/d14952356202c707ffc5c3327a8a8bcc18320438'>Commit.</a> </li>
<li>Add properly scaled Windows icon. <a href='http://commits.kde.org/kdenlive/17e6272aac892b6f3324110c00eed7d6e94bc8ba'>Commit.</a> </li>
<li>Fix crash opening old project file. <a href='http://commits.kde.org/kdenlive/54e3030bab91b4dbfa962d43a22f1e6a8eae62a9'>Commit.</a> </li>
<li>Remove old speed effect from categorization. <a href='http://commits.kde.org/kdenlive/5f30505886409894f70fefa4ef92389310c8cb77'>Commit.</a> </li>
<li>Automatically convert old custom effects to new type (and make a backup copy in the legacy folder). <a href='http://commits.kde.org/kdenlive/88c56c1147af97becf430e3e50d88056af08e829'>Commit.</a> </li>
<li>Fix clip transcode incorrect label. <a href='http://commits.kde.org/kdenlive/f960577dfed12a668562374497852ee6d136228b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407808'>#407808</a></li>
<li>Fix various transcoding issues. <a href='http://commits.kde.org/kdenlive/257806a30b29a30398ba4ac2814298ae68f66d1e'>Commit.</a> See bug <a href='https://bugs.kde.org/407808'>#407808</a></li>
<li>Prevent saving corrupted file (with no tracks). <a href='http://commits.kde.org/kdenlive/1ca29700b03ab68ed9c7194dc6428295bd045abd'>Commit.</a> See bug <a href='https://bugs.kde.org/407798'>#407798</a></li>
<li>Detect corrupted project files on opening, propose to open backup. <a href='http://commits.kde.org/kdenlive/6f1e61c6930cb5342b091c0b3c62a3e697b7d932'>Commit.</a> See bug <a href='https://bugs.kde.org/407798'>#407798</a></li>
<li>Fix timewarp test after rounding change in timewarp clip duration. <a href='http://commits.kde.org/kdenlive/85df567bd0c4e7655aa5270d881f40543d13e773'>Commit.</a> </li>
<li>Use default composition duration instead of full clip length on composition creation. <a href='http://commits.kde.org/kdenlive/960c51c2b0eba229543ef16785dfbca948ac42b4'>Commit.</a> </li>
<li>Fix invalid clip on project opening. <a href='http://commits.kde.org/kdenlive/efbf6bd1002174b893befd1f216f594ba0db1839'>Commit.</a> See bug <a href='https://bugs.kde.org/407778'>#407778</a></li>
<li>Fix 1 frame offset in clip duration after speed change. <a href='http://commits.kde.org/kdenlive/7c9787b8ed76e728af882711b9358f3bc0798f3e'>Commit.</a> </li>
<li>Fix incorrect minimum speed. <a href='http://commits.kde.org/kdenlive/38f48b653469950676e0847a70baa617b5729683'>Commit.</a> </li>
<li>Fix remaining marker issues. <a href='http://commits.kde.org/kdenlive/51e92d90019634edf8976f1eb84be96b971372a3'>Commit.</a> </li>
<li>Don't create producers with non integer length (fixes invalid clip issue). <a href='http://commits.kde.org/kdenlive/407e2e0990d778c16cd3f40fd03c7b3aa6a2db19'>Commit.</a> </li>
<li>Do not use MLT producer's get_length_time methd as it changes the way the length property is stored, causing inconsistencies (clock vs smpte_df). <a href='http://commits.kde.org/kdenlive/bc32c54a408d283de1a0568301dcd3c626967632'>Commit.</a> See bug <a href='https://bugs.kde.org/407778'>#407778</a></li>
<li>Fix crash when marker was at clip start. <a href='http://commits.kde.org/kdenlive/1088dfba64db8ff7e7e59e5350f90ec9ab06d7db'>Commit.</a> </li>
<li>Fix marker position on clip cuts with speed effect. <a href='http://commits.kde.org/kdenlive/d5cb03d4261364b289bc6f52c531e7769e243479'>Commit.</a> </li>
<li>Fix custom effect appearing with wrong name after save. <a href='http://commits.kde.org/kdenlive/f37c1d8e023e1befd41268af6752333cac151327'>Commit.</a> </li>
<li>Use rounder rect icon instead of placeholder folder icon for custom effects. <a href='http://commits.kde.org/kdenlive/77ac9a0011d06d8e608a5bb831dbbdd6acd26b46'>Commit.</a> </li>
<li>Correctly hide/show asset settings when deselected/reselected. <a href='http://commits.kde.org/kdenlive/746678983ccc0c475f7f4599c318dbbfe69066d9'>Commit.</a> </li>
<li>Fix markers and snapping for clips with speed effect. <a href='http://commits.kde.org/kdenlive/f6118f65380a110c366fe55eea83795ffe79fb67'>Commit.</a> </li>
<li>Disable filter clip job on tracks. <a href='http://commits.kde.org/kdenlive/50e1cb6304294eaa871a53acc776d1392bc5771c'>Commit.</a> </li>
<li>Fix crash in audio thumbs with reverse speed clip. <a href='http://commits.kde.org/kdenlive/ea356ac7a354311eae302ffa67ec30391af7675e'>Commit.</a> </li>
<li>Fix mistake in previous commit. <a href='http://commits.kde.org/kdenlive/bc85fb4d01840358f2502404402ee2f7b42c02e6'>Commit.</a> </li>
<li>Fix removeAllKeyframes. <a href='http://commits.kde.org/kdenlive/1f06a9861f1fb35efb7df9ca5c61f8e720ced04a'>Commit.</a> </li>
<li>Make lock track undoable and other fixes for locking + tests. <a href='http://commits.kde.org/kdenlive/4fb549a7b6af23974e164640aa4a07fb72d5acc9'>Commit.</a> </li>
<li>Re-add "go to guide" menu in timeline. <a href='http://commits.kde.org/kdenlive/046df4ca1d8b637bd3fe5eaa5c9428c7645a4cbc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407528'>#407528</a></li>
<li>Fix timeline doesn't scroll with cursor. <a href='http://commits.kde.org/kdenlive/60128fff1596501a3f786937fb6a46f4f585fd0b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407433'>#407433</a></li>
<li>When importing a project file as clip, deduce the empty seek space. <a href='http://commits.kde.org/kdenlive/74072058e88860e28eb2f2315447b7679800aca2'>Commit.</a> </li>
<li>Fix opening project containing invalid clips (when a source file somehow went missing). <a href='http://commits.kde.org/kdenlive/43ae922aebd7721ff6be1f15df718d1e86df5e82'>Commit.</a> </li>
<li>Fix ungrouping when we have a selection of groups and single clips. <a href='http://commits.kde.org/kdenlive/828f24344a942a220c643780d349f755378b16af'>Commit.</a> </li>
<li>Don't invalidate timeline/refresh monitor on audio effects. <a href='http://commits.kde.org/kdenlive/f9d31a61721cb335a3c518def9d65d47e0ba7e57'>Commit.</a> </li>
<li>Fix wrong stream imported by default on multistream clips. <a href='http://commits.kde.org/kdenlive/321f45f60019df268f4e6e2d42f26d728e7ac132'>Commit.</a> </li>
<li>Improve snap behavior on group resizing. <a href='http://commits.kde.org/kdenlive/bcbbdbba25bbd2caef3a9d91b58d01f8e287dcf0'>Commit.</a> </li>
<li>Fix dynamic text broken because of missing font & keyword params. <a href='http://commits.kde.org/kdenlive/fe566c0799f82f69c1583e5ef90b3d0a4a3cbf52'>Commit.</a> </li>
<li>Fix snapping issues (disable snapping on high zoom levels). <a href='http://commits.kde.org/kdenlive/aeb55fceb61b2137230b1e4a101d4637af3b47a2'>Commit.</a> </li>
<li>Better abstraction for locking mechanism. <a href='http://commits.kde.org/kdenlive/9f129e22045bb62b624e597f8c2fbe60aeb89d89'>Commit.</a> </li>
<li>Fix endless clip test. <a href='http://commits.kde.org/kdenlive/ef8942a685c2ec8acd3ab74060b99888cad82dc5'>Commit.</a> </li>
<li>Fix resetView test. <a href='http://commits.kde.org/kdenlive/8371b055b0d344ea5cf4dd90ea0a390885d563a5'>Commit.</a> </li>
<li>Fix edit duration from timeline menu not connected. <a href='http://commits.kde.org/kdenlive/787436dd767b5205d1703d3f092f22a99d5d06eb'>Commit.</a> </li>
<li>Fix crash on resize after recent group resize fix. <a href='http://commits.kde.org/kdenlive/884ad0a2cd417f7665ff18cb87ade63257c05f0f'>Commit.</a> </li>
<li>Restore go to marker/guide context menu in monitor. <a href='http://commits.kde.org/kdenlive/62b260d7c5a598edda5625449fc344b8fb6e76b3'>Commit.</a> </li>
<li>Fix regrouping items loses AVSplit property. <a href='http://commits.kde.org/kdenlive/baee7bf682c2c9926ea52a1f3b1c8124da8f0317'>Commit.</a> </li>
<li>Fix: interpolation in rotoscoping filter. <a href='http://commits.kde.org/kdenlive/71457a78317ca99f8348efe33725608943120ade'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407418'>#407418</a></li>
<li>Fix list parameter default value broken (rotoscoping), ensure we always have a keyframe at in point. <a href='http://commits.kde.org/kdenlive/67bab49b2a99ae9220906be15dee6ee43f3bf373'>Commit.</a> </li>
<li>Allow building on Ubuntu LTS & derivatives. <a href='http://commits.kde.org/kdenlive/72792d492f57482b39a002e8d0b8d3e8f01ae487'>Commit.</a> </li>
<li>Fix context menu "edit guide" leaving empty space in menu. <a href='http://commits.kde.org/kdenlive/09877c3d722b240008b74a1961fa92bc16325fe7'>Commit.</a> </li>
<li>Fix fuzzer compilation. <a href='http://commits.kde.org/kdenlive/b1911119969dceca47698952c4b058a8f2ec247b'>Commit.</a> </li>
<li>Fix timeline preview crash. Since a QCoreApp was created by kdenlive_render, MLT did not create its own QApplication, leading to linking crashes. <a href='http://commits.kde.org/kdenlive/de3e5ee16fa8ac1d6b02451d5c9dc9143f24e74e'>Commit.</a> </li>
<li>Enforce progressive and fps on dnxhd timeline preview profiles. <a href='http://commits.kde.org/kdenlive/ceb8d38d68fab548dd71c54b0c848875cca3370f'>Commit.</a> </li>
<li>Add AppImage specific code to ensure we always set the correct path for MLT, FFmpeg, etc. <a href='http://commits.kde.org/kdenlive/e2f8630071818c7a6fc01364b446ac636154337c'>Commit.</a> </li>
<li>Don't delete timeline preview files on project close. <a href='http://commits.kde.org/kdenlive/5600246b4a47c695a525d036b18144b068368a9a'>Commit.</a> </li>
<li>Fix crash trying to delete first keyframe. Fixes #180. <a href='http://commits.kde.org/kdenlive/1900cc331c24c849c339039abea5dd96059945e1'>Commit.</a> </li>
<li>Revert composition sorting to match previous stable behavior. <a href='http://commits.kde.org/kdenlive/5e34170845391011e582760898d61bb057d03b81'>Commit.</a> </li>
<li>Fix title clip length 1 frame shorter than expected on creation. <a href='http://commits.kde.org/kdenlive/8a1fc7772df834759a1b6bb6ac9845a6508feac3'>Commit.</a> </li>
<li>Fix grouping after copy / paster. <a href='http://commits.kde.org/kdenlive/5c9d4ead11930f30bdb7c9089268a8509d9e4cde'>Commit.</a> </li>
<li>Fix gap on clip move when trying to move clips. <a href='http://commits.kde.org/kdenlive/a72fff76fd6f0ca00e17e7166620c1adb76c4f16'>Commit.</a> </li>
<li>Fix composition tracks listed in reverse order. <a href='http://commits.kde.org/kdenlive/b5b08387c23755e29ebcb6f0e376e6811088f632'>Commit.</a> </li>
<li>Fix copy/paste composition is one frame shorter. <a href='http://commits.kde.org/kdenlive/c9f551ba8921cbc8cd4160c3805d622259f2c10f'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-addons' href='https://cgit.kde.org/kdepim-addons.git'>kdepim-addons</a> <a href='#kdepim-addons' onclick='toggle("ulkdepim-addons", this)'>[Hide]</a></h3>
<ul id='ulkdepim-addons' style='display: block'>
<li>Look for KF5ContactEditor before trying to use it. <a href='http://commits.kde.org/kdepim-addons/14886286b5bb2c24ed3ee81ec218cfa1e46ee18b'>Commit.</a> </li>
<li>Remove default shortcut. it will close main windows... <a href='http://commits.kde.org/kdepim-addons/2f09902bb0ad570d3453dffe261404bcfc60127c'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-apps-libs' href='https://cgit.kde.org/kdepim-apps-libs.git'>kdepim-apps-libs</a> <a href='#kdepim-apps-libs' onclick='toggle("ulkdepim-apps-libs", this)'>[Hide]</a></h3>
<ul id='ulkdepim-apps-libs' style='display: block'>
<li>Use the akonadi-contact version for KF5ContactEditor. <a href='http://commits.kde.org/kdepim-apps-libs/44cca93d8f98617d11b97c4b95ce1ac9e2017ce4'>Commit.</a> </li>
<li>Add the missing KF5ContactEditor dependency. <a href='http://commits.kde.org/kdepim-apps-libs/f1e5df49e577b7ab88052cb9ba4ae25376ed39e5'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Make sure that we reload settings. <a href='http://commits.kde.org/kdepim-runtime/59c11c366369fdf29c1c1f5971f1c739e70c103a'>Commit.</a> </li>
<li>Remove unused class member. <a href='http://commits.kde.org/kdepim-runtime/e2f5852bdc9a7f5bffe7786abe0b34743a8b928c'>Commit.</a> </li>
<li>Improve debug messages. <a href='http://commits.kde.org/kdepim-runtime/a3940fff3759f59e6bc01e606581fe944a3bb29b'>Commit.</a> </li>
<li>Bug 407544: Fix crash when creating new KAlarm resource. <a href='http://commits.kde.org/kdepim-runtime/fe40ba8aa6e61beb9e3bf89742df2b66091739ea'>Commit.</a> </li>
</ul>
<h3><a name='kitinerary' href='https://cgit.kde.org/kitinerary.git'>kitinerary</a> <a href='#kitinerary' onclick='toggle("ulkitinerary", this)'>[Hide]</a></h3>
<ul id='ulkitinerary' style='display: block'>
<li>Check IATA BCBP input a bit more strictly. <a href='http://commits.kde.org/kitinerary/b172633177301dbb643020e11f66d758acf8e830'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407895'>#407895</a></li>
<li>Add German language support for the A&O hostel extractor. <a href='http://commits.kde.org/kitinerary/a73bb5a0b37316a5f5848f398f28cfe272cf7a33'>Commit.</a> </li>
<li>Handle line continuations in Deutsche Bahn tickets. <a href='http://commits.kde.org/kitinerary/2aad4a9bebdaa38a0d9e83b056587c53f145ee57'>Commit.</a> </li>
<li>More robust line continuation detection for the DB extractor. <a href='http://commits.kde.org/kitinerary/940795588be373bf9ce0aafe88cf33cbdaee6bda'>Commit.</a> </li>
<li>Keep the ticket token in the RCT2 extractor. <a href='http://commits.kde.org/kitinerary/779d799a4c0eefb50d388a80dfcf42ea6d6a0b69'>Commit.</a> </li>
<li>Fix PNR extraction for some sort of DB/ÖBB cross-over ticket. <a href='http://commits.kde.org/kitinerary/0ae09e9cdc3342eb91db4cee98e033c52b16cb25'>Commit.</a> </li>
<li>PNR is optional in SNCF booking confirmations. <a href='http://commits.kde.org/kitinerary/ac3f622b157a9a395898d231ff4dcf9a3ce8293f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404451'>#404451</a></li>
</ul>
<h3><a name='kleopatra' href='https://cgit.kde.org/kleopatra.git'>kleopatra</a> <a href='#kleopatra' onclick='toggle("ulkleopatra", this)'>[Hide]</a></h3>
<ul id='ulkleopatra' style='display: block'>
<li>Fix copy&paste error that breaks gpg process calls. <a href='http://commits.kde.org/kleopatra/2e5b420cb6348043491d41efa793783235ac60f9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407594'>#407594</a></li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Const'ify method here. <a href='http://commits.kde.org/kmail/d8ea350f1046c0479b2fd2efca7537cdb1d979ba'>Commit.</a> </li>
<li>Fix crash when we want to open it in akonadi console. <a href='http://commits.kde.org/kmail/d0092780ef536c1d8298f3c74368153a3697728f'>Commit.</a> </li>
<li>Archive now can't work as config dialog is a separate process now. <a href='http://commits.kde.org/kmail/13bd0dad3ab52b9e6d0978ddad6de8a6438244ea'>Commit.</a> </li>
<li>Bug 407967 - new mail: cursor jumps into blindcarboncopy. <a href='http://commits.kde.org/kmail/0a424812893a65b924dbe157a49e87b56b939b46'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407967'>#407967</a></li>
<li>We can save several email as mbox. <a href='http://commits.kde.org/kmail/e7532ca697177e0a6694b551166f53be69913ea1'>Commit.</a> </li>
</ul>
<h3><a name='kmines' href='https://cgit.kde.org/kmines.git'>kmines</a> <a href='#kmines' onclick='toggle("ulkmines", this)'>[Hide]</a></h3>
<ul id='ulkmines' style='display: block'>
<li>Drop unused kmines.notifyrc. <a href='http://commits.kde.org/kmines/704e91530bef67981e3a495fc5f29ea21363ffc0'>Commit.</a> </li>
<li>Add .arcconfig. <a href='http://commits.kde.org/kmines/1622b734ca203205f9a87b0289a4e4d57deffdb5'>Commit.</a> </li>
</ul>
<h3><a name='kmplot' href='https://cgit.kde.org/kmplot.git'>kmplot</a> <a href='#kmplot' onclick='toggle("ulkmplot", this)'>[Hide]</a></h3>
<ul id='ulkmplot' style='display: block'>
<li>Use layouts for QDialogs to make KmPlot adopt the sizes. <a href='http://commits.kde.org/kmplot/95f701c92c17b2c7d6da2b176ba7129514b08322'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407980'>#407980</a></li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Hide]</a></h3>
<ul id='ulkpimtextedit' style='display: block'>
<li>Remove Broken emoji. <a href='http://commits.kde.org/kpimtextedit/7d809f07fb00db6fbf52b2aaa304e85722391716'>Commit.</a> </li>
<li>Not necessary to duplicate info in tooltip. <a href='http://commits.kde.org/kpimtextedit/4a4818dd70c49228e271bcbe9f814da888c1a858'>Commit.</a> </li>
<li>Use NotoColorEmoji font. <a href='http://commits.kde.org/kpimtextedit/c59f225c0db25efd3c52838e9128188fcbd0a7e8'>Commit.</a> </li>
<li>Use NotoColorEmoji font here. <a href='http://commits.kde.org/kpimtextedit/666a62bcb4afac9ac3dc29196e3d93ec28e14fd3'>Commit.</a> </li>
</ul>
<h3><a name='ksudoku' href='https://cgit.kde.org/ksudoku.git'>ksudoku</a> <a href='#ksudoku' onclick='toggle("ulksudoku", this)'>[Hide]</a></h3>
<ul id='ulksudoku' style='display: block'>
<li>Fix opening saved games. <a href='http://commits.kde.org/ksudoku/9849aca3bb891651166f2eca21f5bef6753b3a2c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407768'>#407768</a></li>
</ul>
<h3><a name='libkdepim' href='https://cgit.kde.org/libkdepim.git'>libkdepim</a> <a href='#libkdepim' onclick='toggle("ullibkdepim", this)'>[Hide]</a></h3>
<ul id='ullibkdepim' style='display: block'>
<li>Statusbar progress widget: update up/down icon when dialog goes away. <a href='http://commits.kde.org/libkdepim/8315fe4be9ef5039fac3b86afb2d9bded263e91a'>Commit.</a> </li>
<li>Statusbar progress widget code cleanup: pass Mode to setMode. <a href='http://commits.kde.org/libkdepim/30d17d2d9386a11ff820c90c4f34cd68a794a559'>Commit.</a> </li>
<li>Statusbar progress widget: show button immediately. <a href='http://commits.kde.org/libkdepim/79c215fa91d2586a16c06d5794d5ec91fb22db21'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Hide]</a></h3>
<ul id='ulmailcommon' style='display: block'>
<li>Fix a potential build issue when building KMail. <a href='http://commits.kde.org/mailcommon/cea29406f7a1c1cdca2cbbde19ac9e8008b28342'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/407163'>#407163</a></li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>It's not necessary to create a view. <a href='http://commits.kde.org/messagelib/21121ab09438360a06efca675f3670ed8c9c9b1c'>Commit.</a> See bug <a href='https://bugs.kde.org/391038'>#391038</a></li>
<li>Enable TemplateParserJobTest::test_replyPlain again. <a href='http://commits.kde.org/messagelib/a5d0ad10ced249d74819209ae22052fde481e426'>Commit.</a> </li>
<li>Refresh TemplateParserJobTest::test_convertedHtml logic. <a href='http://commits.kde.org/messagelib/d7f78cc22e3adf24b969759372f203cd4bddb815'>Commit.</a> </li>
<li>Decryption Oracle based on forwarding PGP or S/MIME mails (CVE-2019-10732). <a href='http://commits.kde.org/messagelib/ac360b3a57eacbf0542ed0800e6054db76f01398'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404698'>#404698</a></li>
<li>Make unexpected data leak harder via reply. <a href='http://commits.kde.org/messagelib/8f9b85b664be0987014c5d2485e706ab5a198e1b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404698'>#404698</a></li>
<li>Rename convertedHtmlContent-> convertedHtmlContent for reply_Plain. <a href='http://commits.kde.org/messagelib/676893d23d56a31e6e9af93e40dacf9e3aa7b18a'>Commit.</a> </li>
<li>TemplateParser: test htmlReply. <a href='http://commits.kde.org/messagelib/bd031ba2ede8518f1a1bcdce7c6168f03356d6c0'>Commit.</a> </li>
<li>Add test cases for a Decryption Oracle based on PGP inline. <a href='http://commits.kde.org/messagelib/6173d7fdc198187622ab481cb26fd14dbbd4c700'>Commit.</a> </li>
<li>Templateparserjobtest: HTML mime parts are handled differently. <a href='http://commits.kde.org/messagelib/ab3c3a0de8691e656423afb5c184d0c8b5c8e4d8'>Commit.</a> </li>
<li>Add test for a normal encrypted message, to make sure decrpytion works. <a href='http://commits.kde.org/messagelib/9f77a5fdab234bf48744ef843ba01992ee935618'>Commit.</a> </li>
<li>Test mails for Decryption Oracle based on replying to PGP or S/MIME. <a href='http://commits.kde.org/messagelib/d397aa46e809203c94e31891caac57affac746d9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/404698'>#404698</a></li>
<li>MimeTreeParser returns wrong content for inline mesasges with multiple encrypted blocks. <a href='http://commits.kde.org/messagelib/fa225b1a115d858f3277626e8fc0b580b0a710f1'>Commit.</a> </li>
<li>Distinguish between mMsg and mOrigMsg. <a href='http://commits.kde.org/messagelib/ab56b25bdd214cfc7d8b6bacc8ac92ec9db19d52'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>[EPubGenerator] Avoid pointless scans of the whole document. <a href='http://commits.kde.org/okular/eb41001e0e28b665c7c0f9240756b3f9b819534e'>Commit.</a> </li>
<li>[EPubGenerator] Avoid crashes due to bogus wrapping of content in table. <a href='http://commits.kde.org/okular/9f98c010691ed73d11c83a1694823aba60b12e32'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/406738'>#406738</a>. Fixes bug <a href='https://bugs.kde.org/407140'>#407140</a>. See bug <a href='https://bugs.kde.org/406116'>#406116</a></li>
<li>Revert "Fix build with poppler < 0.51". <a href='http://commits.kde.org/okular/d121461b573367fc9508be1043938cdf2b6d063e'>Commit.</a> </li>
<li>Fix build with poppler < 0.51. <a href='http://commits.kde.org/okular/32786e6b3d8df7e17f03f5c70509f289591bafa7'>Commit.</a> </li>
<li>Fix line annotation leader line angle. <a href='http://commits.kde.org/okular/a0045a97a2219a5fdababffddac166930be051a3'>Commit.</a> </li>
</ul>
<h3><a name='pim-sieve-editor' href='https://cgit.kde.org/pim-sieve-editor.git'>pim-sieve-editor</a> <a href='#pim-sieve-editor' onclick='toggle("ulpim-sieve-editor", this)'>[Hide]</a></h3>
<ul id='ulpim-sieve-editor' style='display: block'>
<li>Fix enable/disable next button. <a href='http://commits.kde.org/pim-sieve-editor/eeb5944c1fb2ff6e776f47d1f9a61314da3a536f'>Commit.</a> </li>
<li>It's already initialized in header file. <a href='http://commits.kde.org/pim-sieve-editor/7d2872ec12ece71bbc67718ebd4a6249bc2bce81'>Commit.</a> </li>
</ul>
<h3><a name='poxml' href='https://cgit.kde.org/poxml.git'>poxml</a> <a href='#poxml' onclick='toggle("ulpoxml", this)'>[Hide]</a></h3>
<ul id='ulpoxml' style='display: block'>
<li>Use https for bugs.kde.org in Report-Msgid-Bugs-To. <a href='http://commits.kde.org/poxml/b343f96f3993c80dc29734a2c426101358e04680'>Commit.</a> </li>
</ul>
<h3><a name='spectacle' href='https://cgit.kde.org/spectacle.git'>spectacle</a> <a href='#spectacle' onclick='toggle("ulspectacle", this)'>[Hide]</a></h3>
<ul id='ulspectacle' style='display: block'>
<li>Don't exit when running in gui mode and notification times out. <a href='http://commits.kde.org/spectacle/c6ad1d6ae9291685b2767ab70bdeaaadb7783658'>Commit.</a> </li>
</ul>
