<?php
    include_once ("functions.inc");
    $translation_file = "www";
    require('../aether/config.php');

    $pageConfig = array_merge($pageConfig, [
        'title' => "Release of KDE Frameworks 5.56.0",
        'cssFile' => '/css/announce.css'
    ]);

    require('../aether/header.php');
    $site_root = "../";
    $release = '5.56.0';
?>

<main class="releaseAnnouncment container">
    <h1 class="announce-title"><a href="/announcements/"><?php i18n("Release Announcements")?></a><?php print i18n_var("KDE Frameworks %1", $release)?></h1>

<?php
  include "./announce-i18n-bar.inc";
?>

<img src="qt-kde.png" width="320" height="180" style="float: right; margin: 1em;" />

<p><?php i18n(" 
March 09, 2019. KDE today announces the release
of <a href='https://www.kde.org/products/frameworks/'>KDE Frameworks</a> 5.56.0.
");?></p>

<p><?php i18n(" 
KDE Frameworks are over 70 addon libraries to Qt which provide a wide
variety of commonly needed functionality in mature, peer reviewed and
well tested libraries with friendly licensing terms.  For an
introduction see the <a
href='https://www.kde.org/products/frameworks/'>KDE Frameworks web page</a>.
");?></p>

<p><?php i18n("
This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.
");?></p>

<?php i18n("
<h2>New in this Version</h2>
");?>

<h3><?php i18n("Baloo");?></h3>

<ul>
<li><?php i18n("Replace several Q_ASSERTs with proper checks");?></li>
<li><?php i18n("Check string length to avoid crash for \"tags:/\" URL");?></li>
<li><?php i18n("[tags_kio] Fix local file tagging by checking only tag: urls for double slashes");?></li>
<li><?php i18n("Hardcoding the Remaining Time Update Interval");?></li>
<li><?php i18n("Fix regression for matching explicitly included folders");?></li>
<li><?php i18n("Cleanup idempotent entries from mimetype mapper table");?></li>
<li><?php i18n("[baloo/KInotify] Notify if folder was moved from unwatched place (bug 342224)");?></li>
<li><?php i18n("Handle folders matching substrings of included/excluded folders correctly");?></li>
<li><?php i18n("[balooctl] Normalize include/exclude paths before using it for the config");?></li>
<li><?php i18n("Optimize Baloo::File copy assign operator, fix Baloo::File::load(url)");?></li>
<li><?php i18n("Use content to determine mime type (bug 403902)");?></li>
<li><?php i18n("[Extractor] Exclude GPG encrypted data from being indexed (bug 386791)");?></li>
<li><?php i18n("[balooctl] Actually abort a malformed command instead of just saying so");?></li>
<li><?php i18n("[balooctl] Add missing help for \"config set\", normalize string");?></li>
<li><?php i18n("Replace recursive isDirHidden with iterative one, allow const argument");?></li>
<li><?php i18n("Make sure only directories are added to the inotify watcher");?></li>
</ul>

<h3><?php i18n("Breeze Icons");?></h3>

<ul>
<li><?php i18n("Add code-oss icon");?></li>
<li><?php i18n("[breeze-icons] Add video camera icons");?></li>
<li><?php i18n("[breeze-icons] Use new suspend, hibernate and switch user icons in Breeze icon theme");?></li>
<li><?php i18n("Add 16 px and 22 px versions of the gamepad icon to devices/");?></li>
<li><?php i18n("Make Breeze theme tooltip texts consistent");?></li>
<li><?php i18n("Add battery icons");?></li>
<li><?php i18n("Rename \"visibility\" and \"hint\" icons to \"view-visible\" and \"view-hidden\"");?></li>
<li><?php i18n("[breeze-icons] Add monochrome/smaller SD card and memory stick icons (bug 404231)");?></li>
<li><?php i18n("Add device icons for drones");?></li>
<li><?php i18n("Change C/C++ header/source mimetype icons to circle/line style");?></li>
<li><?php i18n("Fix missing shadows on C/C++ header mimetype icons (bug 401793)");?></li>
<li><?php i18n("Remove monochrome font preferences icon");?></li>
<li><?php i18n("Improve font selection icon");?></li>
<li><?php i18n("Use new bell-style icon for all users of preferences-desktop-notification (bug 404094)");?></li>
<li><?php i18n("[breeze-icons] Add 16px versions of gnumeric-font.svg and link gnumeric-font.svg to font.svg");?></li>
<li><?php i18n("Add preferences-system-users symlinks that point to yast-users icon");?></li>
<li><?php i18n("Add edit-none icon");?></li>
</ul>

<h3><?php i18n("Extra CMake Modules");?></h3>

<ul>
<li><?php i18n("Fix releaseme checkout when this is included in a sub-directory");?></li>
<li><?php i18n("New find module for Canberra");?></li>
<li><?php i18n("Update Android toolchain files to reality");?></li>
<li><?php i18n("Add compile check to FindEGL");?></li>
</ul>

<h3><?php i18n("KActivities");?></h3>

<ul>
<li><?php i18n("Use natural sorting in ActivityModel (bug 404149)");?></li>
</ul>

<h3><?php i18n("KArchive");?></h3>

<ul>
<li><?php i18n("Guard KCompressionDevice::open being called with no backend available (bug 404240)");?></li>
</ul>

<h3><?php i18n("KAuth");?></h3>

<ul>
<li><?php i18n("Tell people they should mostly be using KF5::AuthCore");?></li>
<li><?php i18n("Compile our own helper against AuthCore and not Auth");?></li>
<li><?php i18n("Introduce KF5AuthCore");?></li>
</ul>

<h3><?php i18n("KBookmarks");?></h3>

<ul>
<li><?php i18n("Replace KIconThemes dependency with equivalent QIcon usage");?></li>
</ul>

<h3><?php i18n("KCMUtils");?></h3>

<ul>
<li><?php i18n("Use KCM name in KCM header");?></li>
<li><?php i18n("Add missing ifndef KCONFIGWIDGETS_NO_KAUTH");?></li>
<li><?php i18n("Adapt to changes in kconfigwidgets");?></li>
<li><?php i18n("Sync QML module padding to reflect system setting pages");?></li>
</ul>

<h3><?php i18n("KCodecs");?></h3>

<ul>
<li><?php i18n("Fix for CVE-2013-0779");?></li>
<li><?php i18n("QuotedPrintableDecoder::decode: return false on error instead of asserting");?></li>
<li><?php i18n("Mark KCodecs::uuencode does nothing");?></li>
<li><?php i18n("nsEUCKRProber/nsGB18030Prober::HandleData don't crash if aLen is 0");?></li>
<li><?php i18n("nsBig5Prober::HandleData: Don't crash if aLen is 0");?></li>
<li><?php i18n("KCodecs::Codec::encode: Don't assert/crash if makeEncoder returns null");?></li>
<li><?php i18n("nsEUCJPProbe::HandleData: Don't crash if aLen is 0");?></li>
</ul>

<h3><?php i18n("KConfig");?></h3>

<ul>
<li><?php i18n("Write valid UTF8 characters without escaping (bug 403557)");?></li>
<li><?php i18n("KConfig: handle directory symlinks correctly");?></li>
</ul>

<h3><?php i18n("KConfigWidgets");?></h3>

<ul>
<li><?php i18n("Skip benchmark if no scheme files can be found");?></li>
<li><?php i18n("Add a note for KF6 to use the core version of KF5::Auth");?></li>
<li><?php i18n("Cache the default KColorScheme configuration");?></li>
</ul>

<h3><?php i18n("KCoreAddons");?></h3>

<ul>
<li><?php i18n("Create tel: links for phone numbers");?></li>
</ul>

<h3><?php i18n("KDeclarative");?></h3>

<ul>
<li><?php i18n("use KPackage::fileUrl to support rcc KCMs packages");?></li>
<li><?php i18n("[GridDelegate] Fix long labels blending into each other (bug 404389)");?></li>
<li><?php i18n("[GridViewKCM] improve contrast and legibility for delegates' inline hover buttons (bug 395510)");?></li>
<li><?php i18n("Correct the accept flag of the event object on DragMove (bug 396011)");?></li>
<li><?php i18n("Use different \"None\" item icon in grid view KCMs");?></li>
</ul>

<h3><?php i18n("KDESU");?></h3>

<ul>
<li><?php i18n("kdesud: KAboutData::setupCommandLine() already sets help &amp; version");?></li>
</ul>

<h3><?php i18n("KDocTools");?></h3>

<ul>
<li><?php i18n("Port cross-compilation support to KF5_HOST_TOOLING");?></li>
<li><?php i18n("Only report DocBookXML as found if it was actually found");?></li>
<li><?php i18n("Update Spanish entities");?></li>
</ul>

<h3><?php i18n("KFileMetaData");?></h3>

<ul>
<li><?php i18n("[Extractor] Add metadata to extractors (bug 404171)");?></li>
<li><?php i18n("Add extractor for AppImage files");?></li>
<li><?php i18n("Cleanup ffmpeg extractor");?></li>
<li><?php i18n("[ExternalExtractor] Provide more helpful output when extractor fails");?></li>
<li><?php i18n("Format EXIF photo flash data (bug 343273)");?></li>
<li><?php i18n("Avoid side effects due to stale errno value");?></li>
<li><?php i18n("Use Kformat for bit and sample rate");?></li>
<li><?php i18n("Add units to framerate and gps data");?></li>
<li><?php i18n("Add string formatting function to property info");?></li>
<li><?php i18n("Avoid leaking a QObject in ExternalExtractor");?></li>
<li><?php i18n("Handle &lt;a&gt; as container element in SVG");?></li>
<li><?php i18n("Check Exiv2::ValueType::typeId before converting it to rational");?></li>
</ul>

<h3><?php i18n("KImageFormats");?></h3>

<ul>
<li><?php i18n("ras: fix crash on broken files");?></li>
<li><?php i18n("ras: protect the palette QVector too");?></li>
<li><?php i18n("ras: tweak max file check");?></li>
<li><?php i18n("xcf: Fix uninitialized memory use on broken documents");?></li>
<li><?php i18n("add const, helps understand the function better");?></li>
<li><?php i18n("ras: tweak max size that \"fits\" in a QVector");?></li>
<li><?php i18n("ras: don't assert because we try to allocate a huge vector");?></li>
<li><?php i18n("ras: Protect against divide by zero");?></li>
<li><?php i18n("xcf: Don't divide by 0");?></li>
<li><?php i18n("tga: fail gracefully if readRawData errors");?></li>
<li><?php i18n("ras: fail gracefully on height*width*bpp &gt; length");?></li>
</ul>

<h3><?php i18n("KIO");?></h3>

<ul>
<li><?php i18n("kioexec: KAboutData::setupCommandLine() already sets help &amp; version");?></li>
<li><?php i18n("Fix crash in Dolphin when dropping trashed file in trash (bug 378051)");?></li>
<li><?php i18n("Middle-elide very long filenames in error strings (bug 404232)");?></li>
<li><?php i18n("Add support for portals in KRun");?></li>
<li><?php i18n("[KPropertiesDialog] Fix group combobox (bug 403074)");?></li>
<li><?php i18n("Properly attempt to locate the kioslave bin in $libexec AND $libexec/kf5");?></li>
<li><?php i18n("Use AuthCore instead of Auth");?></li>
<li><?php i18n("Add icon name to service providers in .desktop file");?></li>
<li><?php i18n("Read IKWS search provider icon from desktop file");?></li>
<li><?php i18n("[PreviewJob] Also pass along that we're the thumbnailer when stat'ing file (bug 234754)");?></li>
</ul>

<h3><?php i18n("Kirigami");?></h3>

<ul>
<li><?php i18n("remove the broken messing with contentY in refreshabeScrollView");?></li>
<li><?php i18n("add OverlayDrawer to the stuff documentable by doxygen");?></li>
<li><?php i18n("map currentItem to the view");?></li>
<li><?php i18n("proper color to the arrow down icon");?></li>
<li><?php i18n("SwipeListItem: make space for the actions when !supportsMouseEvents (bug 404755)");?></li>
<li><?php i18n("ColumnView and partial C++ refactor of PageRow");?></li>
<li><?php i18n("we can use at most controls 2.3 as Qt 5.10");?></li>
<li><?php i18n("fix height of horizontal drawers");?></li>
<li><?php i18n("Improve ToolTip in the ActionTextField component");?></li>
<li><?php i18n("Add an ActionTextField component");?></li>
<li><?php i18n("fix spacing of buttons (bug 404716)");?></li>
<li><?php i18n("fix buttons size (bug 404715)");?></li>
<li><?php i18n("GlobalDrawerActionItem: properly reference icon by using group property");?></li>
<li><?php i18n("show separator if header toolbar is invisible");?></li>
<li><?php i18n("add a default page background");?></li>
<li><?php i18n("DelegateRecycler: Fix translation using the wrong domain");?></li>
<li><?php i18n("Fix warning when using QQuickAction");?></li>
<li><?php i18n("Remove some unnecessary QString constructions");?></li>
<li><?php i18n("Don't show the tooltip when the drop-down menu is shown (bug 404371)");?></li>
<li><?php i18n("hide shadows when closed");?></li>
<li><?php i18n("add the needed properties for the alternate color");?></li>
<li><?php i18n("revert most of the icon coloring heuristics change");?></li>
<li><?php i18n("properly manage grouped properties");?></li>
<li><?php i18n("[PassiveNotification] Don't start timer until window has focus (bug 403809)");?></li>
<li><?php i18n("[SwipeListItem] Use a real toolbutton to improve usability (bug 403641)");?></li>
<li><?php i18n("support for optional alternating backgrounds (bug 395607)");?></li>
<li><?php i18n("only show handles when there are visible actions");?></li>
<li><?php i18n("support colored icons in action buttons");?></li>
<li><?php i18n("always show the back button on layers");?></li>
<li><?php i18n("Update SwipeListItem doc to QQC2");?></li>
<li><?php i18n("fix logic of updateVisiblePAges");?></li>
<li><?php i18n("expose visible pages in pagerow");?></li>
<li><?php i18n("hide breadcrumb when the current page has a toolbar");?></li>
<li><?php i18n("support the toolbarstyle page override");?></li>
<li><?php i18n("new property in page: titleDelegate to override the title in toolbars");?></li>
</ul>

<h3><?php i18n("KItemModels");?></h3>

<ul>
<li><?php i18n("KRearrangeColumnsProxyModel: make the two column-mapping methods public");?></li>
</ul>

<h3><?php i18n("KNewStuff");?></h3>

<ul>
<li><?php i18n("Filter out invalid content in lists");?></li>
<li><?php i18n("Fix mem leak found by asan");?></li>
</ul>

<h3><?php i18n("KNotification");?></h3>

<ul>
<li><?php i18n("port to findcanberra from ECM");?></li>
<li><?php i18n("List Android as officially supported");?></li>
</ul>

<h3><?php i18n("KPackage Framework");?></h3>

<ul>
<li><?php i18n("remove kpackage_install_package deprecation warning");?></li>
</ul>

<h3><?php i18n("KParts");?></h3>

<ul>
<li><?php i18n("templates: KAboutData::setupCommandLine() already sets help &amp; version");?></li>
</ul>

<h3><?php i18n("Kross");?></h3>

<ul>
<li><?php i18n("Install Kross modules to ${KDE_INSTALL_QTPLUGINDIR}");?></li>
</ul>

<h3><?php i18n("KService");?></h3>

<ul>
<li><?php i18n("kbuildsycoca5: no need to repeat work of KAboutData::setupCommandLine()");?></li>
</ul>

<h3><?php i18n("KTextEditor");?></h3>

<ul>
<li><?php i18n("try to improve painting height for text lines - bug 403868 avoid to cut _ and other parts still broken: double height things like mixed english/arab, see bug 404713");?></li>
<li><?php i18n("Use QTextFormat::TextUnderlineStyle instead of QTextFormat::FontUnderline (bug 399278)");?></li>
<li><?php i18n("Make it possible to show all spaces in the document (bug 342811)");?></li>
<li><?php i18n("Do not print indent lines");?></li>
<li><?php i18n("KateSearchBar: Show also search has wrapped hint in nextMatchForSelection() aka Ctrl-H");?></li>
<li><?php i18n("katetextbuffer: refactor TextBuffer::save() to better separate code paths");?></li>
<li><?php i18n("Use AuthCore instead of Auth");?></li>
<li><?php i18n("Refactor KateViewInternal::mouseDoubleClickEvent(QMouseEvent *e)");?></li>
<li><?php i18n("Improvements to completion");?></li>
<li><?php i18n("Set the color scheme to Printing for Print Preview (bug 391678)");?></li>
</ul>

<h3><?php i18n("KWayland");?></h3>

<ul>
<li><?php i18n("Only commit XdgOutput::done if changed (bug 400987)");?></li>
<li><?php i18n("FakeInput: add support for pointer move with absolute coordinates");?></li>
<li><?php i18n("Add missing XdgShellPopup::ackConfigure");?></li>
<li><?php i18n("[server] Add surface data proxy mechanism");?></li>
<li><?php i18n("[server] Add selectionChanged signal");?></li>
</ul>

<h3><?php i18n("KWidgetsAddons");?></h3>

<ul>
<li><?php i18n("Use correct KStandardGuiItem \"no\" icon");?></li>
</ul>

<h3><?php i18n("Plasma Framework");?></h3>

<ul>
<li><?php i18n("[Icon Item] Block next animation also based on window visibility");?></li>
<li><?php i18n("Show a warning if a plugin requires a newer version");?></li>
<li><?php i18n("Bump the theme versions because icons changed, to invalidate old caches");?></li>
<li><?php i18n("[breeze-icons] Revamp system.svgz");?></li>
<li><?php i18n("Make Breeze theme tooltip texts consistent");?></li>
<li><?php i18n("Change glowbar.svgz to smoother style (bug 391343)");?></li>
<li><?php i18n("Do background contrast fallback at runtime (bug 401142)");?></li>
<li><?php i18n("[breeze desktop theme/dialogs] Add rounded corners to dialogs");?></li>
</ul>

<h3><?php i18n("Purpose");?></h3>

<ul>
<li><?php i18n("pastebin: don't show progress notifications (bug 404253)");?></li>
<li><?php i18n("sharetool: Show shared url on top");?></li>
<li><?php i18n("Fix sharing files with spaces or quotes in names via Telegram");?></li>
<li><?php i18n("Have ShareFileItemAction provide an output or an error if they are provided (bug 397567)");?></li>
<li><?php i18n("Enable sharing URLs via email");?></li>
</ul>

<h3><?php i18n("QQC2StyleBridge");?></h3>

<ul>
<li><?php i18n("Use PointingHand when hovering links in Label");?></li>
<li><?php i18n("Respect the display property of buttons");?></li>
<li><?php i18n("clicking on empty areas behaves like pgup/pgdown (bug 402578)");?></li>
<li><?php i18n("Support icon on ComboBox");?></li>
<li><?php i18n("support text positioning api");?></li>
<li><?php i18n("Support icons from local files in buttons");?></li>
<li><?php i18n("Use the correct cursor when hovering over the editable part of a spinbox");?></li>
</ul>

<h3><?php i18n("Solid");?></h3>

<ul>
<li><?php i18n("Bring FindUDev.cmake up to ECM standards");?></li>
</ul>

<h3><?php i18n("Sonnet");?></h3>

<ul>
<li><?php i18n("Handle the case if createSpeller is passed an unavailable language");?></li>
</ul>

<h3><?php i18n("Syntax Highlighting");?></h3>

<ul>
<li><?php i18n("Fix repository deletion warning");?></li>
<li><?php i18n("MustacheJS: also highlight template files, fix syntax and improve support for Handlebars");?></li>
<li><?php i18n("make unused contexts fatal for indexer");?></li>
<li><?php i18n("Update example.rmd.fold and test.markdown.fold with new numbers");?></li>
<li><?php i18n("Install DefinitionDownloader header");?></li>
<li><?php i18n("Update octave.xml to Octave 4.2.0");?></li>
<li><?php i18n("Improve highlighting of TypeScript (and React) and add more tests for PHP");?></li>
<li><?php i18n("Add more highlighting for nested languages in markdown");?></li>
<li><?php i18n("Return sorted definitions for file names and mime types");?></li>
<li><?php i18n("add missing ref update");?></li>
<li><?php i18n("BrightScript: Unary and hex numbers, @attribute");?></li>
<li><?php i18n("Avoid duplicate *-php.xml files in \"data/CMakeLists.txt\"");?></li>
<li><?php i18n("Add functions returning all definitions for a mimetype or file name");?></li>
<li><?php i18n("update literate haskell mimetype");?></li>
<li><?php i18n("prevent assertion in regex load");?></li>
<li><?php i18n("cmake.xml: Updates for version 3.14");?></li>
<li><?php i18n("CubeScript: fixes line continuation escape in strings");?></li>
<li><?php i18n("add some minimal howto for test adding");?></li>
<li><?php i18n("R Markdown: improve folding of blocks");?></li>
<li><?php i18n("HTML: highlight JSX, TypeScript &amp; MustacheJS code in the &lt;script&gt; tag (bug 369562)");?></li>
<li><?php i18n("AsciiDoc: Add folding for sections");?></li>
<li><?php i18n("FlatBuffers schema syntax highlighting");?></li>
<li><?php i18n("Add some Maxima constants and function");?></li>
</ul>

<h3><?php i18n("Security information");?></h3>

<p>The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB</p>
<br clear="all" />
<?php i18n("
<h2>Installing binary packages</h2>
");?>

<p>
<?php print i18n_var("
On Linux, using packages for your favorite distribution is the recommended way to get access to KDE Frameworks.
<a href='%1'>Get KDE Software on Your Linux Distro wiki page</a>.<br />
", "https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro");?>
</p>

<?php i18n("
<h2>Compiling from sources</h2>
");?>
<p>
<?php print i18n_var("The complete source code for KDE Frameworks %1 may be <a href='http://download.kde.org/stable/frameworks/%2/'>freely downloaded</a>. Instructions on compiling and installing KDE Frameworks %1 are available from the <a href='/info/kde-frameworks-%1.php'>KDE Frameworks %1 Info Page</a>.", $release, "5.56");?>
</p>
<p>
<?php print i18n_var("
Building from source is possible using the basic <em>cmake .; make; make install</em> commands. For a single Tier 1 framework, this is often the easiest solution. People interested in contributing to frameworks or tracking progress in development of the entire set are encouraged to <a href='%1'>use kdesrc-build</a>.
Frameworks %2 requires Qt %3.
", "http://kdesrc-build.kde.org/", $release, "5.10");?>
</p>
<p>
<?php print i18n_var("
A detailed listing of all Frameworks and other third party Qt libraries is at <a href='%1'>inqlude.org</a>, the curated archive of Qt libraries.  A complete list with API documentation is on <a href='%2'>api.kde.org</a>.
", "http://inqlude.org", "http://api.kde.org/frameworks-api/frameworks5-apidocs/");?>
</p>
<?php i18n("
<h2>Contribute</h2>
");?>
</p>
<?php print i18n_var("
Those interested in following and contributing to the development of Frameworks can check out the <a href='%1'>git repositories</a>, follow the discussions on the <a href='%2'>KDE Frameworks Development mailing list</a> and contribute patches through <a href='%3'>Phabricator</a>. Policies and the current state of the project and plans are available at the <a href='%4'>Frameworks wiki</a>. Real-time discussions take place on the <a href=%5>#kde-devel IRC channel on freenode.net</a>.
", "https://projects.kde.org/projects/frameworks", "https://mail.kde.org/mailman/listinfo/kde-frameworks-devel",
"https://phabricator.kde.org/project/view/90/", "http://community.kde.org/Frameworks", "irc://#kde-devel@freenode.net");?>
</p>

<!-- // Boilerplate again -->
<h2>
  <?php i18n("Supporting KDE");?>
</h2>
<p>
 <?php i18n("KDE is a <a href='http://www.gnu.org/philosophy/free-sw.html'>Free Software</a> community that exists and grows only because of the help of many volunteers that donate their time and effort. KDE is always looking for new volunteers and contributions, whether it is help with coding, bug fixing or reporting, writing documentation, translations, promotion, money, etc. All contributions are gratefully appreciated and eagerly accepted. Please read through the <a href='/community/donations/'>Donations page</a> for further information or become a KDE e.V. supporting member through our <a href='https://relate.kde.org/civicrm/contribute/transact?id=5'>Join the Game</a> initiative.</p>");?>
<?php
  include($site_root . "/contact/about_kde.inc");
?>
<h2><?php i18n("Press Contacts");?></h2>
<?php
  include($site_root . "/contact/press_contacts.inc");
?>
</main>
<?php
  require('../aether/footer.php');
