<?php

  $page_title = "Annuncio rilascio KDE 4.0.3";
  $site_root = "../";
  include "header.inc";
?>

<p>FOR IMMEDIATE RELEASE</p>

Disponibile anche in:
<a href="announce-4.0.3.php">Inglese</a>
<a href="http://fr.kde.org/announcements/announce-4.0.3.php">Francese</a>
<a href="announce-4.0.3-pt_BR.php">Portoghese (Brasiliano)</a>
<a href="announce-4.0.3-es.php">Spagnolo</a>
<a href="announce-4.0.3-sv.php">Svedese</a>

<!--
<a href="announce-4.0.3-bn_IN.php">Bengali (India)</a>
<a href="announce-4.0.3-ca.php">Catalano</a>
<a href="http://www.kdecn.org/announcements/announce-4.0.3.php">Chinese</a>
<a href="announce-4.0.3-cz.php">Czech</a>
<a href="announce-4.0.3-nl.php">Dutch</a>
<a href="announce-4.0.3.php">English</a>
<a href="http://fr.kde.org/announcements/announce-4.0.3.php">French</a>
<a href="announce-4.0.3-de.php">German</a>
<a href="announce-4.0.3-gu.php">Gujarati</a>
<a href="announce-4.0.3-he.php">Hebrew</a>
<a href="announce-4.0.3-hi.php">Hindi</a>
<a href="announce-4.0.3-it.php">Italian</a>
<a href="announce-4.0.3-lv.php">Latvian</a>
<a href="announce-4.0.3-ml.php">Malayalam</a>
<a href="announce-4.0.3-mr.php">Marathi</a>
<a href="announce-4.0.3-fa.php">Persian</a>
<a href="announce-4.0.3-pl.php">Polish</a>
<a href="announce-4.0.3-pa.php">Punjabi</a>
<a href="announce-4.0.3-pt_BR.php">Portuguese (Brazilian)</a>
<a href="announce-4.0.3-ro.php">Romanian</a>
<a href="announce-4.0.3-ru.php">Russian</a>
<a href="announce-4.0.3-sl.php">Slovenian</a>
<a href="announce-4.0.3-es.php">Spanish</a>
<a href="announce-4.0.3-sv.php">Swedish</a>
<a href="announce-4.0.3-ta.php">Tamil</a>
-->

<!-- // Boilerplate -->

<h3 align="center">
Il progetto KDE rilascia la terza versione di servizio ed internazionalizzazione per la serie 4.0 dell'ambiente Desktop libero più popolare
</h3>

<p align="justify">
  <strong>
    La comunità KDE rilascia la terza versione di servizio e internazionalizzazione del Desktop libero contenente nuove funzionalità nel Desktop Plasma, numerose correzioni, miglioramenti nella velocità e aggiornamenti nelle traduzioni.
</strong>
</p>

<p align="justify">
 2 Aprile 2008 (INTERNET). Il <a href="http://www.kde.org/">Progetto KDE</a> ha oggi annunciato la disponibilità immediata della versione 4.0.3 del Desktop KDE, la terza versione di manutenzione del più avanzato e potente ambiente Desktop libero per GNU/Linux e altri UNIX. KDE 4.0.3 viene rilasciato con i pacchetti di base, amministrazione, educazione, utilità, multimedia, giochi, grafica, sviluppo web e altro. I noti applicativi di KDE sono disponibili in 49 lingue.
</p>
<p align="justify">
 KDE, incluse le librerie e le applicazioni, è disponibile liberamente sotto i termini di licenze Open Source. KDE può essere scaricato come codice sorgente e in numerosi formati binari da  <a href="http://download.kde.org/stable/4.0.3/">http://download.kde.org</a> e può anche essere ottenuto su <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a> alternativamente alle molte <a href="http://www.kde.org/download/distributions.php">distribuzioni GNU/Linux e UNIX</a> disponibili.
</p>

<!-- // Meat -->

<h4>
  <a name="changes">Miglioramenti</a>
</h4>
<p align="justify">
KDE 4.0.3 viene rilasciato con un impressionante numero di correzioni e miglioramenti, molti dei quali sono annotati nel <a href="http://www.kde.org/announcements/changelogs/changelog4_0_2to4_0_3.php">changelog</a>.
KDE continuerà a rilasciare aggiornamenti per la serie 4.0 a cadenza mensile. KDE 4.1, che introdurrà <a href="http://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Feature_Plan">molti miglioramenti</a> al Desktop KDE e alle applicazioni verrà rilasciato a luglio.
<br />
Tra i miglioramenti di KDE 4.0.3 vi sono la risoluzione molti problemi di KDE 4.0.2 e un miglioramento delle traduzioni.
L'apporto delle correzioni è stato fatto stando attenti a correre il minor rischio possobile di regressioni. Per la squadra KDE questo è anche un modo per fornire rapidamente correzioni agli utenti.

Da uno sguardo al registro dei cambiamenti risulta che quasi tutti i moduli di KDE hanno avuto molti miglioramenti. Ulteriormente, la squadra di KHTML ha svolto un ottimo lavoro migliorando ancora l'esperienza utente con con il browser web Konqueror.
<ul>
  <li>Miglioramento dello scorrimento su KHTML, il motore di rendering HTML di KDE</li>
  <li>Migliorate la gestione delle finestre di dialogo su KWin, il gestore delle finestre di KDE </li>
  <li>Altri miglioramenti sul rendering di Okular, il visualizzatore di documenti di KDE</li>
</ul>

<h4>Extragear</h4>
<p align="justify">
Fin da KDE 4.0.0, le applicazioni del modulo <a href="http://extragear.kde.org">Extragear</a> hanno fatto parte dei regolari rilasci di KDE. Le applicazioni Extragear sono applicazioni KDE mature ma che non fanno parte di uno degli altri moduli di KDE. Il modulo Extragear rilasciato con KDE 4.0.3 contiene le seguenti applicazioni:
<ul>
    <li><a href="http://en.wikipedia.org/wiki/KColorEdit">KColoredit</a> -  Un editor per le palette di colore con supporto per le palette di colore di Gimp</li>
    <li>KFax - Un visualizzatore di fax </li>
    <li><a href="http://www.kde-apps.org/content/show.php/KGrab?content=74086">KGrab</a> - 
        Uno strumento avanzato per gli le foto delle schermate</li>
    <li><a href="http://extragear.kde.org/apps/kgraphviewer/">KGraphviewer</a> - Un visualizzatore di grafici GraphViz dot per KDE</li>
    <li><a href="http://w1.1358.telia.com/~u135800018/prog.html#KICONEDIT">KIconedit</a> - 
        Un programma di disegno ottimizzato per la creazione di icone</li>
    <li><a href="http://kmldonkey.org/">KMldonkey</a> - Una interfaccia grafica per Mldonkey, un client per la rete EDonkey</li>
    <li><a href="http://www.kpovmodeler.org/">KPovmodeler</a> - Un modellatore di immagini 3D</li>
    <li>Libksane - Una libreria per la scansione delle immagini</li>
    <li><a href="http://www.rsibreak.org">RSIbreak</a> - Un programma che cerca di evitare lo stress da sforzo ripetuto obbligandoti a fare delle pause</li>
</ul>
Una novità nel ramo Extragear è il nuovo KIO slave per il
<a href="http://en.wikipedia.org/wiki/Gopher_(protocol)">protocollo Gopher</a> che da ora sarà disponibile per tutte le applicazioni KDE.
</p>

<h4>
Le applicazioni educative di KDE
</h4>
<p align="justify">
KDE 4.0.3 viene rilasciato con una suite di programmi di alta qualità indirizzati al settore <a href="http://edu.kde.org">educativo</a>. Le applicazioni spaziano da
<a href="http://edu.kde.org/marble/">Marble</a>, un mappamondo semplice e versatile, ad alcuni divertenti giochi per i più piccoli.
<p align="justify">
Kalzium è una tavola periodica degli elementi. Mostra concetti astratti come l'attrazione degli atomi. Kalzium inoltre introduce diverse modalità di visualizzazione dettagliata delle informazioni a proposito degli elementi. Kalzium è stato sviluppato per essere uno strumento che renda la chimica facilmente comprensibile agli studenti delle scuole superiori, ma è anche un bel giocattolo per i più grandi che si vogliono divertire.

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/kalzium.png">
    <img src="announce_4.0.3/kalzium_thumb.png" align="center"  height="261"  />
  </a>
  <br /><em>Conoscere la chimica con Kalzium</em>
</div>
</p>
<p align="justify">
Parley è un programma che aiuta nella memorizzazione di un vocabolario. Parlay supporta varie funzionalità specifiche per differenti linguaggi ma che possono essere usati anche per altre attività. In Parley viene utilizzato un metodo di memorizzazione conosciuto come "flash cards". La creazione di nuovi vocabolari con Parley è molto semplice, ma sicuramente è molto più semplice utilizzarlo con uno dei vocabolari preconfezionati che possono essere scaricati dal internet.

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/parley.png">
    <img src="announce_4.0.3/parley_thumb.png" align="center"  height="225"  />
  </a>
  <br /><em>Memorizzazione facile con Parley</em>
</div>
</p>
<p align="justify">
Kmplot è un programma per il disegno grafico di funzioni matematiche, un semplice programma per l'apprendimento della matematica. Puoi facilmente inserire funzioni matematiche e vedere i grafici che ne risultano.

<div  align="center" style="width: auto; margin-top: 20px; margin-bottom: 20px;">
  <a href="announce_4.0.3/kmplot.png">
    <img src="announce_4.0.3/kmplot_thumb.png" align="center"  height="341"  />
  </a>
  <br /><em>Fai di conto con Kmplot</em>
</div>
A tutti coloro che vogliono approfondire sulle applicazioni educative di KDE consigliamo di seguire la <a href="http://edu.kde.org/tour_kde4.0/">Panoramica sul progetto KDE Education</a>.
</p>

<!-- // Boilerplate again -->

<h4>
  Installare KDE 4.0.3 con in pacchetti binari
</h4>
<p align="justify">
  <em>Fornitori di Pacchetti</em>.
  Alcuni fornitori di distribuzioni Linux/UNIX hanno gentilmente messo a disposizione i pacchetti binari per KDE 4.0.3, in altri casi dei volontari hanno provveduto a creare dei pacchetti. Alcuni dei pacchetti disponibili sono liberamente scaricabili da  <a href="http://download.kde.org/binarydownload.html?url=/stable/4.0.3/">http://download.kde.org</a>. Altri pacchetti binari, come aggiornamenti ai pacchetti già disponibili, possono essere rilasciati nelle prossime settimane.
</p>

<p align="justify">
  <a name="package_locations"><em>Dove trovare i pacchetti</em></a>.
  Per una lista aggiornata dei pacchetti disponibili dei quali il progetti KDE è stato informato, visitare la <a href="/info/4.0.3.php">pagina di informazioni su KDE 4.0.3</a>.
</p>

<h4>
  Compilare KDE 4.0.3
</h4>
<p align="justify">
  <a name="source_code"></a><em>Codice Sorgente</em>.
  Il codice sorgente completo di KDE 4.0.3 può essere <a href="http://download.kde.org/stable/4.0.3/src/">liberamente scaricato</a>. Istruzioni su come compilare e installare KDE 4.0.3 sono disponibili nella <a href="/info/4.0.3.php#binary">pagina di informazioni su KDE 4.0.3</a>.
</p>

<h4>
 Supporta KDE
</h4>
<p align="justify">
 KDE è un progetto di <a href="http://www.gnu.org/philosophy/free-sw.html">Software Libero</a> che esiste e cresce solamente grazie all'aiuto di molti volontari che donano il loro tempo e impegno. KDE è sempre alla ricerca di nuovi volontari contribuenti che possano dare una mano in settori come la programmazione, la ricerca di bachi, la scrittura della documentazione, la traduzione, la promozione, con donazioni, etc. Tutti i contributi sono apprezzati e accettati. Per favore leggi la pagina<a href="/community/donations/">Supporting KDE</a> per informazioni. </p>

<p align="justify">
Non vediamo l'ora di avere tue notizie!
</p>


<h2>A proposito di KDE 4</h2>
<p>
KDE 4.0 è un innovativo desktop di Software Libero che contiene molte applicazioni per un utilizzo quotidiano come anche per applicazioni specifiche. Plasma è un nuovo gestore per il desktop sviluppato per KDE 4, fornisce un'interfaccia intuitiva per l'interazione con il desktop e con le applicazioni. Konqueror, il web browser, integra il web con il Desktop. Dolphin, il gestore file, Okular, il visualizzatore dei documenti, e le Impostazioni di Sistema forniscono un desktop di base.
<br />
KDE è sviluppato sulle omonime librerie, che forniscono facilità di accesso alle risorse sulla rete con KIO e avanzate capacità grafiche grazie alle Qt4. Phonon e Solid, che sono ugualmente parte delle librerie di KDE, aggiungono un'infrastruttura multimediale e una migliore integrazione dell'hardware con le applicazioni scritte per KDE.
</p>


<?php
  include($site_root . "/contact/about_kde.inc");
?>

<h4>Contatti</h4>

<?php
  include($site_root . "/contact/press_contacts.inc");
  include("footer.inc");
?>
