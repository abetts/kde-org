<?php
  $page_title = "KDE 3.1.1 Release Announcement";
  $site_root = "../";
  include "header.inc";
?>

<p>DATELINE MARCH 20, 2003</p>
<p>FOR IMMEDIATE RELEASE</p>

<h3 align="center">
   KDE Project Ships Translation and Service Release for Leading Open Source Desktop
</h3>

<p align="justify">
  <strong>
    KDE Project Ships Translation and Service Release of Third-Generation
    GNU/Linux - UNIX Desktop, Offering Enterprises and Governments a Compelling
    Free and Open Desktop Solution
  </strong>
</p>

<p align="justify">
  March 20th, 2003 (The INTERNET).  The <a href="http://www.kde.org/">KDE
  Project</a> today announced the immediate availability of KDE 3.1.1,
  a maintenance release for the third generation of the most advanced and
  powerful <em>free</em> desktop for GNU/Linux and other UNIXes.  KDE 3.1.1
  ships with a basic desktop and seventeen other packages
  (PIM, administration, network, edutainment, utilities, multimedia, games,
  artwork, web development and more).  KDE's award-winning tools and
  applications are available in <strong>51 languages</strong>.
</p>

<!--
<p align="justify">
  Consistent with KDE's rapid but disciplined development pace, the release
  of KDE 3.1 heralds an <a href="#changes">impressive catalog</a> of
  feature enhancements and additions.  As has been typical in recent
  KDE major releases, a great many of the new features provide
  welcome news particularly to private and public enterprises.
</p>
-->

<p align="justify">
  KDE, including all its libraries and its applications, is
  available <em><strong>for free</strong></em> under Open Source licenses.
  KDE can be obtained in source and numerous binary formats from the KDE
  <a href="http://download.kde.org/stable/3.1.1/">http</a> or
  <a href="http://www.kde.org/mirrors/ftp.php">ftp</a> mirrors, and can
  also be obtained on <a href="http://www.kde.org/download/cdrom.php">CD-ROM</a>
  or with any of the major GNU/Linux - UNIX systems shipping today.
</p>

<h4>
  <a name="changes">Enhancements</a>
</h4>
<p align="justify">
  KDE 3.1.1 is a maintenance release which provides some new translations
  (Croatian (hr), Icelandic (is), Northern Sami (se) and Swati (ss)), as well
  as corrections of problems reported using the KDE <a href="http://bugs.kde.org/">bug tracking system</a>.
  For a more detailed list of improvements since the KDE 3.1 release in late
  January, please refer to the
  <a href="changelogs/changelog3_1to3_1_1.php">KDE 3.1.1 Changelog</a>.
</p>
<p>
  For additional information about the enhancements of the KDE 3.1.x release
  series, please see the
  <a href="/info/3.1/feature_guide_1.html">KDE 3.1 New Feature Guide</a> and the <a href="announce-3.1.php">KDE 3.1 Announcement</a>.
</p>

<h4>
  Installing KDE 3.1.1 Binary Packages
</h4>
<p align="justify">
  <em>Packaging Policies</em>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of
  KDE 3.1.1 for some versions of their distribution, and in other cases
  community volunteers have done so.
  Some of these binary packages are available for free download from KDE's
  <a href="http://download.kde.org/binarydownload.html?url=/stable/3.1.1/">http</a> or
  <a href="http://www.kde.org/mirrors/ftp.php">ftp</a> mirrors.
  Additional binary packages, as well as updates to the packages now
  available, may become available over the coming weeks.
</p>

<p align="justify">
  Please note that the KDE Project makes these packages available from the
  KDE web site as a convenience to KDE users.  The KDE Project provides source
  code, not binary packages; it is the responsibility of the OS vendors to
  package the source code into binary packages.  The KDE Project is not
  responsible for these packages as they are provided by third
  parties - typically, but not always, the distributor of the relevant
  distribution - using tools, compilers, library versions and quality
  assurance procedures over which KDE exercises no control. If you
  cannot find a binary package for your OS, or you are displeased with
  the quality of binary packages available for your system, please read
  the <a href="http://www.kde.org/download/packagepolicy.php">KDE Binary Package
  Policy</a> and/or contact your OS vendor.
</p>

<p align="justify">
  <a name="package_locations"><em>Package Locations</em></a>.
  For a current list of available binary packages of which the KDE
  Project has been informed, please visit the
  <a href="http://www.kde.org/info/3.1.1.php">KDE 3.1.1 Info Page</a>.
</p>

<h4>
  Compiling KDE 3.1.1
</h4>
<p align="justify">
  <a name="source_code"></a><em>Source Code</em>.
  The complete source code for KDE 3.1.1 may be
  <a href="http://download.kde.org/stable/3.1.1/src/">freely
  downloaded</a>.  Instructions on compiling and installing KDE 3.1.1
  are available from the <a href="/info/3.1.1.php#binary">KDE
  3.1.1 Info Page</a>.
</p>

<h4>
  KDE Sponsorship
</h4>
<p align="justify">
  Besides the superb and invaluable efforts by the
  <a href="http://www.kde.org/people/people.html">KDE developers</a>
  themselves, significant support for KDE development has been provided by
  <a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and
  <a href="http://www.suse.com/">SuSE</a>.  In addition,
  the members of the <a href="http://www.kdeleague.org/">KDE
  League</a> provide significant support for KDE promotion,
  <a href="http://www.ibm.com/">IBM</a> has donated significant hardware
  to the KDE Project, and the
  <a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
  and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
  provide most of the Internet bandwidth for the KDE project.  Thanks!
</p>

<h4>
  About KDE
</h4>
<p align="justify">
  KDE is an independent project of hundreds of developers, translators,
  artists and other professionals worldwide collaborating over the Internet
  to create and freely distribute a sophisticated, customizable and stable
  desktop and office environment employing a flexible, component-based,
  network-transparent architecture and offering an outstanding development
  platform.  KDE provides a stable, mature desktop, a full, component-based
  office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
  set of networking and administration tools and utilities, and an
  efficient, intuitive development environment featuring the excellent IDE
  <a href="http://www.kdevelop.org/">KDevelop</a>.  KDE is working proof
  that the Open Source "Bazaar-style" software development model can yield
  first-rate technologies on par with and superior to even the most complex
  commercial software.
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<p align="justify">
  <font size="2">
  <em>Trademark Notices.</em>
  KDE and K Desktop Environment are trademarks of KDE e.V.

  Linux is a registered trademark of Linus Torvalds.

  UNIX is a registered trademark of The Open Group in the United States and
  other countries.

  All other trademarks and copyrights referred to in this announcement are
  the property of their respective owners.
  </font>
</p>

<hr noshade="noshade" size="1" width="98%" align="center" />

<table border="0" cellpadding="8" cellspacing="0">
  <tr>
    <th colspan="2" align="left">
      Press Contacts:
    </th>
  </tr>
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      United&nbsp;States:
    </td>
    <td nowrap="nowrap">
      Andreas Pour<br />
      KDE League, Inc.<br />
      pour@kdeleague.org<br />
      (1) 402 934 4868
    </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      Europe (French and English):
    </td>
    <td nowrap="nowrap">
      David Faure<br />
      fa&#x75;r&#x65;&#0064;kde&#46;or&#103;<br />
      (33) 4 3250 1445
    </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      Europe (German and English):
    </td>
    <td nowrap="nowrap">
      Ralf Nolden<br />
      &#x6e;&#0111;l&#x64;e&#00110;&#x40;k&#0100;e&#046;&#00111;rg<br />
      (49) 2421 502758
    </td>
  </tr>
<!--
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      Europe (English):
    </td>
    <td nowrap="nowrap">
      Jono Bacon<br />
      j&#111;&#110;&#111;&#x40;&#x6b;d&#101;&#046;o&#x72;&#103;
    </td>
  </tr>
-->
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      Latin&nbsp;America (Portuguese, Spanish and English):
    </td>
    <td nowrap="nowrap">
      Roberto Teixeira<br />
      r&#x6f;b&#00101;&#114;t&#x6f;&#00064;k&#x64;&#0101;&#46;&#x6f;&#114;g<br />
      (55) 41 360 2702
     </td>
  </tr>
  <tr valign="top">
    <td align="right" nowrap="nowrap">
      Southeast Asia (English and Indonesian):
    </td>
    <td nowrap="nowrap">
      Ariya Hidayat<br />
      &#x61;&#00114;&#105;y&#0097;&#0064;&#107;d&#x65;&#x2e;&#x6f;r&#103;<br />
      (62) 815 8703177
    </td>
  </tr>
</table>
<?php

  include("footer.inc");
?>
