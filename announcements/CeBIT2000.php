<?php
  $page_title = "Meet the KDE Team at CeBIT 2000 Show";
  $site_root = "../";
  include "header.inc";
?>

<p>
<a href="http://www.cebit.de">This year's CeBIT</a> appearance of the KDE Project will be hosted by SuSE
on their main booth in hall 3, booth E045. SuSE staff as well as
members of the KDE Core Team will be demonstrating KDE 1.1.2 including
the new SuSE Desktop. The highlight will be a preview of the upcoming
KDE 2.0 release including KOffice, the KDE Office Suite.
</p>
<p>
"We are very glad that we found with SuSE a partner that is able to
host the KDE appearance at CeBIT. We hope a lot of people like the
new, exiting features of KDE 2.0 and KOffice." said KDE Developer
Chris Schlaeger.
</p>
<p>
"KDE 2.0 and KOffice are very exiting technologies that will be the
key to Linux's success in the desktop market." said Roland Dyroff,
CEO of SuSE Linux AG
</p>
<p>
In addition to the demo points regular presentations will be held on a
large video screen to serve a broader audience.
</p>
<p>
About KDE:
<br />
KDE is a collaborative Open Source project of hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop
environment employing a network-transparent, intuitive user
interface. KDE includes applications for email, web browsing, system
administration, dial-up networking, scheduling and many other
tasks. KDE is working proof of the power of the open source software
development model.
</p>
<p>
The elegance and usefulness of the KDE environment has been recognized
through multiple awards, including Linux World magazine's "Editor's
Choice" award in the Desktop Environment category, and Ziff-Davis'
"Innovation of the year 1998/1999" award in the Software category.
</p>
<p>
About SuSE:
<br />
SuSE, with a workforce of over 300 people, is one of the leading Linux
companies worldwide. SuSE Linux is used by more than 50,000 business
customers worldwide due to its stability and high quality.
SuSE offers an extensive range of qualified consulting and support
services, as well as commercial Linux software and complete Linux
systems. SuSE is contributing extensively to the development of Linux
for projects such as the Linux kernel, glibc, XFree86[tm], KDE,
ISDN4Linux, ALSA (Advanced Linux Sound Architecture) and USB
(Universal Serial Bus).
</p>
<p>
Contact:
<br />
KDE <a href="http://www.kde.org">http://www.kde.org</a>
Chris Schlaeger <a href="&#109;&#97;ilto:&#00099;s&#x40;&#00107;d&#x65;&#x2e;&#0111;r&#103;">&#99;s&#64;kde&#046;&#x6f;&#0114;&#x67;</a>
<br />
SuSE <a href="http://www.suse.de/en">http://www.suse.de/en</a>
Chris Egle <a href="mailto:ce@suse.de">ce@suse.de</a>
</p>

<?php include "footer.inc" ?>
