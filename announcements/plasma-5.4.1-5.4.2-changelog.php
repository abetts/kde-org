<?php
include_once ("functions.inc");
$translation_file = "www";
$page_title = i18n_noop("Plasma 5.4.2 complete changelog");
$site_root = "../";
$release = 'plasma-5.4.2';
include "header.inc";
?>
<p><a href="plasma-5.4.2.php">Plasma 5.4.2</a> complete changelog</p>
<script type='text/javascript'>
function toggle(toggleUlId, toggleAElem) {
var e = document.getElementById(toggleUlId)
if (e.style.display == 'none') {
e.style.display='block'
toggleAElem.innerHTML = '[Hide]'
} else {
e.style.display='none'
toggleAElem.innerHTML = '[Show]'
}
}
</script>
<h3><a name='bluedevil' href='http://quickgit.kde.org/?p=bluedevil.git'>Bluedevil</a> </h3>
<ul id='ulbluedevil' style='display: block'>
<li>ReceiveFileJob: Delay emitting result in case of error. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=efc7d1111fbf9643ade79a94075021f724dc62c4'>Commit.</a> </li>
<li>Fix invoking bluedevil-authorize helper. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=0ebc7326c574d58e12183a0db4c815347f6cde84'>Commit.</a> </li>
<li>Rename QCDebug categories. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=d0a05b390c0fd359a6b0874a118a0cbf590331e8'>Commit.</a> </li>
<li>Use "preferences-system-bluetooth" as fallback for device icon. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=551e57b385743c52b3a8095e6e8d759e39b8154d'>Commit.</a> </li>
<li>Applet: Don't show toolbar when Bluetooth is disabled. <a href='http://quickgit.kde.org/?p=bluedevil.git&amp;a=commit&amp;h=933db11db86367780c71a8181b06690044976611'>Commit.</a> </li>
</ul>


<h3><a name='breeze' href='http://quickgit.kde.org/?p=breeze.git'>Breeze</a> </h3>
<ul id='ulbreeze' style='display: block'>
<li>Added "emblem-unavailable" icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=bf77dee2fabd1d32dae64062e69182524b59338f'>Commit.</a> </li>
<li>Added "emblem-locked" icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=01b1d76d6ee819915480bedd7a925fc93b7b4f39'>Commit.</a> </li>
<li>Breeze Dark Icons: sync between Plasma 5.4 and Master. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=db31f9ba36a4764850221202f47b0e939698ba3a'>Commit.</a> </li>
<li>Breeze Icons: sync between Plasma 5.4 and master. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=12303d91b4a8afc24e6576b21d02418c427333bf'>Commit.</a> </li>
<li>Breeze Icons: sync between Plasma 5.4 and master. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=37928e90ce557f2d8006f816c366579ce0274312'>Commit.</a> </li>
<li>Breeze Icons: add application links for ubiquity-kde and kwalletmanager2. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b28575e04542609d0bc57420d1dcb42cba7548ff'>Commit.</a> </li>
<li>Breeze Icons: link inode-directory to the right dictionary (bug). <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=30b78c401f57a3d9b0b72cfac0bfa81f508df52e'>Commit.</a> </li>
<li>Breeze Icons: Digikam icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c19d868b2cd4b6954b2232d99e55e5a7e101166c'>Commit.</a> </li>
<li>Breeze Icons: for Digikam. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c12ba38366043c2e7321e59537c80eec86802753'>Commit.</a> </li>
<li>Breeze Dark sync. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4f72596a82369d7fb724754553ff27eaa6ce4016'>Commit.</a> </li>
<li>Breeze missing icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=29a101dbbb2b2937480d12c135631981d4243a09'>Commit.</a> </li>
<li>Breeze Icons for KMag don't work cause kde4 application. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5ce9b81def92f9352a047fd4d3afd7f685bd5168'>Commit.</a> </li>
<li>Breeze Icons: last missing Oxygen icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=55a0204d1d1adf7db38e26f6be1d002160e70a4e'>Commit.</a> </li>
<li>Missed this one. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=47db8a4baacfb280a8bc79e8d3007c8c55f0d869'>Commit.</a> </li>
<li>New volume and network wired icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d026b55b8e91eec0fd791df3ffe4d653d9a0c89e'>Commit.</a> </li>
<li>22px icons for system-file-manager and utilities-terminal were used for Dolphins toolbr I removed as users complained that the icons were being displayed in the task manager, obviously this means that the bigger icons are now used for the toolbar, so.. yeah. I also removed the 16 and 22 icons of preferences-system to give system settings the same treatment. So far nothing looks broken. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=62533c159e7c74e9e77f73e7083358071e4d4032'>Commit.</a> </li>
<li>32px network folder icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=56e0d9f412e30e61f3bdcece5dd81c549fc8a2a2'>Commit.</a> </li>
<li>Breeze Icons: add action icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=891afa6ef2c487f65ca2b2f37b706e5e3561b45c'>Commit.</a> </li>
<li>Link for Octopi. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d06faae29bbb785bb5c1e3ffb4b1bd0ebc1cc529'>Commit.</a> </li>
<li>Edited description and name in index files. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=7142fcf089d8d9eeed32c26b22a026e2e4124cdd'>Commit.</a> </li>
<li>Edited descriptin and name in index files. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4d74ce27b2dc6f91e126a815e82eaadf1e26b7ee'>Commit.</a> </li>
<li>Forgot to rename this one. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=e050a6f509704e266e3de1a4520faf0c3de862bd'>Commit.</a> </li>
<li>M0ar fixes. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5ef43b4022d5e3a97de2382f391a70bacc214cdc'>Commit.</a> </li>
<li>Got fixes?. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=7d7a8a0aa5ff87fa5c01fb9848678b0730bcd97e'>Commit.</a> </li>
<li>Le icon for plasma mobile phone app. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c0067b27531960cdb52dc73d4d278c9be82c5430'>Commit.</a> </li>
<li>M0ar ic0ns. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=26f843c6ec3c84e02bea0bfb148abffe32541a0a'>Commit.</a> </li>
<li>Breeze Icons: sync with Oxygen. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b15d1d9be10ddf3aa6f1d4f219470e2431004607'>Commit.</a> </li>
<li>Add automated-tasks for Ring. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4ab408c6b7c90ed3c36c695ba40f4aac21ff9fbf'>Commit.</a> </li>
<li>Breeze Icons: add plasma-search for kcm. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=dbe58dcd6e16780ab77971e79687e2207affc42f'>Commit.</a> </li>
<li>Fix all symlinks. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=34deaaee5efd668b942ed56b0f1b44c82d865235'>Commit.</a> </li>
<li>Add octave icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=172b4fc82e3dfb47f242faba85bc3c9b3e879c0d'>Commit.</a> </li>
<li>Added markdown mimetype. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=486538dfcc0bc880967ec683f3a00a48e750065d'>Commit.</a> </li>
<li>Copied missing icons from breeze to breeze-dark. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=c792b4214101214a1724e97e0ee46622c4b78380'>Commit.</a> </li>
<li>Updated index files. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d00cb9d618b612e3a894f06b470e0903b63a9636'>Commit.</a> </li>
<li>Updated index files. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5a584741493e3d5a4462de4617aaee4de81b6a95'>Commit.</a> </li>
<li>Updated index files. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b9bed77625a14c9f87fd1370946d98e5f2fece0c'>Commit.</a> </li>
<li>Breeze Icons: add bluetooth icon for nm CCBug: 346133. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=7a2169ca6ee01b001d529ce00c5a0ee84a63b25e'>Commit.</a> </li>
<li>Breeze Icons some device icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=6977391debc40975b54cc9823eb1e3e0f74153bd'>Commit.</a> </li>
<li>Updated various icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=18035891464f2bd6bd930eb2c7d381755b13b012'>Commit.</a> </li>
<li>Updated various icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=1823953da25ff447be81f0a728c493b038974867'>Commit.</a> </li>
<li>Updated various icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b561568ed8eb6242292a72bc3e5840eaf2a6dbfe'>Commit.</a> </li>
<li>Updated various icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=2c23a8093e604b26f6d14e9b3e4ca19db71108a7'>Commit.</a> </li>
<li>Empty icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4b73ad1d8606d3b9ffccb8be3e0de0280df42edd'>Commit.</a> </li>
<li>Modified octet-stream icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=fb56b880b09941caee959cb9b1f4e6b0f5149e05'>Commit.</a> </li>
<li>Breeze Icons: add some device icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=8cfd4588011e5acaf12551f633f9952e2acdc7dc'>Commit.</a> </li>
<li>Modified window close. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=cb18488960b3794512dcd4f55fa5c4dcb0c74f93'>Commit.</a> </li>
<li>Breeze Icons: add network icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=d1c7b61dbd269decd22496d016c0820345c78a63'>Commit.</a> </li>
<li>License file. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=32d7bc7d8a67603c0268b16760fdf14e5d5551c1'>Commit.</a> </li>
<li>Added updated cpu icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=07270589f8e812aefb1cbac9f8d203cb42c2ddee'>Commit.</a> </li>
<li>Breeze Icons: fix liked icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=944828da11e8ce2477f4ca09bf66cbdabd06c460'>Commit.</a> </li>
<li>Added updated Kalgebra icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=acecd922eedf577434e47d7422965925126c88ba'>Commit.</a> </li>
<li>Added updated CPU icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=fb2bf485285beadea3bdbb3134434262d1368dd8'>Commit.</a> </li>
<li>Added updated CPU icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=83ba4902732e5af116f2e4257c240df57a952657'>Commit.</a> </li>
<li>Added CPU icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=f242c7a67c07b16511645808fe128350090281e9'>Commit.</a> </li>
<li>Fixed broken links. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=cd5825647788d857f2fbf05a7d6afa460142d0dc'>Commit.</a> </li>
<li>Breeze Icons: add icons for kde-connect. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=e216b45ea74f2954f9172abb7f9ff670a7b69a96'>Commit.</a> </li>
<li>Added icon for Kalgebra and gitignore. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=ab759f3227f79377e958b7ae948458671fcd5dff'>Commit.</a> </li>
<li>Breeze Icons kde-connect from uri. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=0c34af0d7883be4a3dd5b20b4ecf5ab0ca81895f'>Commit.</a> </li>
<li>Breeze icon: input-gaming. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=5d19d791bf6996aa00d6a7281816a56855d6bea9'>Commit.</a> </li>
<li>Breeze: change file permission. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=e11b088a325c67154eb9229154dcc7ab46147e69'>Commit.</a> </li>
<li>Breeze Icons: some sync. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=28b8e655fe1dbe1ebaf816d689a7a4c88d346288'>Commit.</a> </li>
<li>Breeze Dark sync. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=7ee5af40ad4baeb8f3b10dfa52230092b9bcdfa7'>Commit.</a> </li>
<li>Breeze Icons add devices icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=2e7ba99478a36f04b009612e3c216656f5bdaa1d'>Commit.</a> </li>
<li>Breeze Icons: add network wired. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=41d060cdcea0d650eadeec8c48a172a321781506'>Commit.</a> </li>
<li>Breeze Icons file permissions to 644. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=1fb85bea34a48e9e5f99afbbd835ae2d0786e444'>Commit.</a> </li>
<li>Breeze Icons: add gtk new tab icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=462730f41bef794334bc6dcea532e4b2f903d474'>Commit.</a> </li>
<li>Breeze Icons: update Security-high icon. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=bb36021f602fc9694340bff50defd79e1febc6ab'>Commit.</a> </li>
<li>Breeze icons: add network icons. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b284b6b73f592b70a1d2e4fd7b355c480114e475'>Commit.</a> </li>
<li>Added markdown mimetype. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=4c4dff532aee1488d1f6aec45c5037ae5a82c5b3'>Commit.</a> </li>
<li>Added markdown mimetype. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=b65a913be3040f7f5004683fecddbf7462ddd58a'>Commit.</a> </li>
<li>Added markdown mimetype. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=be4dd75a66db81001017bb0ade76e2861eee4879'>Commit.</a> </li>
<li>Breeze theme file changes. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=1baa026e34a0b9373df24393fe5553ad501fd82c'>Commit.</a> </li>
<li>Breeze Icons: move from github. <a href='http://quickgit.kde.org/?p=breeze.git&amp;a=commit&amp;h=68251f75bd6f82926388e5f38a50c73b8981ed85'>Commit.</a> </li>
</ul>


<h3><a name='kde-cli-tools' href='http://quickgit.kde.org/?p=kde-cli-tools.git'>kde-cli-tools</a> </h3>
<ul id='ulkde-cli-tools' style='display: block'>
<li>Load the mimetype file lowercase name first. <a href='http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=fd49b7fdcf1c9cc7d98faee9d8d25a74078dd0fc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125420'>#125420</a></li>
</ul>


<h3><a name='kdeplasma-addons' href='http://quickgit.kde.org/?p=kdeplasma-addons.git'>Plasma Addons</a> </h3>
<ul id='ulkdeplasma-addons' style='display: block'>
<li>Fix system loadviewer results. <a href='http://quickgit.kde.org/?p=kdeplasma-addons.git&amp;a=commit&amp;h=564b06d32c2e7c581f2316342854881b08788809'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348385'>#348385</a></li>
</ul>


<h3><a name='ksysguard' href='http://quickgit.kde.org/?p=ksysguard.git'>KSysGuard</a> </h3>
<ul id='ulksysguard' style='display: block'>
<li>Make the FancyPlotter Settings window modal. <a href='http://quickgit.kde.org/?p=ksysguard.git&amp;a=commit&amp;h=cee5e903812b5d2fae9e5be52440b8056626e95f'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125063'>#125063</a>. Fixes bug <a href='https://bugs.kde.org/260869'>#260869</a></li>
</ul>


<h3><a name='kwin' href='http://quickgit.kde.org/?p=kwin.git'>KWin</a> </h3>
<ul id='ulkwin' style='display: block'>
<li>Fix heap-use-after-free use in resolving ClientMachine. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5968096434469811ebdbc71e4141489ebfcedfea'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125458'>#125458</a></li>
<li>Support absolute libexec path configuration. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=85b35157943ab4e7ea874639a4c714a10feccc00'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353154'>#353154</a>. Code review <a href='https://git.reviewboard.kde.org/r/125466'>#125466</a></li>
<li>Log *which* binary failed to start. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=75b08e1ee704c0467562e49bb47d47b2aaa6703b'>Commit.</a> See bug <a href='https://bugs.kde.org/353154'>#353154</a>. Code review <a href='https://git.reviewboard.kde.org/r/125464'>#125464</a></li>
<li>[autotests] Adjust TestScreenEdges::testCallback to changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=7365069ba0fafdc773d3b16dea80da2afca0781a'>Commit.</a> </li>
<li>[autotests] Adjust TestScreenEdges::testFullScreenBlocking for changes. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=acf668d7986967f7180fdb4565f77bb505a5abf4'>Commit.</a> </li>
<li>Restore linked screenedges times. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=1d3a1aa061a79ca4fa21ffef6310f85ffcd32f59'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125143'>#125143</a></li>
<li>[kcmkwin/effects] Support binary effect plugins. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=8686ca7ca0bb8362200777be25a1d9ee44ad29bc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125232'>#125232</a>. Fixes bug <a href='https://bugs.kde.org/352680'>#352680</a></li>
<li>Reset the transientInfo id when cleaning group. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=295132deef776a9e7691b38c3db86f091227fcc1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352483'>#352483</a>. Code review <a href='https://git.reviewboard.kde.org/r/125122'>#125122</a></li>
<li>Ensure to "hide" desktop buttons. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=584850f1604ceff0cd8e3cd5f6b16b2c5c4d0c6d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351869'>#351869</a>. Code review <a href='https://git.reviewboard.kde.org/r/124970'>#124970</a></li>
<li>Fetch motif hints when get them for managed client. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=cc6886d7dd91ab7a4206ae6637886d7664127bb9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347818'>#347818</a>. Code review <a href='https://git.reviewboard.kde.org/r/125007'>#125007</a></li>
<li>Recreate presentwindows grids from desktopgrid. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=57f8c6d5f88cfb05945d8a2837ed0cec3218e2f9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351724'>#351724</a>. See bug <a href='https://bugs.kde.org/326032'>#326032</a>. Code review <a href='https://git.reviewboard.kde.org/r/124960'>#124960</a></li>
<li>[libkwineffects] Proper no-size check in WindowQuadList::splitAt(X|Y). <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=5fb67414d2108445565bc406bda4c98c624ece44'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/236353'>#236353</a>. Fixes bug <a href='https://bugs.kde.org/210467'>#210467</a>. Code review <a href='https://git.reviewboard.kde.org/r/125131'>#125131</a></li>
<li>Fake a leave event for the decoration when client is left. <a href='http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=0b955611938716ae907a0972c23288c1e977ad25'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351984'>#351984</a>. Code review <a href='https://git.reviewboard.kde.org/r/124997'>#124997</a></li>
</ul>


<h3><a name='libkscreen' href='http://quickgit.kde.org/?p=libkscreen.git'>libkscreen</a> </h3>
<ul id='ullibkscreen' style='display: block'>
<li>Fix test. <a href='http://quickgit.kde.org/?p=libkscreen.git&amp;a=commit&amp;h=b645e6a4190e594ea813a10b512a12e5f3dcf326'>Commit.</a> </li>
</ul>


<h3><a name='muon' href='http://quickgit.kde.org/?p=muon.git'>Muon</a> </h3>
<ul id='ulmuon' style='display: block'>
<li>Remove deprecated file, fix install file name. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=5861150a36d34ed0c149a2dbe8ed3722b4ac1ab2'>Commit.</a> </li>
<li>Rename desktop file. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=6243eaa9412d5abc13c1c0bd26f79e6c2017c92b'>Commit.</a> </li>
<li>Require the right ASQt version. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=d513cbf9351b690c6e4fa885385561218e5c1dbe'>Commit.</a> </li>
<li>Use the correct icon for the application menu. <a href='http://quickgit.kde.org/?p=muon.git&amp;a=commit&amp;h=11883b994d90300be535e8c4d69bee11e66f73ab'>Commit.</a> </li>
</ul>


<h3><a name='plasma-desktop' href='http://quickgit.kde.org/?p=plasma-desktop.git'>Plasma Desktop</a> </h3>
<ul id='ulplasma-desktop' style='display: block'>
<li>Kded_keyboard: Fix restoring application/window specific layout settings. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3768587fc6807e06dbf257dd51e2d9f2e42dd1b9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/325572'>#325572</a>. Code review <a href='https://git.reviewboard.kde.org/r/125256'>#125256</a></li>
<li>Completely fix broken translations in keyboard KCM advanced page. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6d192f58efc1f4fc34e4ee1d6d072ba11a6a29a1'>Commit.</a> See bug <a href='https://bugs.kde.org/341527'>#341527</a>. Code review <a href='https://git.reviewboard.kde.org/r/125462'>#125462</a></li>
<li>Remove "Automatically select icons" and "Change pointer shape over icons" options. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=ef882aba3799182e5899cfb9f47616b5d6ef7c4a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/311594'>#311594</a></li>
<li>Consistently use KAUTH_HELPER_INSTALL_ABSOLUTE_DIR. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f367041d84027d8f173d0f0ccb7c893398691365'>Commit.</a> </li>
<li>Fix refactoring regression breaking DND out of Kicker. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0457866a82fb8dcf233b812e24e5c676a909013d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351741'>#351741</a></li>
<li>Kcm_keyboard: Enable selection in Advanced options view. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=e03136cd11083e78347c90b4f42dcaf17c390a24'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346948'>#346948</a>. Code review <a href='https://git.reviewboard.kde.org/r/125411'>#125411</a></li>
<li>Kcm_keyboard: Fix behavior of layout config edit delegates. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=6bb4069a9a40c7cee7ad065263fdfbca34a352bf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350757'>#350757</a>. Code review <a href='https://git.reviewboard.kde.org/r/125405'>#125405</a></li>
<li>Kcm_keyboard: Fix applying repeat mode settings. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=080f186242b945c523a60afa54107e60bc9bc03b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349932'>#349932</a>. Code review <a href='https://git.reviewboard.kde.org/r/125413'>#125413</a></li>
<li>Check service from KOpenWithDialog is really valid. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=79e5a72cf737925b1e877c81e4272b8fedc6bf49'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346909'>#346909</a></li>
<li>Kded_keyboard: Don't save default config in layout memory. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=2670574a61bb044492e297109a3ecf4de7bcf623'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125280'>#125280</a></li>
<li>Kded_keyboard: Fix re-applying layout settings when plugging new keyboard. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=415c962dea42fba31f9486f55e90630e60bb2bc2'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125327'>#125327</a></li>
<li>Kcm_keyboard: Fix configuring Spare Layouts. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=14613700e56904f6bf93d32916d2c85233733203'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125328'>#125328</a></li>
<li>Fix off-by-one-col in rubber band selection in right-to-left alignment. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3e09f7031309dacba57255dc677933d1a0bcbeb6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/349957'>#349957</a></li>
<li>Fix hidden task labels in group popups in vertical orientation. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=5d92be223fa5287e6f74dbd6abcbc903c282dfd4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/351624'>#351624</a></li>
<li>Revert the preceding four commits -- wrong branch. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3aa81a19842a0253fe858af40acbfad359d6ffe3'>Commit.</a> </li>
<li>Merge the favorite-by-DND feature written at the Seoul hackathon. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=26a619e59cb383416c8c0c29cfc8dee02981b1c5'>Commit.</a> </li>
<li>Drop debug for empty context menus, instead refuse to open. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=59cc8302db38b11f5224a996e032465e0c282460'>Commit.</a> </li>
<li>Add a FavoritesModel.maxFavorites prop to limit the model in size. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=88b324ed34d01b6df643002730e009927d6fe232'>Commit.</a> </li>
<li>Add a FavoritesModel.enabled prop to toggle addFavorite/removeFavorite. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=83e9adcab8fbf9d47da3d0f5de8b96225b722199'>Commit.</a> </li>
<li>Update recentApplicationCount in all places we modify applications list. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=f808cd544ea60440d50d2835b0f57cd110519a98'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352047'>#352047</a>. Code review <a href='https://git.reviewboard.kde.org/r/125277'>#125277</a></li>
<li>Intialise pointers in xigetproperty. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=859c4ae66b3515999bca2e5c37c76003eeb2b854'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125296'>#125296</a></li>
<li>Don't read an 8bit boolean as a 32 bit value in xigetproperty. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=a3e5eb82e633c2530200176e81480b23ad289695'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125295'>#125295</a></li>
<li>Address window toggle withholding the mouse leave event resetting the icon state. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=689d70b4b8d70f99fb7da80a32f8f39f325ea3d8'>Commit.</a> </li>
<li>Kcm_keyboard: Fix crash when trying to show invalid preview. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b9cfcbef00481475e300c3a6dff303aa45b943c6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/348694'>#348694</a>. Code review <a href='https://git.reviewboard.kde.org/r/125265'>#125265</a></li>
<li>Kcm_keyboard: Fix showing shortcuts. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=df9e9e08d3ac6f5d81763cb7164c1f71530abff1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350756'>#350756</a>. Code review <a href='https://git.reviewboard.kde.org/r/125264'>#125264</a></li>
<li>Fix KF5 port regression in kcm_standard_actions. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=3afd24f55dcba2cd389c3760d1661dabb9251d14'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/343665'>#343665</a>. Code review <a href='https://git.reviewboard.kde.org/r/123828'>#123828</a></li>
<li>Support dropping task manager entries onto the pager. <a href='http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=b80bd0f74e12241458dba3f366b4c7e6c051f0cc'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352349'>#352349</a>. Code review <a href='https://git.reviewboard.kde.org/r/125073'>#125073</a></li>
</ul>


<h3><a name='plasma-nm' href='http://quickgit.kde.org/?p=plasma-nm.git'>Plasma Networkmanager (plasma-nm)</a> </h3>
<ul id='ulplasma-nm' style='display: block'>
<li>Set proper maximum MTU size for infiniband, wireless and wired connections. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=36be2e97822187da5a822ca9cd7b9fdd4c7cf3d2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/353241'>#353241</a></li>
<li>Use new breeze icons. <a href='http://quickgit.kde.org/?p=plasma-nm.git&amp;a=commit&amp;h=5bc3bab0550cc725d40be5f31452af5271bf1f66'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346133'>#346133</a></li>
</ul>


<h3><a name='plasma-workspace' href='http://quickgit.kde.org/?p=plasma-workspace.git'>Plasma Workspace</a> </h3>
<ul id='ulplasma-workspace' style='display: block'>
<li>Continue even if script returns errors. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=2870b5e69bf892c3d08df2d8ee11491542e0d796'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125323'>#125323</a>. Fixes bug <a href='https://bugs.kde.org/352491'>#352491</a></li>
<li>Make sure the cancel and shutdown/logout button have the same size. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=93bf81eea152e2b8261ac4a854207af448ed7c43'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125235'>#125235</a></li>
<li>Adjust notification count label text size. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=36ee3e705ef66db0872eff8a86e2802e09a973c5'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125196'>#125196</a></li>
<li>Set tooltip icon in notifications applet. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3f8fbd3d4a6b9aafa6bbccdd4282d2538018a7c6'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/125193'>#125193</a></li>
<li>Don't overflow action buttons in notification. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=0d0b49f217ef8a632dc9ec09f3224787feb2a183'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352125'>#352125</a>. Code review <a href='https://git.reviewboard.kde.org/r/125200'>#125200</a></li>
<li>Fix ScreenPositionSelector showing a gap. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=5ad3aa0d3a60d088a9f1893490d18e8bfb69b6a5'>Commit.</a> </li>
<li>Fix crash if trying to load an invalid applet in systray. <a href='http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=e572ec1cb2e0fb4467cdb2e68f4ac2e7341b75bb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/352294'>#352294</a>. Code review <a href='https://git.reviewboard.kde.org/r/125053'>#125053</a></li>
</ul>


<h3><a name='plasma-workspace-wallpapers' href='http://quickgit.kde.org/?p=plasma-workspace-wallpapers.git'>Plasma Workspace-wallpapers</a></h3>
<ul id='ulplasma-workspace-wallpapers' style='display: block'><li>Now built from Git repository (moved from Subversion)</li></ul>
<h3><a name='polkit-kde-agent-1' href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git'>polkit-kde-agent-1</a> </h3>
<ul id='ulpolkit-kde-agent-1' style='display: block'>
<li>Use .cmake for dynamically generated desktop file. <a href='http://quickgit.kde.org/?p=polkit-kde-agent-1.git&amp;a=commit&amp;h=772c884578c0b9f8ff90004fbdcc084c2f7078fa'>Commit.</a> </li>
</ul>


<h3><a name='powerdevil' href='http://quickgit.kde.org/?p=powerdevil.git'>Powerdevil</a> </h3>
<ul id='ulpowerdevil' style='display: block'>
<li>Fix the brightness update. <a href='http://quickgit.kde.org/?p=powerdevil.git&amp;a=commit&amp;h=1005dd9a25c5fafea1f781c6d2a49fe99e219a34'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/346456'>#346456</a>. Fixes bug <a href='https://bugs.kde.org/350676'>#350676</a>. Code review <a href='https://git.reviewboard.kde.org/r/125156'>#125156</a></li>
</ul>


<h3><a name='sddm-kcm' href='http://quickgit.kde.org/?p=sddm-kcm.git'>SDDM KCM</a> </h3>
<ul id='ulsddm-kcm' style='display: block'>
<li>Allow to set minimum user id lower than 1000. <a href='http://quickgit.kde.org/?p=sddm-kcm.git&amp;a=commit&amp;h=9afb3c627bb43969edf1a9bbe83b2107e002eb4f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/347942'>#347942</a>. Code review <a href='https://git.reviewboard.kde.org/r/125191'>#125191</a></li>
<li>Fix loading of the configured cursor theme. <a href='http://quickgit.kde.org/?p=sddm-kcm.git&amp;a=commit&amp;h=c458a098c0271517ba59d65290d5b1b7c2577495'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/344592'>#344592</a>. Code review <a href='https://git.reviewboard.kde.org/r/125189'>#125189</a></li>
</ul>


<?php
  include("footer.inc");
?>
